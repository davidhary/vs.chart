﻿using System;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Visuals
{
    internal static class Extensions
    {

        /// <summary> Returns the number of decimal places for displaying the auto value. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The number of decimal places for displaying the auto value. </returns>
        public static int DecimalPlaces( this double value )
        {
            int candidate = 0;
            value = Math.IEEERemainder( Math.Abs( value ), 1d );
            double minimum = Math.Max( value / 1000.0d, 0.0000001d );
            while ( value > minimum )
            {
                candidate += 1;
                value = Math.IEEERemainder( 10d * value, 1d );
            }

            return candidate;
        }

        /// <summary> Returns the number of decimal places for displaying the auto value. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> the number of decimal places for displaying the auto value. </returns>
        public static int DecimalPlaces( this float value )
        {
            return Conversions.ToDouble( value ).DecimalPlaces();
        }

        /// <summary> Calculate a base 10 logarithm in a safe manner to avoid math exceptions. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The value for which the logarithm is to be calculated. </param>
        /// <returns>
        /// The value of the logarithm, or 0 if the <paramref name="value"/>
        /// argument was negative or zero.
        /// </returns>
        public static double Log10( this double value )
        {
            return Math.Log10( Math.Max( value, double.Epsilon ) );
        }

        /// <summary> Calculate the modulus (remainder) in a safe manner so that divide by zero errors are
        /// avoided. </summary>
        /// <param name="value">    The divisor. </param>
        /// <param name="dividend"> The dividend. </param>
        /// <returns> the value of the modulus, or zero for the divide-by-zero case. </returns>
        public static double Mod( this double value, double dividend )
        {
            if ( dividend == 0d )
            {
                return 0d;
            }

            double temp = value / dividend;
            return dividend * (temp - Math.Floor( temp ));
        }
    }
}