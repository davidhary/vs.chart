using System;
using System.Collections;
using System.Diagnostics;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Visuals.My
{

    /// <summary> Handles application level functionality. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    internal partial class MyApplication
    {

        #region " APPLICATION LOG "

        /// <summary> Adds a log message and severity to the log. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="severity"> Specifies the message severity. </param>
        /// <param name="details">  Specifies the message details. </param>
        /// <returns> Message or empty string. </returns>
        public static string WriteLogEntry( TraceEventType severity, string details )
        {
            if ( details is object )
            {
                MyProject.Application.Log.WriteEntry( details, severity );
                return details;
            }

            return string.Empty;
        }

        /// <summary> Adds a log message and severity to the log. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="severity"> Specifies the message severity. </param>
        /// <param name="format">   Specifies the message format. </param>
        /// <param name="args">     Specified the message arguments. </param>
        /// <returns> Message or empty string. </returns>
        public static string WriteLogEntry( TraceEventType severity, string format, params object[] args )
        {
            return format is object
                ? WriteLogEntry( severity, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) )
                : string.Empty;
        }

        /// <summary> Adds a log message and severity to the log. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="severity"> Specifies the message severity. </param>
        /// <param name="messages"> Message information to log. </param>
        /// <returns> Message or empty string. </returns>
        public static string WriteLogEntry( TraceEventType severity, string[] messages )
        {
            return messages is object ? WriteLogEntry( severity, string.Join( ",", messages ) ) : string.Empty;
        }

        /// <summary> Adds exception details to the error log.  Includes stack and data. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="ex">             Specifies the exception. </param>
        /// <param name="severity">       Specifies the exception severity. </param>
        /// <param name="additionalInfo"> Specifies additional information. </param>
        private static void WriteExceptionDetailsThis( Exception ex, TraceEventType severity, string additionalInfo )
        {
            MyProject.Application.Log.WriteException( ex, severity, additionalInfo );
            if ( ex is object && ex.StackTrace is object )
            {
                var stackTrace = ex.StackTrace.Split( Conversions.ToChar( Environment.NewLine ) );
                _ = WriteLogEntry( severity, stackTrace );
            }

            if ( ex.Data is object && ex.Data.Count > 0 )
            {
                foreach ( DictionaryEntry keyValuePair in ex.Data )
                    MyProject.Application.Log.WriteEntry( keyValuePair.Key.ToString() + "=" + keyValuePair.Value.ToString(), severity );
            }

            if ( ex.InnerException is object )
            {
                WriteExceptionDetailsThis( ex.InnerException, severity, "(Inner Exception)" );
            }
        }

        /// <summary> Adds exception details to the error log. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="ex">             Specifies the exception. </param>
        /// <param name="severity">       Specifies the exception severity. </param>
        /// <param name="additionalInfo"> Specifies additional information. </param>
        public static void WriteExceptionDetails( Exception ex, TraceEventType severity, string additionalInfo )
        {
            // write exception details.
            WriteExceptionDetailsThis( ex, severity, additionalInfo );
        }

        /// <summary> Adds exception details to the error log. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="ex"> Specifies the exception. </param>
        public static void WriteExceptionDetails( Exception ex )
        {
            WriteExceptionDetails( ex, TraceEventType.Error, string.Empty );
        }

        #endregion

        #region " APPLICATION METHODS "

        /// <summary> Returns a string selected value. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="condition"> The condition for selecting the true or false parts. </param>
        /// <param name="truePart">  The part selected if the condition is True. </param>
        /// <param name="falsePart"> The part selected if the condition is false. </param>
        /// <returns>
        /// The <paramref name="truePart"></paramref> if <paramref name="condition"></paramref>
        /// is <c>True</c>; otherwise, the <paramref name="falsePart"></paramref>
        /// </returns>
        public static string ImmediateIf( bool condition, string truePart, string falsePart )
        {
            return condition ? truePart : falsePart;
        }

        /// <summary> Returns an Integer selected value. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="condition"> The condition for selecting the true or false parts. </param>
        /// <param name="truePart">  The part selected if the condition is True. </param>
        /// <param name="falsePart"> The part selected if the condition is false. </param>
        /// <returns>
        /// The <paramref name="truePart"></paramref> if <paramref name="condition"></paramref>
        /// is <c>True</c>; otherwise, the <paramref name="falsePart"></paramref>
        /// </returns>
        public static int ImmediateIf( bool condition, int truePart, int falsePart )
        {
            return condition ? truePart : falsePart;
        }

        /// <summary> Gets the na. </summary>
        /// <value> The na. </value>
        public static string NA => "NA";

        /// <summary> Returns an array of values from 0 to elementCount - 1. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="elementCount"> Number of ramp elements. </param>
        /// <returns> an array of values from 0 to elementCount - 1. </returns>
        public int[] Ramp( int elementCount )
        {
            var values = new int[elementCount];
            for ( int i = 0, loopTo = elementCount - 1; i <= loopTo; i++ )
                values[i] = i;
            return values;
        }

        /// <summary> Converts the Double array to a string array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> the Double array to a string array. </returns>
        private static string[] ToStringArray( double[] values )
        {
            if ( values is null || values.Length == 0 )
            {
                var ranges = Array.Empty<string>();
                return ranges;
            }
            else
            {
                var ranges = new string[values.Length];
                for ( int i = 0, loopTo = values.Length - 1; i <= loopTo; i++ )
                    ranges[i] = values[i].ToString( System.Globalization.CultureInfo.CurrentCulture );
                return ranges;
            }
        }

        /// <summary> Converts the Double array to a string array. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="values"> The values. </param>
        /// <returns> The converted string array. </returns>
        private static string[] ToStringArray( int[] values )
        {
            if ( values is null || values.Length == 0 )
            {
                var ranges = Array.Empty<string>();
                return ranges;
            }
            else
            {
                var ranges = new string[values.Length];
                for ( int i = 0, loopTo = values.Length - 1; i <= loopTo; i++ )
                    ranges[i] = values[i].ToString( System.Globalization.CultureInfo.CurrentCulture );
                return ranges;
            }
        }

        #endregion

    }
}
