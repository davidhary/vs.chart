## ISR Visuals Chart Libraries<sub>&trade;</sub>
### Revision History

*3.1.6667 2018-04-03*  
2018 release.

*3.0.6166 2016-11-18*  
Uses new core libraries.

*2.2.4765 2013-01-17*  
Adds the hidden attribute to all properties that are
not browsable.

*2.2.4763 2013-01-15*  
Do not toggle auto scale when setting auto value
Value.

*2.2.4707 2012-11-20*  
Converted to VS10.

*2.1.4706 2012-11-19*  
Prepared for VS10.

*2.1.4232 2011-08-03*  
Standardize code elements and documentation.

*2.1.4213 2011-07-15*  
Simplifies the assembly information.

*2.1.2961 2008-02-09*  
Update to .NET 3.5.

*2.0.2789 2007-08-21*  
Update to Visual Studio 8.

*1.0.2711 2007-06-04*  
Limit rounding digits between 0 and 15.

*1.0.2228 2006-02-06*  
Replace Is Filled, Is Bold, etc. with Filled, Bold,
etc.

*1.0.2219 2006-01-28*  
Remove Visual Basic import.

*1.0.2206 2006-01-15*  
New support and exceptions libraries. Use integer,
Int64, and Int16 instead of Integer, Long, and Short.

*1.0.2118 2005-10-19 Allow setting the number of data points for the curve.

*1.0.2034 2005-07-27 Add line recorder and a new graphics double buffer.
The double buffer slows down the line recorder significantly.

*1.0.1618 2004-06-06*  
Add time series pan and restore.

*1.0.1615 2004-06-03*  
Rename time series X to T value. Add book makrs to
curve. Add zero time series values and partial copy methods.

*1.0.1612 2004-05-31*  
Add print command. Add time series tags to specify
which time series point to include in auto ranging.

*1.0.1605 2004-05-24*  
Add strip chart point with a given range to auto
range.

*1.0.1598 2004-05-17*  
Allow to draw ignoring missing values for speed
graphing.

*1.0.1597 2004-05-16*  
Add strip charting.

*1.0.1585 2004-05-04*  
Move range and Julian date to ISR Chart.

*1.0.1584 2004-05-03*  
Store Text Appearance font properties in Base font and
use change in Base Font as well as scale to determine if we need to
redrace the font.

*1.0.1581 2004-04-30*  
New library.

\(C\) 2004 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Chart Libraries](https://bitbucket.org/davidhary/vs.chart):
