using System;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary>
    /// Draws a chart.  It is preferable to use the chart control because it has embedded double
    /// buffering whereas with this double buffering must be set.
    /// </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources<para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class ChartPane : Pane, ICloneable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs a <see cref="ChartPane"/> with default values, unit drawing area, and empty labels.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public ChartPane() : base()
        {
        }

        /// <summary>
        /// Constructs a <see cref="ChartPane"/> object with the specified drawing area and axis and
        /// chart labels.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="chartArea"> A rectangular screen area where the chart is to be displayed. This
        /// area can be any size, and can be resize at any time using the
        /// <see cref="Visuals.Pane.PaneArea"/> property. </param>
        public ChartPane( RectangleF chartArea ) : base( chartArea )
        {
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="model"> The ChartPane object from which to copy. </param>
        public ChartPane( ChartPane model ) : base( model )
        {
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // Invoke the base class dispose method

                base.Dispose( disposing );
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the ChartPane. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the ChartPane. </returns>
        public ChartPane Copy()
        {
            return new ChartPane( this );
        }

        /// <summary>
        /// Creates a sample demo chart with three curves, titles, text boxes, arrows and legend.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="clientRectangle"> The client area to use for displaying the chart. </param>
        public void CreateSampleOne( Rectangle clientRectangle )
        {

            // set chart area and titles.
            this.PaneArea = new RectangleF( 10f, 10f, 10f, 10f );
            this.Title.Caption = $"Wacky Widget Company {Environment.NewLine} Production Report";
            var xAxis = this.AddAxis( $"Time, Days {Environment.NewLine} (Since Plant Construction Startup)", AxisType.X );
            xAxis.Grid.Visible = true;
            xAxis.TickLabels.Appearance.Angle = 60.0f;
            var yAxis = this.AddAxis( $"Widget Production {Environment.NewLine} (units/hour)", AxisType.Y );
            yAxis.Grid.Visible = true;
            this.AxisFrame.FillColor = Color.LightGoldenrodYellow;
            this.SetSize( clientRectangle );
            Curve curve;
            var x = new[] { 72d, 200d, 300d, 400d, 500d, 600d, 700d, 800d, 900d, 1000d };
            var y = new[] { 20d, 10d, 50d, 40d, 35d, 60d, 90d, 25d, 48d, 75d };
            curve = this.AddCurve( "Larry", x, y, Color.Red, ShapeType.Circle, xAxis, yAxis );
            curve.Symbol.Size = 14f;
            curve.Cord.LineWidth = 2.0f;
            var x2 = new[] { 300d, 400d, 500d, 600d, 700d, 800d, 900d };
            var y2 = new[] { 75d, 43d, 27d, 62d, 89d, 73d, 12d };
            curve = this.AddCurve( "Moe", x2, y2, Color.Blue, ShapeType.Diamond, xAxis, yAxis );
            curve.Cord.Visible = false;
            curve.Symbol.Filled = true;
            curve.Symbol.Size = 14f;
            var x3 = new[] { 150d, 250d, 400d, 520d, 780d, 940d };
            var y3 = new[] { 5.2d, 49.0d, 33.8d, 88.57d, 99.9d, 36.8d };
            curve = this.AddCurve( "Curly", x3, y3, Color.Green, ShapeType.Triangle, xAxis, yAxis );
            curve.Symbol.Size = 14f;
            curve.Cord.LineWidth = 2.0f;
            curve.Symbol.Filled = true;
            var textBox = this.AddTextBox( $"First Prod {Environment.NewLine} 21-Oct-99", 100.0f, 50.0f );
            textBox.Appearance.Italic = true;
            textBox.Alignment = new Alignment( HorizontalAlignment.Center, VerticalAlignment.Bottom );
            textBox.Appearance.Frame.FillColor = Color.LightBlue;
            var arrow = this.AddArrow( Color.Black, 12.0f, 100.0f, 47.0f, 72.0f, 25.0f );
            arrow.CoordinateFrame = CoordinateFrameType.AxisXYScale;
            textBox = this.AddTextBox( "Upgrade", 700.0f, 50.0f );
            textBox.CoordinateFrame = CoordinateFrameType.AxisXYScale;
            textBox.Appearance.Angle = 90f;
            textBox.Appearance.FontColor = Color.Black;
            textBox.Appearance.Frame.Filled = true;
            textBox.Appearance.Frame.FillColor = Color.LightGoldenrodYellow;
            textBox.Appearance.Frame.Visible = false;
            textBox.Alignment = new Alignment( HorizontalAlignment.Right, VerticalAlignment.Center );
            arrow = this.AddArrow( Color.Black, 15f, 700f, 53f, 700f, 80f );
            arrow.CoordinateFrame = CoordinateFrameType.AxisXYScale;
            arrow.LineWidth = 2.0f;
            textBox = this.AddTextBox( "Confidential", 0.8f, -0.03f );
            textBox.CoordinateFrame = CoordinateFrameType.AxisFraction;
            textBox.Appearance.Angle = 15.0f;
            textBox.Appearance.FontColor = Color.Red;
            textBox.Appearance.Bold = true;
            textBox.Appearance.FontSize = 16f;
            textBox.Appearance.Frame.Visible = true;
            textBox.Appearance.Frame.LineColor = Color.Red;
            textBox.Alignment = new Alignment( HorizontalAlignment.Left, VerticalAlignment.Bottom );
            this.Rescale();
        }

        /// <summary>
        /// Draw all elements in the <see cref="ChartPane"/> to the specified graphics device.  This
        /// routine should be part of the Paint() update process.  Calling This method will redraw all
        /// features of the graph.  No preparation is required other than an instantiated
        /// <see cref="ChartPane"/> object.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        public override void Draw( Graphics graphicsDevice )
        {

            // validate argument.
            if ( graphicsDevice is null )
                throw new ArgumentNullException( nameof( graphicsDevice ) );

            // clear and exit if not visible.
            if ( !this.Visible )
            {
                graphicsDevice.Clear( this.PaneFrame.FillColor );
                return;
            }

            // set the scale factor relative to the base dimension)
            this.ScaleFactor = this.GetScaleFactor( graphicsDevice );

            // allocate the drawing area
            this.SetDrawArea();
            this.SetAxisArea( graphicsDevice );

            // Frame the pane and axis
            this.DrawFrames( graphicsDevice );

            // Draw the Pane Title
            this.Title.Draw( graphicsDevice );

            // Draw the Axes
            this.Axes.Draw( graphicsDevice );

            // Draw the curves
            this.Curves.Draw( graphicsDevice );

            // Draw the Legend
            this.Legend.Draw( graphicsDevice );

            // draw each text box at the screen pixel location
            this.TextBoxes.Draw( graphicsDevice );

            // draw each arrow at its screen coordinates
            this.Arrows.Draw( graphicsDevice );

            // Reset the clipping
            graphicsDevice.ResetClip();
        }

        /// <summary>
        /// Prints all elements in the <see cref="Pane"/> to the specified graphics device.  This routine
        /// should be overridden by the inheriting classes.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to print into.  This is normally
        /// <see cref="System.Drawing.Printing.PrintPageEventArgs.Graphics">graphics</see>
        /// of the <see cref="M:PrintPage"/> delegate. </param>
        /// <param name="printArea">      The <see cref="System.Drawing.RectangleF">area</see> on the print
        /// document allotted for the chart. </param>
        public override void Print( Graphics graphicsDevice, RectangleF printArea )
        {

            // exit if not visible.
            if ( !this.Visible )
            {
                return;
            }

            // validate argument.
            if ( graphicsDevice is null )
                throw new ArgumentNullException( nameof( graphicsDevice ) );

            // save the pane area
            var tempPaneArea = this.PaneArea;

            // set the pane area to the print window
            this.PaneArea = new RectangleF( printArea.Location, printArea.Size );
            this.Draw( graphicsDevice );

            // restore the pane area
            this.PaneArea = tempPaneArea;
        }

        #endregion

    }
}
