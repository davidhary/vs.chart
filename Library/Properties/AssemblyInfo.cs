﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Visuals.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Visuals.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Visuals.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
