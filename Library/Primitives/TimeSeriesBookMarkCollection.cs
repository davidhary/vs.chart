using System;

namespace isr.Visuals
{

    /// <summary>
    /// Contains a list of <see cref="TimeSeriesBookmark"/> objects, which tag a time series.
    /// </summary>
    /// <remarks>
    /// Declare <see cref="A:NotInheritable"/> so as to allow calling base methods in the
    /// constructor. <para>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-06-02, 1.0.1614. Created </para>
    /// </remarks>
    public sealed class TimeSeriesBookmarkCollection : System.Collections.ObjectModel.Collection<TimeSeriesBookmark>, ICloneable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Default constructor for the <see cref="TimeSeriesBookmarkCollection"/> collection class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="curve"> Reference to the <see cref="isr.Visuals.Curve">Curve</see> </param>
        public TimeSeriesBookmarkCollection( Curve curve ) : base()
        {
            this._Curve = curve;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are
        /// null. </exception>
        /// <param name="model"> The TimeSeriesBookmarkCollection object from which to copy. </param>
        public TimeSeriesBookmarkCollection( TimeSeriesBookmarkCollection model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            foreach ( var item in model )
                this.Add( new TimeSeriesBookmark( item ) );
            this._Curve = model._Curve;
        }

        #endregion

        #region " CUSTOM COLLECTION METHODS "

        /// <summary> Add a <see cref="TimeSeriesBookmark"/> to the collection. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="bookmark"> A reference to the <see cref="TimeSeriesBookmark"/> object to add. </param>
        public new void Add( TimeSeriesBookmark bookmark )
        {
            base.Add( bookmark );
            bookmark.SerialNumber = this.Count;
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the TimeSeriesBookmarkCollection. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the TimeSeriesBookmarkCollection. </returns>
        public TimeSeriesBookmarkCollection Copy()
        {
            return new TimeSeriesBookmarkCollection( this );
        }

        /// <summary>
        /// Creates a one-dimensional <see cref="T:System.Array">Array</see>
        /// instance containing the collection items.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> Array of type <see cref="TimeSeriesBookmark">time series Bookmark</see> </returns>
        public TimeSeriesBookmark[] ToArray()
        {
            var bookmarks = new TimeSeriesBookmark[this.Count];
            this.CopyTo( bookmarks, 0 );
            return bookmarks;
        }

        /// <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Curve">curve</see></summary>
        private readonly Curve _Curve;

        #endregion

    }
}
