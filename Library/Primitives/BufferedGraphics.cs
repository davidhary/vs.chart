using System;
using System.ComponentModel;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary>
    /// Implements Double Buffering rendering of the graph to provide smooth refreshing. The double
    /// buffer slows down the line recorder significantly.
    /// </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class BufferedGraphics : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="canvasWidth">  The canvas width. </param>
        /// <param name="canvasHeight"> The canvas height. </param>
        public BufferedGraphics( int canvasWidth, int canvasHeight )
        {
            _ = this.CreateDoubleBuffer( canvasWidth, canvasHeight );
            // _graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        if ( this._Canvas is object )
                        {
                            this._Canvas.Dispose();
                            this._Canvas = null;
                        }

                        if ( this.GraphicsDevice is object )
                        {
                            this.GraphicsDevice.Dispose();
                            this.GraphicsDevice = null;
                        }
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Creates double buffer object. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="width">  width of paint area. </param>
        /// <param name="height"> height of paint area. </param>
        /// <returns> true/false if double buffer is created. </returns>
        public bool CreateDoubleBuffer( int width, int height )
        {
            if ( width == 0 || height == 0 )
            {
                return false;
            }

            if ( width != width || height != height )
            {
                if ( this._Canvas is object )
                {
                    this._Canvas.Dispose();
                    this._Canvas = null;
                }

                if ( this.GraphicsDevice is object )
                {
                    this.GraphicsDevice.Dispose();
                    this.GraphicsDevice = null;
                }

                this._Width = width;
                this._Height = height;
                this._Canvas = new Bitmap( width, height );
                this.GraphicsDevice = Graphics.FromImage( this._Canvas );
            }

            return true;
        }

        /// <summary> Renders the double buffer to the screen. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="graphicsDevice"> Window forms graphics Object. </param>
        public void Render( Graphics graphicsDevice )
        {
            if ( graphicsDevice is object && this._Canvas is object )
            {
                graphicsDevice.DrawImage( this._Canvas, new Rectangle( 0, 0, this._Width, this._Height ), 0, 0, this._Width, this._Height, GraphicsUnit.Pixel );
            }
        }

        /// <summary> Returns true if double buffering can be achieved. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> True if we can double buffer, false if not. </returns>
        public bool CanDoubleBuffer()
        {
            return this.GraphicsDevice is object;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the status message. </summary>
        /// <value> A System.String value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusMessage { get; set; }

        /// <summary> The canvas. </summary>
        private Bitmap _Canvas;

        /// <summary> The width. </summary>
        private int _Width;

        /// <summary> The height. </summary>
        private int _Height;

        /// <summary> Reference to graphics context. </summary>
        /// <value> The graphics device. </value>
        public Graphics GraphicsDevice { get; private set; }

        #endregion

    }
}
