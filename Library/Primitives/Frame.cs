using System;
using System.ComponentModel;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary> Renders frames for text and chart boxes. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class Frame : ICloneable, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs a <see cref="Frame"/> with
        /// <see cref="FrameDefaults">default property values</see>.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public Frame() : base()
        {
            var frameDefaults = FrameDefaults.Get();
            this.FillColor = frameDefaults.FillColor;
            this.Filled = frameDefaults.Filled;
            this.IsOutline = frameDefaults.IsOutline;
            this.Visible = frameDefaults.Visible;
            this.LineColor = frameDefaults.LineColor;
            this.LineWidth = frameDefaults.LineWidth;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Frame object from which to copy. </param>
        public Frame( Frame model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.StatusMessage = model.StatusMessage;
            this.FillColor = model.FillColor;
            this.Filled = model.Filled;
            this.IsOutline = model.IsOutline;
            this.Visible = model.Visible;
            this.LineColor = model.LineColor;
            this.LineWidth = model.LineWidth;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Frame. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of Frame. </returns>
        public Frame Copy()
        {
            return new Frame( this );
        }

        /// <summary> Renders a <see cref="Frame"/>. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="frameArea">      The frame <see cref="RectangleF"/>. </param>
        public void Draw( Graphics graphicsDevice, RectangleF frameArea )
        {
            if ( this.Visible )
            {

                // validate argument.
                if ( graphicsDevice is null )
                    throw new ArgumentNullException( nameof( graphicsDevice ) );

                // If the background is to be filled, fill it
                if ( this.Filled )
                {
                    using var fillBrush = new SolidBrush( this.FillColor );
                    graphicsDevice.FillRectangle( fillBrush, Rectangle.Round( frameArea ) );
                }

                // Draw the outline around 
                if ( this.IsOutline )
                {
                    using var pen = new Pen( this.LineColor, this.LineWidth );
                    graphicsDevice.DrawRectangle( pen, Rectangle.Round( frameArea ) );
                }
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Gets or sets the background color of the frame. Background fill is turned on or off using the
        /// <see cref="Filled"/> property.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> value. </value>
        public Color FillColor { get; set; }

        /// <summary>
        /// Gets or sets the fill mode of the <see cref="Frame"/>.  Set to True to fill the frame with
        /// color, or False otherwise.
        /// </summary>
        /// <value>
        /// True to fill the <see cref="Frame"/> background with the <see cref="FillColor"/>, False to
        /// leave the background transparent.
        /// </value>
        public bool Filled { get; set; }

        /// <summary>
        /// Gets or sets the outline mode of the <see cref="Frame"/>.  Set to True to outline the frame
        /// with color, or False otherwise.
        /// </summary>
        /// <value> The is outline. </value>
        public bool IsOutline { get; set; }

        /// <summary> Gets or sets a property that shows or hides the <see cref="Symbol"/>. </summary>
        /// <value> True to show the symbol, False to hide it. </value>
        public bool Visible { get; set; }

        /// <summary> Gets or sets the pen width for drawing the <see cref="Symbol"/> outline. </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float LineWidth { get; set; }

        /// <summary> Gets or sets the line color of the <see cref="Symbol"/> </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color LineColor { get; set; }

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A System.String value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusMessage { get; set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="Frame"/> class that defines the default property values
    /// for the <see cref="Frame"/> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class FrameDefaults
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private FrameDefaults() : base()
        {
            this.FillColor = Color.White;
            this.Visible = true;
            this.LineWidth = 1.0f;
            this.IsOutline = true;
            this.Filled = true;
            this.LineColor = Color.Black;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static FrameDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static FrameDefaults Get()
        {
            if ( _Instance is null )
            {
                lock ( SyncLocker )
                    _Instance = new FrameDefaults();
            }

            return _Instance;
        }

        #endregion

        /// <summary>
        /// Gets or sets the default color for filling the frame (<see cref="Frame.FillColor"/> property).
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color FillColor { get; set; }

        /// <summary>
        /// Gets or sets the default display mode for Frame (<see cref="Frame.Visible"/> property). True
        /// to display the frame, False to hide the frame.
        /// </summary>
        /// <value> The visible. </value>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the default pen width to be used for drawing frame outline
        /// (<see cref="Frame.LineWidth"/> property).
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float LineWidth { get; set; }

        /// <summary>
        /// Gets or sets the default outline draw mode for Frame (<see cref="Frame.IsOutline"/>
        /// property). True to draw the frame outline, False otherwise.
        /// </summary>
        /// <value> The is outline. </value>
        public bool IsOutline { get; set; }

        /// <summary>
        /// Gets or sets the default fill mode for the frame (<see cref="Frame.Filled"/> property). True
        /// to have Frame filled in with color, False to leave frame as outline.
        /// </summary>
        /// <value> The filled. </value>
        public bool Filled { get; set; }

        /// <summary>
        /// Gets or sets the default color for drawing frame outline (<see cref="Frame.LineColor"/>
        /// property).
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color LineColor { get; set; }
    }
}

#endregion

