using System;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary>
    /// A collection class containing a list of <see cref="isr.Visuals.Arrow"/> type graphic objects
    /// to be displayed on the graph.
    /// </summary>
    /// <remarks>
    /// Declare <see cref="A:NotInheritable"/> so as to allow calling base methods in the
    /// constructor. <para>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581.  Created </para>
    /// </remarks>
    public sealed class ArrowCollection : System.Collections.ObjectModel.Collection<Arrow>, ICloneable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor for the collection class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public ArrowCollection() : base()
        {
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The ArrowCollection object from which to copy. </param>
        public ArrowCollection( ArrowCollection model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            foreach ( var item in model )
                this.Add( new Arrow( item ) );
            this._Pane = model._Pane;
        }

        #endregion

        #region " CUSTOM COLLECTION METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the ArrowCollection. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the ArrowCollection. </returns>
        public ArrowCollection Copy()
        {
            return new ArrowCollection( this );
        }

        /// <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
        private readonly Pane _Pane;

        /// <summary>
        /// Renders all the <see cref="Arrow">Arrows</see> to the specified <see cref="Graphics"/> device.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        public void Draw( Graphics graphicsDevice )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // Loop for each curve
            foreach ( Arrow arrow in this )
                arrow.Draw( graphicsDevice );
        }

        #endregion

    }
}
