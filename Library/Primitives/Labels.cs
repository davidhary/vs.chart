using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;

using isr.Core.Constructs;

namespace isr.Visuals
{

    /// <summary> Handles specification and drawing of axis value labels. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class Labels : ICloneable, IDisposable
    {

        // update:  with getSpace, allocated an area for the label and use the area to 
        // display the labels aligned to the axis.
        // add Tick Label class to handle generating tick labels.

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Default constructor for <see cref="Axis"/> Labels that sets all properties to default values
        /// as defined in the <see cref="LabelsDefaults"/> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public Labels( Pane drawingPane ) : base()
        {
            if ( drawingPane is null )
                throw new ArgumentNullException( nameof( drawingPane ) );
            var labelDefaults = LabelsDefaults.Get();
            this.Appearance = new TextAppearance( labelDefaults.Font, labelDefaults.FontColor );
            this.Appearance.Frame.Filled = labelDefaults.Filled;
            this.Appearance.Frame.IsOutline = labelDefaults.Framed;
            this.Appearance.Frame.Visible = labelDefaults.Framed | labelDefaults.Filled;
            this.DecimalPlaces = labelDefaults.DecimalPlaces.Copy();
            this._TextLabels = Array.Empty<string>();
            this._Pane = drawingPane;
            this.ScaleFormat = labelDefaults.ScaleFormat;
            this.Visible = labelDefaults.Visible;
        }

        /// <summary>
        /// Default constructor for <see cref="Axis"/> Labels that sets all properties to default values
        /// as defined in the <see cref="LabelsDefaults"/> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="textLabels">  The array of text labels. </param>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public Labels( string[] textLabels, Pane drawingPane ) : this( drawingPane )
        {
            if ( textLabels is null )
            {
                throw new ArgumentNullException( nameof( textLabels ) );
            }

            this._TextLabels = textLabels;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Labels object from which to copy. </param>
        public Labels( Labels model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.Appearance = model.Appearance.Copy();
            this.DecimalPlaces = model.DecimalPlaces;
            this.Visible = model.Visible;
            this.ScaleFormat = model.ScaleFormat;
            if ( model._TextLabels is object )
            {
                this._TextLabels = ( string[] ) model._TextLabels.Clone();
            }

            this._Pane = model._Pane;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        this._TextLabels = null;
                        if ( this.Appearance is object )
                        {
                            this.Appearance.Dispose();
                            this.Appearance = null;
                        }
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Labels. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of Labels. </returns>
        public Labels Copy()
        {
            return new Labels( this );
        }

        /// <summary> Renders the specified tick label to the <see cref="Graphics"/> device. </summary>
        /// <remarks> This method centers the label vertical and horizontally. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="[text]">         A <see cref="System.String">String</see> value containing the
        /// text to be displayed.  This can be multiple lines, separated
        /// by new line ('\n')
        /// characters. </param>
        /// <param name="x">              The X location to display the text, in screen coordinates,
        /// relative to the horizontal (<see cref="HorizontalAlignment"/>)
        /// alignment parameter. </param>
        /// <param name="y">              The Y location to display the text, in screen coordinates,
        /// relative to the vertical (<see cref="VerticalAlignment"/>
        /// alignment parameter. </param>
        /// <param name="scaleFactor">    The scaling factor to be used for rendering objects.  This is
        /// calculated and passed down by the parent
        /// <see cref="Pane"/> object using the
        /// <see cref="Pane.GetScaleFactor"/>
        /// method, and is used to proportionally adjust font sizes, etc.
        /// according to the actual size of the graph. </param>
        public void Draw( Graphics graphicsDevice, string text, float x, float y, double scaleFactor )
        {
            if ( !this.Visible || string.IsNullOrWhiteSpace( text ) )
            {
                return;
            }

            // validate arguments.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            this.Appearance.Draw( graphicsDevice, text, x, y, HorizontalAlignment.Center, VerticalAlignment.Center, scaleFactor );
        }

        /// <summary> Draw the major tick labels. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="axis">           Reference to the <see cref="Axis"/> </param>
        /// <param name="tick">           The tick. </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        public void Draw( Graphics graphicsDevice, Axis axis, Tick tick, double scaleFactor )
        {
            if ( !this.Visible )
            {
                return;
            }

            // validate arguments.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            if ( axis is null )
            {
                throw new ArgumentNullException( nameof( axis ) );
            }

            if ( tick is null )
            {
                throw new ArgumentNullException( nameof( tick ) );
            }

            double scaleMultplier;
            float textCenter;

            // save the current clip bounds
            var clipBounds = graphicsDevice.ClipBounds;
            if ( axis.CoordinateScale.IsStripChart )
            {

                // clip to transformed axis area
                var labelArea = Rectangle.Round( axis.TransformedArea );
                // graphicsDevice.SetClip(axis.TransformedArea)
                labelArea.Offset( 1, 0 );
                labelArea.Inflate( -2, 0 );
                graphicsDevice.SetClip( labelArea );

                // get the Y position of the center of the axis labels
                // (the axis itself is referenced at zero)
                textCenter = -0.75f * this._Pane.AxisArea.Height;
                scaleMultplier = 1d;
            }
            else
            {
                // get the Y position of the center of the axis labels
                // (the axis itself is referenced at zero)
                textCenter = tick.GetScaledLength( scaleFactor ) + this.BoundingBox.Height / 2.0f;
                scaleMultplier = Math.Pow( 10.0d, axis.ScaleExponent.Value );
            }

            // loop for each tick
            for ( int i = 0, loopTo = tick.ValidTickCount - 1; i <= loopTo; i++ )
            {

                // draw the label
                string tmpStr = this.GetTickLabel( i, tick.GetValue( i ), axis.CoordinateScale, scaleMultplier );
                if ( !string.IsNullOrWhiteSpace( tmpStr ) )
                {
                    this.Draw( graphicsDevice, tmpStr, tick.GetLocation( i ), 0.0f + textCenter, scaleFactor );
                }
            }

            // reset the strip chart clipping area
            if ( axis.CoordinateScale.IsStripChart )
            {
                // restore the clip bounds
                graphicsDevice.SetClip( clipBounds );
            }
        }

        /// <summary> Returns a value label at the specified ordinal position. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="index">           The zero-based, ordinal index of the label.  For example, a
        /// value of 2 returns the third value label. </param>
        /// <param name="tickValue">       The numeric value associated with the label.  This value is
        /// ignored for log (<see cref="CoordinateScale.IsLog"/>) and
        /// text (<see cref="CoordinateScale.IsText"/>) types. </param>
        /// <param name="coordinateScale"> The <see cref="isr.Visuals.CoordinateScale"/> for the axis
        /// <see cref="Scale"/> </param>
        /// <param name="scaleExponent">   The <see cref="Scale.ScaleExponent"/> value. </param>
        /// <returns> The tick label. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private string GetTickLabel( int index, double tickValue, CoordinateScale coordinateScale, int scaleExponent )
        {
            return this.GetTickLabel( index, tickValue, coordinateScale, Math.Pow( 10.0d, scaleExponent ) );
        }

        /// <summary> Returns a value label at the specified ordinal position. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="index">           The zero-based, ordinal index of the label.  For example, a
        /// value of 2 returns the third value label. </param>
        /// <param name="tickValue">       The numeric value associated with the label.  This value is
        /// ignored for log (<see cref="CoordinateScale.IsLog"/>) and
        /// text (<see cref="CoordinateScale.IsText"/>) types. </param>
        /// <param name="coordinateScale"> The <see cref="isr.Visuals.CoordinateScale"/> for the axis
        /// <see cref="Scale"/> </param>
        /// <param name="scaleMultiplier"> The 10^<see cref="Scale.ScaleExponent"/> value. </param>
        /// <returns> The tick label. </returns>
        private string GetTickLabel( int index, double tickValue, CoordinateScale coordinateScale, double scaleMultiplier )
        {
            switch ( coordinateScale.CoordinateScaleType )
            {
                case CoordinateScaleType.Date:
                    {
                        return JulianDate.ToString( tickValue, this.ScaleFormat );
                    }

                case CoordinateScaleType.Linear:
                    {
                        string tmpStr = "{0:F*}";
                        tmpStr = tmpStr.Replace( "*", this.DecimalPlaces.ToString( "D" ) );
                        return string.Format( System.Globalization.CultureInfo.CurrentCulture, tmpStr, tickValue / scaleMultiplier );
                    }

                case CoordinateScaleType.Log:
                    {
                        return index >= -3 && index <= 4 ? Math.Pow( 10.0d, index ).ToString() : $"1e{index}";
                    }

                case CoordinateScaleType.StripChart:
                    {
                        var timeValue = TimeSeriesPointR.GetDateTime( tickValue );
                        return timeValue.ToString( this.ScaleFormat, System.Globalization.CultureInfo.CurrentCulture );
                    }

                case CoordinateScaleType.Text:
                    {
                        return this._TextLabels is null || index < 0 || index >= this._TextLabels.Length ? string.Empty : this._TextLabels[index];
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "Unhandled coordinate scale type" );
                        return string.Empty;
                    }
            }
        }

        /// <summary> Set the decimal places based on the <see paramref="CoordinateScale"/>. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="axis">            Reference to the <see cref="Axis"/> </param>
        /// <param name="baseValue">       The value to use to figuring auto decimal places. </param>
        /// <param name="scaleExponent">   The <see cref="Scale.ScaleExponent"/> value. </param>
        /// <param name="coordinateScale"> The <see cref="isr.Visuals.CoordinateScale"/> for the axis
        /// <see cref="Scale"/> </param>
        internal void SetDecimalPlaces( Axis axis, double baseValue, int scaleExponent, CoordinateScale coordinateScale )
        {
            switch ( coordinateScale.CoordinateScaleType )
            {
                case CoordinateScaleType.Date:
                    {

                        // Date Scale.  the number of decimal places to display is not used
                        this.DecimalPlaces.Value = 0;
                        break;
                    }

                case CoordinateScaleType.Linear:
                    {

                        // Calculate the appropriate number of decimal places to display if required
                        if ( this.DecimalPlaces.AutoScale )
                        {
                            this.DecimalPlaces.Value = Math.Max( axis.DecimalPlaces, baseValue.DecimalPlaces() - scaleExponent );
                        }

                        break;
                    }

                case CoordinateScaleType.Log:
                    {

                        // Log Scale
                        if ( this.DecimalPlaces.AutoScale )
                        {
                            // The number of decimal places to display is not used
                            this.DecimalPlaces.Value = 0;
                        }

                        break;
                    }

                case CoordinateScaleType.StripChart:
                    {

                        // if this is a strip-chart axis, then ignore as we have ordinal setting
                        this.DecimalPlaces.Value = 0;
                        break;
                    }

                case CoordinateScaleType.Text:
                    {

                        // if this is a text-based axis, then ignore as we have ordinal setting
                        this.DecimalPlaces.Value = 0;
                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "Unhandled coordinate scale type" );
                        break;
                    }
            }
        }

        /// <summary>
        /// Set the bounding box fitting the maximum size of the scale value text that is required to
        /// label the <see cref="Axis"/>. Use <see cref="P:BoundingBox">Bounding Box</see>
        /// to determine how much space is required for the axis labels.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="axis">           Reference to the <see cref="Axis"/> </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        internal void SetBoundingBox( Graphics graphicsDevice, Axis axis, double scaleFactor )
        {
            if ( axis is null )
                throw new ArgumentNullException( nameof( axis ) );
            double scaleMultplier = Math.Pow( 10.0d, axis.ScaleExponent.Value );
            this._BoundingBox = new SizeF( 0f, 0f );

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // loop for each major tick
            for ( int i = 0, loopTo = axis.MajorTick.TickCount - 1; i <= loopTo; i++ )
            {

                // estimate the label size
                string tmpStr = this.GetTickLabel( i, axis.MajorTick.GetValue( i ), axis.CoordinateScale, scaleMultplier );
                var sizeF = this.Appearance.BoundingBox( graphicsDevice, tmpStr, scaleFactor );
                if ( sizeF.Height > this.BoundingBox.Height )
                {
                    this._BoundingBox.Height = sizeF.Height;
                }

                if ( sizeF.Width > this.BoundingBox.Width )
                {
                    this._BoundingBox.Width = sizeF.Width;
                }
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> The appearance. </summary>

        /// <summary>
        /// gets a reference to the <see cref="isr.Visuals.TextAppearance">TextAppearance</see> class
        /// used to render the axis value labels.
        /// </summary>
        /// <value> A <see cref="isr.Visuals.TextAppearance">TextAppearance</see> instance. </value>
        public TextAppearance Appearance { get; private set; }

        /// <summary> The bounding box. </summary>
        private SizeF _BoundingBox = new SizeF( 0f, 0f );

        /// <summary>
        /// Gets a <see cref="System.Drawing.SizeF">size</see> structure representing the width and
        /// height of the bounding box for the axis label.
        /// </summary>
        /// <value> The bounding box. </value>
        public SizeF BoundingBox => this._BoundingBox;

        /// <summary>
        /// Gets or sets the <see cref="AutoValue"/> for the number of decimal places displayed for axis
        /// value labels.  The value can be determined automatically depending on
        /// <see cref="AutoValue.AutoScale"/>.
        /// </summary>
        /// <value> The number of decimal places to be displayed for axis value labels. </value>
        public AutoValue DecimalPlaces { get; set; }

        /// <summary> Determines if the value <see cref="Labels"/> will be drawn. </summary>
        /// <value> A <see cref="System.Boolean">Boolean</see> </value>
        public bool Visible { get; set; }

        /// <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
        private readonly Pane _Pane;

        /// <summary>
        /// The format of the <see cref="Tick"/> labels. This field is only used if the
        /// <see cref="Axis.CoordinateScale"/> is set to <see cref="CoordinateScaleType.Date"/>.
        /// </summary>
        /// <value>
        /// A <see cref="System.String"/> as defined for the <see cref="JulianDate.ToString"/> function.
        /// </value>
        public string ScaleFormat { get; set; }

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A System.String value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusMessage { get; set; }

        /// <summary> The text labels. </summary>
        private string[] _TextLabels;

        /// <summary>
        /// Gets or sets the <see cref="Axis"/> text labels for a
        /// <see cref="CoordinateScaleType.Text"/> axis.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A String() </returns>
        public string[] TextLabels()
        {
            return this._TextLabels;
        }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="Labels"/> class that defines the default property values
    /// for the <see cref="Labels"/> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class LabelsDefaults : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private LabelsDefaults() : base()
        {
            this.Visible = true;
            this.DecimalPlaces = new AutoValue( 0, true );
            this.Font = new Font( "Arial", 12f, FontStyle.Regular );
            this.FontColor = Color.Black;
            this.Filled = false;
            this.Framed = false;
            this.ScaleFormat = "&dd-&mmm-&yy &hh:&nn";
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static LabelsDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static LabelsDefaults Get()
        {
            if ( _Instance is null || _Instance.IsDisposed )
            {
                lock ( SyncLocker )
                    _Instance = new LabelsDefaults();
            }

            return _Instance;
        }

        #region "IDisposable Support"

        /// <summary> Gets or sets a value indicating whether this instance is disposed. </summary>
        /// <value> <c>True</c> if this instance is disposed; otherwise, <c>False</c>. </value>
        private bool IsDisposed { get; set; } // To detect redundant calls

        /// <summary> Releases unmanaged and - optionally - managed resources. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged resources. </param>
        private void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        // dispose managed state (managed objects).
                        if ( this.Font is object )
                        {
                            this.Font.Dispose();
                            this.Font = null;
                        }
                    }
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        #endregion

        /// <summary>
        /// Gets or sets the default display mode for the axis value <see cref="Labels"/>
        /// (<see cref="Labels.Visible"/> property). True to show the labels, False to hide them.
        /// </summary>
        /// <value> The visible. </value>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the default decimal places for the axis value <see cref="Labels"/>
        /// (<see cref="Labels.DecimalPlaces"/> property).
        /// </summary>
        /// <value> The decimal places. </value>
        public AutoValue DecimalPlaces { get; set; }

        /// <summary>
        /// Gets or sets the default <see cref="System.Drawing.Font">Font</see>
        /// of the <see cref="Title"/> caption.
        /// </summary>
        /// <value> The font. </value>
        public Font Font { get; set; }

        /// <summary>
        /// Gets or sets the default font color for the <see cref="Legend"/> entries
        /// (<see cref="TextAppearance.FontColor"/> property).
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color FontColor { get; set; }

        /// <summary>
        /// Gets or sets the default font filled mode for the <see cref="Axis"/> scale value appearance
        /// <see cref="TextAppearance.Frame"/>. True for filled, False otherwise.
        /// </summary>
        /// <value> The filled. </value>
        public bool Filled { get; set; }

        /// <summary>
        /// Gets or sets the default font framed mode for the <see cref="Axis"/> scale value appearance
        /// <see cref="Labels.Appearance"/>
        /// (<see cref="TextAppearance.Frame"/> property). True for framed, False otherwise.
        /// </summary>
        /// <value> The framed. </value>
        public bool Framed { get; set; }

        /// <summary>
        /// Gets or sets the default setting for the <see cref="Axis"/> scale date format string
        /// (<see cref="Tick.ScaleFormat"/> property).  This value is set as per the
        /// <see cref="JulianDate.ToString"/> function.
        /// </summary>
        /// <value> The scale format. </value>
        public string ScaleFormat { get; set; }
    }
}

#endregion

