using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary>
    /// A class representing all the characteristics of the <see cref="Cord"/>
    /// segments that make up a curve on the graph.
    /// </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class Cord : ICloneable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs a <see cref="Cord"/> with default property values values as defined in the
        /// <see cref="CordDefaults"/> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public Cord() : base()
        {
            var cordDefaults = CordDefaults.Get();
            this.LineWidth = cordDefaults.LineWidth;
            this.DashStyle = cordDefaults.DashStyle;
            this.Visible = cordDefaults.Visible;
            this.LineColor = cordDefaults.LineColor;
            this.CordType = cordDefaults.CordType;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Cord object from which to copy. </param>
        public Cord( Cord model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.LineWidth = model.LineWidth;
            this.DashStyle = model.DashStyle;
            this.Visible = model.Visible;
            this.LineColor = model.LineColor;
            this.CordType = model.CordType;
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Cord. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Cord. </returns>
        public Cord Copy()
        {
            return new Cord( this );
        }

        /// <summary>
        /// Renders a single <see cref="Cord"/> segment to the specified
        /// <see cref="Graphics"/> device.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="x1">             The x position of the starting point that defines the line
        /// segment in screen pixels. </param>
        /// <param name="y1">             The y position of the starting point that defines the line
        /// segment in screen pixels. </param>
        /// <param name="x2">             The x position of the ending point that defines the line segment
        /// in screen pixels. </param>
        /// <param name="y2">             The y position of the ending point that defines the line segment
        /// in screen pixels. </param>
        public void Draw( Graphics graphicsDevice, float x1, float y1, float x2, float y2 )
        {
            if ( !this.Visible )
                return;

            // validate argument.
            if ( graphicsDevice is null )
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            using var pen = new Pen( this.LineColor, this.LineWidth ) {
                DashStyle = this.DashStyle
            };
            graphicsDevice.DrawLine( pen, x1, y1, x2, y2 );
        }

        /// <summary>
        /// Renders a series of <see cref="Cord"/> segments to the specified
        /// <see cref="Graphics"/> device.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="x">              The array of x position values that define the line segments in
        /// screen pixels. </param>
        /// <param name="y">              The array of y position values that define the line segments in
        /// screen pixels. </param>
        public void Draw( Graphics graphicsDevice, float[] x, float[] y )
        {
            if ( !this.Visible )
            {
                return;
            }

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            if ( x is null )
            {
                throw new ArgumentNullException( nameof( x ) );
            }

            if ( y is null )
            {
                throw new ArgumentNullException( nameof( y ) );
            }

            using var pen = new Pen( this.LineColor, this.LineWidth ) {
                DashStyle = this.DashStyle
            };
            int nSeg = x.Length - 1;
            int iPlus = 0;
            for ( int i = 0, loopTo = nSeg - 1; i <= loopTo; i++ )
            {
                iPlus += 1;
                if ( x[i] != float.MaxValue && x[iPlus] != float.MaxValue && y[i] != float.MaxValue && y[iPlus] != float.MaxValue )
                {
                    switch ( this.CordType )
                    {
                        case CordType.HoldStep:
                            {
                                graphicsDevice.DrawLine( pen, x[i], y[i], x[i], y[iPlus] );
                                graphicsDevice.DrawLine( pen, x[i], y[iPlus], x[iPlus], y[iPlus] );
                                break;
                            }

                        case CordType.Linear:
                            {
                                graphicsDevice.DrawLine( pen, x[i], y[i], x[iPlus], y[iPlus] );
                                break;
                            }

                        case CordType.StepHold:
                            {
                                graphicsDevice.DrawLine( pen, x[i], y[i], x[iPlus], y[i] );
                                graphicsDevice.DrawLine( pen, x[iPlus], y[i], x[iPlus], y[iPlus] );
                                break;
                            }

                        default:
                            {
                                Debug.Assert( !Debugger.IsAttached, "Unhandled cord type" );
                                break;
                            }
                    }
                }
            }
        }

        /// <summary>
        /// Renders a series of <see cref="Cord"/> segments to the specified
        /// <see cref="Graphics"/> device.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="x">              The array of x position values that define the line segments in
        /// screen pixels. </param>
        /// <param name="y">              The array of y position values that define the line segments in
        /// screen pixels. </param>
        /// <param name="ignoreMissing">  True to ignore missing values. </param>
        public void Draw( Graphics graphicsDevice, float[] x, float[] y, bool ignoreMissing )
        {
            if ( !this.Visible )
            {
                return;
            }

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            if ( x is null )
            {
                throw new ArgumentNullException( nameof( x ) );
            }

            if ( y is null )
            {
                throw new ArgumentNullException( nameof( y ) );
            }

            using var pen = new Pen( this.LineColor, this.LineWidth ) {
                DashStyle = this.DashStyle
            };
            int nSeg = x.Length - 1;
            int iPlus = 0;
            for ( int i = 0, loopTo = nSeg - 1; i <= loopTo; i++ )
            {
                iPlus += 1;
                if ( ignoreMissing || x[i] != float.MaxValue && x[iPlus] != float.MaxValue && y[i] != float.MaxValue && y[iPlus] != float.MaxValue )
                {
                    switch ( this.CordType )
                    {
                        case CordType.HoldStep:
                            {
                                graphicsDevice.DrawLine( pen, x[i], y[i], x[i], y[iPlus] );
                                graphicsDevice.DrawLine( pen, x[i], y[iPlus], x[iPlus], y[iPlus] );
                                break;
                            }

                        case CordType.Linear:
                            {
                                graphicsDevice.DrawLine( pen, x[i], y[i], x[iPlus], y[iPlus] );
                                break;
                            }

                        case CordType.StepHold:
                            {
                                graphicsDevice.DrawLine( pen, x[i], y[i], x[iPlus], y[i] );
                                graphicsDevice.DrawLine( pen, x[iPlus], y[i], x[iPlus], y[iPlus] );
                                break;
                            }

                        default:
                            {
                                Debug.Assert( !Debugger.IsAttached, "Unhandled cord type" );
                                break;
                            }
                    }
                }
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Gets or sets the <see cref="System.Drawing.Drawing2D.DashStyle"/> of the <see cref="Cord"/>
        /// allowing to draw solid, dashed, or dotted cords.
        /// </summary>
        /// <value> The dash style. </value>
        public System.Drawing.Drawing2D.DashStyle DashStyle { get; set; }

        /// <summary> Gets or sets a property that shows or hides the <see cref="Cord"/>. </summary>
        /// <value> True to show the Cord, false to hide it. </value>
        public bool Visible { get; set; }

        /// <summary> The color of the <see cref="Cord"/> </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color LineColor { get; set; }

        /// <summary> The pen width used to draw the <see cref="Cord"/> </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float LineWidth { get; set; }

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A System.String value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusMessage { get; set; }

        /// <summary>
        /// Determines how cords connect <see cref="Curve"/> points.  Points can be connected by directly
        /// connecting the points from the <see cref="Curve.X"/> and
        /// <see cref="Curve.Y"/> data arrays.  If the cord curve is a stair type of either
        /// <see cref="CordType.StepHold"/> or <see cref="CordType.StepHold"/> points
        /// are connected by a series of horizontal and vertical lines that represent discrete, constant
        /// values.  Thus, values can be forward oriented <c>StepHold</c>
        /// (<see cref="isr.Visuals.CordType"/>) or rearward oriented <c>HoldStep</c>. That is, the
        /// points are defined at the beginning or end of the constant value for which they apply,
        /// respectively.
        /// </summary>
        /// <remarks> David, 2007-10-15, 1.0.2844.x Rename from Type to Cord Type. </remarks>
        /// <value> A <see cref="isr.Visuals.CordType"/> value. </value>
        public CordType CordType { get; set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="Cord"/> class that defines the default property values
    /// for the <see cref="isr.Visuals.Cord"/> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class CordDefaults
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private CordDefaults() : base()
        {
            this.LineColor = Color.Red;
            this.Visible = true;
            this.LineWidth = 1.0f;
            this.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this.CordType = CordType.Linear;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary> The instance. </summary>
        private static CordDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static CordDefaults Get()
        {
            if ( _Instance is null )
            {
                lock ( SyncLocker )
                    _Instance = new CordDefaults();
            }

            return _Instance;
        }

        #endregion

        /// <summary>
        /// Gets or sets the default color for curves (Cord segments connecting the points). This is the
        /// default value for the <see cref="Cord.LineColor"/> property.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color LineColor { get; set; }

        /// <summary>
        /// Gets or sets the default mode for displaying line segments (<see cref="Cord.Visible"/>
        /// property).  True to show the line segments, false to hide them.
        /// </summary>
        /// <value> The visible. </value>
        public bool Visible { get; set; }

        /// <summary> The (<see cref="Cord.LineWidth"/> of line segments. </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float LineWidth { get; set; }

        /// <summary>
        /// Gets or sets the default <see cref="System.Drawing.Drawing2D.DashStyle"/> style for line
        /// segments (<see cref="Cord.DashStyle"/> property).
        /// </summary>
        /// <value> The dash style. </value>
        public System.Drawing.Drawing2D.DashStyle DashStyle { get; set; }

        /// <summary>
        /// Default value for the cord type property (<see cref="CordType"/>).
        /// This determines if the curve will be drawn by directly connecting the points from the
        /// <see cref="Curve.X"/> and <see cref="Curve.Y"/> data arrays, or if the curve will be a "stair-
        /// step" in which the points are connected by a series of horizontal and vertical lines that
        /// represent discrete values.  Discrete values can be forward oriented
        /// <code>StepHold</code> (<see cref="CordType"/>) or backward oriented
        /// <code>HoldStep</code>. That is, the points are defined at the beginning or end
        /// of the value for which they apply, respectively.
        /// </summary>
        /// <value> A <see cref="CordType"/> value. </value>
        public CordType CordType { get; set; }
    }
}

#endregion

