using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;

using isr.Core.Constructs;

namespace isr.Visuals
{

    /// <summary> Provides Tick properties. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class Tick : ICloneable, IDisposable
    {

        #region " SHARED "

        /// <summary> Return a standard hour spacing at 1, 2, 3, 6, 12, or 24 hours. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="hour"> Hour to round up to standard spacing. </param>
        /// <returns> A standard hours spacing. </returns>
        public static double SelectHourSpacing( double hour )
        {
            hour = Math.Floor( hour );
            if ( hour > 12.0d )
            {
                return 24.0d;
            }
            else if ( hour > 6.0d )
            {
                return 12.0d;
            }
            else
            {
                return hour > 3.0d ? 6.0d : hour < 1.0d ? 1.0d : hour;
            }
        }

        /// <summary> Return a standard minute spacing at 1, 5, 15 or 30 minutes. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="minute"> Minute to round up to standard spacing. </param>
        /// <returns> A standard minute spacing. </returns>
        public static double SelectMinuteSpacing( double minute )
        {
            minute = Math.Floor( minute );
            // make sure the minute spacing is 1, 5, 15, or 30 minutes
            if ( minute > 15.0d )
            {
                return 30.0d;
            }
            else if ( minute > 5.0d )
            {
                return 15.0d;
            }
            else
            {
                return minute > 1.0d ? 5.0d : minute < 1.0d ? 1.0d : minute;
            }
        }

        /// <summary> Return a standard month spacing at 1, 2, 3, 6, or 12 month. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="month"> Month to round up to standard month spacing. </param>
        /// <returns> A standard month spacing. </returns>
        public static double SelectMonthSpacing( double month )
        {
            month = Math.Floor( month );
            // make sure the minor spacing is 1, 2, 3, 6, or 12 months
            return month > 6d ? 12d : month > 3d ? 6d : month;
        }

        /// <summary> Return a standard second spacing at 1, 5, 15, or 30 seconds. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="second"> Second to round up to standard spacing. </param>
        /// <returns> A standard second spacing. </returns>
        public static double SelectSecondSpacing( double second )
        {
            second = Math.Floor( second );
            // make sure the second spacing is 1, 5, 15, or 30 minutes
            if ( second > 15.0d )
            {
                return 30.0d;
            }
            else if ( second > 5.0d )
            {
                return 15.0d;
            }
            else
            {
                return second > 1.0d ? 5.0d : second < 1.0d ? 1.0d : second;
            }
        }

        /// <summary> Return a standard spacing at 1, 2, 5, or 10 size. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="spacing"> Value to round up to standard spacing. </param>
        /// <returns> A standard spacing. </returns>
        public static double SelectStandardSpacing( double spacing )
        {
            spacing = Math.Floor( spacing );
            // promote the spacing to either 1, 2, 5, or 10
            if ( spacing > 7.5d )
            {
                return 10.0d;
            }
            else
            {
                return spacing > 2.5d ? 5.0d : spacing > 1.5d ? 2.0d : 1d;
            }
        }

        #endregion

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs an instance of this class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="major"> <see cref="System.Boolean">Boolean</see> True for major tick marks
        /// and False for minor tick marks. </param>

        // instantiate the base class
        public Tick( bool major ) : base()
        {
            this.IsMajor = major;
            this.ScaleFormat = TickDefaults.Get().ScaleFormat;
            if ( major )
            {
                var majorTickDefaults = MajorTickDefaults.Get();
                this.IsInside = majorTickDefaults.IsInside;
                this.IsBottom = majorTickDefaults.IsBottom;
                this.IsTop = majorTickDefaults.IsTop;
                this.IsOutside = majorTickDefaults.IsOutside;
                this.Visible = majorTickDefaults.Visible;
                this.LineColor = majorTickDefaults.LineColor;
                this.LineWidth = majorTickDefaults.LineWidth;
                this.Length = majorTickDefaults.Length;
                this._Spacing = majorTickDefaults.Spacing;
                this.AutoSpacing = majorTickDefaults.AutoSpacing;
                this.TimeIntervalType = majorTickDefaults.TimeIntervalType;
            }
            else
            {
                var minorTickDefaults = MinorTickDefaults.Get();
                this.IsInside = minorTickDefaults.IsInside;
                this.IsBottom = minorTickDefaults.IsBottom;
                this.IsTop = minorTickDefaults.IsTop;
                this.IsOutside = minorTickDefaults.IsOutside;
                this.Visible = minorTickDefaults.Visible;
                this.LineColor = minorTickDefaults.LineColor;
                this.LineWidth = minorTickDefaults.LineWidth;
                this.Length = minorTickDefaults.Length;
                this._Spacing = minorTickDefaults.Spacing;
                this.AutoSpacing = minorTickDefaults.AutoSpacing;
                this.TimeIntervalType = minorTickDefaults.TimeIntervalType;
            }
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="model"> The Tick object from which to copy. </param>
        public Tick( Tick model ) : this( true )
        {
            if ( model is object )
            {
                this.IsMajor = model.IsMajor;
                this.AutoSpacing = model.AutoSpacing;
                this.IsInside = model.IsInside;
                this.IsTop = model.IsTop;
                this.IsOutside = model.IsOutside;
                this.Visible = model.Visible;
                this.LineColor = model.LineColor;
                this.LineWidth = model.LineWidth;
                this.Length = model.Length;
                this._Spacing = model._Spacing;
                this.ScaleFormat = model.ScaleFormat;
                this.TimeIntervalType = model.TimeIntervalType;
            }
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }


        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of Tick. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Tick. </returns>
        public Tick Copy()
        {
            return new Tick( this );
        }

        /// <summary> Draws a single tick mark. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice">  Reference to a graphic device to be drawn into.  This is
        /// normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="tickPen">         Reference to the tick
        /// <see cref="System.Drawing.Pen">pen</see> </param>
        /// <param name="x">               The pixel location from the 'left side the
        /// <see cref="Pane.AxisArea"/>. </param>
        /// <param name="transformedArea"> The pixel location of the far side of the
        /// <see cref="Pane.AxisArea"/> from this axis. This value is the
        /// axisArea.Height for the X Axis,
        /// or the axisArea.Width for the Y Axis and Y2 Axis. </param>
        /// <param name="tickSize">        Size of the tick. </param>
        private void Draw( Graphics graphicsDevice, Pen tickPen, float x, RectangleF transformedArea, float tickSize )
        {
            if ( !this.Visible )
            {
                return;
            }

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            if ( this.IsInside || this.IsOutside )
            {
                float y1 = 0.0f;
                float y2 = 0.0f;
                // draw the outside tick
                if ( this.IsOutside )
                {
                    y2 = tickSize;
                }
                // draw the inside tick
                if ( this.IsInside )
                {
                    y1 = -tickSize;
                }
                // update:  for drawing ticks at Axis Point replace 0.0F with LocalTransform Axis point.
                graphicsDevice.DrawLine( tickPen, x, y1, x, y2 );
            }

            // draw the top tick
            if ( this.IsTop )
            {
                float topPosition = transformedArea.Top;
                graphicsDevice.DrawLine( tickPen, x, topPosition, x, topPosition + tickSize );
            }

            // draw the bottom tick
            if ( this.IsBottom )
            {
                float bottomPosition = transformedArea.Bottom;
                graphicsDevice.DrawLine( tickPen, x, bottomPosition, x, bottomPosition - tickSize );
            }
        }

        /// <summary> Draw major tick marks. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="axis">           Reference to the <see cref="Axis"/> </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        public void Draw( Graphics graphicsDevice, Axis axis, double scaleFactor )
        {
            if ( !this.Visible )
            {
                return;
            }

            if ( axis is null )
            {
                throw new ArgumentNullException( nameof( axis ) );
            }

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            if ( axis is null )
            {
                throw new ArgumentNullException( nameof( axis ) );
            }

            float scaledTick = this.GetScaledLength( scaleFactor );
            using var tickPen = new Pen( this.LineColor, this.LineWidth );
            // loop for each major tick
            for ( int i = 0, loopTo = this.ValidTickCount - 1; i <= loopTo; i++ )
                // draw the major tick
                this.Draw( graphicsDevice, tickPen, this._Locations[i], axis.TransformedArea, scaledTick );
        }

        /// <summary>
        /// Calculate a date that is close to the specified date and an even multiple of the selected
        /// <see cref="TimeIntervalType"/> for a
        /// <see cref="CoordinateScaleType.Date"/> scale.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="dateValue"> The date which the calculation should be close to. </param>
        /// <param name="direction"> The desired direction for the date to take. One (1) indicates the
        /// result date should be greater than the specified date parameter.
        /// Minus one (-1) indicates the other direction. </param>
        /// <returns> The calculated date. </returns>
        internal double GetEvenDateSpacing( double dateValue, int direction )
        {
            int year = default, month = default, day = default, hour = default, minute = default, second = default, millisecond = default;
            JulianDate.ToCalendarDate( dateValue, ref year, ref month, ref day, ref hour, ref minute, ref second, ref millisecond );

            // If the direction is -1, then it is sufficient to go to the beginning of
            // the current time period, .e.graphicsDevice., for 15-May-95, and monthly spacing, we
            // can just back up to 1-May-95
            if ( direction < 0 )
            {
                direction = 0;
            }

            switch ( this.TimeIntervalType )
            {
                case TimeIntervalType.Year:
                    {
                        // If the date is already an exact year, then don't space to the next year
                        return direction == 1 && month == 1 && day == 1 && hour == 0 && minute == 0 && second == 0 ? dateValue : JulianDate.FromArbitraryCalendarDate( year + direction, 1, 1, 0, 0, 0, 0 );
                    }

                case TimeIntervalType.Month:
                    {
                        // If the date is already an exact month, then don't space to the next month
                        return direction == 1 && day == 1 && hour == 0 && minute == 0 && second == 0 ? dateValue : JulianDate.FromArbitraryCalendarDate( year, month + direction, 1, 0, 0, 0, 0 );
                    }

                case TimeIntervalType.Day:
                    {
                        // If the date is already an exact Day, then don't space to the next day
                        return direction == 1 && hour == 0 && minute == 0 && second == 0 ? dateValue : JulianDate.FromArbitraryCalendarDate( year, month, day + direction, 0, 0, 0, 0 );
                    }

                case TimeIntervalType.Hour:
                    {
                        // If the date is already an exact hour, then don't space to the next hour
                        return direction == 1 && minute == 0 && second == 0 ? dateValue : JulianDate.FromArbitraryCalendarDate( year, month, day, hour + direction, 0, 0, 0 );
                    }

                case TimeIntervalType.Minute:
                    {
                        // If the date is already an exact minute, then don't space to the next minute
                        return direction == 1 && second == 0 ? dateValue : JulianDate.FromArbitraryCalendarDate( year, month, day, hour, minute + direction, 0, 0 );
                    }

                case TimeIntervalType.Second:
                    {
                        return JulianDate.FromArbitraryCalendarDate( year, month, day, hour, minute, second + direction, 0 );
                    }
            }

            return default;
        }

        /// <summary>
        /// Determines the value for the first major tick mark, i.e., the first value that is an integral
        /// multiple of the tick spacing, taking into account date/time units if appropriate.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="axis"> Reference to the <see cref="Axis"/> </param>
        /// <returns> First major tick mark value. </returns>
        private double GetFirstMajorTickValue( Axis axis )
        {
            if ( axis is null )
                throw new ArgumentNullException( nameof( axis ) );
            switch ( axis.CoordinateScale.CoordinateScaleType )
            {
                case CoordinateScaleType.Date:
                    {
                        int year = default, month = default, day = default, hour = default, minute = default, second = default, millisecond = default;
                        JulianDate.ToCalendarDate( axis.Min.Value, ref year, ref month, ref day, ref hour, ref minute, ref second, ref millisecond );
                        switch ( this.TimeIntervalType )
                        {
                            case TimeIntervalType.Year:
                                {
                                    month = 1;
                                    day = 1;
                                    hour = 0;
                                    minute = 0;
                                    second = 0;
                                    break;
                                }

                            case TimeIntervalType.Month:
                                {
                                    day = 1;
                                    hour = 0;
                                    minute = 0;
                                    second = 0;
                                    break;
                                }

                            case TimeIntervalType.Day:
                                {
                                    hour = 0;
                                    minute = 0;
                                    second = 0;
                                    break;
                                }

                            case TimeIntervalType.Hour:
                                {
                                    minute = 0;
                                    second = 0;
                                    break;
                                }

                            case TimeIntervalType.Minute:
                                {
                                    second = 0;
                                    break;
                                }

                            case TimeIntervalType.Second:
                                {
                                    break;
                                }
                        }

                        double xlsDay = JulianDate.FromArbitraryCalendarDate( year, month, day, hour, minute, second, 0 );
                        if ( xlsDay < axis.Min.Value )
                        {
                            switch ( this.TimeIntervalType )
                            {
                                case TimeIntervalType.Year:
                                    {
                                        year += 1;
                                        break;
                                    }

                                case TimeIntervalType.Month:
                                    {
                                        month += 1;
                                        break;
                                    }

                                case TimeIntervalType.Day:
                                    {
                                        day += 1;
                                        break;
                                    }

                                case TimeIntervalType.Hour:
                                    {
                                        hour += 1;
                                        break;
                                    }

                                case TimeIntervalType.Minute:
                                    {
                                        minute += 1;
                                        break;
                                    }

                                case TimeIntervalType.Second:
                                    {
                                        second += 1;
                                        break;
                                    }
                            }

                            xlsDay = JulianDate.FromArbitraryCalendarDate( year, month, day, hour, minute, second, 0 );
                        }

                        return xlsDay;
                    }

                case CoordinateScaleType.Linear:
                case CoordinateScaleType.StripChart:
                    {

                        // go to the nearest even multiple of the spacing size
                        return this._Spacing * Math.Round( axis.Min.Value / this._Spacing );
                    }
                // Dim decadeScale As Double = Math.Pow(10, Me.DecimalPlaces(axis))
                // Return Math.Round(decadeScale * axis.Min.Value) / decadeScale

                case CoordinateScaleType.Log:
                    {

                        // go to the nearest even multiple of the spacing size

                        return Math.Ceiling( axis.Min.Value.Log10() - 0.00000001d );
                    }

                case CoordinateScaleType.Text:
                    {
                        return 1.0d;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, $"unhandled coordinate scale type {axis.CoordinateScale.CoordinateScaleType}" );
                        break;
                    }
            }

            return default;
        }

        /// <summary> Determine the ordinals of the first minor tick mark. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="axis">                Reference to the <see cref="Axis"/> </param>
        /// <param name="firstMajorTickValue"> The value of the first major tick for the axis. </param>
        /// <returns>
        /// The ordinal position of the first minor tick, relative to the first major tick. This value
        /// can be negative (e.graphicsDevice., -3 means the first minor tick is 3 minor spacing
        /// increments before the first major tick.
        /// </returns>
        private static int GetFirstMinorTickOrdinal( Axis axis, double firstMajorTickValue )
        {
            if ( axis is null )
            {
                throw new ArgumentNullException( nameof( axis ) );
            }

            switch ( axis.CoordinateScale.CoordinateScaleType )
            {
                case CoordinateScaleType.Date:
                    {
                        double offset = axis.Min.Value - firstMajorTickValue;
                        switch ( axis.MajorTick.TimeIntervalType )
                        {
                            case TimeIntervalType.Year:
                                {
                                    return Convert.ToInt32( Math.Floor( offset / 365.0d ) );
                                }

                            case TimeIntervalType.Month:
                                {
                                    return Convert.ToInt32( Math.Floor( offset / 28.0d ) );
                                }

                            case TimeIntervalType.Day:
                                {
                                    return Convert.ToInt32( Math.Floor( offset ) );
                                }

                            case TimeIntervalType.Hour:
                                {
                                    return Convert.ToInt32( Math.Floor( offset * JulianDate.HoursPerDay ) );
                                }

                            case TimeIntervalType.Minute:
                                {
                                    return Convert.ToInt32( Math.Floor( offset * JulianDate.MinutesPerDay ) );
                                }

                            case TimeIntervalType.Second:
                                {
                                    return Convert.ToInt32( Math.Floor( offset * JulianDate.SecondsPerDay ) );
                                }
                        }

                        break;
                    }

                case CoordinateScaleType.Linear:
                case CoordinateScaleType.StripChart:
                    {

                        // regular linear scale
                        return Convert.ToInt32( Math.Floor( (axis.Min.Value - firstMajorTickValue) / axis.MinorTick.Spacing ) );
                    }

                case CoordinateScaleType.Log:
                    {

                        // log scale
                        return -9;
                    }

                case CoordinateScaleType.Text:
                    {

                        // text labels (ordinal scale)

                        // This should never happen (no minor tick marks for text labels)
                        return 0;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, $"unhandled coordinate scale type {axis.CoordinateScale.CoordinateScaleType}" );
                        break;
                    }
            }

            return default;
        }

        /// <summary>
        /// Calculate major tick spacing for a <see cref="CoordinateScaleType.Date"/> scale. This method
        /// is used by <see cref="Axis.Rescale"/>.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="axis">               Reference to the <see cref="Axis"/> </param>
        /// <param name="targetSpacingCount"> The desired "typical" number of tick spacing to divide the
        /// range into. </param>
        /// <returns>
        /// The calculated spacing for the specified data range.  Also calculates and sets the values for
        /// <see cref="TimeIntervalType"/>, and <see cref="ScaleFormat"/>
        /// </returns>
        internal double GetMajorDateSpacing( Axis axis, int targetSpacingCount )
        {
            if ( axis is null )
            {
                throw new ArgumentNullException( nameof( axis ) );
            }

            // Calculate an initial guess at spacing
            double tempSpacing = axis.Range / targetSpacingCount;
            double range = axis.Range;
            if ( range > TickDefaults.Get().RangeYearYear )
            {
                this.TimeIntervalType = TimeIntervalType.Year;
                this.ScaleFormat = "&yyyy";
                tempSpacing = Math.Max( Math.Floor( tempSpacing / 365.0d ), 1.0d );
            }
            else if ( range > TickDefaults.Get().RangeYearMonth )
            {
                this.TimeIntervalType = TimeIntervalType.Year;
                this.ScaleFormat = "&mmm-&yy";
                tempSpacing = 1.0d;
            }
            else if ( range > TickDefaults.Get().RangeMonthMonth )
            {
                this.TimeIntervalType = TimeIntervalType.Month;
                this.ScaleFormat = "&mmm-&yy";
                tempSpacing = Math.Max( Math.Floor( tempSpacing / 30.0d ), 1.0d );
            }
            else if ( range > TickDefaults.Get().RangeDayDay )
            {
                this.TimeIntervalType = TimeIntervalType.Day;
                this.ScaleFormat = "&d-&mmm";
                tempSpacing = Math.Max( Math.Floor( tempSpacing ), 1.0d );
            }
            else if ( range > TickDefaults.Get().RangeDayHour )
            {
                this.TimeIntervalType = TimeIntervalType.Day;
                this.ScaleFormat = "&d-&mmm &hh:&nn";
                tempSpacing = 1.0d;
            }
            else if ( range > TickDefaults.Get().RangeHourHour )
            {
                this.TimeIntervalType = TimeIntervalType.Hour;
                this.ScaleFormat = "&hh:&nn";
                tempSpacing = SelectHourSpacing( tempSpacing * JulianDate.HoursPerDay );
            }
            else if ( range > TickDefaults.Get().RangeHourMinute )
            {
                this.TimeIntervalType = TimeIntervalType.Hour;
                this.ScaleFormat = "&hh:&nn";
                tempSpacing = 1.0d;
            }
            else if ( range > TickDefaults.Get().RangeMinuteMinute )
            {
                this.TimeIntervalType = TimeIntervalType.Minute;
                this.ScaleFormat = "&hh:&nn";
                tempSpacing = SelectHourSpacing( tempSpacing * JulianDate.MinutesPerDay );
            }
            else if ( range > TickDefaults.Get().RangeMinuteSecond )
            {
                this.TimeIntervalType = TimeIntervalType.Minute;
                this.ScaleFormat = "&nn:&ss";
                tempSpacing = 1.0d;
            }
            else
            {
                // SecondSecond
                this.TimeIntervalType = TimeIntervalType.Second;
                this.ScaleFormat = "&nn:&ss";
                tempSpacing = SelectSecondSpacing( tempSpacing * JulianDate.SecondsPerDay );
            }

            return tempSpacing;
        }

        /// <summary> Gets the updated tick count for the axis. </summary>
        /// <remarks> Fix bug by limiting rounding digits to 0 to 15. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="axis"> Reference to the <see cref="Axis"/> </param>
        /// <returns> The tick count. </returns>
        private int GetTickCount( Axis axis )
        {
            if ( axis is null )
            {
                throw new ArgumentNullException( nameof( axis ) );
            }

            switch ( axis.CoordinateScale.CoordinateScaleType )
            {
                case CoordinateScaleType.Date:
                    {
                        double range = axis.Range;

                        // Date-Time scale
                        int year1 = default, year2 = default, month1 = default, month2 = default, day1 = default, day2 = default, hour1 = default, hour2 = default, minute1 = default, minute2 = default, second1 = default, second2 = default, millisecond1 = default, millisecond2 = default;
                        JulianDate.ToCalendarDate( axis.Min.Value, ref year1, ref month1, ref day1, ref hour1, ref minute1, ref second1, ref millisecond1 );
                        JulianDate.ToCalendarDate( axis.Max.Value, ref year2, ref month2, ref day2, ref hour2, ref minute2, ref second2, ref millisecond2 );
                        switch ( this.TimeIntervalType )
                        {
                            case TimeIntervalType.Year:
                                {
                                    return Math.Max( 1, Convert.ToInt32( Math.Floor( (year2 - year1 + 1.0d) / this._Spacing ) ) );
                                }

                            case TimeIntervalType.Month:
                                {
                                    return Math.Max( 1, Convert.ToInt32( Math.Floor( (month2 - month1 + 12.0d * (year2 - year1) + 1.0d) / this._Spacing ) ) );
                                }

                            case TimeIntervalType.Day:
                                {
                                    return Math.Max( 1, Convert.ToInt32( Math.Floor( (range + 1.0d) / this._Spacing ) ) );
                                }

                            case TimeIntervalType.Hour:
                                {
                                    return Math.Max( 1, Convert.ToInt32( Math.Floor( range * JulianDate.HoursPerDay + 1.0d ) ) );
                                }

                            case TimeIntervalType.Minute:
                                {
                                    return Math.Max( 1, Convert.ToInt32( Math.Floor( range * JulianDate.MinutesPerDay + 1.0d ) ) );
                                }

                            case TimeIntervalType.Second:
                                {
                                    return Math.Max( 1, Convert.ToInt32( Math.Floor( range * JulianDate.SecondsPerDay + 1.0d ) ) );
                                }

                            default:
                                {
                                    Debug.Assert( !Debugger.IsAttached, "Unhandled time interval type " + this.TimeIntervalType.ToString() );
                                    break;
                                }
                        }

                        break;
                    }

                case CoordinateScaleType.Linear:
                    {

                        // set the tick count
                        int roundingDigits = this.DecimalPlaces( axis ) + 2;
                        roundingDigits = Math.Max( roundingDigits, 0 );
                        roundingDigits = Math.Min( roundingDigits, 15 );
                        return Convert.ToInt32( Math.Round( Math.Round( axis.Range, roundingDigits ) / this._Spacing ) + 1d );
                    }

                case CoordinateScaleType.Log:
                    {
                        int roundingDigits = axis.DecimalPlaces + 2;
                        roundingDigits = Math.Max( roundingDigits, 0 );
                        roundingDigits = Math.Min( roundingDigits, 15 );
                        return Convert.ToInt32( Math.Floor( Math.Round( axis.Max.Value, roundingDigits ).Log10() ) ) - Convert.ToInt32( Math.Ceiling( Math.Round( axis.Min.Value, roundingDigits ).Log10() ) ) + 1;
                    }

                case CoordinateScaleType.StripChart:
                    {
                        return this.IsMajor ? MajorTickDefaults.Get().TimeSeriesSpacingCount + 1 : MinorTickDefaults.Get().TimeSeriesSpacingCount + 1;
                    }

                case CoordinateScaleType.Text:
                    {

                        // If no array of labels is available, just assume 10 labels so we don't blow up.
                        return axis.TickLabels.TextLabels() is null ? 10 : axis.TickLabels.TextLabels().Length;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "unhandled coordinate type " + axis.CoordinateScale.CoordinateScaleType.ToString() );
                        break;
                    }
            }

            return default;
        }

        /// <summary>
        /// Determine the value for any major tick.  Properly accounts for
        /// <see cref="CoordinateScale.IsLog"/>, <see cref="CoordinateScale.IsText"/>, and other axis
        /// format settings.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="axis">                Reference to the <see cref="Axis"/> </param>
        /// <param name="firstMajorTickValue"> The value of the first major tick. </param>
        /// <param name="tickItem">            The major tick number (0 = first major tick).  For log
        /// scales, this is the actual power of 10. </param>
        /// <returns> The specified major tick value (floating point double). </returns>
        private double GetMajorTickValue( Axis axis, double firstMajorTickValue, int tickItem )
        {
            if ( axis is null )
            {
                throw new ArgumentNullException( nameof( axis ) );
            }

            switch ( axis.CoordinateScale.CoordinateScaleType )
            {
                case CoordinateScaleType.Date:
                    {

                        // date scale
                        return JulianDate.Add( new JulianDate( firstMajorTickValue ), tickItem * this._Spacing, this.TimeIntervalType ).JulianDay;
                    }

                case CoordinateScaleType.Linear:
                case CoordinateScaleType.StripChart:
                    {

                        // regular linear scale
                        return firstMajorTickValue + this._Spacing * tickItem;
                    }

                case CoordinateScaleType.Log:
                    {

                        // log scale
                        return firstMajorTickValue * Math.Pow( 10.0d, tickItem );
                    }

                case CoordinateScaleType.Text:
                    {
                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, $"unhandled coordinate scale type {axis.CoordinateScale.CoordinateScaleType}" );
                        break;
                    }
            }

            return default;
        }

        /// <summary>
        /// Calculate minor tick spacing for a <see cref="CoordinateScaleType.Date"/> scale. This method
        /// is used by <see cref="Axis.Rescale"/>.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="axis">               Reference to the <see cref="Axis"/> </param>
        /// <param name="targetSpacingCount"> The desired "typical" number of tick spacing to divide the
        /// range into. </param>
        /// <returns>
        /// The calculated spacing for the specified data range.  Also calculates and sets the values for
        /// <see cref="TimeIntervalType"/>, and <see cref="ScaleFormat"/>
        /// </returns>
        internal double GetMinorDateSpacing( Axis axis, int targetSpacingCount )
        {
            if ( axis is null )
            {
                throw new ArgumentNullException( nameof( axis ) );
            }

            // Calculate an initial guess at spacing
            double majorspacing = axis.MajorTick.Spacing;
            double unused = majorspacing / targetSpacingCount;
            double range = axis.Range;
            double tempSpacing;
            if ( range > TickDefaults.Get().RangeYearYear )
            {
                this.ScaleFormat = "&yyyy";
                this.TimeIntervalType = TimeIntervalType.Year;
                tempSpacing = majorspacing == 1.0d ? 0.25d : GetSpacing( majorspacing, targetSpacingCount );
            }
            else if ( range > TickDefaults.Get().RangeYearMonth )
            {
                this.ScaleFormat = "&mmm-&yy";
                this.TimeIntervalType = TimeIntervalType.Month;

                // Calculate the minor spacing to give an estimated 4 spacing per major spacing.
                tempSpacing = SelectMonthSpacing( Math.Ceiling( range / (targetSpacingCount * 3) / 30.0d ) );
            }
            else if ( range > TickDefaults.Get().RangeMonthMonth )
            {
                this.ScaleFormat = "&mmm-&yy";
                this.TimeIntervalType = TimeIntervalType.Month;
                tempSpacing = 0.25d;
            }
            else if ( range > TickDefaults.Get().RangeDayDay )
            {
                this.ScaleFormat = "&d-&mmm";
                this.TimeIntervalType = TimeIntervalType.Day;
                tempSpacing = 1.0d;
            }
            else if ( range > TickDefaults.Get().RangeDayHour )
            {
                this.ScaleFormat = "&d-&mmm &hh:&nn";
                this.TimeIntervalType = TimeIntervalType.Hour;
                // Calculate the minor spacing to give an estimated 4 spacing per major spacing.
                tempSpacing = SelectHourSpacing( Math.Ceiling( range / (targetSpacingCount * 3) * JulianDate.HoursPerDay ) );
                // limit to 12 hours
                tempSpacing = Math.Min( tempSpacing, 12d );
            }
            else if ( range > TickDefaults.Get().RangeHourHour )
            {
                this.ScaleFormat = "&hh:&nn";
                this.TimeIntervalType = TimeIntervalType.Hour;
                tempSpacing = 0.25d;
            }
            else if ( range > TickDefaults.Get().RangeHourMinute )
            {
                this.ScaleFormat = "&hh:&nn";
                this.TimeIntervalType = TimeIntervalType.Minute;
                // Calculate the minor spacing to give an estimated 4 spacing per major spacing.
                tempSpacing = SelectMinuteSpacing( Math.Ceiling( range / (targetSpacingCount * 3) * JulianDate.MinutesPerDay ) );
            }
            else if ( range > TickDefaults.Get().RangeMinuteMinute )
            {
                this.ScaleFormat = "&hh:&nn";
                this.TimeIntervalType = TimeIntervalType.Minute;
                tempSpacing = 0.25d;
            }
            else if ( range > TickDefaults.Get().RangeMinuteSecond )
            {
                this.ScaleFormat = "&nn:&ss";
                this.TimeIntervalType = TimeIntervalType.Second;

                // Calculate the minor spacing to give an estimated 4 spacing per major spacing.
                tempSpacing = SelectSecondSpacing( Math.Ceiling( range / (targetSpacingCount * 3) * JulianDate.SecondsPerDay ) );
            }
            else
            {
                // SecondSecond

                this.ScaleFormat = "&nn:&ss";
                this.TimeIntervalType = TimeIntervalType.Second;
                tempSpacing = 0.25d;
            }

            return tempSpacing;
        }

        /// <summary>
        /// Determine the value for any minor tick.  Properly accounts for
        /// <see cref="CoordinateScale.IsLog"/>, <see cref="CoordinateScale.IsText"/>, and other axis
        /// format settings.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="axis">                Reference to the <see cref="Axis"/> </param>
        /// <param name="firstMajorTickValue"> The value of the first major tick.  This tick value is
        /// the base reference for all tick marks (including minor
        /// ones). </param>
        /// <param name="tickItem">            The major tick number (0 = first major tick).  For log
        /// scales, this is the actual power of 10. </param>
        /// <returns> The specified minor tick value. </returns>
        private double GetMinorTickValue( Axis axis, double firstMajorTickValue, int tickItem )
        {
            if ( axis is null )
            {
                throw new ArgumentNullException( nameof( axis ) );
            }

            switch ( axis.CoordinateScale.CoordinateScaleType )
            {
                case CoordinateScaleType.Date:
                    {

                        // date scale
                        return JulianDate.Add( new JulianDate( firstMajorTickValue ), tickItem * this._Spacing, this.TimeIntervalType ).JulianDay;
                    }

                case CoordinateScaleType.Linear:
                case CoordinateScaleType.StripChart:
                    {

                        // regular linear scale
                        return firstMajorTickValue + this._Spacing * tickItem;
                    }

                case CoordinateScaleType.Log:
                    {

                        // log scale
                        return firstMajorTickValue * Math.Pow( 10.0d, tickItem / 9d ) * (tickItem % 9 + 1);
                    }

                case CoordinateScaleType.Text:
                    {
                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, $"unhandled coordinate scale type {axis.CoordinateScale.CoordinateScaleType}" );
                        break;
                    }
            }

            return default;
        }

        /// <summary>
        /// Calculates spacing based on a data range.  This method tries to use the target spacing count
        /// of tick spacing while using a rational increment (1, 2, or 5 -- which are even divisors of
        /// 10).  This method is used by <see cref="Axis.Rescale"/>.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="range">              The range of data in user scale units.  This can be a full
        /// range of the data for the major spacing, or just the
        /// value of the major spacing to calculate the minor
        /// spacing. </param>
        /// <param name="targetSpacingCount"> The desired "typical" number of tick spacing to divide the
        /// range into. </param>
        /// <returns> The calculated spacing for the specified data range. </returns>
        private static double GetSpacing( double range, double targetSpacingCount )
        {

            // Calculate an initial guess at the spacing
            double tempSpacing = range / targetSpacingCount;

            // get the magnitude of the spacing
            double mag = Math.Floor( Math.Log10( tempSpacing ) );
            double magPow = Math.Pow( 10.0d, mag );

            // Get the standard spacing as close as possible to 1, 2, 5, or 10
            double magMsd = SelectStandardSpacing( tempSpacing / magPow );
            return magMsd * magPow;
        }

        /// <summary> Calculate the scaled tick length for this <see cref="Tick"/> </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="scaleFactor"> The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <returns> A <see cref="System.Single">Single</see> in pixels. </returns>
        internal float GetScaledLength( double scaleFactor )
        {
            return Convert.ToSingle( this.Length * scaleFactor + 0.5d );
        }

        /// <summary> The locations. </summary>
        private float[] _Locations;

        /// <summary>
        /// Returns the location of the tick item as calculated by <see cref="M:SetLocations"/>
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="tickItem"> The serial tick item for which to return the location. </param>
        /// <returns> The location. </returns>
        internal float GetLocation( int tickItem )
        {
            return this._Locations[tickItem];
        }

        /// <summary> The values. </summary>
        private double[] _Values;

        /// <summary>
        /// Returns the value of the tick item as calculated by <see cref="M:SetValue"/>
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="tickItem"> The serial tick item for which to return the value. </param>
        /// <returns> The value. </returns>
        internal double GetValue( int tickItem )
        {
            return this._Values[tickItem];
        }

        /// <summary> Set time series tick locations. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="axis"> Reference to the <see cref="Axis"/> </param>
        private void SetStripChartLocations( Axis axis )
        {
            if ( axis is null )
            {
                throw new ArgumentNullException( nameof( axis ) );
            }

            // set the tick spacing
            if ( this.IsMajor )
            {
                this.SetSpacing( axis );
                // axis.MinorTick.SetTickSpacing(axis)
                // call a second time to get it right.
                // _spacing = axis.MinorTick.Spacing * Tick.MinorTickDefaults.TimeSeriesSpacingCount()

                // allocate space
                this._Locations = new float[this.TickCount];
                int tickSpacing = Convert.ToInt32( this._Spacing );
                if ( this.TickOrigin < 0 )
                {
                    this.TickOrigin = tickSpacing;
                }

                int location = this.TickOrigin;
                this.ValidTickCount = 0;
                for ( int i = 0, loopTo = this.TickCount - 1; i <= loopTo; i++ )
                {

                    // set tick location value
                    this._Locations[i] = location;

                    // limit tick to screen scale range plus minus half a pixel.
                    if ( location < axis.ScreenScaleRange.Span + 0.5d )
                    {
                        this.ValidTickCount = i + 1;
                    }

                    // get next tick location
                    location += tickSpacing;
                }
            }
            else if ( this.Visible )
            {

                // set the minor tick spacing
                this.SetSpacing( axis );
                int tickSpacing = Convert.ToInt32( this._Spacing );
                this._Locations = new float[(axis.MajorTick.TickCount - 1) * (this.TickCount - 1) + 1 + 1];
                if ( this.TickOrigin < 0 )
                {
                    this.TickOrigin = tickSpacing;
                }

                int tickLocation = this.TickOrigin;
                for ( int i = 0, loopTo1 = this._Locations.Length - 1; i <= loopTo1; i++ )
                {
                    this._Locations[i] = tickLocation;
                    tickLocation += tickSpacing;
                }
            }
        }

        /// <summary> Set non-time series tick locations. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="axis"> Reference to the <see cref="Axis"/> </param>
        private void SetOtherLocations( Axis axis )
        {
            if ( axis is null )
            {
                throw new ArgumentNullException( nameof( axis ) );
            }

            if ( this.IsMajor )
            {

                // allocate location space
                this._Locations = new float[this.TickCount];

                // loop for each major tick
                for ( int i = 0, loopTo = this.TickCount - 1; i <= loopTo; i++ )

                    // convert the value to a pixel position
                    this._Locations[i] = axis.LocalTransform( this._Values[i] );
            }
            else
            {

                // clear the array
                this._Locations = Array.Empty<float>();
                if ( this.Visible )
                {
                    double firstMajorTickvalue = axis.MajorTick.TickCount > 0 ? axis.MajorTick.GetValue( 0 ) : 0d;
                    double tMajor = axis.MajorTick.Spacing;
                    double tMinor = this._Spacing;
                    if ( axis.CoordinateScale.IsDate )
                    {
                        tMajor *= JulianDate.GetDaysPerTimeInterval( axis.MajorTick.TimeIntervalType );
                        tMinor *= JulianDate.GetDaysPerTimeInterval( this.TimeIntervalType );
                    }

                    if ( axis.CoordinateScale.IsLog || tMinor < tMajor )
                    {

                        // Minor tick marks start at the minimum value and spacing all the way through
                        // the full scale.  This means that if the minor spacing is not
                        // an even division of the major spacing size, the minor tick marks won't
                        // line up with all of the scale labels and major ticks.
                        int tickItem = GetFirstMinorTickOrdinal( axis, firstMajorTickvalue );
                        double tickValue = this.GetMinorTickValue( axis, firstMajorTickvalue, tickItem );

                        // update tick count
                        this.TickCount = this.GetTickCount( axis );

                        // allocate huge space for ticks
                        this._Locations = new float[this.TickCount];

                        // Draw the minor tick marks
                        double tolerance = this._Spacing / 2d;
                        this.ValidTickCount = 0;
                        while ( axis.UserScaleRange.Contains( tickValue, tolerance ) && tickItem < this.TickCount )
                        {
                            if ( tickItem >= 0 )
                            {
                                this._Locations[this.ValidTickCount] = axis.LocalTransform( tickValue );
                                this.ValidTickCount += 1;
                            }

                            tickItem += 1;
                            tickValue = this.GetMinorTickValue( axis, firstMajorTickvalue, tickItem );
                        }

                        // reshape the array to hold only the set values.
                        // ReDim preserve Me._locations(Me._tickCount - 1)

                    }
                }
            }
        }

        /// <summary> Set tick locations. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="axis"> Reference to the <see cref="Axis"/> </param>
        internal void SetLocations( Axis axis )
        {
            if ( axis is null )
            {
                throw new ArgumentNullException( nameof( axis ) );
            }

            if ( axis.CoordinateScale.CoordinateScaleType == CoordinateScaleType.StripChart )
            {
                this.SetStripChartLocations( axis );
            }
            else
            {
                this.SetOtherLocations( axis );
            }
        }

        /// <summary> Set the tick values for the axis and time series data (if relevant) </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="axis"> Reference to the <see cref="Axis"/> </param>
        internal void SetValues( Axis axis )
        {
            if ( axis is null )
            {
                throw new ArgumentNullException( nameof( axis ) );
            }

            // update tick counts
            this.TickCount = this.GetTickCount( axis );
            if ( this.TickCount <= 0 )
            {
                // if zero, return after setting an empty array.
                this._Values = Array.Empty<double>();
                return;
            }
            else
            {
                this._Values = new double[this.TickCount];
            }

            if ( axis.CoordinateScale.CoordinateScaleType != CoordinateScaleType.StripChart )
            {
                if ( this.IsMajor )
                {
                    double tickValue;

                    // the range comparison is necessary because due to real value comparisons,
                    // the first and/or last tick values are out of range.
                    double tolerance = this._Spacing / 100d;

                    // get the first major tick value
                    double firstTickValue = this.GetFirstMajorTickValue( axis );

                    // allocate array space
                    this._Values = new double[this.TickCount];

                    // loop for each major tick
                    int i = 0;
                    do
                    {
                        tickValue = this.GetMajorTickValue( axis, firstTickValue, i );
                        if ( axis.UserScaleRange.Contains( tickValue, tolerance ) )
                        {
                            this._Values[i] = tickValue;
                            this.ValidTickCount = i + 1;
                        }

                        i += 1;
                    }
                    while ( i != this.TickCount );
                }
                else
                {

                    // minor ticks need no values

                }
            }
        }

        /// <summary> Set the tick values for the axis and time series data (if relevant) </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="axis">              Reference to the <see cref="Axis"/> </param>
        /// <param name="timeSeriesPointer"> Pointer to the current point in the time series. </param>
        /// <param name="timeSeries">        The <see cref="isr.Visuals.TimeSeriesPointR">time
        /// series</see> data point. </param>
        internal void SetValues( Axis axis, int timeSeriesPointer, TimeSeriesPointR[] timeSeries )
        {
            if ( axis is null )
                throw new ArgumentNullException( nameof( axis ) );

            // update tick counts
            this.TickCount = this.GetTickCount( axis );
            if ( this.TickCount <= 0 )
            {
                // if zero, return after setting an empty array.
                this._Values = Array.Empty<double>();
                return;
            }
            else
            {
                this._Values = new double[this.TickCount];
            }

            if ( axis.CoordinateScale.CoordinateScaleType == CoordinateScaleType.StripChart )
            {
                if ( this.IsMajor )
                {
                    int timeSeriesLength = timeSeries.Length;

                    // set the time series tick values
                    for ( int i = 0, loopTo = this.TickCount - 1; i <= loopTo; i++ )
                    {

                        // set the location of the time axis division
                        int k = timeSeriesPointer - Convert.ToInt32( axis.ScreenScaleRange.Span - this._Locations[i] );
                        if ( k < 0 )
                        {
                            k += timeSeriesLength;
                        }

                        if ( k >= timeSeries.Length )
                        {
                            // when rescaling we may get into a situation were k is out of bounds, in which case
                            // a temporary shift in time may occur.
                            k = timeSeriesPointer;
                        }

                        this._Values[i] = timeSeries[k].Seconds;
                    }
                }
                else
                {
                    // minor ticks do not need values
                }
            }
            else
            {
                this.SetValues( axis );
            }
        }

        /// <summary>
        /// Increments the screen position for drawing time series tick marks and grid lines.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        internal void IncrementOrigin()
        {
            this.TickOrigin -= 1;
        }

        /// <summary> Set the major tick spacing. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="axis"> Specifies reference to the axis. </param>
        internal void SetSpacing( Axis axis )
        {
            if ( axis is null )
            {
                throw new ArgumentNullException( nameof( axis ) );
            }

            switch ( axis.CoordinateScale.CoordinateScaleType )
            {
                case CoordinateScaleType.Date:
                    {

                        // Date Scale.  Calculate the tick spacing
                        if ( this.AutoSpacing )
                        {
                            this._Spacing = this.IsMajor ? this.GetMajorDateSpacing( axis, MajorTickDefaults.Get().TargetSpacingCount ) : this.GetMinorDateSpacing( axis, MinorTickDefaults.Get().TargetSpacingCount );
                        }

                        break;
                    }

                case CoordinateScaleType.StripChart:
                    {
                        if ( this.IsMajor )
                        {
                            if ( axis.ScreenScaleRange is null )
                            {
                                // Me._spacing = MajorTickDefaults.Spacing ' .2101
                                this._Spacing = axis.MinorTick.Spacing * MinorTickDefaults.Get().TimeSeriesSpacingCount;
                            }
                            else
                            {
                                this._Spacing = Math.Round( axis.ScreenScaleRange.Span / MajorTickDefaults.Get().TimeSeriesSpacingCount );
                            }
                        }
                        // TO_DO: call again with UseWhole
                        // If axis.MinorTick.Spacing > 0 Then
                        // Me._spacing = axis.MinorTick.Spacing * Tick.MinorTickDefaults.TimeSeriesSpacingCount()
                        // End If
                        else
                        {
                            this._Spacing = Math.Round( axis.MajorTick.Spacing / MinorTickDefaults.Get().TimeSeriesSpacingCount );
                        }

                        break;
                    }

                case CoordinateScaleType.Linear:
                    {

                        // linear scale:  Calculate tick spacing
                        double range = axis.Range;
                        if ( this.AutoSpacing )
                        {
                            this._Spacing = this.IsMajor ? GetSpacing( range, MajorTickDefaults.Get().TargetSpacingCount ) : GetSpacing( axis.MajorTick.Spacing, MinorTickDefaults.Get().TargetSpacingCount );
                        }

                        break;
                    }

                case CoordinateScaleType.Log:
                    {

                        // Log Scale spacing is ignored.  set for the validity check.
                        this._Spacing = 1.0d;
                        break;
                    }

                case CoordinateScaleType.Text:
                    {

                        // if this is a text-based axis, then set tick spacing to one
                        this._Spacing = 1.0d;
                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "unhandled coordinate scale" );
                        break;
                    }
            }

            // get he tick count
            this.TickCount = this.GetTickCount( axis );
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Returns the decimal places represented by the tick spacing. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="axis"> Reference to the <see cref="Axis"/> </param>
        /// <returns> An Integer. </returns>
        internal int DecimalPlaces( Axis axis )
        {
            return Math.Max( axis.DecimalPlaces, this._Spacing.DecimalPlaces() );
        }

        /// <summary>
        /// Determines whether or not the scale spacing <see cref="Spacing"/>
        /// is set automatically.  This value will be set to false if <see cref="Spacing"/>
        /// is manually changed.
        /// </summary>
        /// <value> True for automatic mode or False for manual mode. </value>
        public bool AutoSpacing { get; set; }

        /// <summary>
        /// Determines whether or not the tick marks are shown on the inside bottom axis frame.
        /// </summary>
        /// <value> True to show the inside bottom tick marks, false otherwise. </value>
        public bool IsBottom { get; set; }

        /// <summary> Determines if the <see cref="Tick"/> is major or minor tick mark. </summary>
        /// <value> A <see cref="System.Boolean">Boolean</see> </value>
        public bool IsMajor { get; set; }

        /// <summary>
        /// Determines if the <see cref="Tick"/> lines at each labeled value will be drawing.
        /// </summary>
        /// <value> A <see cref="System.Boolean">Boolean</see> </value>
        public bool Visible { get; set; }

        /// <summary>
        /// Determines whether or not outside tick marks are shown.  These are tick marks on the outside
        /// of the <see cref="Axis"/> frame. The tick spacing is controlled by <see cref="Spacing"/>.
        /// </summary>
        /// <value> True to show outside tick marks, false otherwise. </value>
        public bool IsOutside { get; set; }

        /// <summary>
        /// Determines whether or not inside tick marks are shown.  These are tick marks on the inside of
        /// the <see cref="Axis"/> frame. The tick spacing is controlled by <see cref="Spacing"/>.
        /// </summary>
        /// <value> True to show inside tick marks, false otherwise. </value>
        public bool IsInside { get; set; }

        /// <summary>
        /// Determines whether or not the tick marks are shown on the inside top axis frame.
        /// </summary>
        /// <value> True to show the inside top tick marks, false otherwise. </value>
        public bool IsTop { get; set; }

        /// <summary>
        /// The length of the <see cref="Axis"/> tick marks.  This length will be scaled by the
        /// <see cref="Pane.GetScaleFactor"/> of the chart.
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float Length { get; set; }

        /// <summary> The color to use for drawing this <see cref="Tick"/>. </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> value. </value>
        public Color LineColor { get; set; }

        /// <summary>
        /// The pen width to be used when drawing tick marks for this <see cref="Axis"/>
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float LineWidth { get; set; }

        /// <summary>
        /// The format of the <see cref="Tick"/> labels. This field is only used if the
        /// <see cref="Axis.CoordinateScale"/> is set to <see cref="CoordinateScaleType.Date"/>.
        /// </summary>
        /// <value>
        /// A <see cref="System.String"/> as defined for the <see cref="JulianDate.ToString"/> function.
        /// </value>
        public string ScaleFormat { get; set; }

        /// <summary> The spacing. </summary>
        private double _Spacing;

        /// <summary>
        /// The scale spacing is the increment between labeled axis values. This value can be set
        /// automatically based on the state of <see cref="AutoSpacing"/>.
        /// If this value is set manually, then <see cref="AutoSpacing"/> will also be set to false.
        /// This value is ignored for <see cref="CoordinateScaleType.Log"/> and
        /// <see cref="CoordinateScaleType.Text"/> axes.  For <see cref="CoordinateScaleType.Date"/> axes,
        /// this
        /// value is defined by the axis <see cref="Tick.TimeIntervalType"/>.
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in user scale units. </value>
        public double Spacing
        {
            get => this._Spacing;

            set {
                this._Spacing = value;
                this.AutoSpacing = false;
            }
        }

        /// <summary> Gets the status message. </summary>
        /// <value> A System.String value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusMessage { get; set; }

        /// <summary> Returns the tick count. </summary>
        /// <value> The number of ticks. </value>
        public int TickCount { get; private set; }

        /// <summary> Gets the first position of the time series tick. </summary>
        /// <value> The tick origin. </value>
        public int TickOrigin { get; set; }

        /// <summary>
        /// The type of time intervals used for the major spacing (<see cref="Spacing"/>). Only applies
        /// to Date-Time axes (<see cref="CoordinateScaleType.Date"/> = true). The axis is set to date
        /// type with the <see cref="Type"/> property.
        /// </summary>
        /// <value> Enumeration type <see cref="TimeIntervalType"/> </value>
        public TimeIntervalType TimeIntervalType { get; set; }

        /// <summary> Returns the valid tick count. </summary>
        /// <value> The number of valid ticks. </value>
        public int ValidTickCount { get; private set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="Tick"/> class that defines the default property values
    /// for the <see cref="Tick"/> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class TickDefaults
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private TickDefaults() : base()
        {
            this.RangeYearYear = 5d * JulianDate.GetDaysPerTimeInterval( TimeIntervalType.Year );  // = 1825  ' 5 years
            this.RangeYearMonth = JulianDate.GetDaysPerTimeInterval( TimeIntervalType.Year );  // = 365 ' 1 year
            this.RangeMonthMonth = 3d * JulianDate.GetDaysPerTimeInterval( TimeIntervalType.Month ); // 90 ' 3 months
            this.RangeDayDay = 10d; // 10 days
            this.RangeDayHour = 3d; // 3 days
            this.RangeHourHour = 10d * JulianDate.GetDaysPerTimeInterval( TimeIntervalType.Hour ); // 0.4167 ' 10 hours
            this.RangeHourMinute = 3d * JulianDate.GetDaysPerTimeInterval( TimeIntervalType.Hour ); // 0.125 ' 3 hours
            this.RangeMinuteMinute = 10d * JulianDate.GetDaysPerTimeInterval( TimeIntervalType.Minute );  // 0.00694 ' 10 Minutes
            this.RangeMinuteSecond = 3d * JulianDate.GetDaysPerTimeInterval( TimeIntervalType.Minute ); // 0.002083 ' 3 Minutes
            this.ScaleFormat = "&dd-&mmm-&yy &hh:&nn";
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static TickDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static TickDefaults Get()
        {
            if ( _Instance is null )
            {
                lock ( SyncLocker )
                    _Instance = new TickDefaults();
            }

            return _Instance;
        }

        #endregion

        /// <summary>
        /// A default setting for the <see cref="CoordinateScaleType.Date"/>
        /// auto-ranging code in days. If the data span exceeds this value, a selection of major
        /// <see cref="Tick.TimeIntervalType"/> = <see cref="Tick.TimeIntervalType"/>
        /// and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Year"/>
        /// will be made.  The default value is 5 years, i.e., 1825 days.
        /// </summary>
        /// <value> The range year. </value>
        public double RangeYearYear { get; set; }

        /// <summary>
        /// A default setting for the <see cref="CoordinateScaleType.Date"/>
        /// auto-ranging code in days. If the data span exceeds this value, a selection of major
        /// <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Year"/>
        /// and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Month"/>
        /// will be made.  The default value is 1 year, i.e., 365 days.
        /// </summary>
        /// <value> The range year month. </value>
        public double RangeYearMonth { get; set; }

        /// <summary>
        /// A default setting for the <see cref="CoordinateScaleType.Date"/>
        /// auto-ranging code in days. If the data span exceeds this value, a selection of major
        /// <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Month"/>
        /// and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Month"/>
        /// will be made.  The default value is 3 months, i.e., 90 days.
        /// </summary>
        /// <value> The range month. </value>
        public double RangeMonthMonth { get; set; }

        /// <summary>
        /// A default setting for the <see cref="CoordinateScaleType.Date"/>
        /// auto-ranging code in days. If the data span exceeds this value, a selection of major
        /// <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Day"/>
        /// and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Day"/>
        /// will be made.  The default value is 10 days.
        /// </summary>
        /// <value> The range day. </value>
        public double RangeDayDay { get; set; }

        /// <summary>
        /// A default setting for the <see cref="CoordinateScaleType.Date"/>
        /// auto-ranging code in days. If the data span exceeds this value, a selection of major
        /// <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Day"/>
        /// and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Hour"/>
        /// will be made.  The default value is 3 days.
        /// </summary>
        /// <value> The range day hour. </value>
        public double RangeDayHour { get; set; }

        /// <summary>
        /// A default setting for the <see cref="CoordinateScaleType.Date"/>
        /// auto-ranging code in days. If the data span exceeds this value, a selection of major
        /// <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Hour"/>
        /// and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Hour"/>
        /// will be made.  The default value is 10 hours. i.e., 0.4167 days.
        /// </summary>
        /// <value> The range hour. </value>
        public double RangeHourHour { get; set; }

        /// <summary>
        /// A default setting for the <see cref="CoordinateScaleType.Date"/>
        /// auto-ranging code in days. If the data span exceeds this value, a selection of major
        /// <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Hour"/>
        /// and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Minute"/>
        /// will be made.  The default value is 3 hours, i.e., 0.125 days.
        /// </summary>
        /// <value> The range hour minute. </value>
        public double RangeHourMinute { get; set; }

        /// <summary>
        /// A default setting for the <see cref="CoordinateScaleType.Date"/>
        /// auto-ranging code in days. If the data span exceeds this value, a selection of major
        /// <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Minute"/>
        /// and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Minute"/>
        /// will be made.  The default value is 10 minutes, i.e., 0.00694 days.
        /// </summary>
        /// <value> The range minute. </value>
        public double RangeMinuteMinute { get; set; }

        /// <summary>
        /// A default setting for the <see cref="CoordinateScaleType.Date"/>
        /// auto-ranging code in days. If the data span exceeds this value, a selection of major
        /// <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Minute"/>
        /// and minor <see cref="Tick.TimeIntervalType"/> = <see cref="TimeIntervalType.Second"/>
        /// will be made.  The default value is 3 minutes, i.e., 0.002083 days.
        /// </summary>
        /// <value> The range minute second. </value>
        public double RangeMinuteSecond { get; set; }

        /// <summary>
        /// Gets or sets the default setting for the <see cref="Axis"/> scale date format string
        /// (<see cref="Tick.ScaleFormat"/> property).  This value is set as per the
        /// <see cref="JulianDate.ToString"/> function.
        /// </summary>
        /// <value> The scale format. </value>
        public string ScaleFormat { get; set; }
    }

    /// <summary>
    /// A simple subclass of the <see cref="Tick"/> class that defines the default property values
    /// for the major tick instance of the
    /// <see cref="Tick"/> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class MajorTickDefaults
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private MajorTickDefaults() : base()
        {
            this.AutoSpacing = true;
            this.IsBottom = true;
            this.Visible = true;
            this.IsOutside = true;
            this.IsInside = true;
            this.IsTop = true;
            this.Length = 5f;
            this.LineWidth = 1.0f;
            this.LineColor = Color.Black;
            this.Spacing = 0.1f;
            this.TargetSpacingCount = 7;
            this.TimeIntervalType = TimeIntervalType.Year;
            this.TimeSeriesSpacingCount = 10;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static MajorTickDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static MajorTickDefaults Get()
        {
            if ( _Instance is null )
            {
                lock ( SyncLocker )
                    _Instance = new MajorTickDefaults();
            }

            return _Instance;
        }

        #endregion

        /// <summary>
        /// Gets or sets the default auto spacing mode for the major <see cref="Tick"/> lines.
        /// </summary>
        /// <value> The automatic spacing. </value>
        public bool AutoSpacing { get; set; }

        /// <summary>
        /// The display mode for the bottom frame major <see cref="Tick"/> marks
        /// (<see cref="Tick.IsTop"/> property).
        /// </summary>
        /// <value>
        /// True to show the tick marks inside the axis on the top frame side, False otherwise.
        /// </value>
        public bool IsBottom { get; set; }

        /// <summary>
        /// Gets or sets the default display mode for the major <see cref="Tick"/> lines
        /// (<see cref="Tick.Visible"/> property). True to show the major tick lines, false to hide them.
        /// </summary>
        /// <value> The visible. </value>
        public bool Visible { get; set; }

        /// <summary>
        /// The display mode for the major outside <see cref="Tick"/> marks (<see cref="Tick.IsOutside"/>
        /// property).
        /// </summary>
        /// <value> True to show the major tick marks (outside the axis), false otherwise. </value>
        public bool IsOutside { get; set; }

        /// <summary>
        /// The display mode for the major inside <see cref="Tick"/> marks (<see cref="Tick.IsInside"/>
        /// property).
        /// </summary>
        /// <value> True to show the major tick marks (inside the axis), false otherwise. </value>
        public bool IsInside { get; set; }

        /// <summary>
        /// The display mode for the major top frame <see cref="Tick"/> marks (<see cref="Tick.IsTop"/>
        /// property).
        /// </summary>
        /// <value>
        /// True to show the tick marks inside the axis on the top frame side, False otherwise.
        /// </value>
        public bool IsTop { get; set; }

        /// <summary>
        /// Gets or sets the default length of the major <see cref="Tick"/> marks.
        /// (<see cref="Tick.Length"/> property).
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float Length { get; set; }

        /// <summary> Gets or sets the pen width for drawing the major <see cref="Tick"/> marks. </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float LineWidth { get; set; }

        /// <summary>
        /// Gets or sets the default color for drawing the major <see cref="Tick"/> line.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color LineColor { get; set; }

        /// <summary>
        /// Gets or sets the default spacing of the major <see cref="Tick"/> marks.
        /// (<see cref="Tick.Spacing"/> property).
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in user-defined coordinates. </value>
        public float Spacing { get; set; }

        /// <summary>
        /// Gets or sets the default target number of tick spacing for automatically selecting the axis
        /// scale spacing (see <see cref="Axis.Rescale"/>). This number is an initial target value for
        /// the number of major spacing on an axis.
        /// </summary>
        /// <value> The number of target spacings. </value>
        public int TargetSpacingCount { get; set; }

        /// <summary> Gets or sets the default time interval for date scale. </summary>
        /// <value> The type of the time interval. </value>
        public TimeIntervalType TimeIntervalType { get; set; }

        /// <summary> Gets or sets the default time series spacing. </summary>
        /// <value> The number of time series spacings. </value>
        public int TimeSeriesSpacingCount { get; set; }
    }

    /// <summary>
    /// A simple subclass of the <see cref="Tick"/> class that defines the default property values
    /// for the major tick instance of the
    /// <see cref="Tick"/> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class MinorTickDefaults
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private MinorTickDefaults() : base()
        {
            this.AutoSpacing = true;
            this.IsBottom = true;
            this.Visible = true;
            this.IsOutside = true;
            this.IsInside = true;
            this.IsTop = true;
            this.Length = 3f;
            this.LineColor = Color.Black;
            this.LineWidth = 1.0f;
            this.Spacing = 0.1f;
            this.TargetSpacingCount = 5;
            this.TimeIntervalType = TimeIntervalType.Year;
            this.TimeSeriesSpacingCount = 5;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static MinorTickDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static MinorTickDefaults Get()
        {
            if ( _Instance is null )
            {
                lock ( SyncLocker )
                    _Instance = new MinorTickDefaults();
            }

            return _Instance;
        }

        #endregion

        /// <summary>
        /// Gets or sets the default auto spacing mode for the minor <see cref="Tick"/> lines.
        /// </summary>
        /// <value> The automatic spacing. </value>
        public bool AutoSpacing { get; set; }

        /// <summary>
        /// The display mode for the bottom frame minor <see cref="Tick"/> marks
        /// (<see cref="Tick.IsTop"/> property).
        /// </summary>
        /// <value>
        /// True to show the tick marks inside the axis on the top frame side, False otherwise.
        /// </value>
        public bool IsBottom { get; set; }

        /// <summary>
        /// Gets or sets the default display mode for the minor <see cref="Tick"/> lines
        /// (<see cref="Tick.Visible"/> property). True to show the minor tick lines, false to hide them.
        /// </summary>
        /// <value> The visible. </value>
        public bool Visible { get; set; }

        /// <summary>
        /// The display mode for the minor outside <see cref="Tick"/> marks (<see cref="Tick.IsOutside"/>
        /// property).
        /// </summary>
        /// <value> True to show the minor tick marks (outside the axis), false otherwise. </value>
        public bool IsOutside { get; set; }

        /// <summary>
        /// The display mode for the minor inside <see cref="Tick"/> marks (<see cref="Tick.IsInside"/>
        /// property).
        /// </summary>
        /// <value> True to show the minor tick marks (inside the axis), false otherwise. </value>
        public bool IsInside { get; set; }

        /// <summary>
        /// The display mode for the minor top frame <see cref="Tick"/> marks (<see cref="Tick.IsTop"/>
        /// property).
        /// </summary>
        /// <value>
        /// True to show the minor tick marks inside the axis on the top frame side, false otherwise.
        /// </value>
        public bool IsTop { get; set; }

        /// <summary>
        /// Gets or sets the default length of the minor <see cref="Tick"/> marks.
        /// (<see cref="Tick.Length"/> property).
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float Length { get; set; }

        /// <summary>
        /// Gets or sets the default color for drawing the minor <see cref="Tick"/> marks.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color LineColor { get; set; }

        /// <summary>
        /// Gets or sets the default pen width for drawing the minor <see cref="Tick"/> marks.
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float LineWidth { get; set; }

        /// <summary>
        /// Gets or sets the default spacing of the minor <see cref="Tick"/> marks.
        /// (<see cref="Tick.Spacing"/> property).
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in user-defined coordinates. </value>
        public float Spacing { get; set; }

        /// <summary>
        /// Gets or sets the default target number of minor spacing for automatically selecting the axis
        /// scale minor spacing (see <see cref="Axis.Rescale"/>). This number is an initial target value
        /// for the number of minor spacing on an axis.
        /// </summary>
        /// <value> The number of target spacings. </value>
        public int TargetSpacingCount { get; set; }

        /// <summary> Gets or sets the default time interval for date scale. </summary>
        /// <value> The type of the time interval. </value>
        public TimeIntervalType TimeIntervalType { get; set; }

        /// <summary> Gets or sets the default time series spacing. </summary>
        /// <value> The number of time series spacings. </value>
        public int TimeSeriesSpacingCount { get; set; }
    }
}

#endregion

