using System;
using System.ComponentModel;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary>
    /// Handles the rendering of text with variety of alignment options, font settings, colors, frame
    /// and fill modes, and text angles.
    /// </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class TextAppearance : ICloneable, IDisposable
    {

        #region " SHARED "

        /// <summary> Returns a font style. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="bold">      <see cref="System.Boolean"/> <c>True</c> if bold. </param>
        /// <param name="italic">    <see cref="System.Boolean"/> <c>True</c> if italic. </param>
        /// <param name="underline"> <see cref="System.Boolean"/> <c>True</c> if underline. </param>
        /// <returns> A <see cref="System.Drawing.FontStyle"/> </returns>
        public static FontStyle GetFontStyle( bool bold, bool italic, bool underline )
        {
            var style = FontStyle.Regular;
            if ( bold )
            {
                style = FontStyle.Bold;
            }

            if ( italic )
            {
                style |= FontStyle.Italic;
            }

            if ( underline )
            {
                style |= FontStyle.Underline;
            }

            return style;
        }

        #endregion

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Construct <see cref="isr.Visuals.TextAppearance">TextAppearance</see> with given and default
        /// property values values as defined in the <see cref="TextAppearanceDefaults"/> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="font">  A <see cref="System.Drawing.Font">Font</see> </param>
        /// <param name="color"> The color with which to render the font. </param>

        // instantiate the base class
        public TextAppearance( Font font, Color color ) : base()
        {
            if ( font is null )
                throw new ArgumentNullException( nameof( font ) );
            this._BaseFont = new Font( font, font.Style );
            this._LastBaseFont = ( Font ) this._BaseFont.Clone();
            this.ScaledFont = ( Font ) this._BaseFont.Clone();
            this._ScaledFontSize = this._BaseFont.Size;
            // superfluous per Code Analysis: Me._angle = 0.0F
            this.FontColor = color;
            this.Frame = new Frame();
            this.ReconstructFont( 1.0d );
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The TextAppearance object from which to copy. </param>

        // instantiate the base class
        public TextAppearance( TextAppearance model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.FontColor = model.FontColor;
            this.Angle = model.Angle;
            this.Frame = model.Frame.Copy();
            this._BaseFont = ( Font ) model._BaseFont.Clone();
            this._LastBaseFont = ( Font ) model._LastBaseFont.Clone();
            this.ScaledFont = ( Font ) model.ScaledFont.Clone();
            this._ScaledFontSize = model._ScaledFontSize;
            this.ReconstructFont( 1.0d );
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        if ( this.ScaledFont is object )
                        {
                            this.ScaledFont.Dispose();
                            this.ScaledFont = null;
                        }

                        if ( this.Frame is object )
                        {
                            this.Frame.Dispose();
                            this.Frame = null;
                        }

                        if ( this._BaseFont is object )
                        {
                            this._BaseFont.Dispose();
                        }

                        if ( this._LastBaseFont is object )
                        {
                            this._LastBaseFont.Dispose();
                        }

                        if ( this.ScaledFont is object )
                        {
                            this.ScaledFont.Dispose();
                        }

                        this.FontColor = default;
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary>
        /// get a <see cref="System.Drawing.SizeF">Size</see> structure representing the width and height
        /// of the bounding box for the specified text string, based on the scaled font size. This method
        /// differs from <see cref="MeasureString"/> in that it takes into account the rotation angle of
        /// the font, and gives the dimensions of the bounding box that encloses the text at the
        /// specified angle.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="text">           The text string for which the width is to be calculated. </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <returns>
        /// A scaled <see cref="System.Drawing.SizeF">Size</see> of text dimensions, in pixels.
        /// </returns>
        public SizeF BoundingBox( Graphics graphicsDevice, string text, double scaleFactor )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            if ( string.IsNullOrWhiteSpace( text ) )
            {
                return new SizeF( 0.0f, 0.0f );
            }

            this.ReconstructFont( scaleFactor );
            var s = graphicsDevice.MeasureString( text, this.ScaledFont );
            float cs = Convert.ToSingle( Math.Abs( Math.Cos( this.Angle * Math.PI / 180.0d ) ) );
            float sn = Convert.ToSingle( Math.Abs( Math.Sin( this.Angle * Math.PI / 180.0d ) ) );
            var s2 = new SizeF( s.Width * cs + s.Height * sn, s.Width * sn + s.Height * cs );
            return s2;
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the TextAppearance. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns>
        /// A new, independent <see cref="isr.Visuals.TextAppearance">TextAppearance</see> copy.
        /// </returns>
        public TextAppearance Copy()
        {
            return new TextAppearance( this );
        }

        /// <summary>
        /// Renders the specified <paramref name="text"/> to the specified
        /// <see cref="Graphics"/> device.  The text, frame, and fill options
        /// will be rendered as required.
        /// </summary>
        /// <remarks>
        /// Employs the transform matrix in order to position the test at any angle while also allowing
        /// the location, or anchor point, to be at a user-specified alignment.  A caption can be located
        /// at a given point on the graph such that the left or center or right and top or middle or
        /// bottom can be the anchor point.  After thus shifting the origin so that the anchor point will
        /// be at the (x,y), the method rotates the coordinate system to accommodate the text angle.  It
        /// then makes a translation to account for the fact that the text rendering method expects to
        /// draw the test based on a top-center location while the user may have specified another
        /// alignment.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="text">          The text string for which the width is to be calculated. </param>
        /// <param name="x">              The X location to display the text, in screen coordinates,
        /// relative to the horizontal (<see cref="HorizontalAlignment"/>)
        /// alignment parameter <paramref name="alignH"/> </param>
        /// <param name="y">              The Y location to display the text, in screen coordinates,
        /// relative to the vertical (<see cref="VerticalAlignment"/>
        /// alignment parameter <paramref name="alignV"/> </param>
        /// <param name="alignH">         A <see cref="HorizontalAlignment"/> alignment value. </param>
        /// <param name="alignV">         A <see cref="VerticalAlignment"/> alignment value. </param>
        /// <param name="scaleFactor">    The scaling factor to be used for rendering objects.  This is
        /// calculated and passed down by the parent
        /// <see cref="Pane"/> object using the
        /// <see cref="Pane.GetScaleFactor"/>
        /// method, and is used to proportionally adjust font sizes, etc.
        /// according to the actual size of the graph. </param>
        public void Draw( Graphics graphicsDevice, string text, float x, float y, HorizontalAlignment alignH, VerticalAlignment alignV, double scaleFactor )
        {
            if ( string.IsNullOrWhiteSpace( text ) )
                return;

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // make sure the font size is properly scaled
            this.ReconstructFont( scaleFactor );

            // get the width and height of the text
            var sizeF = graphicsDevice.MeasureString( text, this.ScaledFont );

            // Save the old transform matrix for later restoration
            var matrix = graphicsDevice.Transform;

            // Move the coordinate system to local coordinates
            // of this text object (that is, at the specified
            // x,y location)
            graphicsDevice.TranslateTransform( x, y );

            // Since the text will be drawn by graphicsDevice.DrawString() assuming the location is
            // the top-center (the Font is aligned using StringFormat to the center so 
            // multi-line text is center justified), shift the coordinate system so that 
            // we are actually aligned per the caller specified position
            if ( alignH == HorizontalAlignment.Left )
            {
                x = sizeF.Width / 2.0f;
            }
            else if ( alignH == HorizontalAlignment.Right )
            {
                x = -sizeF.Width / 2.0f;
            }
            else
            {
                x = 0.0f;
            }

            if ( alignV == VerticalAlignment.Center )
            {
                y = -sizeF.Height / 2.0f;
            }
            else if ( alignV == VerticalAlignment.Bottom )
            {
                y = -sizeF.Height;
            }
            else
            {
                y = 0.0f;
            }

            // Rotate the coordinate system according to the specified angle
            if ( this.Angle != 0.0f )
            {
                graphicsDevice.RotateTransform( -this.Angle );
            }

            // Shift the coordinates to accommodate the alignment parameters
            graphicsDevice.TranslateTransform( x, y );

            // make a center justified StringFormat alignment for drawing the text
            using var strFormat = new StringFormat();
            strFormat.Alignment = StringAlignment.Center;
            // Create a rectangle representing the frame around the text.  Note that, 
            // while the text is drawn based on the top-center position, the rectangle 
            // is drawn based on the top-left position.  Therefore, move the rectangle
            // width/2 to the left to align it properly
            var rectF = new RectangleF( -sizeF.Width / 2.0f, 0.0f, sizeF.Width, sizeF.Height );

            // draw the frame
            this.Frame.Draw( graphicsDevice, rectF );
            using ( var sb = new SolidBrush( this.FontColor ) )
            {
                // Draw the actual text.  Note that the coordinate system is set up such that
                // 0,0 is at the location where the CenterTop of the text needs to be.
                graphicsDevice.DrawString( text, this.ScaledFont, sb, 0.0f, 0.0f, strFormat );
            }

            // Restore the transform matrix back to original
            graphicsDevice.Transform = matrix;
        }

        /// <summary>
        /// Scale and get the <see cref="System.Drawing.SizeF">Size</see> of the specified text string,
        /// based on the scaled font size.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="text">           The text string for which the width is to be calculated. </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <returns> Scaled <see cref="System.Drawing.SizeF">Size</see> dimensions in pixels,. </returns>
        public SizeF MeasureString( Graphics graphicsDevice, string text, double scaleFactor )
        {

            // validate argument.
            if ( graphicsDevice is null )
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            if ( string.IsNullOrWhiteSpace( text ) )
            {
                return new SizeF( 0.0f, 0.0f );
            }
            else
            {
                this.ReconstructFont( scaleFactor );
                return graphicsDevice.MeasureString( text, this.ScaledFont );
            }
        }

        /// <summary>
        /// get the <see cref="System.Drawing.SizeF">Size</see> of the specified text string, based on
        /// the scaled font size.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="[text]">         The text string for which the width is to be calculated. </param>
        /// <returns> Scaled <see cref="System.Drawing.SizeF">Size</see> dimensions in pixels,. </returns>
        public SizeF MeasureString( Graphics graphicsDevice, string text )
        {

            // validate argument.
            return graphicsDevice is null
                ? throw new ArgumentNullException( nameof( graphicsDevice ) )
                : string.IsNullOrWhiteSpace( text ) ? new SizeF( 0.0f, 0.0f ) : graphicsDevice.MeasureString( text, this.ScaledFont );
        }

        /// <summary> Scale and return the scaled <see cref="System.Drawing.Font">Font</see>. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="scaleFactor"> The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <returns>
        /// A reference to the scaled <see cref="System.Drawing.Font">Font</see> object with a size of
        /// <see cref="_ScaledFontSize"/>, and font <see cref="FontFamily"/>.
        /// </returns>
        public Font ScaleFont( double scaleFactor )
        {
            this.ReconstructFont( scaleFactor );
            return this.ScaledFont;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// The angle at which this <see cref="isr.Visuals.TextAppearance">TextAppearance</see> object is
        /// drawn. The angle is measured in anti-clockwise degrees from horizontal.
        /// Negative values are permitted.
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in radians. </value>
        public float Angle { get; set; }

        /// <summary>Gets or sets the base <see cref="System.Drawing.Font">Font</see> of the
        /// <see cref="isr.Visuals.TextAppearance">TextAppearance</see>, which serves to create the scaled font for
        /// rendering text.</summary>
        private Font _BaseFont;

        /// <summary>
        /// The color of the font characters for this
        /// <see cref="isr.Visuals.TextAppearance">TextAppearance</see>. Note that the frame and
        /// background colors are set using the <see cref="Frame.LineColor"/> and
        /// <see cref="Frame.FillColor"/> properties, respectively.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> value. </value>
        public Color FontColor { get; set; }

        /// <summary>
        /// The size of the font for this <see cref="isr.Visuals.TextAppearance">TextAppearance</see>
        /// object.
        /// </summary>
        /// <remarks> The new size will be scaled by the existing scale factor. </remarks>
        /// <value> A <see cref="System.Single">Single</see> in points (1/72 inch) </value>
        public float FontSize
        {
            get => this._BaseFont.Size;

            set {
                if ( value != this.FontSize )
                {
                    this._BaseFont = new Font( this._BaseFont.FontFamily, value, this._BaseFont.Style );
                }
            }
        }

        /// <summary>
        /// The font family name for this <see cref="isr.Visuals.TextAppearance">TextAppearance</see>.
        /// </summary>
        /// <value>
        /// A <see cref="System.String">String</see> with the
        /// <see cref="System.Drawing.Font">Font</see> family name, e.graphicsDevice., "Arial".
        /// </value>
        public string FontFamilyName
        {
            get => this._BaseFont.FontFamily.Name;

            set {
                if ( (value ?? "") != (this.FontFamilyName ?? "") )
                {
                    this._BaseFont = new Font( value, this._BaseFont.Size, this._BaseFont.Style );
                }
            }
        }

        /// <summary> Gets or sets the frame property. </summary>
        /// <value> A <see cref="Frame"/> property. </value>
        public Frame Frame { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="isr.Visuals.TextAppearance">TextAppearance</see> Bold font style.
        /// </summary>
        /// <value>
        /// A <see cref="System.Boolean">Boolean</see> value, True for bold, false for not Italic.
        /// </value>
        public bool Bold
        {
            get => this._BaseFont.Bold;

            set {
                if ( value != this.Bold )
                {
                    this._BaseFont = new Font( this._BaseFont.FontFamily, this._BaseFont.Size, GetFontStyle( value, this._BaseFont.Italic, this._BaseFont.Underline ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="isr.Visuals.TextAppearance">TextAppearance</see> Italic font
        /// style.
        /// </summary>
        /// <value>
        /// A <see cref="System.Boolean">Boolean</see> value, true for italic, false for normal.
        /// </value>
        public bool Italic
        {
            get => this._BaseFont.Italic;

            set {
                if ( value != this.Italic )
                {
                    this._BaseFont = new Font( this._BaseFont.FontFamily, this._BaseFont.Size, GetFontStyle( this._BaseFont.Bold, value, this._BaseFont.Underline ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="isr.Visuals.TextAppearance">TextAppearance</see> Underline font
        /// style.
        /// </summary>
        /// <value>
        /// A <see cref="System.Boolean">Boolean</see> value, true for underline, false for normal.
        /// </value>
        public bool IsUnderline
        {
            get => this._BaseFont.Underline;

            set {
                if ( value != this.IsUnderline )
                {
                    this._BaseFont = new Font( this._BaseFont.FontFamily, this._BaseFont.Size, GetFontStyle( this._BaseFont.Bold, this._BaseFont.Italic, value ) );
                }
            }
        }

        /// <summary>Gets or sets the last base font that was constructed for drawing.  This stored value
        /// is used for comparing the new with the old for determining if a new
        /// scaled font needs to be constructed.</summary>
        private Font _LastBaseFont;

        /// <summary> Gets the scaled font. </summary>
        /// <value> A <see cref="System.Drawing.Font">Font</see> property. </value>
        public Font ScaledFont { get; private set; }

        /// <summary>Private field that temporarily stores the scaled size of the font for this
        /// <see cref="isr.Visuals.TextAppearance">TextAppearance</see> object.  This represents the actual on-screen
        /// size, rather than the <see cref="Size"/> that represents the reference
        /// size for a "full-sized" <see cref="Pane"/>.</summary>
        private float _ScaledFontSize;

        /// <summary> Gets or sets the status message. </summary>
        /// <remarks> Use this property to get or set the file name. </remarks>
        /// <value> A <see cref="System.String"/> value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusMessage { get; set; }

        #endregion

        #region " PRIVATE  and  PROTECTED "

        /// <summary> Reconstruct the scaled font if different than the existing. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="scaleFactor"> The chart scaling factor relative to the
        /// <see cref="Pane.BaseDimension"/>, as calculated by
        /// <see cref="Pane.GetScaleFactor"/>. </param>
        private void ReconstructFont( double scaleFactor )
        {

            // get the requested new size
            float newSize = Convert.ToSingle( this._BaseFont.Size * scaleFactor );

            // Regenerate the font only if the size has changed significantly or the font is
            // new 
            if ( Math.Abs( newSize - this._ScaledFontSize ) > TextAppearanceDefaults.Get().SignificantFontSizeChange || !this._BaseFont.Equals( this._LastBaseFont ) )
            {

                // make the new font.
                this.ScaledFont = new Font( this._BaseFont.FontFamily, newSize, this._BaseFont.Style );

                // save the new size
                this._ScaledFontSize = newSize;

                // update the last font.
                this._LastBaseFont = ( Font ) this._BaseFont.Clone();
            }
        }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="isr.Visuals.TextAppearance">TextAppearance</see> class
    /// that defines the default property values for the
    /// <see cref="isr.Visuals.TextAppearance">TextAppearance</see> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class TextAppearanceDefaults
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private TextAppearanceDefaults() : base()
        {
            this.SignificantFontSizeChange = 0.1d;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static TextAppearanceDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static TextAppearanceDefaults Get()
        {
            if ( _Instance is null )
            {
                lock ( SyncLocker )
                    _Instance = new TextAppearanceDefaults();
            }

            return _Instance;
        }

        #endregion

        /// <summary> Gets or sets the change in font size that is considered significant. </summary>
        /// <value> The significant font size change. </value>
        public double SignificantFontSizeChange { get; set; }
    }
}

#endregion

