using System;
using System.ComponentModel;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary> Handles the drawing of the strip chart pen <see cref="Stylus"/>. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class Stylus : ICloneable
    {

        #region " TYPES "

        #endregion

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs a <see cref="Stylus"/> with default values as defined in the
        /// <see cref="StylusDefaults"/> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public Stylus( Pane drawingPane ) : base()
        {
            if ( drawingPane is null )
            {
                throw new ArgumentNullException( nameof( drawingPane ) );
            }

            var stylusDefaults = StylusDefaults.Get();
            this.Size = stylusDefaults.Size;
            this.LineWidth = stylusDefaults.LineWidth;
            this.LineColor = stylusDefaults.LineColor;
            this.Visible = stylusDefaults.Visible;
            this.Filled = stylusDefaults.Filled;
            this._Pane = drawingPane;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Stylus object from which to copy. </param>
        public Stylus( Stylus model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.Size = model.Size;
            this.LineWidth = model.LineWidth;
            this.LineColor = model.LineColor;
            this.Visible = model.Visible;
            this.Filled = model.Filled;
            this._Pane = model._Pane;
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Stylus. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Stylus. </returns>
        public Stylus Copy()
        {
            return new Stylus( this );
        }

        /// <summary>
        /// Draw the <see cref="Stylus"/> to the specified <see cref="Graphics"/> device at the specified
        /// location.  This method draws a single Stylus.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="y">              The y position of the tip of the Stylus in screen pixels. </param>
        /// <param name="axisArea">       The rectangle that contains the area bounded by the axes, in
        /// pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso> </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        public void Draw( Graphics graphicsDevice, float y, RectangleF axisArea, double scaleFactor )
        {

            // Only draw if the Stylus is visible
            if ( this.Visible )
            {

                // validate argument.
                if ( graphicsDevice is null )
                {
                    throw new ArgumentNullException( nameof( graphicsDevice ) );
                }

                float scaledSize = Convert.ToSingle( this.Size * scaleFactor );
                float x = axisArea.X + axisArea.Width;
                using ( var sb = new SolidBrush( Color.Black ) )
                {
                    // draw the stylus background
                    graphicsDevice.FillRectangle( sb, x, axisArea.Y - scaledSize / 2f, scaledSize, axisArea.Height + scaledSize );
                }

                using var brush = new SolidBrush( this.LineColor );
                using var pen = new Pen( this.LineColor, this.LineWidth );
                // Fill or draw the Stylus as required
                if ( this.Filled )
                {
                    this.Fill( graphicsDevice, x, y, scaleFactor, brush );
                }
                else
                {
                    this.Outline( graphicsDevice, x, y, scaleFactor, pen );
                }
            }
        }

        /// <summary>
        /// Draw the <see cref="Symbol"/> (outline only) to the specified
        /// <see cref="Graphics"/> device at the specified location.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="x">              The x position of the center of the symbol in screen pixels. </param>
        /// <param name="y">              The y position of the center of the symbol in screen pixels. </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <param name="pen">            A pen with attributes of <see cref="Color"/> and
        /// <see cref="LineWidth"/> for this symbol. </param>
        private void Outline( Graphics graphicsDevice, float x, float y, double scaleFactor, Pen pen )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            graphicsDevice.DrawPolygon( pen, this.GetStylusPolygon( x, y, scaleFactor ) );
        }

        /// <summary>
        /// Renders the filled <see cref="Stylus"/> to the specified <see cref="Graphics"/>
        /// device at the specified location.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="x">              The x position of the center of the Stylus in screen pixels. </param>
        /// <param name="y">              The y position of the center of the Stylus in screen pixels. </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <param name="brush">          A brush with the <see cref="Color"/> attribute for this Stylus. </param>
        private void Fill( Graphics graphicsDevice, float x, float y, double scaleFactor, Brush brush )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            graphicsDevice.FillPolygon( brush, this.GetStylusPolygon( x, y, scaleFactor ) );
        }

        /// <summary> Returns the pen stylus polygon. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x">           The x position of the center of the Stylus in screen pixels. </param>
        /// <param name="y">           The y position of the center of the Stylus in screen pixels. </param>
        /// <param name="scaleFactor"> The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <returns> The stylus polygon. </returns>
        private PointF[] GetStylusPolygon( float x, float y, double scaleFactor )
        {
            float scaledSize = Convert.ToSingle( this.Size * scaleFactor );
            float hsize = scaledSize / 2f;
            var polyPt = new PointF[4];
            polyPt[0].X = x;
            polyPt[0].Y = y;
            polyPt[1].X = x + scaledSize;
            polyPt[1].Y = y + hsize;
            polyPt[2].X = polyPt[1].X;
            polyPt[2].Y = y - hsize;
            polyPt[3] = polyPt[0];
            return polyPt;
        }

        /// <summary>
        /// Adjusts the <see cref="Pane.AxisArea"/> for the
        /// <see cref="isr.Visuals.Stylus">Stylus</see> size.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="scaleFactor"> The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <param name="axisArea">    The rectangle that contains the area bounded by the axes, in
        /// pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso> </param>
        /// <returns> Adjusted <see cref="Pane.AxisArea"/> </returns>
        public RectangleF GetAxisArea( double scaleFactor, RectangleF axisArea )
        {

            // Leave room for the stylus
            if ( this.Visible )
            {

                // Leave room for the stylus 
                axisArea.Width -= Convert.ToSingle( this.Size * scaleFactor );
            }

            // return adjusted axis rectangle
            return axisArea;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Gets or sets the filled mode of the <see cref="Stylus"/> shape.  Set to True to fill the
        /// shape with color, or False for an outline Stylus.  Note that symbols that are not closed,
        /// such as <see cref="isr.Visuals.ShapeType.Plus"/>
        /// cannot be filled.
        /// </summary>
        /// <value> The filled. </value>
        public bool Filled { get; set; }

        /// <summary> Gets or sets a property that shows or hides the <see cref="Stylus"/>. </summary>
        /// <value> True to show the Stylus, False to hide it. </value>
        public bool Visible { get; set; }

        /// <summary> Gets or sets the pen width for drawing the <see cref="Stylus"/> outline. </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float LineWidth { get; set; }

        /// <summary> Gets or sets the line color of the <see cref="Stylus"/> </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color LineColor { get; set; }

        /// <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
        private readonly Pane _Pane;

        /// <summary> Gets or sets the size of the <see cref="Stylus"/> </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float Size { get; set; }

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A System.String value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusMessage { get; set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="Stylus"/> class that defines the default property values
    /// for the <see cref="isr.Visuals.Stylus"/> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class StylusDefaults
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private StylusDefaults() : base()
        {
            this.Filled = true;
            this.Visible = true;
            this.LineColor = Color.Red;
            this.LineWidth = 1.0f;
            this.Size = 8f;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static StylusDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static StylusDefaults Get()
        {
            if ( _Instance is null )
            {
                lock ( SyncLocker )
                    _Instance = new StylusDefaults();
            }

            return _Instance;
        }

        #endregion

        /// <summary>
        /// Gets or sets the default fill mode for symbols (<see cref="Stylus.Filled"/> property). true
        /// to have symbols filled in with color, false to leave them as outlines.
        /// </summary>
        /// <value> The filled. </value>
        public bool Filled { get; set; }

        /// <summary>
        /// Gets or sets the default display mode for symbols (<see cref="Stylus.Visible"/> property).
        /// true to display symbols, false to hide them.
        /// </summary>
        /// <value> The visible. </value>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the default color for drawing symbols (<see cref="Stylus.LineColor"/> property).
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color LineColor { get; set; }

        /// <summary>
        /// Gets or sets the default pen width to be used for drawing curve symbols
        /// (<see cref="Stylus.LineWidth"/> property).
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float LineWidth { get; set; }

        /// <summary>
        /// Gets or sets the default size for curve symbols (<see cref="Stylus.Size"/>.
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float Size { get; set; }
    }
}
#endregion

