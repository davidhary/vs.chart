
namespace isr.Visuals
{

    /// <summary> Handles coordinate scale type and values. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public struct CoordinateScale
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="type"> The type. </param>
        public CoordinateScale( CoordinateScaleType type )
        {
            this.CoordinateScaleType = type;
        }

        #region " EQUALS "

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( CoordinateScale left, CoordinateScale right )
        {
            return Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( CoordinateScale left, CoordinateScale right )
        {
            return !Equals( left, right );
        }

        /// <summary> Returns True if equal. </summary>
        /// <remarks>
        /// The two Coordinate Scales are the same if the have the same
        /// <see cref="System.Type">type</see>.
        /// </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public static bool Equals( CoordinateScale left, CoordinateScale right )
        {
            return left.Equals( right );
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="obj"> Another object to compare to. </param>
        /// <returns>
        /// <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
        /// same value; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return obj is object && ReferenceEquals( this.GetType(), obj.GetType() ) && this.Equals( ( CoordinateScale ) obj );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="other"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// Coordinate Scales are the same if the have the same
        /// <see cref="System.Type">type</see>.
        /// </remarks>
        /// <param name="other"> The <see cref="CoordinateScale">Coordinate Scale</see> to compare for
        /// equality with this instance. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public bool Equals( CoordinateScale other )
        {
            return this.CoordinateScaleType.Equals( other.CoordinateScaleType );
        }

        #endregion

        /// <summary>
        /// Returns the hash code for this <see cref="CoordinateScale"/> structure.
        /// In this case, the hash code is simply the equivalent hash code for the
        /// <see cref="CoordinateScaleType.Linear"/> <see cref="System.Type"/>.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return this.CoordinateScaleType.GetHashCode();
        }

        /// <summary> Gets the <see cref="CoordinateScaleType"/> </summary>
        /// <remarks> David, 2007-10-15, 1.0.2844 Rename to CoordinateScaleType. </remarks>
        /// <value> The type of the coordinate scale. </value>
        public CoordinateScaleType CoordinateScaleType { get; set; }

        /// <summary>
        /// Determines if the <see cref="Scale"/> is date-time type.  To make this property True, set
        /// <see cref="System.Type"/> to <see cref="CoordinateScaleType.Date"/>.
        /// </summary>
        /// <value> True for a date axis, False otherwise. </value>
        public bool IsDate => CoordinateScaleType.Date == this.CoordinateScaleType;

        /// <summary>
        /// Determines if the <see cref="Scale"/> is linear.  To make this property True, set
        /// <see cref="System.Type"/> to <see cref="CoordinateScaleType.Log"/>.
        /// </summary>
        /// <value> True for a linear axis, False otherwise. </value>
        public bool IsLinear => CoordinateScaleType.Linear == this.CoordinateScaleType;

        /// <summary>
        /// Determines if the <see cref="Scale"/> is logarithmic (base 10).  To make this property True,
        /// set <see cref="System.Type"/> to <see cref="CoordinateScaleType.Log"/>.
        /// </summary>
        /// <value> True for a logarithmic axis, False otherwise. </value>
        public bool IsLog => CoordinateScaleType.Log == this.CoordinateScaleType;

        /// <summary>
        /// Determines if the <see cref="Scale"/> is strip-chart type.  To make this property True, set
        /// <see cref="System.Type"/> to <see cref="CoordinateScaleType.StripChart"/>.
        /// </summary>
        /// <value> True for a strip-chart axis, False otherwise. </value>
        public bool IsStripChart => CoordinateScaleType.StripChart == this.CoordinateScaleType;

        /// <summary>
        /// Returns true if the <see cref="Scale"/> is labeled with user provided text labels rather than
        /// calculated numeric values.  The text labels are provided via the
        /// <see cref="System.Type"/> property.  Internally, the axis is still handled with
        /// ordinal values such that the axis <see cref="Axis.Min"/> is set to 1.0, and the axis
        /// <see cref="Axis.Max"/> is set to the number of labels.  To make this property True,
        /// set <see cref="System.Type"/> to <see cref="CoordinateScaleType.Text"/>.
        /// </summary>
        /// <value>
        /// True for a text-based axis, False otherwise.  If this property is true, then an array of
        /// labels must be provided via <see cref="Axis.TickLabels"/>.
        /// </value>
        public bool IsText => CoordinateScaleType.Text == this.CoordinateScaleType;
    }
}
