namespace isr.Visuals
{

    /// <summary> Provides alignment properties. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public struct Alignment
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs an instance of this class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="horizontal"> The <see cref="HorizontalAlignment"/>  setting. </param>
        /// <param name="vertical">   <see cref="VerticalAlignment"/> setting. </param>
        public Alignment( HorizontalAlignment horizontal, VerticalAlignment vertical )
        {
            this.Horizontal = horizontal;
            this.Vertical = vertical;
        }

        #endregion

        #region " EQUALS "

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( Alignment left, Alignment right )
        {
            return Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( Alignment left, Alignment right )
        {
            return !Equals( left, right );
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns>
        /// <c>True</c> if values are the same type and represent the same value; otherwise, <c>False</c>.
        /// </returns>
        public static bool Equals( Alignment left, Alignment right )
        {
            return left.Equals( right );
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="obj"> Another object to compare to. </param>
        /// <returns>
        /// <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
        /// same value; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return obj is object && ReferenceEquals( this.GetType(), obj.GetType() ) && this.Equals( ( Alignment ) obj );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="other"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// Alignments are the same if the have the same
        /// <see cref="Horizontal"/> and <see cref="Horizontal"/> ranges.
        /// </remarks>
        /// <param name="other"> The <see cref="Alignment">Alignment</see> to compare for equality with
        /// this instance. </param>
        /// <returns>
        /// <c>True</c> if <paramref name="other" /> and this instance are the same type and represent
        /// the same value; otherwise, <c>False</c>.
        /// </returns>
        public bool Equals( Alignment other )
        {
            return this.Horizontal.Equals( other.Horizontal ) && this.Vertical.Equals( other.Vertical );
        }

        #endregion

        #region " METHODS "

        /// <summary> Returns the hash code for this <see cref="Alignment"/> structure. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> An <see cref="T:System.integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return this.Horizontal.GetHashCode() ^ this.Vertical.GetHashCode();
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets or sets the horizontal alignment setting. </summary>
        /// <value> A <see cref="HorizontalAlignment"/> value. </value>
        public HorizontalAlignment Horizontal { get; set; }

        /// <summary> Gets or sets the vertical alignment setting. </summary>
        /// <value> A <see cref="VerticalAlignment"/> value. </value>
        public VerticalAlignment Vertical { get; set; }

        #endregion

    }
}
