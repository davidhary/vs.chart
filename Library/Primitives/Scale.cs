using System;
using System.ComponentModel;

using isr.Core.Constructs;

namespace isr.Visuals
{

    /// <summary> Handles axis and data scaling. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class Scale : ICloneable, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Default constructor for <see cref="Axis"/> scale that sets all properties to default values
        /// as defined in the <see cref="ScaleDefaults"/> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public Scale() : base()
        {
            var scaleDefaults = ScaleDefaults.Get();
            this.Reversed = scaleDefaults.Reversed;
            this.Visible = scaleDefaults.Visible;
            this.Max = scaleDefaults.Max.Copy();
            this.Min = scaleDefaults.Min.Copy();
            this.CoordinateScale = new CoordinateScale( scaleDefaults.CoordinateScale );
            this.ScaleExponent = scaleDefaults.ScaleExponent.Copy();
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Scale object from which to copy. </param>
        public Scale( Scale model ) : this()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.Visible = model.Visible;
            this.Reversed = model.Reversed;
            this.Min = model.Min;
            this.Max = model.Max;
            this.CoordinateScale = model.CoordinateScale;
            this.ScaleExponent = model.ScaleExponent;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Scale. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of Scale. </returns>
        public Scale Copy()
        {
            return new Scale( this );
        }

        /// <summary>
        /// Calculate a date that is close to the specified date and an
        /// even multiple of the selected <see cref="TimeIntervalType"/> for a
        /// <see cref="CoordinateScaleType.Date"/> scale. This method is used by
        /// <see cref="Axis.Rescale"/>.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="dateValue">        The date which the calculation should be close to. </param>
        /// <param name="direction">        The desired direction for the date to take. 1 indicates the
        /// result date should be greater than the specified date
        /// parameter.  -1 indicates the other direction. </param>
        /// <param name="timeIntervalType"> The<see cref="TimeIntervalType"/> </param>
        /// <returns> The even date spacing. </returns>
        protected static double GetEvenDateSpacing( double dateValue, int direction, TimeIntervalType timeIntervalType )
        {
            int year = default, month = default, day = default, hour = default, minute = default, second = default, milliseconds = default;
            JulianDate.ToCalendarDate( dateValue, ref year, ref month, ref day, ref hour, ref minute, ref second, ref milliseconds );

            // If the direction is -1, then it is sufficient to go to the beginning of
            // the current time period, .e.graphicsDevice., for 15-May-95, and monthly spacing, we
            // can just back up to 1-May-95
            if ( direction < 0 )
            {
                direction = 0;
            }

            switch ( timeIntervalType )
            {
                case TimeIntervalType.Year:
                    {
                        // If the date is already an exact year, then don't space to the next year
                        return direction == 1 && month == 1 && day == 1 && hour == 0 && minute == 0 && second == 0 ? dateValue : JulianDate.FromArbitraryCalendarDate( year + direction, 1, 1, 0, 0, 0, 0 );
                    }

                case TimeIntervalType.Month:
                    {
                        // If the date is already an exact month, then don't space to the next month
                        return direction == 1 && day == 1 && hour == 0 && minute == 0 && second == 0 ? dateValue : JulianDate.FromArbitraryCalendarDate( year, month + direction, 1, 0, 0, 0, 0 );
                    }

                case TimeIntervalType.Day:
                    {
                        // If the date is already an exact Day, then don't space to the next day
                        return direction == 1 && hour == 0 && minute == 0 && second == 0 ? dateValue : JulianDate.FromArbitraryCalendarDate( year, month, day + direction, 0, 0, 0, 0 );
                    }

                case TimeIntervalType.Hour:
                    {
                        // If the date is already an exact hour, then don't space to the next hour
                        return direction == 1 && minute == 0 && second == 0 ? dateValue : JulianDate.FromArbitraryCalendarDate( year, month, day, hour + direction, 0, 0, 0 );
                    }

                case TimeIntervalType.Minute:
                    {
                        // If the date is already an exact minute, then don't space to the next minute
                        return direction == 1 && second == 0 ? dateValue : JulianDate.FromArbitraryCalendarDate( year, month, day, hour, minute + direction, 0, 0 );
                    }

                case TimeIntervalType.Second:
                    {
                        return JulianDate.FromArbitraryCalendarDate( year, month, day, hour, minute, second + direction, 0 );
                    }
            }

            return default;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Gets the <see cref="isr.Visuals.CoordinateScale"/> for this <see cref="Scale"/>.
        /// </summary>
        /// <value> The coordinate scale. </value>
        public CoordinateScale CoordinateScale { get; set; }

        /// <summary> Determines if the scale values are reversed for this <see cref="Scale"/> </summary>
        /// <value>
        /// True for the X values to decrease to the right or the Y values to decrease upwards, false
        /// otherwise.
        /// </value>
        public bool Reversed { get; set; }

        /// <summary> Determines if the value <see cref="Labels"/> will be drawn. </summary>
        /// <value> A <see cref="System.Boolean">Boolean</see> </value>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets maximum auto value for this axis.  This value can be set automatically based on the
        /// state of <see cref="AutoValueR.AutoScale"/>.  If this value is set manually, then
        /// <see cref="AutoValueR.AutoScale"/> is set to False.
        /// </summary>
        /// <value>
        /// A <see cref="AutoValueR"/> value in user scale units for
        /// <see cref="CoordinateScaleType.Log"/> and <see cref="CoordinateScaleType.Linear"/> axes. For
        /// <see cref="CoordinateScaleType.Text"/> axes, this value is an ordinal starting with 1.0.
        /// For <see cref="CoordinateScaleType.Date"/> axes, this value is in an internal
        /// (<see cref="System.Double"/> date format based on the <see cref="JulianDate"/>.
        /// </value>
        public AutoValueR Max { get; set; }

        /// <summary>
        /// Gets minimum auto value for this axis.  The value of this auto value automatically based on
        /// the state of <see cref="AutoValueR.AutoScale"/>.  If this value is set manually, then
        /// <see cref="AutoValueR.AutoScale"/> is set to false.
        /// </summary>
        /// <value>
        /// A <see cref="AutoValueR"/> value in user scale units for
        /// <see cref="CoordinateScaleType.Log"/> and <see cref="CoordinateScaleType.Linear"/> axes. For
        /// <see cref="CoordinateScaleType.Text"/> axes, this value is an ordinal starting with 1.0.
        /// For <see cref="CoordinateScaleType.Date"/> axes, this value is in an internal
        /// (<see cref="System.Double"/> date format based on the <see cref="JulianDate"/>.
        /// </value>
        public AutoValueR Min { get; set; }

        /// <summary> Gets the axis range. </summary>
        /// <value> Max - Min values. </value>
        public double Range => this.Max.Value - this.Min.Value;

        /// <summary>
        /// Gets or sets the magnitude multiplier decade for scale values. This is used to limit the size
        /// of the displayed value labels.  For example, if the value is really 2000000, then the graph
        /// could instead display 2000 with a magnitude multiplier decade of 3 (10^3).  The Value can be
        /// determined automatically depending on the state of <see cref="AutoValue.AutoScale"/>. If the
        /// Value is set manually by the user, then <see cref="AutoValue.AutoScale"/>
        /// is set to False. The magnitude multipliers.
        /// </summary>
        /// <value> The magnitude multiplier (power of 10) for the scale value labels. </value>
        public AutoValue ScaleExponent { get; set; }

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A System.String value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusMessage { get; set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="Scale"/> class that defines the default property values
    /// for the <see cref="Scale"/> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class ScaleDefaults
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private ScaleDefaults() : base()
        {
            this.CoordinateScale = CoordinateScaleType.Linear;
            this.IgnoreInitial = false;
            this.Reversed = false;
            this.Visible = true;
            this.Max = new AutoValueR( 1d, true );
            this.Min = new AutoValueR( 0d, true );
            this.MinRange = 1.0E-20d;
            this.ScaleExponent = new AutoValue( 0, false );
            this.ZeroLever = 0.25d;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static ScaleDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static ScaleDefaults Get()
        {
            if ( _Instance is null )
            {
                lock ( SyncLocker )
                    _Instance = new ScaleDefaults();
            }

            return _Instance;
        }

        #endregion

        /// <summary>
        /// Gets or sets the default setting for the <see cref="Axis"/> coordinate scale type
        /// (<see cref="Scale.CoordinateScale"/> property).  This value is set as per the
        /// <see cref="CoordinateScaleType"/> enumeration.
        /// </summary>
        /// <value> The coordinate scale. </value>
        public CoordinateScaleType CoordinateScale { get; set; }

        /// <summary>
        /// Gets or sets the default settings for the <see cref="Axis"/> scale ignore initial zero values
        /// option (<see cref="Pane.IgnoreInitial"/> property). true to have the auto-scale-range code
        /// ignore the initial data points until the first non-zero Y value, false otherwise.
        /// </summary>
        /// <value> The ignore initial. </value>
        public bool IgnoreInitial { get; set; }

        /// <summary>
        /// Gets or sets the default reverse mode for the <see cref="Axis"/> scale
        /// (<see cref="Axis.Reversed"/> property). true for a reversed scale (X decreasing to the left,
        /// Y/Y2 decreasing upwards), false otherwise.
        /// </summary>
        /// <value> The reversed. </value>
        public bool Reversed { get; set; }

        /// <summary>
        /// Gets or sets the default display mode for the axis <see cref="Scale"/>
        /// (<see cref="Scale.Visible"/> property). True to show the scale, False to hide it.
        /// </summary>
        /// <value> The visible. </value>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the default maximum point for the axis <see cref="Scale"/>
        /// (<see cref="Scale.Max"/> property).
        /// </summary>
        /// <value> The maximum value. </value>
        public AutoValueR Max { get; set; }

        /// <summary>
        /// Gets or sets the default minimum point for the axis <see cref="Scale"/>
        /// (<see cref="Scale.Min"/> property).
        /// </summary>
        /// <value> The minimum value. </value>
        public AutoValueR Min { get; set; }

        /// <summary> Gets or sets the default minimum range for the axis <see cref="Scale"/>. </summary>
        /// <value> The minimum range. </value>
        public double MinRange { get; set; }

        /// <summary>
        /// Gets or sets the default scale decade for the axis value <see cref="Labels"/>
        /// (<see cref="Scale.ScaleExponent"/> property).
        /// </summary>
        /// <value> The scale exponent. </value>
        public AutoValue ScaleExponent { get; set; }

        /// <summary>
        /// Gets or sets the default "zero lever" for automatically selecting the axis scale range (see
        /// <see cref="Axis.Rescale"/>). This number is used to determine when an axis scale range should
        /// be extended to include the zero value.  This value is maintained only in the
        /// <see cref="ScaleDefaults"/> class, and can be changed after compilation.
        /// The Zero Lever is the allowable fraction that the scale range can be extended to include the
        /// zero value. For example, if the Zero Lever is 0.25 and the data range is from 2 to 12, then
        /// the scale would be extended to the range of 0.0 to 12 because the zero value lies 20% (2/(12-
        /// 2)) outside the actual data range.
        /// </summary>
        /// <value> The zero lever. </value>
        public double ZeroLever { get; set; }
    }
}
#endregion


