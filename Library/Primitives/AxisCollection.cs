using System;
using System.Diagnostics;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary>
    /// A collection class containing a list of <see cref="Axis"/> objects that define the set of
    /// Axes to be displayed on the graph.
    /// </summary>
    /// <remarks>
    /// Declare <see cref="A:NotInheritable"/> so as to allow calling base methods in the
    /// constructor. <para>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public sealed class AxisCollection : System.Collections.ObjectModel.Collection<Axis>, ICloneable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor for the collection class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public AxisCollection( Pane drawingPane ) : base()
        {
            if ( drawingPane is null )
                throw new ArgumentNullException( nameof( drawingPane ) );
            this._Pane = drawingPane;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Axis Collection object from which to copy. </param>
        public AxisCollection( AxisCollection model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            foreach ( Axis item in model )
                this.Add( item );
            this._Pane = model._Pane;
        }

        #endregion

        #region " CUSTOM COLLECTION METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the AxisCollection. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the AxisCollection. </returns>
        public AxisCollection Copy()
        {
            return new AxisCollection( this );
        }

        /// <summary>
        /// Renders all the <see cref="Axis"/> objects in the list to the specified
        /// <see cref="Graphics"/> device by calling the <see cref="Axis.Draw"/>
        /// method of each <see cref="Axis"/> object.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        public void Draw( Graphics graphicsDevice )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // Clip everything to the pane area
            graphicsDevice.SetClip( this._Pane.PaneArea );

            // Loop for each axis
            foreach ( Axis axis in this )

                // Render the axis
                axis.Draw( graphicsDevice );
        }

        /// <summary> Adjusts the <see cref="Pane.AxisArea"/> for the axis space. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <param name="axisArea">       The rectangle that contains the area bounded by the axes, in
        /// pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso> </param>
        /// <returns> Adjusted <see cref="Pane.AxisArea"/> </returns>
        public RectangleF GetAxisArea( Graphics graphicsDevice, double scaleFactor, RectangleF axisArea )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // calculate the margins required for the X, Y, and Y2 axes
            var margins = new MarginsF( 0f, 0f, 0f, 0f );
            SizeF axisSpace;

            // Loop for each axis
            foreach ( Axis axis in this )
            {

                // get width and height required for axis space
                axisSpace = axis.GetSpace( graphicsDevice, scaleFactor );

                // Set margins based on the axis
                switch ( axis.AxisType )
                {
                    case AxisType.X:
                        {
                            margins.Bottom = Math.Max( margins.Bottom, axisSpace.Height );
                            margins.Left = Math.Max( margins.Left, axisSpace.Width );
                            margins.Right = Math.Max( margins.Right, axisSpace.Width );
                            break;
                        }

                    case AxisType.Y:
                        {
                            margins.Left = Math.Max( margins.Left, axisSpace.Width );
                            margins.Bottom = Math.Max( margins.Bottom, axisSpace.Height );
                            margins.Top = Math.Max( margins.Top, axisSpace.Height );
                            break;
                        }

                    case AxisType.Y2:
                        {
                            margins.Right = Math.Max( margins.Right, axisSpace.Width );
                            margins.Bottom = Math.Max( margins.Bottom, axisSpace.Height );
                            margins.Top = Math.Max( margins.Top, axisSpace.Height );
                            break;
                        }

                    default:
                        {
                            Debug.Assert( !Debugger.IsAttached, "Unhandled axis type" );
                            break;
                        }
                }
            }

            // return adjusted axis rectangle
            return margins.GetAdjustedArea( axisArea );
        }

        /// <summary>
        /// Rescale all the <see cref="Axis"/> objects in the list to the specified
        /// <see cref="CurveCollection"/> by calling the <see cref="Axis.Rescale"/>
        /// method of each <see cref="Axis"/> object.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="curves">        The curves. </param>
        /// <param name="ignoreInitial"> True to ignore initial. </param>
        public void Rescale( CurveCollection curves, bool ignoreInitial )
        {

            // validate argument.
            if ( curves is null )
            {
                throw new ArgumentNullException( nameof( curves ) );
            }

            // Get the scale range of the X-Y curves
            var xyRange = curves.GetRange( ignoreInitial, false );

            // Get the scale range of the X-Y2 curves
            var xy2Range = curves.GetRange( ignoreInitial, true );

            // set the X range to include both ranges.
            var xRange = xyRange.X.ExtendedRange( xy2Range.X );

            // Loop for each axis
            foreach ( Axis item in this )
            {
                // Pick new scales based on the range
                switch ( item.AxisType )
                {
                    case AxisType.X:
                        {
                            item.Rescale( xRange.Min, xRange.Max );
                            break;
                        }

                    case AxisType.Y:
                        {
                            item.Rescale( xyRange.Y.Min, xyRange.Y.Max );
                            break;
                        }

                    case AxisType.Y2:
                        {
                            item.Rescale( xy2Range.Y.Min, xy2Range.Y.Max );
                            break;
                        }

                    default:
                        {
                            Debug.Assert( !Debugger.IsAttached, "Unhandled axis type" );
                            break;
                        }
                }
            }
        }

        /// <summary> Sets the screen ranges for rendering the <see cref="Axis"/>. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="axisArea"> The <see cref="System.Drawing.RectangleF"/> that that contains the
        /// area bounded by the axes. </param>
        public void SetScreenRange( RectangleF axisArea )
        {

            // Loop for each axis
            foreach ( Axis axis in this )
                // Render the axis
                axis.SetScreenRange( axisArea );
        }

        /// <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
        private readonly Pane _Pane;

        #endregion

    }
}
