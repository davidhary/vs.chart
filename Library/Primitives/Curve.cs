using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using isr.Core.Constructs;

namespace isr.Visuals
{

    /// <summary>
    /// Contains the data and methods for an individual curves within a graph pane.  It carries the
    /// settings for the curve including colors, symbols and sizes, line types, etc.
    /// </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. </para><para>
    /// David, 2004-05-28, 1.0.1609. Add time series point tag  </para><para>
    /// David, 2004-06-02, 1.0.1614. Add time series book marks </para>
    /// </remarks>
    public class Curve : ICloneable, IDisposable
    {

        #region " SHARED "

        /// <summary> Draw a series of points with symbols. </summary>
        /// <remarks> The default symbol is a rectangle. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> The graphics context. </param>
        /// <param name="graphBrush">     graph brush. </param>
        /// <param name="graphPen">       graph pen. </param>
        /// <param name="dataSpace">      The data space rectangle. </param>
        /// <param name="screenSpace">    The screen space rectangle. </param>
        /// <param name="graphScale">     The scale conversion 'point', specifying the horizontal (X) and
        /// vertical (Y) conversion scales from the data to the screen
        /// space. </param>
        /// <param name="dataPoints">     An array of data points which to draw. </param>
        /// <param name="pointSize">      Defines the size of the symbol. </param>
        public static void DrawPoints( Graphics graphicsDevice, Brush graphBrush, Pen graphPen, RectangleF dataSpace, Rectangle screenSpace, Core.Constructs.PointF graphScale, Core.Constructs.PointF[] dataPoints, Size pointSize )
        {
            if ( graphicsDevice is null )
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            if ( graphBrush is null )
                throw new ArgumentNullException( nameof( graphBrush ) );
            if ( graphPen is null )
                throw new ArgumentNullException( nameof( graphPen ) );
            if ( graphScale is null )
                throw new ArgumentNullException( nameof( graphScale ) );
            if ( dataPoints is null )
                throw new ArgumentNullException( nameof( dataPoints ) );
            var screenSymbol = new Rectangle( 0, 0, 0, 0 ) { Size = pointSize };

            // Draw the points.
            int pt;
            float xOffset = screenSpace.X - Convert.ToSingle( pointSize.Width ) / 2f;
            float yOffset = screenSpace.Y - Convert.ToSingle( pointSize.Height ) / 2f;
            var loopTo = dataPoints.GetUpperBound( 0 );
            for ( pt = 0; pt <= loopTo; pt++ )
            {
                screenSymbol.X = Convert.ToInt32( (dataPoints[pt].X - dataSpace.X) * graphScale.X + xOffset );
                screenSymbol.Y = Convert.ToInt32( (dataPoints[pt].Y - dataSpace.Y) * graphScale.Y + yOffset );
                graphicsDevice.FillRectangle( graphBrush, screenSymbol );
                graphicsDevice.DrawRectangle( graphPen, screenSymbol );
            }
        }

        /// <summary> Draws a series of zero-based rectangles. </summary>
        /// <remarks> Use this method to draw a series of rectangles. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> The graphics context. </param>
        /// <param name="graphBrush">     graph brush. </param>
        /// <param name="graphPen">       graph pen. </param>
        /// <param name="dataSpace">      The data space rectangle. </param>
        /// <param name="screenSpace">    The screen space rectangle. </param>
        /// <param name="graphScale">     The scale conversion 'point', specifying the horizontal (X) and
        /// vertical (Y) conversion scales from the data to the screen
        /// space. </param>
        /// <param name="dataPoints">     An array of data points which to draw. </param>
        public static void DrawRectangles( Graphics graphicsDevice, Brush graphBrush, Pen graphPen, RectangleF dataSpace, Rectangle screenSpace, Core.Constructs.PointF graphScale, Core.Constructs.PointF[] dataPoints )
        {
            if ( graphicsDevice is null )
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            if ( graphBrush is null )
                throw new ArgumentNullException( nameof( graphBrush ) );
            if ( graphPen is null )
                throw new ArgumentNullException( nameof( graphPen ) );
            if ( graphScale is null )
                throw new ArgumentNullException( nameof( graphScale ) );
            if ( dataPoints is null )
                throw new ArgumentNullException( nameof( dataPoints ) );

            // Make the transformed rectangles.
            int numRectangles;
            numRectangles = dataPoints.GetUpperBound( 0 ) - 1;
            var rectangles = new RectangleF[numRectangles + 1];
            int pt;
            var loopTo = numRectangles;
            for ( pt = 0; pt <= loopTo; pt++ )
            {
                var rect = rectangles[pt];
                rect.X = (dataPoints[pt].X - dataSpace.X) * graphScale.X + screenSpace.X;
                rect.Y = screenSpace.Y - dataSpace.Y * graphScale.Y;
                rect.Width = (dataPoints[pt + 1].X - dataPoints[pt].X) * graphScale.X;
                rect.Height = dataPoints[pt].Y * graphScale.Y;
                if ( rect.Width < 0f )
                {
                    rect.X += rect.Width;
                    rect.Width = -rect.Width;
                }

                if ( rect.Height < 0f )
                {
                    rect.Y += rect.Height;
                    rect.Height = -rect.Height;
                }
            }

            // Draw the rectangles.
            graphicsDevice.FillRectangles( graphBrush, rectangles );
            graphicsDevice.DrawRectangles( graphPen, rectangles );
        }

        /// <summary> Draws a line segment between two points in the data coordinate space. </summary>
        /// <remarks> Draw a segment in the data coordinate space. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice">     The graphics context. </param>
        /// <param name="graphPen">           graph pen. </param>
        /// <param name="dataSpace">          The data space rectangle. </param>
        /// <param name="screenSpace">        The screen space rectangle. </param>
        /// <param name="graphScale">         The scale conversion 'point', specifying the horizontal (X)
        /// and vertical (Y) conversion scales from the data to the
        /// screen space. </param>
        /// <param name="segmentOrigin">      The origin point of the segment. </param>
        /// <param name="segmentTermination"> The termination point of the segment. </param>
        public static void DrawSegment( Graphics graphicsDevice, Pen graphPen, RectangleF dataSpace, Rectangle screenSpace, Core.Constructs.PointF graphScale, Core.Constructs.PointF segmentOrigin, Core.Constructs.PointF segmentTermination )
        {
            if ( graphicsDevice is null )
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            if ( graphPen is null )
                throw new ArgumentNullException( nameof( graphPen ) );
            if ( graphScale is null )
                throw new ArgumentNullException( nameof( graphScale ) );
            if ( segmentOrigin is null )
                throw new ArgumentNullException( nameof( segmentOrigin ) );
            if ( segmentTermination is null )
                throw new ArgumentNullException( nameof( segmentTermination ) );
            var sgmtOrigin = new System.Drawing.PointF( 0f, 0f );
            var sgmtTermination = new System.Drawing.PointF( 0f, 0f );
            sgmtOrigin.X = (segmentOrigin.X - dataSpace.X) * graphScale.X + screenSpace.X;
            sgmtOrigin.Y = (segmentOrigin.Y - dataSpace.Y) * graphScale.Y + screenSpace.Y;
            sgmtTermination.X = (segmentTermination.X - dataSpace.X) * graphScale.X + screenSpace.X;
            sgmtTermination.Y = (segmentTermination.Y - dataSpace.Y) * graphScale.Y + screenSpace.Y;
            graphicsDevice.DrawLine( graphPen, sgmtOrigin, sgmtTermination );
        }

        /// <summary> Draws a series of connected segments. </summary>
        /// <remarks> Draw a series of connected points. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> The graphics context. </param>
        /// <param name="graphPen">       graph pen. </param>
        /// <param name="dataSpace">      The data space rectangle. </param>
        /// <param name="screenSpace">    The screen space rectangle. </param>
        /// <param name="graphScale">     The scale conversion 'point', specifying the horizontal (X) and
        /// vertical (Y) conversion scales from the data to the screen
        /// space. </param>
        /// <param name="dataPoints">     An array of data points which to draw. </param>
        public static void DrawSegments( Graphics graphicsDevice, Pen graphPen, RectangleF dataSpace, Rectangle screenSpace, Core.Constructs.PointF graphScale, System.Drawing.PointF[] dataPoints )
        {
            if ( graphicsDevice is null )
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            if ( graphPen is null )
                throw new ArgumentNullException( nameof( graphPen ) );
            if ( graphScale is null )
                throw new ArgumentNullException( nameof( graphScale ) );
            if ( dataPoints is null )
                throw new ArgumentNullException( nameof( dataPoints ) );

            // Transform the points.
            int numPoints;
            numPoints = dataPoints.GetUpperBound( 0 );
            System.Drawing.PointF[] screenPoints;
            screenPoints = new System.Drawing.PointF[numPoints + 1];
            int pt;
            var loopTo = numPoints;
            for ( pt = 0; pt <= loopTo; pt++ )
            {
                screenPoints[pt].X = (dataPoints[pt].X - dataSpace.X) * graphScale.X + screenSpace.X;
                screenPoints[pt].Y = (dataPoints[pt].Y - dataSpace.Y) * graphScale.Y + screenSpace.Y;
            }

            // Draw the transformed points.
            graphicsDevice.DrawLines( graphPen, screenPoints );
        }

        /// <summary> Returns the data space rectangle. </summary>
        /// <remarks>
        /// get the standard data space for drawing.  Unlike the original data space, the standard data
        /// space for graphics is referenced at the top left (xMin,yMax) corresponding to the screen
        /// space origin.  Consequently, a typical data space will have a negative height.
        /// </remarks>
        /// <param name="xMin"> The minimum horizontal data space. </param>
        /// <param name="xMax"> The maximum horizontal data space. </param>
        /// <param name="yMin"> The minimum vertical coordinate space. </param>
        /// <param name="yMax"> The maximum vertical coordinate space. </param>
        /// <returns> The data space. </returns>
        public static RectangleF GetDataSpace( float xMin, float xMax, float yMin, float yMax )
        {
            return new RectangleF( xMin, yMax, xMax - xMin, yMin - yMax );
        }

        /// <summary> Compute the graph scale coefficients. </summary>
        /// <remarks>
        /// The graph scale coefficients convert from data to screen space coordinates.  The data space
        /// coordinates are set for the standard graphing with a top left corner at (xMin,yMax).
        /// </remarks>
        /// <param name="dataSpace">   The data space rectangle. </param>
        /// <param name="screenSpace"> The screen space rectangle. </param>
        /// <returns> The graph scale. </returns>
        public static Core.Constructs.PointF GetGraphScale( RectangleF dataSpace, Rectangle screenSpace )
        {
            var graphScale = new Core.Constructs.PointF( 0f, 0f );
            if ( dataSpace.Width != 0f )
            {
                graphScale.X = screenSpace.Width / dataSpace.Width;
            }

            if ( dataSpace.Height != 0f )
            {
                // note that we have inverted the data space by placing Y max at the 
                // top left corner.  Consequently, the data space height needs to be treated 
                // as negative
                graphScale.Y = screenSpace.Height / dataSpace.Height;
            }

            return graphScale;
        }

        /// <summary>
        /// Returns the rectangle of screen coordinates (pixels) allotted for the graph.
        /// </summary>
        /// <remarks>
        /// The margins x, y elements specify the top left offsets of the graph space from the chart
        /// control border.  The width and height accommodates the combined margins (left+right) and
        /// (top+bottom).  The graph margin provides an effective way of setting the graph space
        /// independent of the control size.
        /// </remarks>
        /// <param name="screenSpace">  The screen space rectangle. </param>
        /// <param name="graphMargins"> The screen space rectangle. </param>
        /// <returns> The graph space. </returns>
        public static Rectangle GetGraphSpace( Rectangle screenSpace, Rectangle graphMargins )
        {
            var graphSpace = screenSpace;
            graphSpace.Inflate( new Size( -graphMargins.Width, -graphMargins.Height ) );
            graphSpace.Location = graphMargins.Location;
            return graphSpace;
        }

        #endregion

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs a <see cref="Curve"/> without specifying the X and Y data sets.  This permits
        /// setting a curve for time series update-able data.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="type">        A <see cref="CurveType">Curve Type</see> value. </param>
        /// <param name="label">       A <see cref="System.String">String</see> label (legend entry) for
        /// this curve. </param>
        /// <param name="xAxis">       Reference to the X <see cref="Axis"/> </param>
        /// <param name="yAxis">       Reference to the Y <see cref="Axis"/> </param>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public Curve( CurveType type, string label, Axis xAxis, Axis yAxis, Pane drawingPane ) : base()
        {
            if ( drawingPane is null )
                throw new ArgumentNullException( nameof( drawingPane ) );
            if ( xAxis is null )
                throw new ArgumentNullException( nameof( xAxis ) );
            if ( yAxis is null )
                throw new ArgumentNullException( nameof( yAxis ) );
            if ( string.IsNullOrWhiteSpace( label ) )
            {
                label = string.Empty;
            }

            this._TimeSeriesLength = 1200;
            this._Pane = drawingPane;
            this.Cord = new Cord();
            this.IgnoreMissing = CurveDefaults.Get().IgnoreMissing;
            this.Symbol = new Symbol( this._Pane );
            this.Label = label;
            this.XAxis = xAxis;
            this.YAxis = yAxis;
            // superfluous per Code Analysis: Me._x = Nothing
            // superfluous per Code Analysis: Me._y = Nothing
            this.CurveType = type;
            if ( this.CurveType == CurveType.StripChart )
            {
                // instantiate a stylus
                this.Stylus = new Stylus( this._Pane ) { LineColor = this.Cord.LineColor };
                this.TimeSeriesBookmarks = new TimeSeriesBookmarkCollection( this );
                this.ClearTimeSeries();
            }
        }

        /// <summary>
        /// Constructs a <see cref="Curve"/> with given and default values as defined in the
        /// <see cref="CurveDefaults"/> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="label">       A <see cref="System.String">String</see> label (legend entry) for
        /// this curve. </param>
        /// <param name="x">           A array of <see cref="System.Double">Double Precision</see>
        /// values that define the independent (X axis) values for this
        /// curve. </param>
        /// <param name="y">           A array of <see cref="System.Double">Double Precision</see>
        /// values that define the dependent (Y axis) values for this curve.
        /// </param>
        /// <param name="xAxis">       Reference to the X <see cref="Axis"/> </param>
        /// <param name="yAxis">       Reference to the Y <see cref="Axis"/> </param>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public Curve( string label, double[] x, double[] y, Axis xAxis, Axis yAxis, Pane drawingPane ) : this( CurveType.XY, label, xAxis, yAxis, drawingPane )
        {
            this._X = x;
            this._Y = y;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Curve object from which to copy. </param>
        public Curve( Curve model ) : base()
        {
            if ( model is null )
                throw new ArgumentNullException( nameof( model ) );
            this.Symbol = model.Symbol.Copy();
            this.Cord = model.Cord.Copy();
            this.IgnoreMissing = model.IgnoreMissing;
            this.Label = model.Label;
            this.XAxis = model.XAxis;
            this.YAxis = model.YAxis;
            this.CurveType = model.CurveType;
            this.UseTimeSeriesTag = model.UseTimeSeriesTag;
            this.UseBookmark = model.UseBookmark;
            this.TimeSeriesBookmark = model.TimeSeriesBookmark;
            if ( model.Stylus is object )
            {
                this.Stylus = model.Stylus.Copy();
            }

            if ( model._X is object )
            {
                this._X = ( double[] ) model._X.Clone();
            }

            if ( model._Y is object )
            {
                this._Y = ( double[] ) model._Y.Clone();
            }

            this._Pane = model._Pane;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        this._X = null;
                        this._Y = null;
                        this.XAxis = null;
                        this.YAxis = null;
                        this.TimeSeriesBookmarks = null;
                        this.TimeSeriesBookmark = default;
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Adds a new time series book mark. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="bookmark"> The new <see cref="isr.Visuals.TimeSeriesBookmark">time series book
        /// mark</see> </param>
        /// <returns>
        /// The added <see cref="isr.Visuals.TimeSeriesBookmark">time series book mark</see> with its
        /// serial number set to the index location in the collection.
        /// </returns>
        public TimeSeriesBookmark AddBookmark( TimeSeriesBookmark bookmark )
        {
            bookmark.SerialNumber = this.TimeSeriesBookmarks.Count;
            this.TimeSeriesBookmarks.Add( bookmark );
            return bookmark;
        }

        /// <summary>
        /// Adds a new time series points and shifts the time series axis and origin of time series tick
        /// marks.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="timeSeriesPoint"> The new <see cref="isr.Visuals.TimeSeriesPointR">time series
        /// data point</see> </param>
        /// <returns>
        /// The new <see cref="isr.Visuals.TimeSeriesPointR">time series data point</see>
        /// </returns>
        private TimeSeriesPointR AddDataPoint( TimeSeriesPointR timeSeriesPoint )
        {

            // update the time series saved point
            _ = this.UpdateDataPoint( timeSeriesPoint );

            // update time series.
            this._TimeSeries[this._TimeSeriesPointer] = this._TimeSeriesPoint;

            // increment and roll over the pointer
            this._TimeSeriesPointer = (this._TimeSeriesPointer + 1) % this._TimeSeriesLength;
            this._TimeSeriesPointerKeeper = this._TimeSeriesPointer;
            this._TimeSeriesCount = Math.Min( this._TimeSeriesCount + 1, this._TimeSeriesLength );

            // increment the axis origins
            this.XAxis.IncrementTickOrigin();

            // return the time series point
            return this._TimeSeriesPoint;
        }

        /// <summary>
        /// Updates the time series curve data and adjusts the amplitude and time ranges.
        /// </summary>
        /// <remarks>
        /// Use this method to add a new value to the strip chart.  Shifts the buffer one notch every
        /// time a new data points comes in thus 'scrolling' the data along.
        /// </remarks>
        /// <param name="[time]">    The time (X or horizontal axis) value. </param>
        /// <param name="amplitude"> The amplitude (Y, or vertical axis) value. </param>
        /// <param name="tag">       Specifies the <see cref="TimeSeriesPointTags">time series tag</see> </param>
        /// <returns>
        /// The new <see cref="isr.Visuals.TimeSeriesPointR">time series data point</see>
        /// </returns>
        public TimeSeriesPointR AddDataPoint( DateTime time, double amplitude, TimeSeriesPointTags tag )
        {

            // add the time series point and 'shift' time series axis and tick
            // positions
            _ = this.AddDataPoint( new TimeSeriesPointR( time, amplitude, tag, this.SerialNumber + 1 ) );
            if ( this.YAxis.Max.AutoScale || this.YAxis.Min.AutoScale )
            {

                // rescale the y axis.  We assume at this point that we have only a single
                // strip chart.  Later on, we need to call the Curves.Rescale to rescale
                // all their axes.
                // Dim range As RangeR = Me.getTimeSeriesAmplitudeRange()
                // _dataRange = PlanarRangeR.Empty
                this._DataRange = new PlanarRangeR( RangeR.Empty, this.GetTimeSeriesAmplitudeRange() );
                this.YAxis.Rescale( this._DataRange.Y.Min, this._DataRange.Y.Max );
            }

            // set the time axis major and minor tick positions
            // this can only be done when drawing!
            // Me._xAxis.SetTimeSeriesTicks(Me._timeSeriesPointer, Me._timeSeries)

            // return the time series point
            return this._TimeSeriesPoint;
        }

        /// <summary>
        /// Updates the time series curve data and adjusts the amplitude and time ranges.
        /// </summary>
        /// <remarks>
        /// Use this method to add a new value to the strip chart.  Shifts the buffer one notch every
        /// time a new data points comes in thus 'scrolling' the data along.
        /// </remarks>
        /// <param name="[time]">    The time (X or horizontal axis) value. </param>
        /// <param name="amplitude"> The amplitude (Y, or vertical axis) value. </param>
        /// <returns>
        /// The new <see cref="isr.Visuals.TimeSeriesPointR">time series data point</see>
        /// </returns>
        public TimeSeriesPointR AddDataPoint( DateTime time, double amplitude )
        {

            // add the time series point and 'shift' time series axis and tick
            // positions
            _ = this.AddDataPoint( new TimeSeriesPointR( time, amplitude, TimeSeriesPointTags.None, this.SerialNumber + 1 ) );
            if ( this.YAxis.Max.AutoScale || this.YAxis.Min.AutoScale )
            {

                // rescale the y axis.  We assume at this point that we have only a single
                // strip chart.  Later on, we need to call the Curves.Rescale to rescale
                // all their axes.
                var range = this.GetTimeSeriesAmplitudeRange();
                this.YAxis.Rescale( range.Min, range.Max );
            }

            // set the time axis major and minor tick positions
            // this can only be done when drawing!
            // Me._xAxis.SetTimeSeriesTicks(Me._timeSeriesPointer, Me._timeSeries)

            // return the time series point
            return this._TimeSeriesPoint;
        }

        /// <summary>
        /// Updates the time series curve data and adjusts the amplitude and time ranges.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="[time]">         The time (X or horizontal axis) value. </param>
        /// <param name="amplitude">      The amplitude (Y, or vertical axis) value. </param>
        /// <param name="amplitudeRange"> The amplitude <see cref="RangeR">range</see>. </param>
        /// <returns>
        /// The new <see cref="isr.Visuals.TimeSeriesPointR">time series data point</see>
        /// </returns>
        public TimeSeriesPointR AddDataPoint( DateTime time, double amplitude, RangeR amplitudeRange )
        {
            if ( amplitudeRange is null )
                throw new ArgumentNullException( nameof( amplitudeRange ) );

            // add the time series point and 'shift' time series axis and tick positions
            _ = this.AddDataPoint( new TimeSeriesPointR( time, amplitude, this.SerialNumber + 1 ) );
            if ( this.YAxis.Max.AutoScale || this.YAxis.Min.AutoScale )
            {

                // rescale the y axis.  to the given range.
                this.YAxis.Rescale( amplitudeRange.Min, amplitudeRange.Max );
            }
            else
            {

                // set the range to the given range.
                this.YAxis.SetRange( amplitudeRange.Min, amplitudeRange.Max );

                // set spacing -- this will not auto scale because auto scaling is off
                this.YAxis.Rescale( amplitudeRange.Min, amplitudeRange.Max );
            }

            // return the time series point
            return this._TimeSeriesPoint;
        }

        /// <summary> Clears the time series. </summary>
        /// <remarks>
        /// Use this method to re-size and clear the buffer.  The Y value is set to zero and the X value
        /// to the current time.
        /// </remarks>
        public void ClearTimeSeries()
        {

            // clear the time series book marks
            this.TimeSeriesBookmarks.Clear();

            // clear the count of data points
            this._TimeSeriesCount = 0;
            this._TimeSeriesPointer = 0;
            this._TimeSeriesPointerKeeper = this._TimeSeriesPointer;

            // set the buffer size to match the primary screen length
            this._TimeSeriesLength = Math.Max( Screen.PrimaryScreen.WorkingArea.Width, this._TimeSeriesLength );
            var zeroPoint = new TimeSeriesPointR( DateTime.UtcNow, 0d, 0 );
            this._TimeSeries = new TimeSeriesPointR[this._TimeSeriesLength];
            for ( int i = 0, loopTo = this._TimeSeriesLength - 1; i <= loopTo; i++ )
                this._TimeSeries[i] = zeroPoint;
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Curve. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Curve. </returns>
        public Curve Copy()
        {
            return new Curve( this );
        }

        /// <summary>
        /// Renders this <see cref="Curve"/> to the specified
        /// <see cref="Graphics"/> device.  This method is normally only
        /// called by the Draw method of the parent <see cref="isr.Visuals.CurveCollection"/>
        /// collection object.  This version is optimized for speed and should be much faster than the
        /// regular Draw() routine.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        private void DrawLineChart( Graphics graphicsDevice )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            int numPoints = this.PointCount;
            if ( numPoints <= 0 )
            {
                return;
            }

            var axisArea = this._Pane.AxisArea;
            // Dim scaleFactor As Double = Me._pane.ScaleFactor

            // set clip area
            graphicsDevice.SetClip( axisArea );
            float[] tmpX;
            float[] tmpY;
            tmpX = new float[numPoints];
            tmpY = new float[numPoints];

            // Loop over each point in the curve
            for ( int i = 0, loopTo = numPoints - 1; i <= loopTo; i++ )
            {
                if ( !this.IgnoreMissing && (this._X[i] == double.MaxValue || this._Y[i] == double.MaxValue || this.XAxis.CoordinateScale.IsLog && this._X[i] <= 0.0d || this.YAxis.CoordinateScale.IsLog && this._Y[i] <= 0.0d) )
                {
                    tmpX[i] = float.MaxValue;
                    tmpY[i] = float.MaxValue;
                }
                else
                {
                    // Transform the current point from user scale units to
                    // screen coordinates
                    tmpX[i] = this.XAxis.Transform( this._X[i] );
                    tmpY[i] = this.YAxis.Transform( this._Y[i] );
                    if ( false )
                    {
                        // use the draw area to contain the range for preventing overflows.?
                        if ( !this._Pane.DrawArea.Contains( tmpX[i], tmpY[i] ) )
                        {
                            if ( i == 0 )
                            {
                                tmpX[i] = axisArea.X;
                                tmpY[i] = axisArea.Y;
                            }
                            else
                            {
                                tmpX[i] = tmpX[i - 1];
                                tmpY[i] = tmpY[i - 1];
                            }
                        }
                    }
                }
            }

            this.Cord.Draw( graphicsDevice, tmpX, tmpY, this.IgnoreMissing );
            this.Symbol.Draw( graphicsDevice, tmpX, tmpY, this._Pane.ScaleFactor, this.IgnoreMissing );
        }

        /// <summary>
        /// Renders this <see cref="Curve"/> to the specified
        /// <see cref="Graphics"/> device.  This method is normally only
        /// called by the Draw method of the parent <see cref="isr.Visuals.CurveCollection"/>
        /// collection object.  This version is optimized for speed and should be much faster than the
        /// regular Draw() routine.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        private void DrawStripChart( Graphics graphicsDevice )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            var axisArea = this._Pane.AxisArea;
            double scaleFactor = this._Pane.ScaleFactor;
            int numPoints = this.PointCount;
            if ( numPoints == 0 )
            {
                if ( this.Stylus is object )
                {
                    float pixY = 0.5f * (axisArea.Top + axisArea.Bottom);
                    this.Stylus.Draw( graphicsDevice, pixY, axisArea, scaleFactor );
                }

                return;
            }

            // draw the stylus
            if ( this.Stylus is object )
            {
                double y = this.GetClippedTimeSeriesAmplitude();
                float pixY = this.YAxis.Transform( y );
                this.Stylus.Draw( graphicsDevice, pixY, axisArea, scaleFactor );
            }

            // set clip area
            graphicsDevice.SetClip( axisArea );
            float[] tmpX;
            float[] tmpY;
            tmpX = new float[numPoints];
            tmpY = new float[numPoints];

            // get the first data point to draw.
            int j;
            if ( this.UseBookmark )
            {
                j = this.TimeSeriesBookmark.FromPoint.Index;
            }
            else
            {
                j = this._TimeSeriesPointer - numPoints;
                if ( j < 0 )
                {
                    j += this._TimeSeriesCount;
                }
            }

            // set initial x value
            float x = this.XAxis.ScreenScaleRange.Max - numPoints + 1f;

            // set the values from the earliest to the latest value
            for ( int i = 0, loopTo = numPoints - 1; i <= loopTo; i++ )
            {

                // get the pointer to the next data point
                j %= this._TimeSeriesLength;

                // set the X coordinate to map onto the axis range
                tmpX[i] = x;

                // Transform the time series amplitude from user scale units to screen coordinates
                tmpY[i] = this.YAxis.Transform( this._TimeSeries[j].Y );

                // get the pointer to the next data point
                j += 1;

                // increment x position
                x += 1f;
            }

            this.Cord.Draw( graphicsDevice, tmpX, tmpY, this.IgnoreMissing );
            this.Symbol.Draw( graphicsDevice, tmpX, tmpY, this._Pane.ScaleFactor, this.IgnoreMissing );
        }

        /// <summary>
        /// Renders this <see cref="Curve"/> to the specified
        /// <see cref="Graphics"/> device.  This method is normally only
        /// called by the Draw method of the parent <see cref="isr.Visuals.CurveCollection"/>
        /// collection object.  This version is optimized for speed and should be much faster than the
        /// regular Draw() routine.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        public void Draw( Graphics graphicsDevice )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            if ( CurveType.StripChart == this.CurveType )
            {
                this.DrawStripChart( graphicsDevice );
            }
            else
            {
                this.DrawLineChart( graphicsDevice );
            }
        }

        /// <summary>
        /// Checks if the <see cref="X"/> or <see cref="Y"/> data arrays are missing for this
        /// <see cref="Curve"/>.  If so, provide a suitable default array using ordinal values.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void CreateDefaultData()
        {
            if ( CurveType.StripChart == this.CurveType )
            {
            }
            else
            {

                // See if a default X array is required
                if ( this._X is null )
                {
                    // if a Y array is available, just make the same number of elements
                    this._X = this._Y is null ? this.XAxis.MakeDefaultArray() : MakeDefaultArray( this._Y.Length );
                } // see if a default Y array is required

                if ( this._Y is null )
                {
                    // if an X array is available, just make the same number of elements
                    this._Y = this._X is null ? this.YAxis.MakeDefaultArray() : MakeDefaultArray( this._X.Length );
                }
            }
        }

        /// <summary>
        /// Adjusts the <see cref="Pane.AxisArea"/> for the
        /// <see cref="isr.Visuals.Stylus">Stylus</see> size.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="scaleFactor"> The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <param name="axisArea">    The rectangle that contains the area bounded by the axes, in
        /// pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso> </param>
        /// <returns> Adjusted <see cref="Pane.AxisArea"/> </returns>
        internal RectangleF GetAxisArea( double scaleFactor, RectangleF axisArea )
        {

            // Leave room for the stylus
            if ( this.Stylus is object )
            {

                // adjust the stylus 
                return this.Stylus.GetAxisArea( scaleFactor, axisArea );
            }

            // return adjusted axis rectangle
            return axisArea;
        }

        /// <summary> get the range of this curve. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="ignoreInitial"> Affects how initial zero y values are treated for setting the
        /// range of X values.  If True, then initial zero data points are
        /// ignored when determining the X range.  All data after the first
        /// non-zero Y value are included. </param>
        /// <returns> The calculated range. </returns>
        internal PlanarRangeR GetRange( bool ignoreInitial )
        {

            // empty the range
            this._DataRange = PlanarRangeR.Empty;
            if ( CurveType.StripChart == this.CurveType )
            {
                _ = this._DataRange.ExtendRange( new PlanarRangeR( this._DataRange.X, this.GetTimeSeriesAmplitudeRange() ) );
            }
            else
            {

                // generate default arrays of ordinal values if any data arrays are missing
                // 1.0.2118 no way.  This just causes problems.  Me.CheckData()

                // Call the getRange() member function for the current
                // curve to get the min and max values
                _ = this._DataRange.ExtendRange( PlanarRangeR.GetRange( this._X, this._Y, ignoreInitial ) );
            }

            if ( this._DataRange.Equals( PlanarRangeR.Empty ) )
            {
                // Use unit range if no data were available
                return PlanarRangeR.Unity;
            }
            else
            {
                return this._DataRange;
            }
        }

        /// <summary> Returns the time series amplitude clipped to the axis range. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The clipped time series amplitude. </returns>
        private double GetClippedTimeSeriesAmplitude()
        {
            double y = this._TimeSeriesPoint.Y;
            y = Math.Max( y, this.YAxis.Min.Value );
            y = Math.Min( y, this.YAxis.Max.Value );
            return y;
        }

        /// <summary>
        /// gets the amplitude range for the <see cref="isr.Visuals.TimeSeriesPointR">Time Series</see>
        /// data array.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The calculated time series amplitude range. </returns>
        private RangeR GetTimeSeriesAmplitudeRange()
        {

            // return the unit range if no data
            if ( this._TimeSeries is null )
            {
                return RangeR.Unity;
            }

            int numPoints = this.XAxis.ScreenScaleRange is null ? this._TimeSeriesCount : Convert.ToInt32( Math.Min( this.XAxis.ScreenScaleRange.Span, this._TimeSeriesCount ) );
            if ( numPoints == 0 )
            {
                return RangeR.Unity;
            }

            // get the first data point to draw.
            int j = this._TimeSeriesPointer - numPoints;
            if ( j < 0 )
            {
                j += this._TimeSeriesCount;
            }

            double yTemp;
            double yMin;
            double yMax;
            if ( this.UseTimeSeriesTag )
            {

                // initialize arbitrary data just so that we have some range values
                var tempPoint = this._TimeSeries[j];
                bool hasRangePoints = false;
                yTemp = tempPoint.Y;
                yMin = yTemp;
                yMax = yTemp;

                // set the values from the earliest to the latest value
                for ( int i = 0, loopTo = numPoints - 1; i <= loopTo; i++ )
                {

                    // get the pointer to the next data point
                    j %= this._TimeSeriesLength;

                    // Transform the time series amplitude from user scale units to screen coordinates
                    tempPoint = this._TimeSeries[j];

                    // check if we already have range points
                    if ( hasRangePoints )
                    {
                        // if so, check if this is a new range point
                        if ( tempPoint.IsRangePoint )
                        {
                            // if so, update the range
                            yTemp = tempPoint.Y;
                            if ( yTemp < yMin )
                            {
                                yMin = yTemp;
                            }
                            else if ( yTemp > yMax )
                            {
                                yMax = yTemp;
                            }
                        }
                    }
                    // if no range point yet, check if this one is a range point
                    else if ( tempPoint.IsRangePoint )
                    {
                        // if first range point than initialize values
                        hasRangePoints = true;
                        yTemp = tempPoint.Y;
                        yMin = yTemp;
                        yMax = yTemp;
                    }

                    // get the pointer to the next data point
                    j += 1;
                }
            }
            else
            {
                // ignore range points. 
                // initialize the values 
                yTemp = this._TimeSeries[j].Y;
                yMin = yTemp;
                yMax = yTemp;

                // set the values from the earliest to the latest value
                for ( int i = 0, loopTo1 = numPoints - 1; i <= loopTo1; i++ )
                {

                    // get the pointer to the next data point
                    j %= this._TimeSeriesLength;

                    // Transform the time series amplitude from user scale units to screen coordinates
                    yTemp = this._TimeSeries[j].Y;
                    if ( yTemp < yMin )
                    {
                        yMin = yTemp;
                    }
                    else if ( yTemp > yMax )
                    {
                        yMax = yTemp;
                    }

                    // get the pointer to the next data point
                    j += 1;
                }
            }

            return new RangeR( yMin, yMax );
        }

        /// <summary>
        /// gets the time range for the <see cref="isr.Visuals.TimeSeriesPointR">Time Series</see>
        /// data array.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The calculated time series time range. </returns>
        public RangeR GetTimeSeriesTimeRange()
        {

            // return the unit range if no data
            if ( this._TimeSeries is null )
            {
                return RangeR.Unity;
            }

            int numPoints = Convert.ToInt32( Math.Min( this.XAxis.ScreenScaleRange.Span, this._TimeSeriesCount ) );
            if ( numPoints < 2 )
            {
                return RangeR.Unity;
            }

            // get the last data point to draw.
            int j = this._TimeSeriesPointer - 1;
            if ( j < 0 )
            {
                j += this._TimeSeriesCount;
            }

            // get the first data point to draw.
            int i = this._TimeSeriesPointer - numPoints;
            if ( i < 0 )
            {
                i += this._TimeSeriesCount;
            }

            // get time scale in seconds since 0001-01-01
            double startTime = this._TimeSeries[i].Seconds;
            double endTime = this._TimeSeries[j].Seconds;
            double timeRange = endTime - startTime;
            if ( this.XAxis.ScreenScaleRange.Span > this._TimeSeriesCount )
            {

                // if we have not collected a full range, extrapolate the range from the
                // given data.
                timeRange *= this.XAxis.ScreenScaleRange.Span / this._TimeSeriesCount;
                return new RangeR( endTime - timeRange, endTime );
            }
            else
            {

                // if we have collected all data than the range is determined by
                // the first and last data points
                return new RangeR( startTime, endTime );
            }
        }

        /// <summary> generate a default array of ordinal values. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="length"> The number of values to generate. </param>
        /// <returns> a floating point double type array of default ordinal values. </returns>
        public static double[] MakeDefaultArray( int length )
        {
            var defaultArray = new double[length + 1];
            for ( int i = 0, loopTo = length - 1; i <= loopTo; i++ )
                defaultArray[i] = i + 1.0d;
            return defaultArray;
        }

        /// <summary>
        /// Pans the time series to display the onset of the book mark at the beginning of the time
        /// series chart.  If the time series has fewer points than the screen range, the last points of
        /// the time series will show time earlier than the first points reflecting the circular nature
        /// of the time series.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="bookmark">       The new <see cref="isr.Visuals.TimeSeriesBookmark">time series
        /// book mark</see> </param>
        /// <param name="indexLeftShift"> The left shift to add to the pan. </param>
        public void PanTimeSeries( TimeSeriesBookmark bookmark, int indexLeftShift )
        {

            // adjust the time series pointer to display the Bookmark at the beginning
            // of the current chart span.
            this._TimeSeriesPointer = bookmark.FromPoint.Index + this.PointCount - 1 - indexLeftShift;
            // Me._timeSeriesPointer = Bookmark.FromPoint.Index + Convert.ToInt32(Me._xAxis.ScreenScaleRange.Range) - 1 - indexLeftShift
            // If Bookmark.FromPoint.Index < Me._timeSeriesPointerKeeper Then
            // Me._timeSeriesPointer = Math.Min(Me._timeSeriesPointer, Me._timeSeriesPointerKeeper)
            // End If
            if ( this._TimeSeriesPointer >= this._TimeSeriesLength )
            {
                this._TimeSeriesPointer -= this._TimeSeriesLength;
            }
        }

        /// <summary>
        /// Restores the time series time reference.  This should be done after pan and zoom operations.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void RestoreTimeSeries()
        {
            this._TimeSeriesPointer = this._TimeSeriesPointerKeeper;
        }

        /// <summary>
        /// Updates the time series curve data and adjusts the amplitude and time ranges.
        /// </summary>
        /// <remarks>
        /// Use this method to add a new value to the strip chart.  Shifts the buffer one notch every
        /// time a new data points comes in thus 'scrolling' the data along.
        /// </remarks>
        public void ScaleTimeSeriesAxis()
        {
            if ( this.XAxis.CoordinateScale.CoordinateScaleType == CoordinateScaleType.StripChart )
            {
                // this can only be done when drawing!
                // set major tick locations and values
                this.XAxis.MajorTick.SetLocations( this.XAxis );
                this.XAxis.MajorTick.SetValues( this.XAxis, this._TimeSeriesPointer, this._TimeSeries );
            }
        }

        /// <summary> Sets the Cartesian X, Y arrays for the given time series indexes. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="fromIndex"> The starting index. </param>
        /// <param name="toIndex">   The ending index. </param>
        public void SetCartesianTimeSeries( int fromIndex, int toIndex )
        {
            if ( fromIndex < 0 )
            {
                fromIndex += this._TimeSeriesLength;
            }

            if ( toIndex < 0 )
            {
                toIndex += this._TimeSeriesLength;
            }

            int length = toIndex - fromIndex + 1;
            if ( length <= 0 )
            {
                length += this._TimeSeriesLength;
            }

            this._X = new double[length];
            this._Y = new double[length];
            for ( int i = 0, loopTo = length - 1; i <= loopTo; i++ )
            {
                this._X[i] = this._TimeSeries[fromIndex].Seconds;
                this._Y[i] = this._TimeSeries[fromIndex].Y;
                fromIndex += 1;
                if ( fromIndex >= this._TimeSeriesLength )
                {
                    fromIndex = 0;
                }
            }
        }

        /// <summary> Updates a new time series point without adding it to the time series. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="timeSeriesPoint"> The <see cref="isr.Visuals.TimeSeriesPointR">time series data
        /// point</see> </param>
        /// <returns>
        /// A <see cref="isr.Visuals.TimeSeriesPointR">time series data point</see>
        /// updated with current time series information including the time series index.
        /// </returns>
        public TimeSeriesPointR UpdateDataPoint( TimeSeriesPointR timeSeriesPoint )
        {

            // update the time series saved point
            this._TimeSeriesPoint = timeSeriesPoint;

            // record the time series index
            this._TimeSeriesPoint.Index = this._TimeSeriesPointer;

            // return the time series point
            return this._TimeSeriesPoint;
        }

        /// <summary> Updates the references to the X, Y data arrays. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x"> A array of <see cref="System.Double">Double Precision</see> values that
        /// define the independent (X axis) values for this curve. </param>
        /// <param name="y"> A array of <see cref="System.Double">Double Precision</see> values that
        /// define the dependent (Y axis) values for this curve. </param>
        public void UpdateData( double[] x, double[] y )
        {
            this._X = x;
            this._Y = y;

            // set the point count so that it would be ignored
            this._PointCount = _IgnorePointCount;
        }

        /// <summary>
        /// Updates the references to the X, Y data arrays and sets the point count which to plot.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x">          A array of <see cref="System.Double">Double Precision</see> values
        /// that define the independent (X axis) values for this curve. </param>
        /// <param name="y">          A array of <see cref="System.Double">Double Precision</see> values
        /// that define the dependent (Y axis) values for this curve. </param>
        /// <param name="pointCount"> The number of points to plot. </param>
        public void UpdateData( double[] x, double[] y, int pointCount )
        {
            this._X = x;
            this._Y = y;
            this._PointCount = pointCount;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// gets a reference to the <see cref="isr.Visuals.Cord"/> class defined for this
        /// <see cref="Curve"/>.
        /// </summary>
        /// <value> The cord. </value>
        public Cord Cord { get; private set; }

        /// <summary>
        /// Gets the missing value mode for drawing graphs.  When true, graphs are drawing assuming all
        /// values are valid.  This is important for quick graphics.
        /// </summary>
        /// <value> A <see cref="System.Boolean">True</see> </value>
        public bool IgnoreMissing { get; set; }

        /// <summary>
        /// Gets the condition for the curve aught to use time series tag for such methods as fixing its
        /// range.
        /// </summary>
        /// <value> The use time series tag. </value>
        public bool UseTimeSeriesTag { get; set; }

        /// <summary> Gets the book mark drawing option. </summary>
        /// <value> The use bookmark. </value>
        public bool UseBookmark { get; set; }

        /// <summary>
        /// Determines if this <see cref="Curve"/> is assigned to the
        /// <see cref="AxisType.Y2"/>.
        /// </summary>
        /// <value>
        /// True if the curve is assigned to the <see cref="isr.Visuals.AxisType.Y2"/>, False is the
        /// curve is assigned to the <see cref="isr.Visuals.AxisType.Y"/>
        /// </value>
        public bool IsY2Axis => this.YAxis.AxisType == AxisType.Y2;

        /// <summary>
        /// A <see cref="System.String">String</see> that represents the <see cref="isr.Visuals.Legend"/>
        /// entry for the this
        /// <see cref="Curve"/> object.
        /// </summary>
        /// <value> The label. </value>
        public string Label { get; set; }

        /// <summary>Gets or sets the value telling the curve use array size when
        /// plotting.</summary>
        private const int _IgnorePointCount = -1;

        /// <summary> Number of points. </summary>
        private int _PointCount = _IgnorePointCount;

        /// <summary>
        /// Gets the number of points that define this <see cref="Curve"/>. Returns the internal point
        /// count cache if it is non-negative.  Otherwise the number of points in the <see cref="X"/> and
        /// <see cref="Y"/> data arrays is returned.
        /// </summary>
        /// <value> The number of points. </value>
        public int PointCount => CurveType.StripChart == this.CurveType ? Convert.ToInt32( Math.Min( this.XAxis.ScreenScaleRange.Span, this._TimeSeriesCount ) ) : this._PointCount != _IgnorePointCount ? this._PointCount : this._X is null || this._Y is null ? 0 : Convert.ToInt32( Math.Min( this._X.Length, this._Y.Length ) );

        /// <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
        private readonly Pane _Pane;

        /// <summary> The data range. </summary>
        private PlanarRangeR _DataRange = PlanarRangeR.Empty;

        /// <summary>
        /// Returns the data range calculated when updating strip chart data or with
        /// <see cref="M:getRange"/>.  With time series only the amplitude range is valid.
        /// </summary>
        /// <value> A <see cref="isr.Visuals.PlanarRangeR">range</see> value. </value>
        public PlanarRangeR DataRange => this._DataRange;

        /// <summary> Gets the one-based serial order of the curve in the group. </summary>
        /// <value> An <see cref="T:System.Integer">integer</see> value. </value>
        public int SerialNumber { get; set; }

        /// <summary> Gets the strip chart <see cref="isr.Visuals.Stylus">Stylus</see> </summary>
        /// <value> A reference to the strip chart <see cref="isr.Visuals.Stylus">Stylus</see> </value>
        public Stylus Stylus { get; private set; }

        /// <summary> Gets the status message. </summary>
        /// <value> A System.String value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusMessage { get; set; }

        /// <summary>
        /// gets a reference to the <see cref="isr.Visuals.Symbol"/> class defined for this
        /// <see cref="Curve"/>.
        /// </summary>
        /// <value> The symbol. </value>
        public Symbol Symbol { get; private set; }

        /// <summary> The time series. </summary>
        private TimeSeriesPointR[] _TimeSeries;

        /// <summary>
        /// Gets the current book mark to use for drawing if
        /// <see cref="P:UseBookmark"/> is set.
        /// </summary>
        /// <value> A <see cref="TimeSeriesBookmark">time series book mark</see> value. </value>
        public TimeSeriesBookmark TimeSeriesBookmark { get; set; }

        /// <summary> Gets the book marks collection for a time series curve. </summary>
        /// <value> The time series bookmarks. </value>
        public TimeSeriesBookmarkCollection TimeSeriesBookmarks { get; private set; }

        /// <summary>Gets or sets the number of points collected since the last reset</summary>
        private int _TimeSeriesCount;

        /// <summary> Length of the time series. </summary>
        private int _TimeSeriesLength;

        /// <summary>
        /// Gets or sets the length of the time series.  This value is set automatically to at least the
        /// width of the active screen area.
        /// </summary>
        /// <value> The length of the time series. </value>
        public int TimeSeriesLength
        {
            get => this._TimeSeriesLength;

            set => this._TimeSeriesLength = Math.Max( Screen.PrimaryScreen.WorkingArea.Width, value );
        }

        /// <summary>Gets or sets the current time timer series pointer during pan and
        /// zoom operations.  This value is refreshed only when resetting or adding
        /// time series points thus keeping the time-series point
        /// irrespective of changes to the operational time series points.</summary>
        private int _TimeSeriesPointerKeeper;

        /// <summary>Gets or sets the pointer to the first time series data point.</summary>
        private int _TimeSeriesPointer;

        /// <summary> The time series point. </summary>
        private TimeSeriesPointR _TimeSeriesPoint;

        /// <summary> Gets the last data point added to the time series. </summary>
        /// <value> A <see cref="isr.Visuals.TimeSeriesPointR">TimeSeriesPointF</see> value. </value>
        public TimeSeriesPointR TimeSeriesPoint => this._TimeSeriesPoint;

        /// <summary> Determines how this curve is drawn. </summary>
        /// <remarks> David, 2007-10-15, 1.0.2844. Rename to CurveType. </remarks>
        /// <value> A <see cref="Visuals.CurveType"/> value. </value>
        public CurveType CurveType { get; set; }

        /// <summary> The x coordinate. </summary>
        private double[] _X = Array.Empty<double>();

        /// <summary>
        /// Returns the array of independent (X Axis) values that define this
        /// <see cref="Curve"/>. The size of this array determines the number of points
        /// that are plotted.  <see cref="System.Double.MaxValue"/> values are considered "missing"
        /// values, and are not plotted.  The curve will have a break at these points to indicate values
        /// are missing.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A Double() </returns>
        public double[] X()
        {
            return this._X;
        }

        /// <summary> The y coordinate. </summary>
        private double[] _Y = Array.Empty<double>();

        /// <summary>
        /// Returns the array of dependent (Y Axis) values that define this
        /// <see cref="Curve"/>. The size of this array determines the number of points
        /// that are plotted.  Note that values defined as System.Double.maxValue are considered
        /// "missing" values, and are not plotted.  The curve will have a break at these points to
        /// indicate values are missing.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A Double() </returns>
        public double[] Y()
        {
            return this._Y;
        }

        /// <summary>
        /// Gets or sets a reference to the horizontal <see cref="Axis"/> associated with this curve.
        /// </summary>
        /// <value> An <see cref="Axis"/> reference. </value>
        public Axis XAxis { get; set; }

        /// <summary>
        /// Gets or sets a reference to the vertical <see cref="Axis"/> associated with this curve.
        /// </summary>
        /// <value> An <see cref="Axis"/> reference. </value>
        public Axis YAxis { get; set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="Curve"/> class that defines the default property values
    /// for the <see cref="Curve"/> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class CurveDefaults
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private CurveDefaults() : base()
        {
            this.CurveType = CurveType.XY;
            this.IgnoreMissing = true;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static CurveDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static CurveDefaults Get()
        {
            if ( _Instance is null )
            {
                lock ( SyncLocker )
                    _Instance = new CurveDefaults();
            }

            return _Instance;
        }

        #endregion

        /// <summary> Default value for the curve type property (<see cref="CurveType"/>). </summary>
        /// <value> A <see cref="CurveType"/> value. </value>
        public CurveType CurveType { get; set; }

        /// <summary> Default value for the way the curve handles missing values. </summary>
        /// <value> A <see cref="Curve.IgnoreMissing"/> property. </value>
        public bool IgnoreMissing { get; set; }
    }
}

#endregion



