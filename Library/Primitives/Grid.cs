using System;
using System.ComponentModel;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary> Provides grid properties. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class Grid : ICloneable, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs an instance of this class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>

        // instantiate the base class
        public Grid() : base()
        {
            var gridDefaults = GridDefaults.Get();
            this.DashOff = gridDefaults.DashOff;
            this.DashOn = gridDefaults.DashOn;
            this.Visible = gridDefaults.Visible;
            this.LineColor = gridDefaults.LineColor;
            this.LineWidth = gridDefaults.LineWidth;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The grid object from which to copy. </param>
        public Grid( Grid model ) : this()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.DashOff = model.DashOff;
            this.DashOn = model.DashOn;
            this.Visible = model.Visible;
            this.LineColor = model.LineColor;
            this.LineWidth = model.LineWidth;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of grid. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the grid. </returns>
        public Grid Copy()
        {
            return new Grid( this );
        }

        /// <summary> Draw the grid. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="tick">           Reference to the axis <see cref="isr.Visuals.Tick">tick
        /// specification</see>. </param>
        /// <param name="topPix">         The pixel location of the far side of the Axis Area from this
        /// axis. This value is the axisArea.Height for the XAxis, or the
        /// axisArea.Width for the YAxis and Y2Axis. </param>
        public void Draw( Graphics graphicsDevice, Tick tick, float topPix )
        {
            if ( graphicsDevice is null )
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            if ( graphicsDevice is null )
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            if ( tick is null )
                throw new ArgumentNullException( nameof( tick ) );
            using var gridPen = new Pen( this.LineColor, this.LineWidth );
            gridPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Custom;
            var pattern = new float[2];
            pattern[0] = this.DashOn;
            pattern[1] = this.DashOff;
            gridPen.DashPattern = pattern;

            // loop for each major tick
            float x;
            for ( int i = 1, loopTo = tick.ValidTickCount - 1; i <= loopTo; i++ )
            {
                // draw the grid
                if ( this.Visible )
                {
                    x = tick.GetLocation( i );
                    graphicsDevice.DrawLine( gridPen, x, 0.0f, x, topPix );
                }
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// The "Dash Off" mode for drawing the grid.  This is the distance, in pixels, of the spaces
        /// between the dash segments that make up the dashed grid lines.
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        /// <seealso cref="DashOn"/>
        /// <seealso cref="Visible"/>
        public float DashOff { get; set; }

        /// <summary>
        /// The "Dash On" mode for drawing the grid.  This is the distance, in pixels, of the dash
        /// segments that make up the dashed grid lines.
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        /// <seealso cref="DashOff"/>
        /// <seealso cref="Visible"/>
        public float DashOn { get; set; }

        /// <summary>
        /// Determines if the major <see cref="Axis"/> grid lines (at each labeled value) will be shown.
        /// </summary>
        /// <value> True to show the grid lines, false otherwise. </value>
        public bool Visible { get; set; }

        /// <summary>
        /// The color to use for drawing this <see cref="Axis"/> grid.  This affects only the grid lines,
        /// since the <see cref="Title.Appearance"/> and
        /// <see cref="isr.Visuals.Labels.Appearance"/> both have their own color specification.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color LineColor { get; set; }

        /// <summary> Gets or sets the default pen width used for drawing the grid lines. </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        /// <seealso cref="Visible"/>
        public float LineWidth { get; set; }

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A System.String value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusMessage { get; set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="Grid"/> class that defines the default property values
    /// for the <see cref="Grid"/> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class GridDefaults
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private GridDefaults() : base()
        {
            this.DashOn = 1.0f;
            this.DashOff = 5.0f;
            this.LineWidth = 1.0f;
            this.LineColor = Color.Black;
            this.Visible = true;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static GridDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static GridDefaults Get()
        {
            if ( _Instance is null )
            {
                lock ( SyncLocker )
                    _Instance = new GridDefaults();
            }

            return _Instance;
        }

        #endregion

        /// <summary>
        /// Gets or sets the default "dash on" size for drawing the <see cref="Axis"/> grid
        /// (<see cref="Grid.DashOn"/> property).
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float DashOn { get; set; }

        /// <summary>
        /// Gets or sets the default "dash off" size for drawing the <see cref="Axis"/> grid
        /// (<see cref="Grid.DashOff"/> property).
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float DashOff { get; set; }

        /// <summary>
        /// Gets or sets the default pen width for drawing the <see cref="Axis"/> grid
        /// (<see cref="Grid.LineWidth"/> property).
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float LineWidth { get; set; }

        /// <summary>
        /// Gets or sets the default color for the <see cref="Axis"/> grid lines
        /// (<see cref="Axis.LineColor"/> property).  This color only affects the grid lines.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color LineColor { get; set; }

        /// <summary>
        /// Gets or sets the default display mode for the <see cref="Axis"/> grid lines
        /// (<see cref="Grid.Visible"/> property). True to show the grid lines, false to hide them.
        /// </summary>
        /// <value> The visible. </value>
        public bool Visible { get; set; }
    }
}

#endregion

