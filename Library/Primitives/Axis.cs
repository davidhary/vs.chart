using System;
using System.Diagnostics;
using System.Drawing;

using isr.Core.Constructs;

namespace isr.Visuals
{

    /// <summary> Defines a graph Axis. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class Axis : IDisposable
    {

        // update: Axis
        // add AxisPoint to specify the point on the perpendicular AxisArea location of the axis
        // allow to add multiple X axis like multiple Y axis
        // each curve is associated with a pair of X,Y axis
        // eventually we should be able to draw axis for each curve if not
        // the same as other axes.
        // Allow to draw axis tick marks at AxisPoint (default inside, outside)
        // update: check why Y axis is off one pixel

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs an <see cref="Axis"/> with specific title and type and default properties from the
        /// <see cref="AxisDefaults"/> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="title">       The <see cref="Title"/> caption for this axis. </param>
        /// <param name="type">        The <see cref="AxisType"/> for this axis. </param>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public Axis( string title, AxisType type, Pane drawingPane ) : base()
        {
            this.StatusMessage = string.Empty;
            if ( string.IsNullOrWhiteSpace( title ) )
            {
                title = string.Empty;
            }

            if ( drawingPane is null )
            {
                throw new ArgumentNullException( nameof( drawingPane ) );
            }

            this._Pane = drawingPane;
            this.MajorTick = new Tick( true );
            this.MinorTick = new Tick( false );
            this.Grid = new Grid();
            this.TickLabels = new Labels( this._Pane );
            this.Title = new AxisTitle( title, this._Pane );
            var scaleDefaults = ScaleDefaults.Get();
            this.Visible = scaleDefaults.Visible;
            this.Max = scaleDefaults.Max.Copy();
            this.Min = scaleDefaults.Min.Copy();
            this.ScaleExponent = scaleDefaults.ScaleExponent.Copy();
            this.CoordinateScale = new CoordinateScale() { CoordinateScaleType = scaleDefaults.CoordinateScale };
            this.Reversed = scaleDefaults.Reversed;
            this.Visible = true;
            this.LineColor = AxisDefaults.Get().LineColor;
            this.AxisType = type;
            switch ( this.AxisType )
            {
                case AxisType.X:
                    {
                        this.Title.Appearance.Angle = 0.0f;
                        this.TickLabels.Appearance.Angle = 0.0f;
                        break;
                    }

                case AxisType.Y:
                    {
                        this.Title.Appearance.Angle = -180.0f;
                        this.TickLabels.Appearance.Angle = 90.0f;
                        break;
                    }

                case AxisType.Y2:
                    {
                        this.Title.Appearance.Angle = 0.0f;
                        this.TickLabels.Appearance.Angle = -90.0f;
                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "Unhandled axis type" );
                        break;
                    }
            }
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Axis object from which to copy. </param>
        protected Axis( Axis model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.AxisType = model.AxisType;
            this.Visible = model.Visible;
            this.Reversed = model.Reversed;
            this.Min = model.Min;
            this.Max = model.Max;
            this.CoordinateScale = model.CoordinateScale;
            this.ScaleExponent = model.ScaleExponent;
            this.Grid = model.Grid.Copy();
            this.MajorTick = model.MajorTick.Copy();
            this.MinorTick = model.MinorTick.Copy();
            this.TickLabels = model.TickLabels.Copy();
            this.Title = model.Title.Copy();
            this.Visible = model.Visible;
            this.LineColor = model.LineColor;
            this._Pane = model._Pane;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        this.LineColor = default;
                        if ( this.Grid is object )
                        {
                            this.Grid.Dispose();
                            this.Grid = null;
                        }

                        if ( this.MajorTick is object )
                        {
                            this.MajorTick.Dispose();
                            this.MajorTick = null;
                        }

                        if ( this.MinorTick is object )
                        {
                            this.MinorTick.Dispose();
                            this.MinorTick = null;
                        }

                        if ( this.TickLabels is object )
                        {
                            this.TickLabels.Dispose();
                            this.TickLabels = null;
                        }

                        if ( this.Title is object )
                        {
                            this.Title.Dispose();
                            this.Title = null;
                        }

                        this.Max = null;
                        this.Min = null;
                        this.ScaleExponent = null;
                    }

                    // Free shared unmanaged resources
                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary>
        /// Renders the <see cref="Axis"/> to the specified <see cref="Graphics"/>
        /// device.  This method is normally only called by the Draw method of the parent
        /// <see cref="Pane"/> object.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        public void Draw( Graphics graphicsDevice )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            var axisArea = this._Pane.AxisArea;
            double scaleFactor = this._Pane.ScaleFactor;
            if ( this.Min.Value < this.Max.Value )
            {

                // save the transform matrix
                var saveMatrix = graphicsDevice.Transform;
                this.SetTransformMatrix( graphicsDevice, axisArea );
                if ( this.Valid )
                {
                    if ( this.Visible )
                    {

                        // draw the axis at the bottom
                        // upgrade: draw to defined axis point.
                        using var p = new Pen( this.MajorTick.LineColor, this.MajorTick.LineWidth );
                        graphicsDevice.DrawLine( p, this._TransformedArea.Left, this._TransformedArea.Bottom, this._TransformedArea.Right, this._TransformedArea.Bottom );
                    }

                    if ( this.MajorTick.Visible || this.MinorTick.Visible || this.TickLabels.Visible || this.Grid.Visible )
                    {
                        if ( this.CoordinateScale.CoordinateScaleType != CoordinateScaleType.StripChart )
                        {
                            // set the major tick locations
                            this.MajorTick.SetLocations( this );

                            // set the minor tick locations
                            this.MinorTick.SetLocations( this );
                        }

                        // draw the major ticks
                        this.MajorTick.Draw( graphicsDevice, this, scaleFactor );

                        // draw minor ticks
                        this.MinorTick.Draw( graphicsDevice, this, scaleFactor );

                        // draw the grid
                        this.Grid.Draw( graphicsDevice, this.MajorTick, this._TransformedArea.Top );

                        // draw ticks labels 
                        this.TickLabels.Draw( graphicsDevice, this, this.MajorTick, scaleFactor );
                    }
                }

                this.DrawTitle( graphicsDevice, scaleFactor );

                // restore the transform matrix
                graphicsDevice.Transform = saveMatrix;
            }
        }

        /// <summary> Get the range of the <see cref="Axis"/> for date scale. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The calculated date range. </returns>
        public RangeDateTime GetDateRange()
        {
            return new RangeDateTime( JulianDate.ToDateTime( this.Min.Value ), JulianDate.ToDateTime( this.Max.Value ) );
        }

        /// <summary>
        /// Calculate the space required for this <see cref="Axis"/>
        /// object.  This is the space between the drawArea and the <see cref="Pane.AxisArea"/> for this
        /// particular axis.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <returns> A <see cref="System.Drawing.SizeF">SizeF</see> in pixels. </returns>
        internal SizeF GetSpace( Graphics graphicsDevice, double scaleFactor )
        {

            // axisArea is the actual area of the plot as bounded by the axes
            float width = 0f;
            float height = 0f;
            var space = default( float );
            SizeF spaceSize;
            if ( !this.Visible )
            {
                return new SizeF( width, height );
            }

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // Account for the Axis
            if ( this.MajorTick.Visible )
            {
                // get space required for ticks
                space = this.MajorTick.GetScaledLength( scaleFactor );
            }
            else if ( this.MinorTick.Visible )
            {
                // get space required for ticks
                space = this.MinorTick.GetScaledLength( scaleFactor );
            }

            if ( this.Horizontal )
            {
                height += space;
            }
            else
            {
                width += space;
            }

            if ( this.TickLabels.Visible && !this.CoordinateScale.IsStripChart )
            {

                // set bounding box for text labels
                this.TickLabels.SetBoundingBox( graphicsDevice, this, scaleFactor );

                // get space required for tick labels
                spaceSize = this.TickLabels.BoundingBox;
                if ( this.Horizontal )
                {
                    height += spaceSize.Height;
                    width += spaceSize.Width / 2f;
                }
                else
                {
                    // the Y axis size returns rotated size.
                    height += spaceSize.Width / 2f;
                    width += spaceSize.Height;
                }
            }

            if ( this.Title.Visible )
            {
                // Add space for the axis title
                spaceSize = this.Title.MeasureString( graphicsDevice, scaleFactor );
                if ( this.Horizontal )
                {
                    height += spaceSize.Height;
                }
                else
                {
                    width += spaceSize.Height;
                }
            }

            return new SizeF( width, height );
        }

        /// <summary>
        /// Increments the screen positions for drawing time series major and minor tick marks and grid
        /// lines.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        internal void IncrementTickOrigin()
        {
            this.MinorTick.IncrementOrigin();
            this.MajorTick.IncrementOrigin();
        }

        /// <summary>
        /// Reverse transform the user coordinates (scale value)
        /// given a graphics device coordinate (pixels).  This method takes into account the scale range
        /// (<see cref="Min"/> and <see cref="Max"/>), logarithmic state
        /// (<see cref="CoordinateScale.IsLog"/>), scale reverse state (<see cref="Reversed"/>) and axis
        /// type (<see cref="AxisType.X"/>,
        /// <see cref="AxisType.Y"/>, or <see cref="AxisType.Y2"/>).  Note that
        /// screen range must be set before calling this method.  This is best done every time the axis
        /// is drawn (<see cref="Axis.Draw"/>.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="screenValue"> The screen pixel value, in graphics device coordinates to
        /// transform. </param>
        /// <returns>
        /// A <see cref="System.Single">Single</see> user scale value that corresponds to a screen pixel
        /// location.
        /// </returns>
        public double InverseTransform( float screenValue )
        {
            double userValue = this.UserScaleRange.Span / this.ScreenScaleRange.Span;

            // see if the sign of the equation needs to be reversed
            if ( this.Reversed == this.Horizontal )
            {
                userValue *= this.ScreenScaleRange.Max - screenValue;
            }
            else
            {
                userValue *= screenValue - this.ScreenScaleRange.Min;
            }

            userValue += this.UserScaleRange.Min;
            if ( this.CoordinateScale.IsLog )
            {
                userValue = Math.Pow( 10.0d, userValue );
            }

            return userValue;
        }

        /// <summary>
        /// Transform the coordinate value from user coordinates (scale value)
        /// to graphics device coordinates (pixels), assuming that the origin has been set to the "left"
        /// of this axis, facing from the label side. Note that the left side corresponds to the scale
        /// minimum for the X and Y2 axes, but it is the scale maximum for the Y axis. This method takes
        /// into account the scale range (<see cref="Min"/> and
        /// <see cref="Max"/>), scale type (<see cref="CoordinateScale.IsLog"/>), scale reverse state
        /// (<see cref="Reversed"/>) and axis type (<see cref="AxisType.X"/>,
        /// <see cref="AxisType.Y"/>, or <see cref="AxisType.Y2"/>).  Note that
        /// the axis screen range must set before calling this method.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x"> The coordinate value, in user scale units, to be transformed. </param>
        /// <returns>
        /// A <see cref="System.Single">Single</see> value transformed to screen space for use in calling
        /// the <see cref="Draw"/> sub-methods.
        /// </returns>
        internal float LocalTransform( double x )
        {

            // Must take into account Log, and Reverse Axes
            double ratio;
            float rv;
            ratio = this.CoordinateScale.IsLog ? (x.Log10() - this.UserScaleRange.Min) / this.UserScaleRange.Span : (x - this.UserScaleRange.Min) / this.UserScaleRange.Span;
            rv = this.Reversed && this.AxisType != AxisType.Y || !this.Reversed && this.AxisType == AxisType.Y ? Convert.ToSingle( this.ScreenScaleRange.Span * (1.0d - ratio) ) : Convert.ToSingle( this.ScreenScaleRange.Span * ratio );
            return rv;
        }

        /// <summary>
        /// Create an array of default values for a curve associated with this axis.
        /// The default values are simple ordinals based on the number of axis labels.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> An ordinal <see cref="System.Double">Double</see> array. </returns>
        public double[] MakeDefaultArray()
        {
            int length = 10;
            if ( this.CoordinateScale.IsText && this.TickLabels.TextLabels() is object )
            {
                length = this.TickLabels.TextLabels().Length;
            }

            var defaultArray = new double[length + 1];
            int i;
            var loopTo = length - 1;
            for ( i = 0; i <= loopTo; i++ )
                defaultArray[i] = i;
            return defaultArray;
        }

        /// <summary>
        /// Use existing range to set tick spacing, decimal spaces and scale magnitude multiplier.
        /// </summary>
        /// <remarks> Use this method to rescale after setting the range manually. </remarks>
        public void Rescale()
        {

            // call rescale with existing values
            this.Rescale( this.Min.Value, this.Max.Value );
        }

        /// <summary>
        /// Select a scale range for the data.  The scale auto ranges and sets the tick spacing, decimal
        /// spaces and scale magnitude multiplier as necessary.
        /// The scaling also adjusts the minimum and maximum based on zero level.
        /// <para>On Exit:</para>
        /// <para><see cref="Min"/> is set to scale minimum (if
        /// <see cref="isr.Visuals.AutoValueR.AutoScale"/> = True)</para>
        /// <para><see cref="Max"/> is set to scale maximum (if
        /// <see cref="isr.Visuals.AutoValueR.AutoScale"/> = True)</para>
        /// <para><see cref="MajorTick"/> is set (if <see cref="isr.Visuals.AutoValueR.AutoScale"/> =
        /// True)</para>
        /// <para><see cref="MinorTick"/> is set (if <see cref="isr.Visuals.AutoValueR.AutoScale"/> =
        /// True)</para>
        /// <para><see cref="ScaleExponent"/> is set</para>
        /// <para><see cref="Labels.DecimalPlaces"/> is set if
        /// <see cref="isr.Visuals.AutoValueR.AutoScale"/> = True)</para>
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="minValue"> The minimum value of the data range for setting this
        /// <see cref="Axis"/> scale range. </param>
        /// <param name="maxValue"> The maximum value of the data range for setting this
        /// <see cref="Axis"/> scale range. </param>
        public void Rescale( double minValue, double maxValue )
        {

            // set initial range
            this.SetInitialUserScaleRange( minValue, maxValue );

            // adjust the scale range for the major spacing
            this.SetFinalUserScaleRange();
            if ( this.TickLabels.Visible )
            {

                // Set the labels scale format (valid for date type only)
                this.TickLabels.ScaleFormat = this.MajorTick.ScaleFormat;

                // set the label decimal places
                this.TickLabels.SetDecimalPlaces( this, this.MajorTick.Spacing, this.ScaleExponent.Value, this.CoordinateScale );
            }
        }

        /// <summary> Set the range of the <see cref="Axis"/> for date scale. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="fromDate"> Starting <see cref="System.DateTime">DateTime</see> </param>
        /// <param name="toDate">   Ending <see cref="System.DateTime">DateTime</see> </param>
        public void SetDateRange( DateTime fromDate, DateTime toDate )
        {
            this.Min.Value = JulianDate.FromDateTime( fromDate );
            this.Max.Value = JulianDate.FromDateTime( toDate );
        }

        /// <summary> Set the range of the <see cref="Axis"/> for date scale. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="fromDate"> Starting <see cref="System.DateTimeOffset">DateTime</see> </param>
        /// <param name="toDate">   Ending <see cref="System.DateTimeOffset">DateTime</see> </param>
        public void SetDateRange( DateTimeOffset fromDate, DateTimeOffset toDate )
        {
            this.SetDateRange( fromDate.DateTime, toDate.DateTime );
        }

        /// <summary>
        /// Sets the axis scale to specified range.  Rescale can be called later to adjust the tick
        /// spacing and tick locations.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="minValue"> The minimum value of the data range for setting this
        /// <see cref="Axis"/> scale range. </param>
        /// <param name="maxValue"> The maximum value of the data range for setting this. </param>
        public void SetRange( double minValue, double maxValue )
        {

            // set the range
            this.Min.Value = minValue;
            this.Max.Value = maxValue;
        }

        /// <summary> Sets the screen range for rendering the <see cref="Axis"/>. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="axisArea"> The <see cref="System.Drawing.RectangleF"/> that that contains the
        /// area bounded by the axes. </param>
        internal void SetScreenRange( RectangleF axisArea )
        {

            // save the <see cref="Pane.AxisArea"/> data for transforming scale values to pixels
            this.ScreenScaleRange = this.Horizontal ? new RangeF( axisArea.Left, axisArea.Right ) : new RangeF( axisArea.Top, axisArea.Bottom );
        }

        /// <summary>
        /// Transform the coordinate value from user coordinates (scale value)
        /// to graphics device coordinates (pixels).  This method takes into account the scale range
        /// (<see cref="Min"/> and <see cref="Max"/>), scale type (<see cref="CoordinateScale.IsLog"/>),
        /// scale reverse state (<see cref="Reversed"/>)
        /// and axis type (<see cref="AxisType.X"/>, <see cref="AxisType.Y"/>, or
        /// <see cref="AxisType.Y2"/>).
        /// Note that the screen ranges must be set before using this method.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x"> The coordinate value, in user scale units, to be transformed. </param>
        /// <returns>
        /// A <see cref="System.Single">Single</see> coordinate value transformed to screen space for use
        /// in the <see cref="Graphics"/> draw routines.
        /// </returns>
        public float Transform( double x )
        {

            // Must take into account Log, and Reverse Axes
            double ratio = this.CoordinateScale.IsLog ? (x.Log10() - this.UserScaleRange.Min) / this.UserScaleRange.Span : (x - this.UserScaleRange.Min) / this.UserScaleRange.Span;
            double offset = this.ScreenScaleRange.Span * ratio;
            if ( this.Reversed && this.Horizontal )
            {
                return Convert.ToSingle( this.ScreenScaleRange.Max - offset );
            }
            else
            {
                return this.Horizontal
                    ? Convert.ToSingle( this.ScreenScaleRange.Min + offset )
                    : this.Reversed ? Convert.ToSingle( this.ScreenScaleRange.Min + offset ) : Convert.ToSingle( this.ScreenScaleRange.Max - offset );
            }
        }

        #endregion

        #region " PRIVATE  and  PROTECTED "

        /// <summary>
        /// Draw the title for this <see cref="Axis"/>.  On entry, it is assumed that the graphics
        /// transform has been configured so that the origin is at the left side of this axis, and the
        /// axis is aligned along the X coordinate direction.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        private void DrawTitle( Graphics graphicsDevice, double scaleFactor )
        {
            if ( !this.Visible )
            {
                return;
            }

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            string str = this.Title.ToString( this.ScaleExponent.Value );

            // If the Axis is visible, draw the title
            if ( str.Length > 0 )
            {

                // Calculate the title position in screen coordinates
                float x = this.ScreenScaleRange.Span / 2f;
                float y = this.MajorTick.GetScaledLength( scaleFactor ) + this.TickLabels.BoundingBox.Height;
                this.Title.Draw( graphicsDevice, this, str, x, y, scaleFactor );
            }
        }

        /// <summary>
        /// Adjust the scale range to match the tick spacing and set the user scale range and exponent.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private void SetFinalUserScaleRange()
        {

            // reset the spacing for getting the correct tick count.
            this.MajorTick.SetSpacing( this );
            this.MinorTick.SetSpacing( this );
            switch ( this.CoordinateScale.CoordinateScaleType )
            {
                case CoordinateScaleType.Date:
                    {

                        // set the user range
                        this.UserScaleRange = new RangeR( this.Min.Value, this.Max.Value );

                        // Never use a magnitude shift for date scales
                        this.ScaleExponent.Value = 0;
                        break;
                    }

                case CoordinateScaleType.Linear:
                case CoordinateScaleType.StripChart:
                    {

                        // Calculate the scale minimum
                        if ( this.Min.AutoScale && this.Min.Value != 0d )
                        {
                            this.Min.Value -= this.Min.Value.Mod( this.MajorTick.Spacing );
                            if ( this.Min.Value != 0d )
                            {
                                // _min.Value = majorTickSpacing * Math.Round(Me._min.Value / majorTickSpacing)
                                int roundingDigits = this.MajorTick.DecimalPlaces( this ) + 2;
                                roundingDigits = Math.Max( roundingDigits, 0 );
                                roundingDigits = Math.Min( roundingDigits, 15 );
                                this.Min.Value = Math.Round( this.Min.Value, roundingDigits );
                            }
                        }
                        // Calculate the scale maximum
                        if ( this.Max.AutoScale )
                        {
                            double range = this.Max.Value - this.Min.Value;
                            double rangeRemainder = range.Mod( this.MajorTick.Spacing );
                            if ( rangeRemainder != 0d )
                            {
                                range += this.MajorTick.Spacing - range.Mod( this.MajorTick.Spacing );
                            }
                            // range = majorTickSpacing * Math.Round(range / majorTickSpacing)
                            int roundingDigits = this.MajorTick.DecimalPlaces( this ) + 2;
                            roundingDigits = Math.Max( roundingDigits, 0 );
                            roundingDigits = Math.Min( roundingDigits, 15 );
                            range = Math.Round( range, roundingDigits );
                            this.Max.Value = this.Min.Value + range;
                        }

                        // set the scale range
                        this.UserScaleRange = new RangeR( this.Min.Value, this.Max.Value );

                        // set the scale magnitude if required
                        if ( this.ScaleExponent.AutoScale )
                        {

                            // get the scale exponent based on the range limits
                            this.ScaleExponent.Value = this.UserScaleRange.GetExponent( this.IsEngineeringScale );
                        }

                        break;
                    }

                case CoordinateScaleType.Log:
                    {

                        // set the user range
                        this.UserScaleRange = new RangeR( this.Min.Value.Log10(), this.Max.Value.Log10() );
                        if ( this.ScaleExponent.AutoScale )
                        {
                            // Never use a magnitude shift for log scales
                            this.ScaleExponent.Value = 0;
                        }

                        break;
                    }

                case CoordinateScaleType.Text:
                    {

                        // set the user range
                        this.UserScaleRange = new RangeR( this.Min.Value, this.Max.Value );

                        // Never use a magnitude shift for text scales
                        this.ScaleExponent.Value = 0;
                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, $"Unhandled coordinate scale type {this.CoordinateScale.CoordinateScaleType}" );
                        break;
                    }
            }

            // set the tick values
            this.MajorTick.SetValues( this );
        }

        /// <summary>
        /// Sets a reasonable scale given a range of data values.  The scale range is chosen based on
        /// increments of 1, 2, or 5 (because they are even divisors of
        /// 10).  This method auto scales based on the <see cref="AutoValueR.AutoScale"/> and
        /// <see cref="AutoValueR.AutoScale"/> settings.  <para>On Exit:</para>
        /// <para><see cref="Min"/> is set to scale minimum (if <see cref="AutoValueR.AutoScale"/> =
        /// True)</para>
        /// <para><see cref="Max"/> is set to scale maximum (if <see cref="AutoValueR.AutoScale"/> =
        /// True)</para>
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="minValue"> The minimum value of the data range for setting this
        /// <see cref="Axis"/> scale range. </param>
        /// <param name="maxValue"> The maximum value of the data range for setting this
        /// <see cref="Axis"/> scale range. </param>
        private void SetInitialUserScaleRange( double minValue, double maxValue )
        {

            // if the scales are auto ranged, use the actual data values for the range
            if ( this.Min.AutoScale )
            {
                this.Min.Value = minValue;
            }

            if ( this.Max.AutoScale )
            {
                this.Max.Value = maxValue;
            }

            switch ( this.CoordinateScale.CoordinateScaleType )
            {
                case CoordinateScaleType.Date:
                    {

                        // Date Scale

                        // Test for trivial condition of range = 0 and pick a suitable default
                        if ( this.Range < ScaleDefaults.Get().MinRange )
                        {
                            if ( this.Max.AutoScale )
                            {
                                this.Max.Value = 1.0d + this.Min.Value;
                            }
                            else
                            {
                                this.Min.Value = this.Max.Value - 1.0d;
                            }
                        }

                        // Calculate the scale minimum
                        if ( this.Min.AutoScale )
                        {
                            this.Min.Value = this.MajorTick.GetEvenDateSpacing( this.Min.Value, -1 );
                        }

                        // Calculate the scale maximum
                        if ( this.Max.AutoScale )
                        {
                            this.Max.Value = this.MajorTick.GetEvenDateSpacing( this.Max.Value, 1 );
                        }

                        break;
                    }

                case CoordinateScaleType.Linear:
                case CoordinateScaleType.StripChart:
                    {

                        // linear scale
                        // Test for trivial condition of range = 0 and pick a suitable default
                        if ( this.Range < ScaleDefaults.Get().MinRange )
                        {
                            if ( this.Max.AutoScale )
                            {
                                this.Max.Value = 1.0d + this.Min.Value;
                            }
                            else
                            {
                                this.Min.Value = this.Max.Value - 1.0d;
                            }
                        }

                        // This is the zero-lever test.  If minValue is within the zero lever fraction
                        // of the data range, then use zero.
                        if ( this.Min.AutoScale && this.Min.Value > 0d && this.Min.Value / this.Range < ScaleDefaults.Get().ZeroLever )
                        {
                            this.Min.Value = 0d;
                        }
                        // Repeat the zero-lever test for cases where the maxValue is less than zero
                        if ( this.Max.AutoScale && this.Max.Value < 0d && Math.Abs( this.Max.Value / this.Range ) < ScaleDefaults.Get().ZeroLever )
                        {
                            this.Max.Value = 0d;
                        }

                        break;
                    }

                case CoordinateScaleType.Log:
                    {

                        // Log Scale: Check for bad data range

                        if ( this.Min.Value <= 0.0d && this.Max.Value <= 0.0d )
                        {
                            this.Min.Value = 1.0d;
                            this.Max.Value = 10.0d;
                        }
                        else if ( this.Min.Value <= 0.0d )
                        {
                            this.Min.Value = this.Max.Value / 10.0d;
                        }
                        else if ( this.Max.Value <= 0.0d )
                        {
                            this.Max.Value = this.Min.Value * 10.0d;
                        }

                        // Test for trivial condition of range = 0 and pick a suitable default
                        if ( this.Range < ScaleDefaults.Get().MinRange )
                        {
                            if ( this.Max.AutoScale )
                            {
                                this.Max.Value = this.Min.Value * 10.0d;
                            }
                            else
                            {
                                this.Min.Value = this.Max.Value / 10.0d;
                            }
                        }

                        // Get the nearest power of 10 (no partial log cycles allowed)
                        if ( this.Min.AutoScale )
                        {
                            this.Min.Value = Math.Pow( 10.0d, Math.Floor( Math.Log10( this.Min.Value ) ) );
                        }

                        if ( this.Max.AutoScale )
                        {
                            this.Max.Value = Math.Pow( 10.0d, Math.Ceiling( Math.Log10( this.Max.Value ) ) );
                        }

                        break;
                    }

                case CoordinateScaleType.Text:
                    {

                        // if this is a text-based axis, then ignore all settings and make it simply ordinal

                        // if text labels are provided, then auto range to the number of labels
                        if ( this.TickLabels.TextLabels() is object )
                        {
                            if ( this.Min.AutoScale )
                            {
                                this.Min.Value = 0d;
                            }

                            if ( this.Max.AutoScale )
                            {
                                this.Max.Value = this.TickLabels.TextLabels().Length + 1;
                            }
                        }

                        // Test for trivial condition of range = 0 and pick a suitable default
                        if ( this.Range < 0.1d )
                        {
                            if ( this.Max.AutoScale )
                            {
                                this.Max.Value = this.Min.Value + 10.0d;
                            }
                            else
                            {
                                this.Min.Value = this.Max.Value - 10.0d;
                            }
                        }

                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, $"Unhandled coordinate scale type {this.CoordinateScale.CoordinateScaleType}" );
                        break;
                    }
            }
        }

        /// <summary> Setup the Transform Matrix to handle drawing of this <see cref="Axis"/> </summary>
        /// <remarks>
        /// The flexibility offered by the transform matrix of the GDI+ drawing library allows the same
        /// code to be employed for drawing all three axes. This methods translates and rotates the axis
        /// so that it is oriented along the X direction with the origin at the left edge of the axis
        /// when looking from the label size (i.e., from the left of the left Y axis or the right of the
        /// right Y axis).
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="axisArea">       The axis area. </param>
        private void SetTransformMatrix( Graphics graphicsDevice, RectangleF axisArea )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            switch ( this.AxisType )
            {
                case AxisType.X:
                    {

                        // Move the origin to the BottomLeft of the axisArea, which is the left
                        // side of the X axis (facing from the label side)
                        graphicsDevice.TranslateTransform( axisArea.Left, axisArea.Bottom );
                        this._TransformedArea = new RectangleF( 0f, -axisArea.Height, axisArea.Width, axisArea.Height );
                        break;
                    }

                case AxisType.Y:
                    {

                        // Move the origin to the TopLeft of the axisArea, which is the left
                        // side of the axis (facing from the label side)
                        graphicsDevice.TranslateTransform( axisArea.Left, axisArea.Top );

                        // rotate so this axis is in the left-right direction
                        graphicsDevice.RotateTransform( 90f );

                        // define the transformed area
                        this._TransformedArea = new RectangleF( 0f, -axisArea.Width, axisArea.Height, axisArea.Width );
                        break;
                    }

                case AxisType.Y2:
                    {

                        // Move the origin to the BottomRight of the axisArea, which is the left
                        // side of the Y2 axis (facing from the label side)
                        graphicsDevice.TranslateTransform( axisArea.Right, axisArea.Bottom );

                        // rotate so this axis is in the left-right direction
                        graphicsDevice.RotateTransform( -90 );

                        // define the transformed area
                        this._TransformedArea = new RectangleF( 0f, axisArea.Width, axisArea.Height, -axisArea.Width );
                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "Unhandled axis type" );
                        break;
                    }
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Gets the decimal places represented by the minimum absolute value of the range, min, or
        /// maximum values.
        /// </summary>
        /// <value> The decimal places. </value>
        public int DecimalPlaces
        {
            get {
                double baseValue = Math.Abs( this.Range );
                double baseCandidate = Math.Abs( this.Max.Value );
                if ( baseCandidate > ScaleDefaults.Get().MinRange && baseCandidate < baseValue )
                {
                    baseValue = baseCandidate;
                }

                baseCandidate = Math.Abs( this.Min.Value );
                if ( baseCandidate > ScaleDefaults.Get().MinRange && baseCandidate < baseValue )
                {
                    baseValue = baseCandidate;
                }

                return baseValue.DecimalPlaces();
            }
        }

        /// <summary> Gets or sets the <see cref="Grid"/> for drawing major grid lines. </summary>
        /// <value> A <see cref="Grid"/> value. </value>
        public Grid Grid { get; set; }

        /// <summary>
        /// Gets or sets the overall auto scale property of the axis.  When set, this property sets the
        /// axis range and spacing to auto scale.
        /// </summary>
        /// <value> The automatic scale. </value>
        public bool AutoScale
        {
            get => this.Min.AutoScale;

            set {
                this.Min.AutoScale = value;
                this.Max.AutoScale = value;
                this.MajorTick.AutoSpacing = value;
                this.MinorTick.AutoSpacing = value;
            }
        }

        /// <summary> Gets the horizontal type of the Axis. </summary>
        /// <value> The horizontal. </value>
        public bool Horizontal => this.AxisType == AxisType.X;

        /// <summary> Gets the validity status of the axis. </summary>
        /// <remarks>
        /// The axis is valid if is has a none empty range, non-negative spacing, and a manageable
        /// spacing count.
        /// </remarks>
        /// <value> A <see cref="System.Boolean">Boolean</see> </value>
        public bool Valid =>
                // check for validity of range and spacing
                !(this.Range <= 0d || this.MajorTick.Visible && this.MajorTick.Spacing <= 0d || this.MinorTick.Visible && this.MinorTick.Spacing <= 0d || this.MajorTick.TickCount > 1000 || this.MinorTick.TickCount > 5000);

        /// <summary> Returns the Vertical property of the axis. </summary>
        /// <value>
        /// <see cref="System.Boolean">True</see> if vertical or
        /// <see cref="System.Boolean">True</see> otherwise.
        /// </value>
        public bool IsVertical => this.AxisType == AxisType.Y || this.AxisType == AxisType.Y2;

        /// <summary>
        /// Determines whether or not the <see cref="Axis"/> is shown. Note that even if the axis is not
        /// visible, it can still be actively used to draw curves on a graph, it will just be invisible
        /// to the user.
        /// </summary>
        /// <value> True to show the axis, false to disable all drawing of this axis. </value>
        public bool Visible { get; set; }

        /// <summary> The color to use for drawing the <see cref="Axis"/>. </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> value. </value>
        public Color LineColor { get; set; }

        /// <summary>
        /// Gets reference to the major <see cref="Tick"/> object determining how major tick marks are
        /// drawn.
        /// </summary>
        /// <value> A <see cref="Tick"/> reference. </value>
        public Tick MajorTick { get; private set; }

        /// <summary>
        /// Gets references to the minor <see cref="Tick"/> object determining how major tick marks are
        /// drawn.
        /// </summary>
        /// <value> A <see cref="Tick"/> reference. </value>
        public Tick MinorTick { get; private set; }

        /// <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
        private readonly Pane _Pane;

        /// <summary> Gets the axis range in screen coordinates. </summary>
        /// <value> A <see cref="RangeF">Range</see> value. </value>
        public RangeF ScreenScaleRange { get; private set; }

        /// <summary> Gets the status message. </summary>
        /// <value> A System.String value. </value>
        protected string StatusMessage { get; set; }

        /// <summary> Gets the tick <see cref="Labels"/> </summary>
        /// <value> The tick labels. </value>
        public Labels TickLabels { get; set; }

        /// <summary>
        /// Gets the <see cref="Axis"/> <see cref="isr.Visuals.AxisTitle"/>. This normally shows the
        /// basis and dimensions of the scale range, such as "Time (Years)".
        /// </summary>
        /// <value> A <see cref="isr.Visuals.AxisTitle"/> property. </value>
        public AxisTitle Title { get; private set; }

        /// <summary> The transformed area. </summary>
        private RectangleF _TransformedArea;

        /// <summary>
        /// Gets the transformed area of the axis that it is oriented along the X direction with the
        /// origin at the left edge of the axis when looking from the label size (i.e., from the left of
        /// the left Y axis or the right of the right Y axis).
        /// </summary>
        /// <value> A <see cref="System.Drawing.RectangleF">rectangle</see> </value>
        internal RectangleF TransformedArea => this._TransformedArea;

        /// <summary> Gets the <see cref="AxisType"/> </summary>
        /// <remarks> David, 2007-10-15, 1.0.2844. Rename to AxisType. </remarks>
        /// <value> The type of the axis. </value>
        public AxisType AxisType { get; set; }

        #endregion

        #region " SCALE "

        /// <summary>
        /// Gets the <see cref="isr.Visuals.CoordinateScale"/> for this <see cref="Scale"/>.
        /// </summary>
        /// <value> The coordinate scale. </value>
        public CoordinateScale CoordinateScale { get; set; }

        /// <summary> Determines if the scale values are reversed for this <see cref="Scale"/> </summary>
        /// <value>
        /// True for the X values to decrease to the right or the Y values to decrease upwards, false
        /// otherwise.
        /// </value>
        protected bool Reversed { get; set; }

        /// <summary>
        /// Gets maximum auto value for this axis.  This value can be set automatically based on the
        /// state of <see cref="AutoValueR.AutoScale"/>.  If this value is set manually, then
        /// <see cref="AutoValueR.AutoScale"/> is set to False.
        /// </summary>
        /// <value>
        /// A <see cref="AutoValueR"/> value in user scale units for
        /// <see cref="CoordinateScaleType.Log"/> and <see cref="CoordinateScaleType.Linear"/> axes. For
        /// <see cref="CoordinateScaleType.Text"/> axes, this value is an ordinal starting with 1.0.
        /// For <see cref="CoordinateScaleType.Date"/> axes, this value is in an internal
        /// (<see cref="System.Double"/> date format based on the <see cref="JulianDate"/>.
        /// </value>
        public AutoValueR Max { get; set; }

        /// <summary>
        /// Gets minimum auto value for this axis.  The value of this auto value automatically based on
        /// the state of <see cref="AutoValueR.AutoScale"/>.  If this value is set manually, then
        /// <see cref="AutoValueR.AutoScale"/> is set to false.
        /// </summary>
        /// <value>
        /// A <see cref="AutoValueR"/> value in user scale units for
        /// <see cref="CoordinateScaleType.Log"/> and <see cref="CoordinateScaleType.Linear"/> axes. For
        /// <see cref="CoordinateScaleType.Text"/> axes, this value is an ordinal starting with 1.0.
        /// For <see cref="CoordinateScaleType.Date"/> axes, this value is in an internal
        /// (<see cref="System.Double"/> date format based on the <see cref="JulianDate"/>.
        /// </value>
        public AutoValueR Min { get; set; }

        /// <summary> Gets the axis range. </summary>
        /// <value> Max - Min values. </value>
        public double Range => this.Max.Value - this.Min.Value;

        /// <summary>
        /// Gets the engineering scale mode for setting the scale exponent.  If True, the exponent is in
        /// multiples of 3.
        /// </summary>
        /// <value> The is engineering scale. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private bool IsEngineeringScale { get; set; } = true;

        /// <summary>
        /// Gets the exponent for scale values. This is used to limit the size of the displayed value
        /// labels.  For example, if the value is really 2000000, then the graph could instead display
        /// 2000 with a magnitude multiplier decade of 3 (10^3).  The Value can be determined
        /// automatically depending on the state of <see cref="AutoValue.AutoScale"/>. If the Value is
        /// set manually by the user, then <see cref="AutoValue.AutoScale"/>
        /// is set to False. The magnitude multipliers.
        /// </summary>
        /// <value> The magnitude multiplier (power of 10) for the scale value labels. </value>
        public AutoValue ScaleExponent { get; set; }

        /// <summary>
        /// Gets the user scale range for transforming user to screen coordinates. Values are in user
        /// coordinates.
        /// </summary>
        /// <value> The user scale range. </value>
        public RangeR UserScaleRange { get; private set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="Axis"/> class that defines the default property values
    /// for the <see cref="Axis"/> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class AxisDefaults
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private AxisDefaults() : base()
        {
            this.LineColor = Color.Black;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static AxisDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static AxisDefaults Get()
        {
            if ( _Instance is null )
            {
                lock ( SyncLocker )
                    _Instance = new AxisDefaults();
            }

            return _Instance;
        }

        #endregion

        /// <summary>
        /// Gets or sets the default color for the <see cref="Axis"/> itself
        /// (<see cref="Axis.LineColor"/> property).  This color only affects the tick marks and the axis
        /// border.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color LineColor { get; set; }
    }
}

#endregion

