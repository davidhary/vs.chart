using System;

namespace isr.Visuals
{

    /// <summary> Defines a <see cref="T:System.integer">integer</see> auto value. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class AutoValue : ICloneable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs an auto value. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="[value]">   A <see cref="T:System.Integer">Integer</see> value. </param>
        /// <param name="autoScale"> True to auto scale. </param>
        public AutoValue( int value, bool autoScale ) : base()
        {
            this.Value = value;
            this.AutoScale = autoScale;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Arrow object from which to copy. </param>
        public AutoValue( AutoValue model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.AutoScale = model.AutoScale;
            this.Value = model.Value;
        }

        #endregion

        #region " EQUALS "

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( AutoValue left, AutoValue right )
        {
            return Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( AutoValue left, AutoValue right )
        {
            return !Equals( left, right );
        }

        /// <summary> Returns True if the values are equal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public static bool Equals( AutoValue left, AutoValue right )
        {
            return left is null
                ? right is null
                : !(right is null) && left.Value.Equals( right.Value ) && left.AutoScale.Equals( right.AutoScale );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public override bool Equals( object obj )
        {
            return Equals( this, obj as AutoValue );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="compared"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// Auto Values are the same if the have the same
        /// <see cref="Value"/> and <see cref="AutoScale"/> ranges.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="compared"> The <see cref="AutoValue">AutoValue</see> to compare for equality
        /// with this instance. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public bool Equals( AutoValue compared )
        {
            return compared is null ? throw new ArgumentNullException( nameof( compared ) ) : Equals( this, compared );
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Arrow. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Arrow. </returns>
        public AutoValue Copy()
        {
            return new AutoValue( this );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> An <see cref="T:System.integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode() + this.AutoScale.GetHashCode();
        }

        /// <summary> Returns the default string representation of the range. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The default string representation of the range. </returns>
        public override string ToString()
        {
            return this.Value.ToString( System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary> Returns the default string representation of the range. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="format"> The format string. </param>
        /// <returns> The formatted string representation of the range. </returns>
        public string ToString( string format )
        {
            return string.IsNullOrWhiteSpace( format )
                ? throw new ArgumentNullException( nameof( format ) )
                : this.Value.ToString( format, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary> Gets or sets the <see cref="AutoValue"/> value. </summary>
        /// <value> A <see cref="T:System.Integer">Integer</see> value. </value>
        public int Value { get; set; }

        /// <summary>
        /// Gets or sets the auto setting state of this AutoValue which determines if the value is set
        /// automatically.  This state is set to False if the Value is manually changed.
        /// </summary>
        /// <value> A <see cref="System.Boolean">Boolean</see> value. </value>
        public bool AutoScale { get; set; }

        #endregion

    }

    /// <summary> Defines a <see cref="System.Single">Single</see> auto value. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class AutoValueF : ICloneable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs a auto value. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="[value]">   A <see cref="System.Single">Single</see> value. </param>
        /// <param name="autoScale"> True to auto scale. </param>
        public AutoValueF( float value, bool autoScale ) : base()
        {
            this.Value = value;
            this.AutoScale = autoScale;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Arrow object from which to copy. </param>
        public AutoValueF( AutoValueF model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.AutoScale = model.AutoScale;
            this.Value = model.Value;
        }

        #endregion

        #region " EQUALS "

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Automatic value f to be compared. </param>
        /// <param name="right"> Automatic value f to be compared. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( AutoValueF left, AutoValueF right )
        {
            return Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Automatic value f to be compared. </param>
        /// <param name="right"> Automatic value f to be compared. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( AutoValueF left, AutoValueF right )
        {
            return !Equals( left, right );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Automatic value f to be compared. </param>
        /// <param name="right"> Automatic value f to be compared. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public static bool Equals( AutoValueF left, AutoValueF right )
        {
            return left is null
                ? right is null
                : !(right is null) && left.Value.Equals( right.Value ) && left.AutoScale.Equals( right.AutoScale );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return Equals( this, obj as AutoValueF );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="compared"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// Auto Values are the same if the have the same
        /// <see cref="Value"/> and <see cref="AutoScale"/> ranges.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="compared"> The <see cref="AutoValueF">AutoValueF</see> to compare for equality
        /// with this instance. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public bool Equals( AutoValueF compared )
        {
            return compared is null ? throw new ArgumentNullException( nameof( compared ) ) : Equals( this, compared );
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Arrow. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Arrow. </returns>
        public AutoValueF Copy()
        {
            return new AutoValueF( this );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode() + this.AutoScale.GetHashCode();
        }

        /// <summary> Returns the default string representation of the range. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The default string representation of the range. </returns>
        public override string ToString()
        {
            return this.Value.ToString( System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary> Returns the default string representation of the range. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="format"> The format string. </param>
        /// <returns> The formatted string representation of the range. </returns>
        public string ToString( string format )
        {
            return string.IsNullOrWhiteSpace( format )
                ? throw new ArgumentNullException( nameof( format ) )
                : this.Value.ToString( format, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary>
        /// Returns a caption for displaying the value with additional decimal value relative to the base
        /// value.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="extraDecimalPlaces"> The extra decimal places. </param>
        /// <returns>
        /// a caption for displaying the value with additional decimal value relative to the base value.
        /// </returns>
        public string Caption( int extraDecimalPlaces )
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, "{0:F" + (this.Value.DecimalPlaces() + extraDecimalPlaces).ToString( "D", System.Globalization.CultureInfo.CurrentCulture ) + "}", this.Value );
        }

        /// <summary> Gets or sets the <see cref="AutoValue"/> value. </summary>
        /// <value> A <see cref="System.Single">Single</see> value. </value>
        public float Value { get; set; }

        /// <summary>
        /// Gets or sets the auto setting state of this AutoValue which determines if the value is set
        /// automatically.  This state is set to False if the Value is manually changed.
        /// </summary>
        /// <value> A <see cref="System.Boolean">Boolean</see> value. </value>
        public bool AutoScale { get; set; }

        #endregion

    }

    /// <summary> Defines a <see cref="System.Double">Double</see> auto value. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class AutoValueR : ICloneable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs a auto value. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="[value]">   A <see cref="System.Double">Double</see> value. </param>
        /// <param name="autoScale"> True to auto scale. </param>
        public AutoValueR( double value, bool autoScale ) : base()
        {
            this.Value = value;
            this.AutoScale = autoScale;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Arrow object from which to copy. </param>
        public AutoValueR( AutoValueR model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.AutoScale = model.AutoScale;
            this.Value = model.Value;
        }

        #endregion

        #region " EQUALS "

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( AutoValueR left, AutoValueR right )
        {
            return Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( AutoValueR left, AutoValueR right )
        {
            return !Equals( left, right );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public static bool Equals( AutoValueR left, AutoValueR right )
        {
            return left is null
                ? right is null
                : !(right is null) && left.Value.Equals( right.Value ) && left.AutoScale.Equals( right.AutoScale );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return Equals( this, obj as AutoValueR );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="compared"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// Auto Values are the same if the have the same
        /// <see cref="Value"/> and <see cref="AutoScale"/> ranges.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="compared"> The <see cref="AutoValueR">AutoValueR</see> to compare for equality
        /// with this instance. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public bool Equals( AutoValueR compared )
        {
            return compared is null ? throw new ArgumentNullException( nameof( compared ) ) : Equals( this, compared );
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Arrow. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Arrow. </returns>
        public AutoValueR Copy()
        {
            return new AutoValueR( this );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode() + this.AutoScale.GetHashCode();
        }

        /// <summary> Returns the default string representation of the range. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The default string representation of the range. </returns>
        public override string ToString()
        {
            return this.Value.ToString( System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary> Returns the default string representation of the range. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="format"> The format string. </param>
        /// <returns> The formatted string representation of the range. </returns>
        public string ToString( string format )
        {
            return string.IsNullOrWhiteSpace( format )
                ? throw new ArgumentNullException( nameof( format ) )
                : this.Value.ToString( format, System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary>
        /// Returns a caption for displaying the value with additional decimal value relative to the base
        /// value.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="extraDecimalPlaces"> The extra decimal places. </param>
        /// <returns>
        /// a caption for displaying the value with additional decimal value relative to the base value.
        /// </returns>
        public string Caption( int extraDecimalPlaces )
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, "{0:F" + (this.Value.DecimalPlaces() + extraDecimalPlaces).ToString( "D", System.Globalization.CultureInfo.CurrentCulture ) + "}", this.Value );
        }

        /// <summary>Gets or sets the <see cref="AutoValue"/> value</summary>
        /// <value>A <see cref="System.Double">Double</see> value</value>
        public double Value { get; set; }

        /// <summary>
        /// Gets or sets the auto setting state of this AutoValue which determines if the value is set
        /// automatically.
        /// </summary>
        /// <value> A <see cref="System.Boolean">Boolean</see> value. </value>
        public bool AutoScale { get; set; }

        #endregion

    }
}
