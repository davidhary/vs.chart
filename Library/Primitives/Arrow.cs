using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary>
    /// Represents a graphic arrow or Cord object on the graph.  A list of Arrow objects is
    /// maintained by the <see cref="ArrowCollection"/> collection class.
    /// </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created
    /// </para>
    /// </remarks>
    public class Arrow : ICloneable, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs a instance defining the position of the arrow to be pre-specified.  All other
        /// properties are set to default values.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="origin">      The starting point of the <see cref="Arrow"/>. </param>
        /// <param name="destination"> The ending point of the <see cref="Arrow"/>. </param>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public Arrow( PointF origin, PointF destination, Pane drawingPane ) : base()
        {
            if ( drawingPane is null )
            {
                throw new ArgumentNullException( nameof( drawingPane ) );
            }

            var arrowDefaults = ArrowDefaults.Get();
            this.LineColor = arrowDefaults.LineColor;
            this.ArrowheadSize = arrowDefaults.ArrowheadSize;
            this.LineWidth = arrowDefaults.LineWidth;
            this.Origin = new PointF( 0f, 0f );
            this.Destination = new PointF( 0.2f, 0.2f );
            this.Arrowhead = arrowDefaults.Arrowhead;
            this.CoordinateFrame = arrowDefaults.CoordinateFrame;
            this.Origin = origin;
            this.Destination = destination;
            this._Pane = drawingPane;
        }

        /// <summary>
        /// Constructs a instance defining the position of the arrow to be pre-specified.  All other
        /// properties are set to default values.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x1">          The x position of the starting point that defines the
        /// <see cref="Arrow"/>.  The units of this position are specified by
        /// the
        /// <see cref="CoordinateFrame"/> property. </param>
        /// <param name="y1">          The y position of the starting point that defines the
        /// <see cref="Arrow"/>.  The units of this position are specified by
        /// the
        /// <see cref="CoordinateFrame"/> property. </param>
        /// <param name="x2">          The x position of the ending point that defines the
        /// <see cref="Arrow"/>.  The units of this position are specified by
        /// the
        /// <see cref="CoordinateFrame"/> property. </param>
        /// <param name="y2">          The y position of the ending point that defines the
        /// <see cref="Arrow"/>.  The units of this position are specified by
        /// the
        /// <see cref="CoordinateFrame"/> property. </param>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public Arrow( float x1, float y1, float x2, float y2, Pane drawingPane ) : this( new PointF( x1, y1 ), new PointF( x2, y2 ), drawingPane )
        {
        }

        /// <summary>
        /// Constructs a instance defining the position, color, and size of the
        /// <see cref="Arrow"/> to be pre-specified.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="lineColor">     The line <see cref="System.Drawing.Color">Color</see> for the
        /// arrow. </param>
        /// <param name="arrowheadSize"> The size of the arrowhead, measured in points. </param>
        /// <param name="x1">            The x position of the starting point that defines the arrow.  The
        /// units of this position are specified by the
        /// <see cref="CoordinateFrame"/> property. </param>
        /// <param name="y1">            The y position of the starting point that defines the arrow.  The
        /// units of this position are specified by the
        /// <see cref="CoordinateFrame"/> property. </param>
        /// <param name="x2">            The x position of the ending point that defines the arrow.  The
        /// units of this position are specified by the
        /// <see cref="CoordinateFrame"/> property. </param>
        /// <param name="y2">            The y position of the ending point that defines the arrow.  The
        /// units of this position are specified by the
        /// <see cref="CoordinateFrame"/> property. </param>
        /// <param name="drawingPane">   Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public Arrow( Color lineColor, float arrowheadSize, float x1, float y1, float x2, float y2, Pane drawingPane ) : this( x1, y1, x2, y2, drawingPane )
        {
            this.LineColor = lineColor;
            this.ArrowheadSize = arrowheadSize;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Arrow object from which to copy. </param>
        public Arrow( Arrow model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.Origin = model.Origin;
            this.Destination = model.Destination;
            this.ArrowheadSize = model.ArrowheadSize;
            this.LineColor = model.LineColor;
            this.LineWidth = model.LineWidth;
            this.Arrowhead = model.Arrowhead;
            this.CoordinateFrame = model.CoordinateFrame;
            this._Pane = model._Pane;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        this.LineColor = default;
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Arrow. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Arrow. </returns>
        public Arrow Copy()
        {
            return new Arrow( this );
        }

        /// <summary> Renders this object to the specified <see cref="Graphics"/> device. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        public void Draw( Graphics graphicsDevice )
        {

            // validate argument.
            if ( graphicsDevice is null )
                throw new ArgumentNullException( nameof( graphicsDevice ) );

            // get the he origin of the arrow in screen coordinates
            var fromPix = this._Pane.GeneralTransform( this.Origin, this.CoordinateFrame );
            var toPix = this._Pane.GeneralTransform( this.Destination, this.CoordinateFrame );
            if ( this._Pane.DrawArea.Contains( fromPix ) & this._Pane.DrawArea.Contains( toPix ) )
            {

                // get a scaled size for the arrowhead
                float scaledSize = Convert.ToSingle( this.ArrowheadSize * this._Pane.ScaleFactor );

                // calculate the length and the angle of the arrow "vector"
                double dy = toPix.Y - fromPix.Y;
                double dx = toPix.X - fromPix.X;
                float angle = Convert.ToSingle( Math.Atan2( dy, dx ) ) * 180.0f / Convert.ToSingle( Math.PI );
                float length = Convert.ToSingle( Math.Sqrt( dx * dx + dy * dy ) );

                // Save the old transform matrix
                var transformMatrix = graphicsDevice.Transform;

                // Move the coordinate system so it is located at the starting point
                // of this arrow
                graphicsDevice.TranslateTransform( fromPix.X, fromPix.Y );

                // Rotate the coordinate system according to the angle of this arrow
                // about the starting point
                graphicsDevice.RotateTransform( angle );

                // get a pen according to this arrow properties
                using ( var pen = new Pen( this.LineColor, this.LineWidth ) )
                {
                    // Draw the line segment for this arrow
                    graphicsDevice.DrawLine( pen, 0f, 0f, length, 0f );
                }

                // Only show the arrowhead if required
                if ( this.Arrowhead )
                {
                    using var brush = new SolidBrush( this.LineColor );
                    // Create a polygon representing the arrowhead based on the scaled size
                    var polyPt = new PointF[4];
                    float xSize = scaledSize;
                    float ySize = scaledSize / 3.0f;
                    polyPt[0].X = length;
                    polyPt[0].Y = 0f;
                    polyPt[1].X = length - xSize;
                    polyPt[1].Y = ySize;
                    polyPt[2].X = length - xSize;
                    polyPt[2].Y = -ySize;
                    polyPt[3] = polyPt[0];

                    // render the arrowhead
                    graphicsDevice.FillPolygon( brush, polyPt );
                }

                // Restore the transform matrix back to its original state
                graphicsDevice.Transform = transformMatrix;
            }
            else
            {
                Debug.Assert( !Debugger.IsAttached, "Arrow out of range" );
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Gets or sets the size of the arrowhead. The display of the arrowhead can be enabled or
        /// disabled with the <see cref="Arrowhead"/> property.
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float ArrowheadSize { get; set; }

        /// <summary>
        /// Gets or sets the coordinate system to be used for defining the <see cref="Arrow"/> position.
        /// </summary>
        /// <value> A <see cref="CoordinateFrameType"/> </value>
        public CoordinateFrameType CoordinateFrame { get; set; }

        /// <summary>
        /// Gets or sets the destination or ending point of <see cref="Arrow"/> segment.
        /// </summary>
        /// <value>
        /// A <see cref="System.Drawing.PointF">Point</see> with units according to the
        /// <see cref="CoordinateFrame"/>
        /// </value>
        public PointF Destination { get; set; }

        /// <summary> Determines whether or not to draw an arrowhead. </summary>
        /// <value>
        /// A <see cref="System.Boolean">Boolean</see> <c>True</c> if an arrowhead is to be drawn, False
        /// otherwise.
        /// </value>
        public bool Arrowhead { get; set; }

        /// <summary>
        /// The <see cref="System.Drawing.Color">Color</see> of the arrowhead and line segment.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color LineColor { get; set; }

        /// <summary> The width of the line segment for the <see cref="Arrow"/> </summary>
        /// <value> A <see cref="System.Single">Single</see> width in pixels. </value>
        public float LineWidth { get; set; }

        /// <summary> Gets or sets the origin or starting point of <see cref="Arrow"/> segment. </summary>
        /// <value>
        /// A <see cref="System.Drawing.PointF">Point</see> with units according to the
        /// <see cref="CoordinateFrame"/>
        /// </value>
        public PointF Origin { get; set; }

        /// <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
        private readonly Pane _Pane;

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A System.String value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusMessage { get; set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="Arrow" /> class that defines the default property values
    /// for the <see cref="Arrow" /> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class ArrowDefaults
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private ArrowDefaults() : base()
        {
            this.ArrowheadSize = 12.0f;
            this.CoordinateFrame = CoordinateFrameType.AxisXYScale;
            this.Arrowhead = true;
            this.LineWidth = 1.0f;
            this.LineColor = Color.Red;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static ArrowDefaults _Instance;

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static ArrowDefaults Get()
        {
            if ( _Instance is null )
            {
                lock ( SyncLocker )
                    _Instance = new ArrowDefaults();
            }

            return _Instance;
        }

        #endregion

        /// <summary> Gets or sets the default size for the <see cref="Arrow"/>. </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float ArrowheadSize { get; set; }

        /// <summary>
        /// Gets or sets the default coordinate system to be used for defining the
        /// <see cref="Arrow"/> location coordinates
        /// (<see cref="Arrow.CoordinateFrame"/> property).
        /// </summary>
        /// <value> A <see cref="CoordinateFrameType"/> </value>
        public CoordinateFrameType CoordinateFrame { get; set; }

        /// <summary>
        /// Gets or sets the default display mode for the <see cref="Arrow"/> item Arrowhead
        /// (<see cref="Arrow.Arrowhead"/> property).  true to show the arrowhead, false to hide it.
        /// </summary>
        /// <value> The arrowhead. </value>
        public bool Arrowhead { get; set; }

        /// <summary>
        /// Gets or sets the default line width used for the <see cref="Arrow"/> line segment
        /// (<see cref="Arrow.LineWidth"/> property).
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float LineWidth { get; set; }

        /// <summary>
        /// Gets or sets the default color used for the <see cref="Arrow"/> line segment and arrowhead
        /// (<see cref="Arrow.LineColor"/> property).
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color LineColor { get; set; }
    }
}

#endregion

