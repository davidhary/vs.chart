using System;
using System.Drawing;

using isr.Core.Constructs;

namespace isr.Visuals
{

    /// <summary>
    /// Defines a <see cref="System.Double">Double</see> two-dimensional range structure.
    /// </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public struct PlanarRangeR
    {

        #region " SHARED "

        /// <summary>Gets or sets the empty range</summary>
        /// <value>A <see cref="PlanarRangeR"/> value with empty X and Y ranges</value>
        public static PlanarRangeR Empty => new PlanarRangeR( RangeR.Empty, RangeR.Empty );

        /// <summary>
        /// gets the planar range for the <see cref="X"/> and <see cref="Y"/> data arrays.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x">             The x data array. </param>
        /// <param name="y">             The y data array. </param>
        /// <param name="ignoreInitial"> Affects how initial zero y values are treated for setting the
        /// range of X values.  If True, then initial zero data points are
        /// ignored when determining the X range.  All data after the first
        /// non-zero Y value are included. </param>
        /// <returns> The calculated range. </returns>
        public static PlanarRangeR GetRange( double[] x, double[] y, bool ignoreInitial )
        {

            // return the unit range if no data
            if ( x is null || y is null || x.Length == 0 || y.Length == 0 )
            {
                return Unity;
            }

            int numPoints = Convert.ToInt32( Math.Min( x.Length, y.Length ) );

            // initialize the values to the empty range
            double yTemp;
            double xTemp;
            yTemp = y[0];
            xTemp = x[0];
            double xMin = xTemp;
            double yMin = yTemp;
            double xMax = xTemp;
            double yMax = yTemp;

            // Loop over each point in the arrays
            for ( int i = 0, loopTo = numPoints - 1; i <= loopTo; i++ )
            {
                yTemp = y[i];
                xTemp = x[i];

                // ignoreInitial becomes false at the first non-zero Y value
                if ( ignoreInitial && yTemp != 0d && yTemp != double.MaxValue )
                {
                    ignoreInitial = false;
                }

                if ( !ignoreInitial && xTemp != double.MaxValue && yTemp != double.MaxValue )
                {
                    if ( xTemp < xMin )
                    {
                        xMin = xTemp;
                    }
                    else if ( xTemp > xMax )
                    {
                        xMax = xTemp;
                    }

                    if ( yTemp < yMin )
                    {
                        yMin = yTemp;
                    }
                    else if ( yTemp > yMax )
                    {
                        yMax = yTemp;
                    }
                }
            }

            return new PlanarRangeR( new RangeR( xMin, xMax ), new RangeR( yMin, yMax ) );
        }

        /// <summary> Gets the unit range. </summary>
        /// <value> A <see cref="PlanarRangeR"/> value with unity X and Y ranges. </value>
        public static PlanarRangeR Unity => new PlanarRangeR( RangeR.Unity, RangeR.Unity );

        /// <summary> Gets the zero range value. </summary>
        /// <value> A <see cref="PlanarRangeR"/> value. </value>
        public static PlanarRangeR Zero => new PlanarRangeR( RangeR.Zero, RangeR.Zero );

        #endregion

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Contracts a planar range based on the X and Y ranges. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x"> The horizontal <see cref="RangeR"/> </param>
        /// <param name="y"> The vertical <see cref="RangeR"/> </param>
        public PlanarRangeR( RangeR x, RangeR y )
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="model"> The PlanarRangeR object from which to copy. </param>
        public PlanarRangeR( PlanarRangeR model )
        {
            this.X = model.X;
            this.Y = model.Y;
        }

        #endregion

        #region " EQUALS "

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Planar range r to be compared. </param>
        /// <param name="right"> Planar range r to be compared. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( PlanarRangeR left, PlanarRangeR right )
        {
            return Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Planar range r to be compared. </param>
        /// <param name="right"> Planar range r to be compared. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( PlanarRangeR left, PlanarRangeR right )
        {
            return !Equals( left, right );
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Planar range r to be compared. </param>
        /// <param name="right"> Planar range r to be compared. </param>
        /// <returns>
        /// <c>True</c> if values are the same type and represent the same value; otherwise, <c>False</c>.
        /// </returns>
        public static bool Equals( PlanarRangeR left, PlanarRangeR right )
        {
            return left.Equals( right );
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="obj"> Another object to compare to. </param>
        /// <returns>
        /// <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
        /// same value; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return obj is object && ReferenceEquals( this.GetType(), obj.GetType() ) && this.Equals( ( PlanarRangeR ) obj );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="other"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// Planar Ranges are the same if the have the same
        /// <see cref="X"/> and <see cref="Y"/> ranges.
        /// </remarks>
        /// <param name="other"> The <see cref="PlanarRangeR">PlanarRangeR</see> to compare for equality
        /// with this instance. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public bool Equals( PlanarRangeR other )
        {
            return this.X.Equals( other.X ) && this.Y.Equals( other.Y );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return this.X.GetHashCode() ^ this.Y.GetHashCode();
        }

        #endregion

        #region " METHODS "

        /// <summary>
        /// Returns true if the <see cref="PlanarRangeR"/> contains the
        /// <see cref="PointF">Point</see>
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="point"> A <see cref="PointF">Point</see> </param>
        /// <returns> True if the object is in this collection, false if not. </returns>
        public bool Contains( Core.Constructs.PointF point )
        {
            return point is null ? throw new ArgumentNullException( nameof( point ) ) : this.X.Contains( point.X ) && this.Y.Contains( point.Y );
        }

        /// <summary>
        /// Returns true if the <see cref="PlanarRangeR"/> contains the
        /// <see cref="PointF">Point</see>
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="point">     A <see cref="PointF">Point</see> </param>
        /// <param name="tolerance"> A <see cref="SizeF">Tolerance</see> around the point. </param>
        /// <returns> True if the object is in this collection, false if not. </returns>
        public bool Contains( Core.Constructs.PointF point, SizeF tolerance )
        {
            return point is null
                ? throw new ArgumentNullException( nameof( point ) )
                : this.X.Contains( point.X, tolerance.Width ) && this.Y.Contains( point.Y, tolerance.Height );
        }

        /// <summary>
        /// Extend this range to include both its present values and the specified range.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="range"> A <see cref="PlanarRangeR"/> value. </param>
        /// <returns> A PlanarRangeR. </returns>
        public PlanarRangeR ExtendRange( PlanarRangeR range )
        {
            this.X.ExtendRange( range.X );
            this.Y.ExtendRange( range.Y );
            return this;
        }

        /// <summary> Sets the PlanarRangeR based on the extrema. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="x"> The horizontal <see cref="RangeR"/>. </param>
        /// <param name="y"> The vertical <see cref="RangeR"/>. </param>
        public void SetPlanarRange( RangeR x, RangeR y )
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary> Returns the default string representation of the PlanarRangeR. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The fully qualified type name. </returns>
        public override string ToString()
        {
            return $"[{this.X},{this.Y}]";
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets or sets the horizontal range. </summary>
        /// <value> A <see cref="RangeR"/> value. </value>
        public RangeR X { get; set; }

        /// <summary> Gets or sets the vertical range. </summary>
        /// <value> A <see cref="RangeR"/> value. </value>
        public RangeR Y { get; set; }

        #endregion

    }
}
