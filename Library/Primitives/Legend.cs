using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary>
    /// Encapsulates the chart <see cref="Legend"/> that is displayed in the <see cref="Pane"/>
    /// </summary>
    /// <remarks>
    /// Use this class to ....(c) 2004 Integrated Scientific Resources, Inc. All rights reserved.
    /// <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class Legend : ICloneable, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs a <see cref="Legend"/> with default values as defined in the
        /// <see cref="LegendDefaults"/> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public Legend( Pane drawingPane ) : base()
        {
            if ( drawingPane is null )
            {
                throw new ArgumentNullException( nameof( drawingPane ) );
            }

            var legendDefaults = LegendDefaults.Get();
            this.Placement = legendDefaults.Placement;
            this.Frame = new Frame() {
                IsOutline = legendDefaults.Framed,
                Filled = legendDefaults.Filled,
                Visible = legendDefaults.Filled | legendDefaults.Framed,
                LineColor = legendDefaults.FrameColor,
                LineWidth = legendDefaults.FrameWidth,
                FillColor = legendDefaults.FillColor
            };
            this.HorizontalStack = legendDefaults.HorizontalStack;
            this.Visible = legendDefaults.Visible;
            this.Appearance = new TextAppearance( legendDefaults.Font, legendDefaults.FontColor );
            this.Appearance.Frame.Visible = false;
            this._Pane = drawingPane;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The XAxis object from which to copy. </param>
        public Legend( Legend model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this._Area = model._Area;
            this.Placement = model.Placement;
            this.Frame = model.Frame.Copy();
            this.HorizontalStack = model.HorizontalStack;
            this.Visible = model.Visible;
            this.Appearance = model.Appearance.Copy();
            this._Pane = model._Pane;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets (private) the dispose status sentinel. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        if ( this.Appearance is object )
                        {
                            this.Appearance.Dispose();
                            this.Appearance = null;
                        }

                        if ( this.Frame is object )
                        {
                            this.Frame.Dispose();
                            this.Frame = null;
                        }
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary>Private values the determine how legend space is allocated.
        /// <code>
        /// item:     space  Cord  space      text      space<p>
        /// width:    0.5w    2w   0.5w  Maximum Width  0.5w</p>
        /// </code></summary>
        private readonly float _LineLeftMarginCharacterWidth = 0.5f;

        /// <summary> Width of the line right margin character. </summary>
        private readonly float _LineRightMarginCharacterWidth = 0.5f;

        /// <summary> Width of the line length character. </summary>
        private readonly float _LineLengthCharacterWidth = 2f;

        /// <summary> Width of the right margin character. </summary>
        private readonly float _RightMarginCharacterWidth = 0.5f;

        /// <summary> Width of the legend horizontalgap character. </summary>
        private readonly float _LegendHorizontalgapCharacterWidth = 0.5f;

        /// <summary> Height of the legend verticalgap character. </summary>
        private readonly float _LegendVerticalgapCharacterHeight = 0.5f;

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Legend. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Legend. </returns>
        public Legend Copy()
        {
            return new Legend( this );
        }

        /// <summary>
        /// Renders the <see cref="Legend"/> to the specified <see cref="Graphics"/> device This method
        /// is normally only called by the Draw method of the parent <see cref="Pane"/> object.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        public void Draw( Graphics graphicsDevice )
        {

            // if the legend is not visible, do nothing
            if ( !this.Visible )
            {
                return;
            }

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // Clip everything to the draw area
            graphicsDevice.SetClip( this._Pane.DrawArea );

            // draw the frame before drawing the legend so that it appears in the background
            if ( this._Pane.Curves.Count > 0 )
            {
                this.Frame.Draw( graphicsDevice, this._Area );
            }

            // Set up some scaled dimensions for calculating sizes and locations
            float charWidth = this.Appearance.MeasureString( graphicsDevice, "x", this._Pane.ScaleFactor ).Width;
            float charHeight = this.Appearance.ScaledFont.Height;
            float halfCharHeight = charHeight / 2.0f;
            float x, y;

            // Loop for each curve in Curve Collection
            foreach ( Curve curve in this._Pane.Curves )
            {

                // Calculate the x,y (TopLeft) location of the current curve legend label
                x = this._Area.Left + charWidth * this._LineLeftMarginCharacterWidth + (curve.SerialNumber - 1) % this.Columns * this.ColumnWidth;
                y = this._Area.Top + Convert.ToSingle( Math.Floor( (curve.SerialNumber - 1) / ( double ) this.Columns ) ) * charHeight;

                // Draw the legend label for the current curve
                this.Appearance.Draw( graphicsDevice, curve.Label, x + (this._LineRightMarginCharacterWidth + this._LineLengthCharacterWidth) * charWidth, y, HorizontalAlignment.Left, VerticalAlignment.Top, this._Pane.ScaleFactor );

                // Draw a sample curve to the left of the label text
                curve.Cord.Draw( graphicsDevice, x, y + halfCharHeight, x + this._LineLengthCharacterWidth * charWidth, y + halfCharHeight );

                // Draw a sample symbol to the left of the label text				
                curve.Symbol.Draw( graphicsDevice, x + charWidth * this._LineLengthCharacterWidth / 2f, y + halfCharHeight, this._Pane.ScaleFactor );
            }
        }

        /// <summary>
        /// Calculates the <see cref="Legend"/> rectangle (<see cref="Area"/>), taking into account the
        /// number of required legend entries, and the legend drawing preferences.  Adjusts the size of
        /// the <see cref="Pane.AxisArea"/>
        /// for the parent <see cref="Pane"/> to accommodate the space required by the legend.  The
        /// legends are drawn into equally sized columns that are stacked per the
        /// <see cref="HorizontalStack"/> property.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="curves">         A reference to the <see cref="CurveCollection"/> collection of
        /// curves for which a legend is required. </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <param name="drawArea">       The rectangle that contains the drawing area in pixels. </param>
        /// <param name="axisArea">       The rectangle that contains the area bounded by the axes, in
        /// pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso> </param>
        /// <returns> Adjusted <see cref="Pane.AxisArea"/> </returns>
        /// <seealso cref="Pane.DrawArea"> DrawArea</seealso>
        /// <seealso cref="Columns"/>
        internal RectangleF GetAxisArea( Graphics graphicsDevice, CurveCollection curves, double scaleFactor, RectangleF drawArea, RectangleF axisArea )
        {

            // If the legend is invisible, don't do anything
            if ( !this.Visible )
            {
                return axisArea;
            }

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // Start with an empty rectangle
            this._Area = RectangleF.Empty;
            this.Columns = 1;
            this.ColumnWidth = 1f;

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            float charWidth = this.Appearance.MeasureString( graphicsDevice, "x", scaleFactor ).Width;
            float charHeight = this.Appearance.ScaledFont.Height;
            float verticalgap = charHeight * this._LegendVerticalgapCharacterHeight;
            float horizontalgap = charWidth * this._LegendHorizontalgapCharacterWidth;
            float maxLabelWidth = 0f;
            float widthAvailable;

            // Loop through each curve in the curve list
            // Find the maximum width of the legend labels
            foreach ( Curve curve in curves )

                // Calculate the width of the label save the max width
                maxLabelWidth = Math.Max( maxLabelWidth, this.Appearance.MeasureString( graphicsDevice, curve.Label, scaleFactor ).Width );

            // Is this legend horizontally stacked?
            if ( this.HorizontalStack )
            {

                // Determine the available space for horizontal stacking
                switch ( this.Placement )
                {
                    case PlacementType.Right:
                    case PlacementType.Left:
                        {

                            // Never stack if the legend is to the right or left
                            widthAvailable = 0f;
                            break;
                        }

                    case PlacementType.TopLeft:
                    case PlacementType.TopRight:
                    case PlacementType.BottomLeft:
                    case PlacementType.BottomRight:
                        {

                            // for the top & bottom, the axis frame width is available
                            widthAvailable = axisArea.Width;
                            break;
                        }

                    case PlacementType.InsideTopRight:
                    case PlacementType.InsideTopLeft:
                    case PlacementType.InsideBottomRight:
                    case PlacementType.InsideBottomLeft:
                        {

                            // for inside the axis area, use 1/2 of the axis frame width
                            widthAvailable = axisArea.Width / 2f;
                            break;
                        }

                    default:
                        {

                            // shouldn't ever happen
                            Debug.Assert( !Debugger.IsAttached, "Invalid legend location option" );
                            widthAvailable = 0f;
                            break;
                        }
                }

                // width of one legend entry
                this.ColumnWidth = charWidth * (this._LineLeftMarginCharacterWidth + this._LineRightMarginCharacterWidth + this._LineLengthCharacterWidth + this._RightMarginCharacterWidth) + maxLabelWidth;

                // Calculate the number of columns to accommodate all the legends in 
                // one or more
                if ( maxLabelWidth > 0f )
                {
                    this.Columns = Convert.ToInt32( Math.Floor( widthAvailable / this.ColumnWidth ) );
                }
                // You can never have more columns than legend entries
                if ( this.Columns > curves.Count )
                {
                    this.Columns = curves.Count;
                }
                // a safety check
                if ( this.Columns == 0 )
                {
                    this.Columns = 1;
                }
            }
            else
            {
                this.ColumnWidth = charWidth * (this._LineLeftMarginCharacterWidth + this._LineRightMarginCharacterWidth + this._LineLengthCharacterWidth + this._RightMarginCharacterWidth) + maxLabelWidth;
            }

            // total legend width
            float legendWidth = this.Columns * this.ColumnWidth;

            // The height of the legend is the actual height of the lines of text
            // (curves.Count/columns * height) 
            float legendHeight = Convert.ToSingle( Math.Ceiling( curves.Count / ( double ) this.Columns ) ) * charHeight;

            // calculate the legend area based on the above determined parameters
            // Also, adjust the plotArea and axisArea to reflect the space for the legend
            if ( curves.Count > 0 )
            {

                // The switch statement assigns the left and top edges, and adjusts the axisArea
                // as required.  The right and bottom edges are calculated at the bottom of the switch.
                switch ( this.Placement )
                {
                    case PlacementType.Right:
                        {
                            this._Area.X = drawArea.Right - legendWidth;
                            this._Area.Y = axisArea.Top;
                            axisArea.Width -= legendWidth + horizontalgap;
                            break;
                        }

                    case PlacementType.TopLeft:
                        {
                            this._Area.X = axisArea.Left;
                            this._Area.Y = axisArea.Top;
                            axisArea.Y += legendHeight + verticalgap;
                            axisArea.Height -= legendHeight + verticalgap;
                            break;
                        }

                    case PlacementType.TopRight:
                        {
                            this._Area.X = axisArea.Right - legendWidth;
                            this._Area.Y = axisArea.Top;
                            axisArea.Y += legendHeight + verticalgap;
                            axisArea.Height -= legendHeight + verticalgap;
                            break;
                        }

                    case PlacementType.BottomLeft:
                        {
                            this._Area.X = axisArea.Left;
                            this._Area.Y = drawArea.Bottom - legendHeight;
                            axisArea.Height -= legendHeight + verticalgap;
                            break;
                        }

                    case PlacementType.BottomRight:
                        {
                            this._Area.X = axisArea.Right - legendWidth;
                            this._Area.Y = drawArea.Bottom - legendHeight;
                            axisArea.Height -= legendHeight + verticalgap;
                            break;
                        }

                    case PlacementType.Left:
                        {
                            this._Area.X = drawArea.Left;
                            this._Area.Y = axisArea.Top;
                            axisArea.X += legendWidth + horizontalgap;
                            axisArea.Width -= legendWidth + horizontalgap;
                            break;
                        }

                    case PlacementType.InsideTopRight:
                        {
                            this._Area.X = axisArea.Right - legendWidth;
                            this._Area.Y = axisArea.Top;
                            break;
                        }

                    case PlacementType.InsideTopLeft:
                        {
                            this._Area.X = axisArea.Left;
                            this._Area.Y = axisArea.Top;
                            break;
                        }

                    case PlacementType.InsideBottomRight:
                        {
                            this._Area.X = axisArea.Right - legendWidth;
                            this._Area.Y = axisArea.Bottom - legendHeight;
                            break;
                        }

                    case PlacementType.InsideBottomLeft:
                        {
                            this._Area.X = axisArea.Left;
                            this._Area.Y = axisArea.Bottom - legendHeight;
                            break;
                        }
                }

                // Calculate the Right and Bottom edges of the area
                this._Area.Width = legendWidth;
                this._Area.Height = legendHeight;
            }

            // return adjusted axis rectangle
            return axisArea;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> The appearance. </summary>

        /// <summary>
        /// Gets the <see cref="isr.Visuals.TextAppearance">TextAppearance</see> class used to render the
        /// <see cref="Legend"/> entries.
        /// </summary>
        /// <value> A <see cref="isr.Visuals.TextAppearance">TextAppearance</see> instance. </value>
        public TextAppearance Appearance { get; private set; }

        /// <summary>
        /// Gets the number of columns (horizontal stacking) to be used for drawing the legend.
        /// </summary>
        /// <value> A <see cref="T:System.Integer">integer</see> value. </value>
        public int Columns { get; set; }

        /// <summary> Gets the width of each column in the legend (pixels) </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float ColumnWidth { get; set; }

        /// <summary> Gets the <see cref="Legend"/> <see cref="Frame"/>. </summary>
        /// <value> A <see cref="Frame"/> value. </value>
        public Frame Frame { get; set; }

        /// <summary> Gets a property that shows or hides the <see cref="Legend"/> entirely. </summary>
        /// <value>
        /// A <see cref="System.Boolean">Boolean</see> True to show the
        /// <see cref="Legend"/> or False to hide it.
        /// </value>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets a property that allows the <see cref="Legend"/> items to stack horizontally in addition
        /// to the vertical stacking.
        /// </summary>
        /// <value>
        /// A <see cref="System.Boolean">Boolean</see> True to allow horizontal stacking or False
        /// otherwise.
        /// </value>
        public bool HorizontalStack { get; set; }

        /// <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
        private readonly Pane _Pane;

        /// <summary>
        /// Gets the location of the <see cref="Legend"/> on the
        /// <see cref="Pane"/> using the <see cref="Placement"/> type.
        /// </summary>
        /// <value> A <see cref="PlacementType"/> </value>
        public PlacementType Placement { get; set; }

        /// <summary> The area. </summary>
        private RectangleF _Area;

        /// <summary>
        /// get the bounding rectangle for the <see cref="Legend"/> in screen coordinates.
        /// </summary>
        /// <value> A screen rectangle in pixels. </value>
        public RectangleF Area => this._Area;

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A System.String value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusMessage { get; set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="Legend"/> class that defines the default property values
    /// for the <see cref="isr.Visuals.Legend"/> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class LegendDefaults : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private LegendDefaults() : base()
        {
            this.FrameWidth = 1f;
            this.FrameColor = Color.Black;
            this.FillColor = Color.White;
            this.Placement = PlacementType.TopLeft;
            this.Framed = true;
            this.Visible = true;
            this.Filled = true;
            this.HorizontalStack = true;
            this.Font = new Font( "Arial", 12f, FontStyle.Regular );
            this.FontColor = Color.Black;
            this.FontBold = false;
            this.FontItalic = false;
            this.FontUnderline = false;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static LegendDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static LegendDefaults Get()
        {
            if ( _Instance is null || _Instance.IsDisposed )
            {
                lock ( SyncLocker )
                    _Instance = new LegendDefaults();
            }

            return _Instance;
        }

        #region "IDisposable Support"

        /// <summary> Gets or sets a value indicating whether this instance is disposed. </summary>
        /// <value> <c>True</c> if this instance is disposed; otherwise, <c>False</c>. </value>
        private bool IsDisposed { get; set; } // To detect redundant calls

        /// <summary> Releases unmanaged and - optionally - managed resources. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged resources. </param>
        private void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        // dispose managed state (managed objects).
                        if ( this.Font is object )
                        {
                            this.Font.Dispose();
                            this.Font = null;
                        }
                    }
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }
        #endregion

        #endregion

        /// <summary>
        /// Gets or sets the default pen width for the <see cref="Legend"/> frame border.
        /// (<see cref="Legend.Frame"/> property).
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float FrameWidth { get; set; }

        /// <summary>
        /// Gets or sets the default color for the <see cref="Legend"/> frame border.
        /// (<see cref="Legend.Frame"/> property).
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color FrameColor { get; set; }

        /// <summary>
        /// Gets or sets the default color for the <see cref="Legend"/> background.
        /// (<see cref="Legend.Frame"/> property).  Use of this color depends on the status of the
        /// <see cref="Legend.Frame"/>property.
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color FillColor { get; set; }

        /// <summary>
        /// Gets or sets the default placement for the <see cref="Legend"/> on the graph
        /// (<see cref="Legend.Placement"/> property).  This property is defined as a
        /// <see cref="isr.Visuals.PlacementType"/> enumeration.
        /// </summary>
        /// <value> The placement. </value>
        public PlacementType Placement { get; set; }

        /// <summary>
        /// Gets or sets the default frame mode for the <see cref="Legend"/>. (<see cref="Legend.Frame"/>
        /// property). true to draw a frame around the <see cref="Legend.Area"/>, false otherwise.
        /// </summary>
        /// <value> The framed. </value>
        public bool Framed { get; set; }

        /// <summary>
        /// Gets or sets the default display mode for the <see cref="Legend"/>.
        /// (<see cref="Legend.Visible"/> property). true to show the legend, false to hide it.
        /// </summary>
        /// <value> The visible. </value>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the default fill mode for the <see cref="Legend"/> background
        /// (<see cref="Legend.Frame"/> property). true to fill-in the background with color, false to
        /// leave the background transparent.
        /// </summary>
        /// <value> The filled. </value>
        public bool Filled { get; set; }

        /// <summary>
        /// Gets or sets the default horizontal stacking mode for the <see cref="Legend"/>
        /// (<see cref="Legend.HorizontalStack"/> property). True to allow horizontal legend item
        /// stacking, false to allow only vertical legend orientation.
        /// </summary>
        /// <value> A stack of horizontals. </value>
        public bool HorizontalStack { get; set; }

        /// <summary>
        /// Gets or sets the default <see cref="System.Drawing.Font">Font</see>
        /// of the <see cref="Title"/> caption.
        /// </summary>
        /// <value> The font. </value>
        public Font Font { get; set; }

        /// <summary>
        /// Gets or sets the default font color for the <see cref="Legend"/> entries
        /// (<see cref="TextAppearance.FontColor"/> property).
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color FontColor { get; set; }

        /// <summary>
        /// Gets or sets the default font bold mode for the <see cref="Legend"/> entries
        /// (<see cref="TextAppearance.Bold"/> property). true
        /// for a bold typeface, false otherwise.
        /// </summary>
        /// <value> The font bold. </value>
        public bool FontBold { get; set; }

        /// <summary>
        /// Gets or sets the default font italic mode for the <see cref="Legend"/> entries
        /// (<see cref="TextAppearance.Italic"/> property). true for an italic typeface, false otherwise.
        /// </summary>
        /// <value> The font italic. </value>
        public bool FontItalic { get; set; }

        /// <summary>
        /// Gets or sets the default font underline mode for the <see cref="Legend"/> entries
        /// (<see cref="TextAppearance.IsUnderline"/> property). true for an underlined typeface, false
        /// otherwise.
        /// </summary>
        /// <value> The font underline. </value>
        public bool FontUnderline { get; set; }
    }
}
#endregion

