using System;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary> Contains a list of <see cref="TextBox"/> objects to display on the graph. </summary>
    /// <remarks>
    /// Declare <see cref="A:NotInheritable"/> so as to allow calling base methods in the
    /// constructor. <para>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public sealed class TextBoxCollection : System.Collections.ObjectModel.Collection<TextBox>, ICloneable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Default constructor for the <see cref="TextBoxCollection"/> collection class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are
        /// null. </exception>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public TextBoxCollection( Pane drawingPane ) : base()
        {
            if ( drawingPane is null )
            {
                throw new ArgumentNullException( nameof( drawingPane ) );
            }

            this._Pane = drawingPane;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The TextBoxCollection object from which to copy. </param>
        public TextBoxCollection( TextBoxCollection model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            foreach ( var item in model )
                this.Add( new TextBox( item ) );
            this._Pane = model._Pane;
        }

        #endregion

        #region " CUSTOM COLLECTION METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the TextBoxCollection. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the TextBoxCollection. </returns>
        public TextBoxCollection Copy()
        {
            return new TextBoxCollection( this );
        }

        /// <summary>
        /// Renders all the <see cref="TextBox">Test Boxes</see> to the specified <see cref="Graphics"/>
        /// device.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        public void Draw( Graphics graphicsDevice )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // Clip everything to the panel area
            graphicsDevice.SetClip( this._Pane.PaneArea );

            // Loop for each curve
            foreach ( TextBox textBox in this )
                textBox.Draw( graphicsDevice );
        }

        /// <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
        private readonly Pane _Pane;

        #endregion

    }
}
