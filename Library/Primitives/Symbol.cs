using System;
using System.ComponentModel;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary>
    /// Handles the drawing of the curve <see cref="Symbol"/> objects. The symbols are the small
    /// shapes that appear over each defined point along the curve.
    /// </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class Symbol : ICloneable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs a <see cref="Symbol"/> with default values as defined in the
        /// <see cref="SymbolDefaults"/> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public Symbol( Pane drawingPane ) : base()
        {
            var symbolDefaults = SymbolDefaults.Get();
            this.Size = symbolDefaults.Size;
            this.Shape = symbolDefaults.Shape;
            this.LineWidth = symbolDefaults.LineWidth;
            this.LineColor = symbolDefaults.LineColor;
            this.Visible = symbolDefaults.Visible;
            this.Filled = symbolDefaults.Filled;
            this._Pane = drawingPane;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Symbol object from which to copy. </param>
        public Symbol( Symbol model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.Size = model.Size;
            this.Shape = model.Shape;
            this.LineWidth = model.LineWidth;
            this.LineColor = model.LineColor;
            this.Visible = model.Visible;
            this.Filled = model.Filled;
            this._Pane = model._Pane;
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Symbol. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Symbol. </returns>
        public Symbol Copy()
        {
            return new Symbol( this );
        }

        /// <summary>
        /// Draw the <see cref="Symbol"/> to the specified <see cref="Graphics"/> device at the specified
        /// location.  This method draws a single symbol.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="x">              The x position of the center of the symbol in screen pixels. </param>
        /// <param name="y">              The y position of the center of the symbol in screen pixels. </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        public void Draw( Graphics graphicsDevice, float x, float y, double scaleFactor )
        {

            // Only draw if the symbol is visible
            if ( !this.Visible )
                return;
            if ( graphicsDevice is null )
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            using var brush = new SolidBrush( this.LineColor );
            using var pen = new Pen( this.LineColor, this.LineWidth );
            // Fill or draw the symbol as required
            if ( this.Filled )
            {
                this.Fill( graphicsDevice, x, y, scaleFactor, pen, brush );
            }
            else
            {
                this.Draw( graphicsDevice, x, y, scaleFactor, pen );
            }
        }

        /// <summary>
        /// Draw the <see cref="Symbol"/> to the specified <see cref="Graphics"/>
        /// device at the specified list of locations.  This method draws a series of symbols, and is
        /// intended to provide a speed improvement over the single Draw() method.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="x">              The x position of the center of the symbol in screen pixels. </param>
        /// <param name="y">              The y position of the center of the symbol in screen pixels. </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        public void Draw( Graphics graphicsDevice, float[] x, float[] y, double scaleFactor )
        {

            // Only draw if the symbol is visible
            if ( !this.Visible )
            {
                return;
            }

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            if ( x is null )
            {
                throw new ArgumentNullException( nameof( x ) );
            }

            if ( y is null )
            {
                throw new ArgumentNullException( nameof( y ) );
            }

            // Only draw if the symbol is visible
            using var brush = new SolidBrush( this.LineColor );
            using var pen = new Pen( this.LineColor, this.LineWidth );
            int numPoints = x.Length;
            int i;
            var loopTo = numPoints - 1;
            for ( i = 0; i <= loopTo; i++ )
            {
                if ( x[i] != float.MaxValue && y[i] != float.MaxValue )
                {
                    // Fill or draw the symbol as required
                    if ( this.Filled )
                    {
                        this.Fill( graphicsDevice, x[i], y[i], scaleFactor, pen, brush );
                    }
                    else
                    {
                        this.Draw( graphicsDevice, x[i], y[i], scaleFactor, pen );
                    }
                }
            }
        }

        /// <summary>
        /// Draw the <see cref="Symbol"/> to the specified <see cref="Graphics"/>
        /// device at the specified list of locations.  This method draws a series of symbols, and is
        /// intended to provide a speed improvement over the single Draw() method.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="x">              The x position of the center of the symbol in screen pixels. </param>
        /// <param name="y">              The y position of the center of the symbol in screen pixels. </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <param name="ignoreMissing">  True to ignore missing values. </param>
        public void Draw( Graphics graphicsDevice, float[] x, float[] y, double scaleFactor, bool ignoreMissing )
        {

            // Only draw if the symbol is visible
            if ( !this.Visible )
            {
                return;
            }

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            if ( x is null )
            {
                throw new ArgumentNullException( nameof( x ) );
            }

            if ( y is null )
            {
                throw new ArgumentNullException( nameof( y ) );
            }

            using var brush = new SolidBrush( this.LineColor );
            using var pen = new Pen( this.LineColor, this.LineWidth );
            int numPoints = x.Length;
            int i;
            var loopTo = numPoints - 1;
            for ( i = 0; i <= loopTo; i++ )
            {
                if ( ignoreMissing || x[i] != float.MaxValue && y[i] != float.MaxValue )
                {
                    // Fill or draw the symbol as required
                    if ( this.Filled )
                    {
                        this.Fill( graphicsDevice, x[i], y[i], scaleFactor, pen, brush );
                    }
                    else
                    {
                        this.Draw( graphicsDevice, x[i], y[i], scaleFactor, pen );
                    }
                }
            }
        }

        /// <summary>
        /// Draw the <see cref="Symbol"/> (outline only) to the specified
        /// <see cref="Graphics"/> device at the specified location.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="x">              The x position of the center of the symbol in screen pixels. </param>
        /// <param name="y">              The y position of the center of the symbol in screen pixels. </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <param name="pen">            A pen with attributes of <see cref="Color"/> and
        /// <see cref="LineWidth"/> for this symbol. </param>
        public void Draw( Graphics graphicsDevice, float x, float y, double scaleFactor, Pen pen )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            float scaledSize = Convert.ToSingle( this.Size * scaleFactor );
            float hsize = scaledSize / 2f;
            float hsize1 = hsize + 1f;
            switch ( this.Shape )
            {
                case ShapeType.Square:
                    {
                        graphicsDevice.DrawLine( pen, x - hsize, y - hsize, x + hsize, y - hsize );
                        graphicsDevice.DrawLine( pen, x + hsize, y - hsize, x + hsize, y + hsize );
                        graphicsDevice.DrawLine( pen, x + hsize, y + hsize, x - hsize, y + hsize );
                        graphicsDevice.DrawLine( pen, x - hsize, y + hsize, x - hsize, y - hsize );
                        break;
                    }

                case ShapeType.Diamond:
                    {
                        graphicsDevice.DrawLine( pen, x, y - hsize, x + hsize, y );
                        graphicsDevice.DrawLine( pen, x + hsize, y, x, y + hsize );
                        graphicsDevice.DrawLine( pen, x, y + hsize, x - hsize, y );
                        graphicsDevice.DrawLine( pen, x - hsize, y, x, y - hsize );
                        break;
                    }

                case ShapeType.Triangle:
                    {
                        graphicsDevice.DrawLine( pen, x, y - hsize, x + hsize, y + hsize );
                        graphicsDevice.DrawLine( pen, x + hsize, y + hsize, x - hsize, y + hsize );
                        graphicsDevice.DrawLine( pen, x - hsize, y + hsize, x, y - hsize );
                        break;
                    }

                case ShapeType.Circle:
                    {
                        graphicsDevice.DrawEllipse( pen, x - hsize, y - hsize, scaledSize, scaledSize );
                        break;
                    }

                case ShapeType.XCross:
                    {
                        graphicsDevice.DrawLine( pen, x - hsize, y - hsize, x + hsize1, y + hsize1 );
                        graphicsDevice.DrawLine( pen, x + hsize, y - hsize, x - hsize1, y + hsize1 );
                        break;
                    }

                case ShapeType.Plus:
                    {
                        graphicsDevice.DrawLine( pen, x, y - hsize, x, y + hsize1 );
                        graphicsDevice.DrawLine( pen, x - hsize, y, x + hsize1, y );
                        break;
                    }

                case ShapeType.Star:
                    {
                        graphicsDevice.DrawLine( pen, x, y - hsize, x, y + hsize1 );
                        graphicsDevice.DrawLine( pen, x - hsize, y, x + hsize1, y );
                        graphicsDevice.DrawLine( pen, x - hsize, y - hsize, x + hsize1, y + hsize1 );
                        graphicsDevice.DrawLine( pen, x + hsize, y - hsize, x - hsize1, y + hsize1 );
                        break;
                    }

                case ShapeType.TriangleDown:
                    {
                        graphicsDevice.DrawLine( pen, x, y + hsize, x + hsize, y - hsize );
                        graphicsDevice.DrawLine( pen, x + hsize, y - hsize, x - hsize, y - hsize );
                        graphicsDevice.DrawLine( pen, x - hsize, y - hsize, x, y + hsize );
                        break;
                    }
            }
        }

        /// <summary>
        /// Renders the filled <see cref="Symbol"/> to the specified <see cref="Graphics"/>
        /// device at the specified location.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="x">              The x position of the center of the symbol in screen pixels. </param>
        /// <param name="y">              The y position of the center of the symbol in screen pixels. </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <param name="pen">            A pen with attributes of <see cref="Color"/> and
        /// <see cref="LineWidth"/> for this symbol. </param>
        /// <param name="brush">          A brush with the <see cref="Color"/> attribute for this symbol. </param>
        public void Fill( Graphics graphicsDevice, float x, float y, double scaleFactor, Pen pen, Brush brush )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            if ( brush is null )
            {
                throw new ArgumentNullException( nameof( brush ) );
            }

            float scaledSize = Convert.ToSingle( this.Size * scaleFactor );
            float hsize = scaledSize / 2f;
            float hsize4 = scaledSize / 3f;
            float hsize41 = hsize4 + 1f;
            float hsize1 = hsize + 1f;
            var polyPt = new PointF[5];
            switch ( this.Shape )
            {
                case ShapeType.Square:
                    {
                        graphicsDevice.FillRectangle( brush, x - hsize, y - hsize, scaledSize, scaledSize );
                        break;
                    }

                case ShapeType.Diamond:
                    {
                        polyPt[0].X = x;
                        polyPt[0].Y = y - hsize;
                        polyPt[1].X = x + hsize;
                        polyPt[1].Y = y;
                        polyPt[2].X = x;
                        polyPt[2].Y = y + hsize;
                        polyPt[3].X = x - hsize;
                        polyPt[3].Y = y;
                        polyPt[4] = polyPt[0];
                        graphicsDevice.FillPolygon( brush, polyPt );
                        break;
                    }

                case ShapeType.Triangle:
                    {
                        polyPt[0].X = x;
                        polyPt[0].Y = y - hsize;
                        polyPt[1].X = x + hsize;
                        polyPt[1].Y = y + hsize;
                        polyPt[2].X = x - hsize;
                        polyPt[2].Y = y + hsize;
                        polyPt[3] = polyPt[0];
                        graphicsDevice.FillPolygon( brush, polyPt );
                        break;
                    }

                case ShapeType.Circle:
                    {
                        graphicsDevice.FillEllipse( brush, x - hsize, y - hsize, scaledSize, scaledSize );
                        break;
                    }

                case ShapeType.XCross:
                    {
                        graphicsDevice.FillRectangle( brush, x - hsize4, y - hsize4, hsize4 + hsize41, hsize4 + hsize41 );
                        graphicsDevice.DrawLine( pen, x - hsize, y - hsize, x + hsize1, y + hsize1 );
                        graphicsDevice.DrawLine( pen, x + hsize, y - hsize, x - hsize1, y + hsize1 );
                        break;
                    }

                case ShapeType.Plus:
                    {
                        graphicsDevice.FillRectangle( brush, x - hsize4, y - hsize4, hsize4 + hsize41, hsize4 + hsize41 );
                        graphicsDevice.DrawLine( pen, x, y - hsize, x, y + hsize1 );
                        graphicsDevice.DrawLine( pen, x - hsize, y, x + hsize1, y );
                        break;
                    }

                case ShapeType.Star:
                    {
                        graphicsDevice.FillRectangle( brush, x - hsize4, y - hsize4, hsize4 + hsize41, hsize4 + hsize41 );
                        graphicsDevice.DrawLine( pen, x, y - hsize, x, y + hsize1 );
                        graphicsDevice.DrawLine( pen, x - hsize, y, x + hsize1, y );
                        graphicsDevice.DrawLine( pen, x - hsize, y - hsize, x + hsize1, y + hsize1 );
                        graphicsDevice.DrawLine( pen, x + hsize, y - hsize, x - hsize1, y + hsize1 );
                        break;
                    }

                case ShapeType.TriangleDown:
                    {
                        polyPt[0].X = x;
                        polyPt[0].Y = y + hsize;
                        polyPt[1].X = x + hsize;
                        polyPt[1].Y = y - hsize;
                        polyPt[2].X = x - hsize;
                        polyPt[2].Y = y - hsize;
                        polyPt[3] = polyPt[0];
                        graphicsDevice.FillPolygon( brush, polyPt );
                        break;
                    }
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Gets or sets the filled mode of the <see cref="Symbol"/> shape.  Set to True to fill the
        /// shape with color, or False for an outline symbol.  Note that symbols that are not closed,
        /// such as <see cref="ShapeType.Plus"/>
        /// cannot be filled.
        /// </summary>
        /// <value> The filled. </value>
        public bool Filled { get; set; }

        /// <summary> Gets or sets a property that shows or hides the <see cref="Symbol"/>. </summary>
        /// <value> True to show the symbol, False to hide it. </value>
        public bool Visible { get; set; }

        /// <summary> Gets or sets the pen width for drawing the <see cref="Symbol"/> outline. </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float LineWidth { get; set; }

        /// <summary> Gets or sets the line color of the <see cref="Symbol"/> </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color LineColor { get; set; }

        /// <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
        private readonly Pane _Pane;

        /// <summary> Gets or sets the size of the <see cref="Symbol"/> </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float Size { get; set; }

        /// <summary> Gets or sets the type (shape) of the <see cref="Symbol"/> </summary>
        /// <value> A <see cref="ShapeType"/> value. </value>
        public ShapeType Shape { get; set; }

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A System.String value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusMessage { get; set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="Symbol"/> class that defines the default property values
    /// for the <see cref="isr.Visuals.Symbol"/> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class SymbolDefaults
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private SymbolDefaults() : base()
        {
            this.Filled = false;
            this.Visible = true;
            this.LineColor = Color.Red;
            this.LineWidth = 1.0f;
            this.Shape = ShapeType.Square;
            this.Size = 7f;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static SymbolDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static SymbolDefaults Get()
        {
            if ( _Instance is null )
            {
                lock ( SyncLocker )
                    _Instance = new SymbolDefaults();
            }

            return _Instance;
        }

        #endregion

        /// <summary>
        /// Gets or sets the default fill mode for symbols (<see cref="Symbol.Filled"/> property). true
        /// to have symbols filled in with color, false to leave them as outlines.
        /// </summary>
        /// <value> The filled. </value>
        public bool Filled { get; set; }

        /// <summary>
        /// Gets or sets the default display mode for symbols (<see cref="Symbol.Visible"/> property).
        /// true to display symbols, false to hide them.
        /// </summary>
        /// <value> The visible. </value>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the default color for drawing symbols (<see cref="Symbol.LineColor"/> property).
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color LineColor { get; set; }

        /// <summary>
        /// Gets or sets the default pen width to be used for drawing curve symbols
        /// (<see cref="Symbol.LineWidth"/> property).
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float LineWidth { get; set; }

        /// <summary>
        /// Gets or sets the default symbol shape type for curves (<see cref="isr.Visuals.ShapeType"/>
        /// property). Defined as a <see cref="isr.Visuals.ShapeType"/> enumeration.
        /// </summary>
        /// <value> The shape. </value>
        public ShapeType Shape { get; set; }

        /// <summary>
        /// Gets or sets the default size for curve symbols (<see cref="Symbol.Size"/>.
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float Size { get; set; }
    }
}
#endregion

