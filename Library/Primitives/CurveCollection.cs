using System;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary>
    /// A collection class containing a list of <see cref="Curve"/> objects that define the set of
    /// curves to be displayed on the graph.
    /// </summary>
    /// <remarks>
    /// Declare <see cref="A:NotInheritable"/> so as to allow calling base methods in the
    /// constructor. <para>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public sealed class CurveCollection : System.Collections.ObjectModel.Collection<Curve>, ICloneable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor for the collection class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public CurveCollection( Pane drawingPane ) : base()
        {
            this._Pane = drawingPane;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The XAxis object from which to copy. </param>
        public CurveCollection( CurveCollection model ) : base()
        {
            if ( model is null )
                throw new ArgumentNullException( nameof( model ) );
            foreach ( var item in model )
                this.Add( new Curve( item ) );
            this._Pane = this._Pane;
        }

        #endregion

        #region " CUSTOM COLLECTION METHODS "

        /// <summary> Add a <see cref="Curve"/> to the collection. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="curve"> A reference to the <see cref="Curve"/> object to add. </param>
        public new void Add( Curve curve )
        {
            if ( curve is null )
            {
                throw new ArgumentNullException( nameof( curve ) );
            }

            base.Add( curve );
            curve.SerialNumber = this.Count;
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the CurveCollection. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the CurveCollection. </returns>
        public CurveCollection Copy()
        {
            return new CurveCollection( this );
        }

        /// <summary>
        /// Renders all the <see cref="Curve">Curves</see> to the specified <see cref="Graphics"/> device.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        public void Draw( Graphics graphicsDevice )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // Loop for each curve
            foreach ( Curve curve in this )
                // Render the curve
                curve.Draw( graphicsDevice ); // , axisArea, scaleFactor)
        }

        /// <summary>
        /// Adjusts the <see cref="Pane.AxisArea"/> for the
        /// <see cref="isr.Visuals.Stylus">Stylus</see> size.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="scaleFactor"> The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <param name="axisArea">    The rectangle that contains the area bounded by the axes, in
        /// pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso> </param>
        /// <returns> Adjusted <see cref="Pane.AxisArea"/> </returns>
        public RectangleF GetAxisArea( double scaleFactor, RectangleF axisArea )
        {

            // Loop for each curve
            foreach ( Curve curve in this )
                // Render the curve
                axisArea = curve.GetAxisArea( scaleFactor, axisArea );

            // return adjusted axis rectangle
            return axisArea;
        }

        /// <summary> get the extended range of all specified curves. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="ignoreInitial"> Affects how initial zero y values are treated for setting the
        /// range of X values.  If True, then initial zero data points are
        /// ignored when determining the X range.  All data after the first
        /// non-zero Y value are included. </param>
        /// <param name="isY2Axis">      True to use Y2 Axis.  Otherwise Y Axis. </param>
        /// <returns> The calculated range. </returns>
        public PlanarRangeR GetRange( bool ignoreInitial, bool isY2Axis )
        {

            // get the empty range
            var range = PlanarRangeR.Empty;

            // Loop over each curve in the collection
            foreach ( Curve curve in this )
            {
                if ( isY2Axis && curve.IsY2Axis || !isY2Axis && !curve.IsY2Axis )
                {
                    _ = range.ExtendRange( curve.GetRange( ignoreInitial ) );
                }
            }

            if ( range.Equals( PlanarRangeR.Empty ) )
            {
                // Use unit range if no data were available
                return PlanarRangeR.Unity;
            }
            else
            {
                return range;
            }
        }

        /// <summary>
        /// Determine if there is any data in any of the <see cref="Curve"/>
        /// objects for this graph.  This method does not verify valid data, it only checks to see if
        /// <see cref="Curve.PointCount"/> &gt; 0.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> <c>True</c> if there is any data, false otherwise. </returns>
        public bool HasData()
        {
            foreach ( Curve curve in this )
            {
                if ( curve.PointCount > 0 )
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Rescale all the time series <see cref="Axis"/> objects in the list to the specified
        /// <see cref="CurveCollection"/> by calling the <see cref="Axis.Rescale"/>
        /// method of each <see cref="Axis"/> object.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void ScaleTimeSeriesAxis()
        {

            // Loop for each curve
            foreach ( Curve curve in this )
                curve.ScaleTimeSeriesAxis();
        }

        /// <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
        private readonly Pane _Pane;

        #endregion

    }
}
