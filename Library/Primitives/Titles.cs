using System;
using System.ComponentModel;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary> Handles specification and drawing of title. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class Title : ICloneable, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Default constructor for <see cref="Pane"/> Title sets all title properties to default values
        /// as defined in the <see cref="TitleDefaults"/> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public Title( Pane drawingPane ) : base()
        {
            if ( drawingPane is null )
                throw new ArgumentNullException( nameof( drawingPane ) );
            this.Caption = string.Empty;
            var titleDefaults = TitleDefaults.Get();
            this.Visible = titleDefaults.Visible;
            this.Appearance = new TextAppearance( titleDefaults.Font, titleDefaults.FontColor );
            this.Appearance.Frame.Filled = titleDefaults.Filled;
            this.Appearance.Frame.IsOutline = titleDefaults.Framed;
            this.Appearance.Frame.Visible = titleDefaults.Framed | titleDefaults.Filled;
            this.Pane = drawingPane;
        }

        /// <summary>
        /// Default constructor for <see cref="Pane"/> Title sets all title properties to default values
        /// as defined in the <see cref="TitleDefaults"/> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="title">       The title string. </param>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public Title( string title, Pane drawingPane ) : this( drawingPane )
        {
            if ( string.IsNullOrWhiteSpace( title ) )
            {
                title = string.Empty;
            }

            this.Caption = title;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Title object from which to copy. </param>
        public Title( Title model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.Appearance = model.Appearance.Copy();
            this.Visible = model.Visible;
            this.Caption = model.Caption;
            this.Pane = model.Pane;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !(this.IsDisposed == true) )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        if ( this.Appearance is object )
                        {
                            this.Appearance.Dispose();
                            this.Appearance = null;
                        }
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Title. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of Title. </returns>
        public Title Copy()
        {
            return new Title( this );
        }

        /// <summary>
        /// Renders the title to the specified <see cref="Graphics"/> device.
        /// The text, frame, and fill options will be rendered as required.
        /// </summary>
        /// <remarks>
        /// Employs the transform matrix in order to position the test at any angle while also allowing
        /// the location, or anchor point, to be at a user-specified alignment.  A caption can be located
        /// at a given point on the graph such that the left or center or right and top or middle or
        /// bottom can be the anchor point.  After thus shifting the origin so that the anchor point will
        /// be at the (x,y), the method rotates the coordinate system to accommodate the text angle.  It
        /// then makes a translation to account for the fact that the text rendering method expects to
        /// draw the test based on a top-center location while the user may have specified another
        /// alignment.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="[text]">         A <see cref="System.String">String</see> value containing the
        /// text to be displayed.  This can be multiple lines, separated
        /// by new line ('\n')
        /// characters. </param>
        /// <param name="x">              The X location to display the text, in screen coordinates,
        /// relative to the horizontal (<see cref="HorizontalAlignment"/>)
        /// alignment parameter <paramref name="alignH"/> </param>
        /// <param name="y">              The Y location to display the text, in screen coordinates,
        /// relative to the vertical (<see cref="VerticalAlignment"/>
        /// alignment parameter <paramref name="alignV"/> </param>
        /// <param name="alignH">         A <see cref="HorizontalAlignment"/> alignment value. </param>
        /// <param name="alignV">         A <see cref="VerticalAlignment"/> alignment value. </param>
        /// <param name="scaleFactor">    The scaling factor to be used for rendering objects.  This is
        /// calculated and passed down by the parent
        /// <see cref="Pane"/> object using the
        /// <see cref="Pane.GetScaleFactor"/>
        /// method, and is used to proportionally adjust font sizes, etc.
        /// according to the actual size of the graph. </param>
        public virtual void Draw( Graphics graphicsDevice, string text, float x, float y, HorizontalAlignment alignH, VerticalAlignment alignV, double scaleFactor )
        {
            if ( !this.Visible || string.IsNullOrWhiteSpace( text ) )
            {
                return;
            }

            // validate arguments.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            this.Appearance.Draw( graphicsDevice, text, x, y, alignH, alignV, scaleFactor );
        }

        /// <summary>
        /// Renders the title to the specified <see cref="Graphics"/> device.
        /// The text, frame, and fill options will be rendered as required.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        public virtual void Draw( Graphics graphicsDevice )
        {
            if ( !this.Visible || string.IsNullOrWhiteSpace( this.Caption ) )
            {
                return;
            }

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // Clip everything to the PaneArea
            graphicsDevice.SetClip( this.Pane.PaneArea );
            this.Draw( graphicsDevice, this.Caption, Convert.ToSingle( this.Pane.DrawArea.Left + this.Pane.DrawArea.Right ) / 2f, this.Pane.DrawArea.Top, HorizontalAlignment.Center, VerticalAlignment.Top, this.Pane.ScaleFactor );
        }

        /// <summary> Adjusts the <see cref="Pane.AxisArea"/> for the title size. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <param name="axisArea">       The rectangle that contains the area bounded by the axes, in
        /// pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso> </param>
        /// <returns> Adjusted <see cref="Pane.AxisArea"/> </returns>
        internal RectangleF GetAxisArea( Graphics graphicsDevice, double scaleFactor, RectangleF axisArea )
        {
            if ( !this.Visible )
            {
                return axisArea;
            }

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // Leave room for the pane title

            // get scaled values for the pane gap and character height
            // Dim halfCharHeight As Single = Me._appearance.ScaleFont(scaleFactor).Height / 2.0F
            var titleSize = this.Appearance.MeasureString( graphicsDevice, this.Caption, scaleFactor );

            // Leave room for the title height, plus a line spacing of charHeight/2
            axisArea.Y += titleSize.Height; // + halfCharHeight
            axisArea.Height -= titleSize.Height; // + halfCharHeight

            // return adjusted axis rectangle
            return axisArea;
        }

        /// <summary>
        /// get a <see cref="System.Drawing.SizeF">Size</see> structure representing the width and height
        /// of the title caption based on the scaled font size.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
        /// <see cref="Pane.BaseDimension"/>.  This scaling factor is
        /// calculated by the <see cref="Pane.GetScaleFactor"/> method.
        /// The scale factor is applied to fonts, symbols, etc. </param>
        /// <returns> Scaled <see cref="System.Drawing.SizeF">Size</see> dimensions in pixels,. </returns>
        internal SizeF MeasureString( Graphics graphicsDevice, double scaleFactor )
        {

            // validate argument.
            return graphicsDevice is null
                ? throw new ArgumentNullException( nameof( graphicsDevice ) )
                : this.Appearance.MeasureString( graphicsDevice, this.Caption, scaleFactor );
        }

        /// <summary> Return the title caption. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A <see cref="System.String"/> value. </returns>
        public override string ToString()
        {
            return this.Caption;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// gets a reference to the <see cref="isr.Visuals.TextAppearance">TextAppearance</see> class
        /// used to render the title.
        /// </summary>
        /// <value> A <see cref="isr.Visuals.TextAppearance">TextAppearance</see> instance. </value>
        public TextAppearance Appearance { get; set; }

        /// <summary>
        /// Gets or sets the title text.  This text can be multiple lines, separated by new line
        /// characters. For <see cref="AxisTitle"/> this normally shows the basis and dimensions of the
        /// scale range, such as "Time, [Years]".
        /// </summary>
        /// <value> A <see cref="System.String"/> property. </value>
        public string Caption { get; set; }

        /// <summary> Determines if the value <see cref="Labels"/> will be drawn. </summary>
        /// <value> A <see cref="System.Boolean">Boolean</see> </value>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see>
        /// </summary>
        /// <value> The pane. </value>
        protected Pane Pane { get; set; }

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A System.String value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusMessage { get; set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="Labels"/> class that defines the default property values
    /// for the <see cref="Labels"/> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class TitleDefaults : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private TitleDefaults() : base()
        {
            this.Visible = true;
            this.Font = new Font( "Arial", 16f, FontStyle.Regular | FontStyle.Bold );
            this.FontColor = Color.Black;
            this.Filled = false;
            this.Framed = false;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static TitleDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static TitleDefaults Get()
        {
            if ( _Instance is null || _Instance.IsDisposed )
            {
                lock ( SyncLocker )
                    _Instance = new TitleDefaults();
            }

            return _Instance;
        }

        #region "IDisposable Support"

        /// <summary> Gets or sets a value indicating whether this instance is disposed. </summary>
        /// <value> <c>True</c> if this instance is disposed; otherwise, <c>False</c>. </value>
        private bool IsDisposed { get; set; } // To detect redundant calls

        /// <summary> Releases unmanaged and - optionally - managed resources. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged resources. </param>
        private void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        // dispose managed state (managed objects).
                        if ( this.Font is object )
                        {
                            this.Font.Dispose();
                            this.Font = null;
                        }
                    }
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }
        #endregion

        #endregion

        /// <summary>
        /// Gets or sets the default display mode for the axis value <see cref="Labels"/>
        /// (<see cref="Labels.Visible"/> property). True to show the labels, False to hide them.
        /// </summary>
        /// <value> The visible. </value>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the default <see cref="System.Drawing.Font">Font</see>
        /// of the <see cref="Title"/> caption.
        /// </summary>
        /// <value> The font. </value>
        public Font Font { get; set; }

        /// <summary>
        /// Gets or sets the default font color for the <see cref="Legend"/> entries
        /// (<see cref="TextAppearance.FontColor"/> property).
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color FontColor { get; set; }

        /// <summary>
        /// Gets or sets the default font filled mode for the <see cref="Title"/> caption font
        /// specification <see cref="Title.Appearance"/>
        /// (<see cref="Frame.Filled"/> property). True for filled, False otherwise.
        /// </summary>
        /// <value> The filled. </value>
        public bool Filled { get; set; }

        /// <summary>
        /// Gets or sets the default font framed mode for the <see cref="Title"/> caption font
        /// specification <see cref="Title.Appearance"/>
        /// (<see cref="Frame.IsOutline"/> property). True for framed, False otherwise.
        /// </summary>
        /// <value> The framed. </value>
        public bool Framed { get; set; }
    }

    #endregion

    /// <summary> Handles specification and drawing of the axis title. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public class AxisTitle : Title
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Default constructor for <see cref="Axis"/> Title sets all title properties to default values
        /// as defined in the <see cref="AxisTitleDefaults"/> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public AxisTitle( Pane drawingPane ) : base( drawingPane )
        {
            var axisTitleDefaults = AxisTitleDefaults.Get();
            this.Visible = axisTitleDefaults.Visible;
            this.Appearance = new TextAppearance( axisTitleDefaults.Font, axisTitleDefaults.FontColor );
            this.Appearance.Frame.Filled = axisTitleDefaults.Filled;
            this.Appearance.Frame.IsOutline = axisTitleDefaults.Framed;
            this.Appearance.Frame.Visible = axisTitleDefaults.Framed | axisTitleDefaults.Filled;
            this.IsShowMagnitude = axisTitleDefaults.IsShowMagnitude;
        }

        /// <summary>
        /// Default constructor for <see cref="Axis"/> Title sets all title properties to default values
        /// as defined in the <see cref="AxisTitleDefaults"/> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="title">       The title string. </param>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public AxisTitle( string title, Pane drawingPane ) : this( drawingPane )
        {
            if ( string.IsNullOrWhiteSpace( title ) )
            {
                title = string.Empty;
            }

            this.Caption = title;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="model"> The Title object from which to copy. </param>
        public AxisTitle( AxisTitle model ) : base( model )
        {
            if ( model is object )
            {
                this.IsShowMagnitude = model.IsShowMagnitude;
            }
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the Title. </returns>
        public new object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of Title. </returns>
        public new AxisTitle Copy()
        {
            return new AxisTitle( this );
        }

        /// <summary>
        /// Renders the title to the specified <see cref="Graphics"/> device.
        /// The text, frame, and fill options will be rendered as required.
        /// </summary>
        /// <remarks>
        /// Employs the transform matrix in order to position the test at any angle while also allowing
        /// the location, or anchor point, to be at a user-specified alignment.  A caption can be located
        /// at a given point on the graph such that the left or center or right and top or middle or
        /// bottom can be the anchor point.  After thus shifting the origin so that the anchor point will
        /// be at the (x,y), the method rotates the coordinate system to accommodate the text angle.  It
        /// then makes a translation to account for the fact that the text rendering method expects to
        /// draw the test based on a top-center location while the user may have specified another
        /// alignment.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <param name="axis">           A <see cref="System.String">String</see> value containing the
        /// text to be displayed.  This can be multiple lines, separated
        /// by new line ('\n')
        /// characters. </param>
        /// <param name="[text]">         The [text]. </param>
        /// <param name="x">              The X location to display the text, in screen coordinates,
        /// relative to the horizontal (<see cref="HorizontalAlignment"/>)
        /// alignment parameter. </param>
        /// <param name="y">              The Y location to display the text, in screen coordinates,
        /// relative to the vertical (<see cref="VerticalAlignment"/>
        /// alignment parameter. </param>
        /// <param name="scaleFactor">    The scaling factor to be used for rendering objects.  This is
        /// calculated and passed down by the parent
        /// <see cref="Pane"/> object using the
        /// <see cref="Pane.GetScaleFactor"/>
        /// method, and is used to proportionally adjust font sizes, etc.
        /// according to the actual size of the graph. </param>
        public void Draw( Graphics graphicsDevice, Axis axis, string text, float x, float y, double scaleFactor )
        {
            if ( !this.Visible || string.IsNullOrWhiteSpace( text ) )
            {
                return;
            }

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            if ( axis is null )
            {
                throw new ArgumentNullException( nameof( axis ) );
            }

            var alignV = VerticalAlignment.Top;
            if ( axis.AxisType == AxisType.Y )
            {
                alignV = VerticalAlignment.Bottom;
            }

            // Draw the title
            base.Draw( graphicsDevice, text, x, y, HorizontalAlignment.Center, alignV, scaleFactor );
        }

        /// <summary> Return the title caption. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A <see cref="System.String"/> value. </returns>
        public override string ToString()
        {
            return this.Caption;
        }

        /// <summary>
        /// Returns the title caption including the <see cref="Scale.ScaleExponent"/>
        /// if <see cref="AxisTitle.IsShowMagnitude"/>
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="scaleExponent"> the magnitude multiplier decade for scale values. This is used to
        /// limit the size of the displayed value labels.  For example, if
        /// the value is really 2000000, then the graph could instead display
        /// 2000 with a magnitude multiplier decade of 3 (10^3). </param>
        /// <returns> A <see cref="System.String"/> value. </returns>
        public string ToString( int scaleExponent )
        {
            return scaleExponent != 0 && this.IsShowMagnitude ? $"{this.Caption} (10^{scaleExponent})" : base.ToString();
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Gets or sets the mode of displaying the magnitude label for large scale values. A "magnitude"
        /// value (power of 10) is automatically used for scaling the graph.
        /// This magnitude value is automatically appended to the end of the <see cref="Axis"/>
        /// <see cref="Title"/> (e.graphicsDevice., "(10^4)") to indicate that a magnitude is in use.
        /// This property controls whether or not the magnitude is included in the title.
        /// Note that it only affects the axis title; a magnitude value may still be used even if it is
        /// not shown in the title.
        /// </summary>
        /// <value> True to show the magnitude value, false to hide it. </value>
        public bool IsShowMagnitude { get; set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="AxisTitle" /> class that defines the default property
    /// values for the <see cref="AxisTitle" /> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class AxisTitleDefaults : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private AxisTitleDefaults() : base()
        {
            this.Visible = true;
            this.Font = new Font( "Arial", 14f, FontStyle.Regular | FontStyle.Bold );
            this.FontColor = Color.Black;
            this.Filled = false;
            this.Framed = false;
            this.IsShowMagnitude = false;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static AxisTitleDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static AxisTitleDefaults Get()
        {
            if ( _Instance is null || _Instance.IsDisposed )
            {
                lock ( SyncLocker )
                    _Instance = new AxisTitleDefaults();
            }

            return _Instance;
        }

        #region "IDisposable Support"

        /// <summary> Gets or sets a value indicating whether this instance is disposed. </summary>
        /// <value> <c>True</c> if this instance is disposed; otherwise, <c>False</c>. </value>
        private bool IsDisposed { get; set; }

        /// <summary> Releases unmanaged and - optionally - managed resources. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged resources. </param>
        private void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        // dispose managed state (managed objects).
                        if ( this.Font is object )
                        {
                            this.Font.Dispose();
                            this.Font = null;
                        }
                    }
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }
        #endregion


        #endregion

        /// <summary>
        /// Gets or sets the default display mode for the axis value <see cref="Labels"/>
        /// (<see cref="Labels.Visible"/> property). True to show the labels, False to hide them.
        /// </summary>
        /// <value> The visible. </value>
        public bool Visible { get; set; }

        /// <summary>
        /// Gets or sets the default <see cref="System.Drawing.Font">Font</see>
        /// of the <see cref="Title"/> caption.
        /// </summary>
        /// <value> The font. </value>
        public Font Font { get; set; }

        /// <summary>
        /// Gets or sets the default font color for the <see cref="Legend"/> entries
        /// (<see cref="TextAppearance.FontColor"/> property).
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color FontColor { get; set; }

        /// <summary>
        /// Gets or sets the default font filled mode for the <see cref="Title"/> caption font
        /// specification <see cref="Title.Appearance"/>
        /// (<see cref="Frame.Filled"/> property). True for filled, False otherwise.
        /// </summary>
        /// <value> The filled. </value>
        public bool Filled { get; set; }

        /// <summary>
        /// Gets or sets the default font framed mode for the <see cref="Title"/> caption font
        /// specification <see cref="Title.Appearance"/>
        /// (<see cref="Frame.IsOutline"/> property). True for framed, False otherwise.
        /// </summary>
        /// <value> The framed. </value>
        public bool Framed { get; set; }

        /// <summary>
        /// Gets or sets the default display mode for the title magnitude
        /// (<see cref="AxisTitle.IsShowMagnitude"/> property). True to show the magnitude, False to omit
        /// the magnitude.
        /// </summary>
        /// <value> The is show magnitude. </value>
        public bool IsShowMagnitude { get; set; }
    }
}

#endregion

