using System;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary> Removes flickering from painting controls with gdi+. </summary>
    /// <remarks>
    /// The class removes flickering by double buffering the drawing. It draws
    /// on a separate bitmap canvas file. Once all drawing is complete, you can then push the
    /// drawing to the main drawing canvas all at once. This will kill most if not all flickering
    /// the drawing produces. The double buffer slows down the line recorder significantly without
    /// removing much of the flickering. <para>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2005-07-27, 1.0.2034. Created </para>
    /// </remarks>
    public class GraphicsDoubleBuffer : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="canvasWidth">  The canvas width. </param>
        /// <param name="canvasHeight"> The canvas height. </param>
        public GraphicsDoubleBuffer( int canvasWidth, int canvasHeight )
        {
            this.Canvas = new Bitmap( canvasWidth, canvasHeight );
            this.GraphicsDevice = Graphics.FromImage( this.Canvas );
            this.GraphicsDevice.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        if ( this.Canvas is object )
                        {
                            this.Canvas.Dispose();
                            this.Canvas = null;
                        }

                        if ( this.GraphicsDevice is object )
                        {
                            this.GraphicsDevice.Dispose();
                            this.GraphicsDevice = null;
                        }
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS  and  PROPERTIES "

        /// <summary>The bitmap where drawing is down before it is pushed to the
        /// object.</summary>
        public Bitmap Canvas { get; set; }

        /// <summary> Reference to graphics context for the buffered canvas. </summary>
        /// <value> The graphics device. </value>
        public Graphics GraphicsDevice { get; private set; }

        /// <summary> Renders the double buffer to the screen. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="graphicsDevice"> Window forms graphics Object. </param>
        public void Render( Graphics graphicsDevice )
        {
            if ( graphicsDevice is object && this.Canvas is object )
            {
                graphicsDevice.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
                graphicsDevice.DrawImage( this.Canvas, 0, 0 );
            }
        }

        /// <summary> Returns true if double buffering can be achieved. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> True if we can double buffer, false if not. </returns>
        public bool CanDoubleBuffer()
        {
            return Graphics.FromImage( this.Canvas ) is object;
        }

        #endregion

    }
}
