using System;
using System.Diagnostics;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary>
    /// An abstract base class required to define a charts and graphs.  This class is inherited by
    /// the <see cref="Pane"/> class to define specific characteristics for those charts or graph
    /// types.
    /// </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581.x. </para>
    /// </remarks>
    public abstract class Pane : IDisposable
    {

        /// <summary>
        /// Constructs a <see cref="Pane"/> with specified and default values as defined in the
        /// <see cref="PaneDefaults"/> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="chartArea"> A rectangular screen area where the chart is to be displayed. This
        /// area can be any size, and can be resize at any time using the
        /// <see cref="PaneArea"/> property. </param>
        protected Pane( RectangleF chartArea ) : base()
        {

            /// <summary> The text boxes. </summary>
            this.TextBoxes = new TextBoxCollection( this );
            this.PaneArea = chartArea;
            this.Axes = new AxisCollection( this );
            this.Legend = new Legend( this );
            this.Curves = new CurveCollection( this );
            this.TextBoxes = new TextBoxCollection( this );
            this.Arrows = new ArrowCollection();
            this.Title = new Title( string.Empty, this );
            this.IgnoreInitial = ScaleDefaults.Get().IgnoreInitial;
            var paneDeaults = PaneDefaults.Get();
            this.Visible = paneDeaults.Filled | paneDeaults.Framed;
            this.PaneFrame = new Frame() {
                IsOutline = paneDeaults.Framed,
                Filled = paneDeaults.Filled,
                Visible = paneDeaults.Filled | paneDeaults.Framed,
                LineColor = paneDeaults.FrameLineColor,
                LineWidth = paneDeaults.FrameLineWidth,
                FillColor = paneDeaults.FrameFillColor
            };
            this.AxisFrame = new Frame() {
                IsOutline = paneDeaults.Framed,
                Filled = paneDeaults.Filled,
                Visible = paneDeaults.Filled | paneDeaults.Framed,
                LineColor = paneDeaults.FrameLineColor,
                LineWidth = paneDeaults.FrameLineWidth,
                FillColor = paneDeaults.FrameFillColor
            };
            this.BaseDimension = paneDeaults.BaseDimension;
            this._InnerGap = paneDeaults.InnerGap;
            this.OuterGap = paneDeaults.OuterGap;
            this.DrawAreaMargins = new MarginsF( this._InnerGap, this._InnerGap, this._InnerGap, this._InnerGap );
        }

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs a <see cref="Pane"/> with default area. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        protected Pane() : this( new RectangleF( 0f, 0f, 1f, 1f ) )
        {
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The Pane object from which to copy. </param>
        protected Pane( Pane model ) : this()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.Visible = model.Visible;
            this.PaneArea = model.PaneArea;
            this.Axes = model.Axes.Copy();
            this.Legend = model.Legend.Copy();
            this.Curves = model.Curves.Copy();
            this.TextBoxes = model.TextBoxes.Copy();
            this.Arrows = model.Arrows.Copy();
            this.Title = model.Title.Copy();
            this.PaneFrame = model.PaneFrame.Copy();
            this.AxisFrame = model.AxisFrame.Copy();
            this.IgnoreInitial = model.IgnoreInitial;
            this.BaseDimension = model.BaseDimension;
            this._InnerGap = model._InnerGap;
            this.OuterGap = model.OuterGap;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        this.Arrows = null;
                        this.Axes = null;
                        this.Curves = null;
                        this.TextBoxes = null;
                        if ( this.AxisFrame is object )
                        {
                            this.AxisFrame.Dispose();
                            this.AxisFrame = null;
                        }

                        if ( this.Legend is object )
                        {
                            this.Legend.Dispose();
                            this.Legend = null;
                        }

                        if ( this.PaneFrame is object )
                        {
                            this.PaneFrame.Dispose();
                            this.PaneFrame = null;
                        }

                        if ( this.Title is object )
                        {
                            this.Title.Dispose();
                            this.Title = null;
                        }

                        if ( this._XAxis is object )
                        {
                            this._XAxis.Dispose();
                            this._XAxis = null;
                        }

                        if ( this._YAxis is object )
                        {
                            this._YAxis.Dispose();
                            this._YAxis = null;
                        }

                        if ( this._Y2Axis is object )
                        {
                            this._Y2Axis.Dispose();
                            this._Y2Axis = null;
                        }
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Add an <see cref="isr.Visuals.Arrow">Arrow</see> to the plot. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="lineColor">     The line <see cref="System.Drawing.Color">Color</see> for the
        /// arrow. </param>
        /// <param name="arrowheadSize"> The size of the arrowhead, measured in points. </param>
        /// <param name="x1">            The x position of the starting point that defines the arrow.  The
        /// units of this position are specified by the
        /// <see cref="CoordinateScale"/> property. </param>
        /// <param name="y1">            The y position of the starting point that defines the arrow.  The
        /// units of this position are specified by the
        /// <see cref="CoordinateScale"/> property. </param>
        /// <param name="x2">            The x position of the ending point that defines the arrow.  The
        /// units of this position are specified by the
        /// <see cref="CoordinateScale"/> property. </param>
        /// <param name="y2">            The y position of the ending point that defines the arrow.  The
        /// units of this position are specified by the
        /// <see cref="CoordinateScale"/> property. </param>
        /// <returns> A reference to the new <see cref="isr.Visuals.Arrow">Arrow</see>. </returns>
        public Arrow AddArrow( Color lineColor, float arrowheadSize, float x1, float y1, float x2, float y2 )
        {
            var arrow = new Arrow( lineColor, arrowheadSize, x1, y1, x2, y2, this );
            this.Arrows.Add( arrow );
            return arrow;
        }

        /// <summary> Add an (<see cref="Axis">Axis</see> object to the chart. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="caption">  The <see cref="Axis.Title"/>. </param>
        /// <param name="axisType"> Specifies the <see cref="AxisType"/> </param>
        /// <returns>
        /// A reference to the newly created <see cref="Axis"/>. This can then be used to access all of
        /// the axis properties that are not defined as arguments to the <see cref="AddAxis"/> method.
        /// </returns>
        public Axis AddAxis( string caption, AxisType axisType )
        {

            // validate arguments.
            if ( string.IsNullOrWhiteSpace( caption ) )
            {
                caption = string.Empty;
            }

            // remember the reference to the axis for transformations
            switch ( axisType )
            {
                case AxisType.X:
                    {
                        this._XAxis = new Axis( caption, axisType, this );
                        this.Axes.Add( this._XAxis );
                        break;
                    }

                case AxisType.Y:
                    {
                        this._YAxis = new Axis( caption, axisType, this );
                        this.Axes.Add( this._YAxis );
                        break;
                    }

                case AxisType.Y2:
                    {
                        this._Y2Axis = new Axis( caption, axisType, this );
                        this.Axes.Add( this._Y2Axis );
                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "Unhandled axis type" );
                        break;
                    }
            }

            return this.Axes[this.Axes.Count - 1];
        }

        /// <summary>
        /// Add a curve (<see cref="Curve"/> object) to the plot with the reference to the given axes.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="type">  A <see cref="CurveType">Curve Type</see> value. </param>
        /// <param name="label"> A <see cref="System.String">String</see> label (legend entry) for this
        /// curve. </param>
        /// <param name="xAxis"> Reference to the X <see cref="Axis"/> </param>
        /// <param name="yAxis"> Reference to the Y <see cref="Axis"/> </param>
        /// <returns>
        /// A <see cref="Curve"/> class for the newly created curve. This can then be used to access all
        /// of the curve properties that are not defined as arguments to the <see cref="AddCurve"/>
        /// method.
        /// </returns>
        public Curve AddCurve( CurveType type, string label, Axis xAxis, Axis yAxis )
        {
            if ( string.IsNullOrWhiteSpace( label ) )
                label = string.Empty;
            if ( xAxis is null )
                throw new ArgumentNullException( nameof( xAxis ) );
            if ( yAxis is null )
                throw new ArgumentNullException( nameof( yAxis ) );
            var curve = new Curve( type, label, xAxis, yAxis, this );
            this.Curves.Add( curve );
            return curve;
        }

        /// <summary>
        /// Add a curve (<see cref="Curve"/> object) to the plot with the given properties.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="type">        A <see cref="CurveType">Curve Type</see> value. </param>
        /// <param name="label">       The text label (string) for the curve that will be used as a
        /// <see cref="Legend"/> entry. </param>
        /// <param name="lineColor">   The color to used for the curve line, symbols, etc. </param>
        /// <param name="symbolShape"> A symbol shape (<see cref="ShapeType"/>)
        /// that will be used for this curve. </param>
        /// <param name="xAxis">       Reference to the X <see cref="Axis"/> </param>
        /// <param name="yAxis">       Reference to the Y <see cref="Axis"/> </param>
        /// <returns>
        /// A <see cref="Curve"/> class for the newly created curve. This can then be used to access all
        /// of the curve properties that are not defined as arguments to the <see cref="AddCurve"/>
        /// method.
        /// </returns>
        public Curve AddCurve( CurveType type, string label, Color lineColor, ShapeType symbolShape, Axis xAxis, Axis yAxis )
        {
            if ( string.IsNullOrWhiteSpace( label ) )
                label = string.Empty;
            if ( xAxis is null )
                throw new ArgumentNullException( nameof( xAxis ) );
            if ( yAxis is null )
                throw new ArgumentNullException( nameof( yAxis ) );
            var curve = new Curve( type, label, xAxis, yAxis, this );
            curve.Cord.LineColor = lineColor;
            curve.Symbol.LineColor = lineColor;
            curve.Symbol.Shape = symbolShape;
            this.Curves.Add( curve );
            return curve;
        }

        /// <summary>
        /// Add a curve (<see cref="Curve"/> object) to the plot with the given data points and
        /// properties.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="label">       The text label (string) for the curve that will be used as a
        /// <see cref="Legend"/> entry. </param>
        /// <param name="x">           An array of <see cref="System.Double">Double Precision</see>
        /// X values (the independent values) that define the curve. </param>
        /// <param name="y">           An array of <see cref="System.Double">Double Precision</see>
        /// Y values (the dependent values) that define the curve. </param>
        /// <param name="lineColor">   The color to used for the curve line, symbols, etc. </param>
        /// <param name="symbolShape"> A symbol shape (<see cref="ShapeType"/>)
        /// that will be used for this curve. </param>
        /// <param name="xAxis">       Reference to the X <see cref="Axis"/> </param>
        /// <param name="yAxis">       Reference to the Y <see cref="Axis"/> </param>
        /// <returns>
        /// A <see cref="Curve"/> class for the newly created curve. This can then be used to access all
        /// of the curve properties that are not defined as arguments to the <see cref="AddCurve"/>
        /// method.
        /// </returns>
        public Curve AddCurve( string label, double[] x, double[] y, Color lineColor, ShapeType symbolShape, Axis xAxis, Axis yAxis )
        {
            if ( x is null )
                throw new ArgumentNullException( nameof( x ) );
            if ( y is null )
                throw new ArgumentNullException( nameof( y ) );
            if ( string.IsNullOrWhiteSpace( label ) )
                label = string.Empty;
            if ( xAxis is null )
                throw new ArgumentNullException( nameof( xAxis ) );
            if ( yAxis is null )
                throw new ArgumentNullException( nameof( yAxis ) );
            var curve = new Curve( label, x, y, xAxis, yAxis, this );
            curve.Cord.LineColor = lineColor;
            curve.Symbol.LineColor = lineColor;
            curve.Symbol.Shape = symbolShape;
            this.Curves.Add( curve );
            return curve;
        }

        /// <summary> Add a <see cref="isr.Visuals.TextBox">Text Box</see> to the plot. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="[text]"> Test string. </param>
        /// <param name="x">      The x position of the text.  Units are based on the
        /// <see cref="CoordinateScale"/> property.  The text will be aligned to
        /// this position based
        /// on the <see cref="Alignment.Horizontal"/> property. </param>
        /// <param name="y">      The y position of the text.  Units are specified by the
        /// <see cref="CoordinateScale"/> property.  The text will be aligned to
        /// this position based
        /// on the <see cref="Alignment.Vertical"/> property. </param>
        /// <returns> A TextBox. </returns>
        public TextBox AddTextBox( string text, float x, float y )
        {
            if ( string.IsNullOrWhiteSpace( text ) )
                text = string.Empty;
            var textBox = new TextBox( text, x, y, this );
            this.TextBoxes.Add( textBox );
            return textBox;
        }

        /// <summary>
        /// Draws all elements in the <see cref="Pane"/> to the specified graphics device.  This routine
        /// should be overridden by the inheriting classes.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to draw into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the <see cref="M:Paint"/> method. </param>
        public abstract void Draw( Graphics graphicsDevice );

        /// <summary>
        /// Draw the frame border around the <see cref="PaneArea"/> and
        /// <see cref="Pane.AxisArea"/>.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the <see cref="M:Paint"/> method. </param>
        protected void DrawFrames( Graphics graphicsDevice )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // Clip everything to the PaneArea
            graphicsDevice.SetClip( this.PaneArea );
            this.PaneFrame.Draw( graphicsDevice, this.PaneArea );
            this.AxisFrame.Draw( graphicsDevice, this._AxisArea );
        }

        /// <summary>
        /// Transform a data point from the specified coordinate type (<see cref="CoordinateFrameType"/>)
        /// to screen coordinates (pixels).
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="userPoint">       The X,Y pair that defines the point in user coordinates. </param>
        /// <param name="coordinateFrame"> A <see cref="CoordinateFrameType"/> type that defines the
        /// coordinate system in which the X,Y pair is defined. </param>
        /// <returns>
        /// A point in screen coordinates that corresponds to the specified <see paramref="userPoint"/>.
        /// </returns>
        internal PointF GeneralTransform( PointF userPoint, CoordinateFrameType coordinateFrame )
        {
            var screenPoint = new PointF();
            if ( coordinateFrame == CoordinateFrameType.AxisFraction )
            {
                screenPoint.X = this._AxisArea.Left + userPoint.X * this._AxisArea.Width;
                screenPoint.Y = this._AxisArea.Top + userPoint.Y * this._AxisArea.Height;
            }
            else if ( coordinateFrame == CoordinateFrameType.AxisXYScale )
            {
                screenPoint.X = this._XAxis.Transform( userPoint.X );
                screenPoint.Y = this._YAxis.Transform( userPoint.Y );
            }
            else if ( coordinateFrame == CoordinateFrameType.AxisXY2Scale )
            {
                screenPoint.X = this._XAxis.Transform( userPoint.X );
                screenPoint.Y = this._Y2Axis.Transform( userPoint.Y );
            }
            else if ( coordinateFrame == CoordinateFrameType.PaneFraction )
            {
                screenPoint.X = this.PaneArea.Left + userPoint.X * this.PaneArea.Width;
                screenPoint.Y = this.PaneArea.Top + userPoint.Y * this.PaneArea.Height;
            }
            else
            {
                Debug.Assert( !Debugger.IsAttached, "Unhandled coordinate frame reference" );
                screenPoint.X = userPoint.X;
                screenPoint.Y = userPoint.Y;
            }

            return screenPoint;
        }

        /// <summary>
        /// Calculate the scaling factor based on the ratio of the current
        /// <see cref="PaneArea"/> dimensions and the <see cref="BaseDimension"/>.
        /// This scaling factor is used to proportionally scale the features of the
        /// <see cref="Pane"/> so that small graphs don't have huge fonts, and vice versa.
        /// The scale factor applies to font sizes, symbol sizes, etc.
        /// </summary>
        /// <remarks>
        /// Assume the standard width (BaseDimension) is 8.0 inches.  Therefore, if the PaneArea is 8.0
        /// inches wide, then the fonts will be scaled at 1.0 if the PaneArea is 4.0 inches wide, the
        /// fonts will be half-sized. If the PaneArea is
        /// 16.0 inches wide, the fonts will be double-sized.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        /// <returns>
        /// A <see cref="System.Double">Double</see> value representing the scaling factor to use for the
        /// rendering calculations.
        /// </returns>
        protected double GetScaleFactor( Graphics graphicsDevice )
        {
            double xInch;
            double yInch;

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // Determine the size of the PaneArea in inches

            // check if we have printer or screen resolution
            if ( graphicsDevice.DpiX > 150f )
            {
                // assuming we are drawing to the printer, use document coordinates.
                xInch = 0.01d * this.PaneArea.Width;
                yInch = 0.1d * this.PaneArea.Height;
            }
            else
            {
                // assuming that we print to screen, set the range based on the 
                // pixels
                xInch = this.PaneArea.Width / graphicsDevice.DpiX;
                yInch = this.PaneArea.Height / graphicsDevice.DpiY;
            }

            // Limit the aspect ratio so Int64 plots don't have outrageous font sizes
            xInch = Math.Min( xInch, yInch * PaneDefaults.Get().MaxAspectRatio );

            // Scale the size depending on the client area width in linear fashion, limited
            // to a minimum
            return Math.Max( xInch / this.BaseDimension, PaneDefaults.Get().MinScaleFactor );
        }

        /// <summary>
        /// Prints all elements in the <see cref="Pane"/> to the specified graphics device.  This routine
        /// should be overridden by the inheriting classes.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="graphicsDevice"> Reference to a graphic device to print into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics">graphics</see>
        /// of the <see cref="M:PrintPage"/> delegate. </param>
        /// <param name="printArea">      The <see cref="System.Drawing.RectangleF">area</see> on the print
        /// document allotted for the chart. </param>
        public abstract void Print( Graphics graphicsDevice, RectangleF printArea );

        /// <summary>
        /// Recalculate the axes scale ranges based on the current data range. Call this function anytime
        /// you change, add, or remove curve data.  This method calculates a scale minimum, maximum, and
        /// spacing for each axis based on the current curve data. Only the axis attributes (min, max,
        /// spacing) that are set to auto-range (<see cref="Axis.Min"/>, <see cref="Axis.Max"/>,
        /// <see cref="Tick.Spacing"/>) will be modified.  You must call
        /// Invalidate() after calling AxisChange to make sure the display gets updated.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void Rescale()
        {

            // rescale all the axes
            this.Axes.Rescale( this.Curves, this.IgnoreInitial );
        }

        /// <summary>
        /// Restore the scale ranging to automatic mode, and recalculate the
        /// <see cref="Axis"/> scale ranges.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        protected void ResetAutoScale()
        {
            foreach ( Axis axis in this.Axes )
                axis.AutoScale = true;
            this.Rescale();
        }

        /// <summary>
        /// Calculate and set the <see cref="Pane.AxisArea"/> based on the
        /// <see cref="DrawArea"/>.  The <see cref="Pane.AxisArea"/> is the plot area bounded by
        /// the axes.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        protected void SetAxisArea( Graphics graphicsDevice )
        {

            // validate argument.
            if ( graphicsDevice is null )
                throw new ArgumentNullException( nameof( graphicsDevice ) );

            // Axis area starts out at the drawing area.  It gets reduced to make room for the 
            // legend, scales, titles, etc.
            this._AxisArea = this._DrawArea;

            // Calculate the areas required for the X, Y, and Y2 axes, and reduce the 
            // AxisArea by these amounts.
            this._AxisArea = this.Axes.GetAxisArea( graphicsDevice, this.ScaleFactor, this._AxisArea );

            // adjust axis area for title space.
            this._AxisArea = this.Title.GetAxisArea( graphicsDevice, this.ScaleFactor, this._AxisArea );

            // Calculate the stylus area, and back it out of the current axisArea
            this._AxisArea = this.Curves.GetAxisArea( this.ScaleFactor, this._AxisArea );

            // Calculate the legend area, and back it out of the current axisArea
            this._AxisArea = this.Legend.GetAxisArea( graphicsDevice, this.Curves, this.ScaleFactor, this._DrawArea, this._AxisArea );

            // set screen range for all axis.
            this.Axes.SetScreenRange( this._AxisArea );
        }

        /// <summary>
        /// Calculates and sets the <see cref="DrawArea"/> based on the
        /// <see cref="DrawAreaMargins"/>.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        protected void SetDrawArea()
        {
            this._DrawArea = this.DrawAreaMargins.GetAdjustedArea( this.PaneArea, this.ScaleFactor );
        }

        /// <summary> Sets the pane size based on the client rectangle. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="clientRectangle"> The <see cref="System.Windows.Forms.Control.ClientRectangle"/> </param>
        public void SetSize( Rectangle clientRectangle )
        {
            RectangleF area = clientRectangle;
            area.Inflate( -this.OuterGap, -this.OuterGap );
            this.PaneArea = area;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>Gets or sets lines/arrows on the graph</summary>

        /// <summary> Gets the list of <see cref="Arrow"/> items for this <see cref="Pane"/> </summary>
        /// <value> A reference to an <see cref="Arrows"/> collection object. </value>
        protected ArrowCollection Arrows { get; private set; }

        /// <summary> Gets the <see cref="AxisCollection"/> </summary>
        /// <value> The axes. </value>
        protected AxisCollection Axes { get; private set; }

        /// <summary> Gets the <see cref="Axis"/> <see cref="Frame"/>. </summary>
        /// <value> A <see cref="Frame"/> value. </value>
        public Frame AxisFrame { get; private set; }

        /// <summary>The rectangle that contains the area bounded by the axes, in
        /// pixels.</summary>
        private RectangleF _AxisArea;

        /// <summary>
        /// gets the rectangle that contains the area bounded by the axes (<see cref="AxisType.X"/>,
        /// <see cref="AxisType.Y"/>, and <see cref="AxisType.Y2"/>)
        /// </summary>
        /// <value> A <see cref="System.Drawing.RectangleF">Rectangle</see> in screen pixels. </value>
        internal RectangleF AxisArea => this._AxisArea;

        /// <summary>
        /// BaseDimension is A <see cref="System.Double">Double</see> value that sets "normal" plot size
        /// on which all the settings are based.  The BaseDimension is in inches.  For example, if the
        /// BaseDimension is 8.0 inches and the <see cref="Pane"/>
        /// <see cref="Title"/> size is 14 points.  Then the pane title font
        /// will be 14 points high when the <see cref="PaneArea"/> is approximately 8.0 inches wide.  If
        /// the PaneArea is 4.0 inches wide, the pane title font will be
        /// 7 points high.  Most features of the graph are scaled in this manner.
        /// </summary>
        /// <value> The base dimension reference for the <see cref="Pane"/> in inches. </value>
        protected double BaseDimension { get; set; }

        /// <summary> Gets the list of <see cref="Curve"/> items for this <see cref="Pane"/> </summary>
        /// <value> A reference to a <see cref="CurveCollection"/> collection object. </value>
        internal CurveCollection Curves { get; private set; }

        /// <summary>The rectangle that defines the drawing area for the chart.
        /// Units are pixels.</summary>
        private RectangleF _DrawArea;

        /// <summary>
        /// Gets the rectangle that defines the drawing area within the
        /// <see cref="PaneArea"/>.
        /// </summary>
        /// <value> A <see cref="System.Drawing.RectangleF">Rectangle</see> in screen pixels. </value>
        internal RectangleF DrawArea => this._DrawArea;

        /// <summary> Gets or sets the draw area margins. </summary>
        /// <value> A <see cref="isr.Visuals.Margins"/> property. </value>
        protected MarginsF DrawAreaMargins { get; set; }

        /// <summary> The inner gap. </summary>
        private float _InnerGap;

        /// <summary>
        /// Gets or sets the gap between the <see cref="PaneArea"/> and
        /// <see cref="DrawArea"/> borders. Scales linearly with the chart size.
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        protected float InnerGap
        {
            get => this._InnerGap;

            set {
                this._InnerGap = value;
                this.DrawAreaMargins = new MarginsF( value, value, value, value );
            }
        }

        /// <summary>
        /// A <see cref="System.Boolean">Boolean</see> value that affects the data range that is
        /// considered for the automatic scale ranging.  If true, then initial data points where the Y
        /// value is zero are not included when automatically determining the scale
        /// <see cref="Axis.Min"/>, <see cref="Axis.Max"/>, and <see cref="Tick.Spacing"/> size. All data
        /// after the first non-zero Y value are included.
        /// </summary>
        /// <value> The ignore initial. </value>
        protected bool IgnoreInitial { get; set; }

        /// <summary> Determines if the <see cref="Pane"/> will be shown. </summary>
        /// <value> True to draw the chart. </value>
        public bool Visible { get; set; }

        /// <summary> Accesses the <see cref="Legend"/> for this <see cref="Pane"/> </summary>
        /// <value> A reference to a <see cref="Legend"/> object. </value>
        public Legend Legend { get; private set; }

        /// <summary>
        /// Gets the gap between the client area and the
        /// <see cref="PaneArea"/> borders.
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        protected float OuterGap { get; set; }

        /// <summary> Gets the <see cref="Pane"/> <see cref="Frame"/>. </summary>
        /// <value> A <see cref="Frame"/> value. </value>
        public Frame PaneFrame { get; set; }

        /// <summary>
        /// Gets the rectangle that defines the full area into which the
        /// <see cref="Pane"/> can be rendered.
        /// </summary>
        /// <value> A <see cref="System.Drawing.RectangleF">Rectangle</see> in screen pixels. </value>
        public RectangleF PaneArea { get; set; }

        /// <summary>
        /// Gets the scale factor for the current size of the chart relative to the original size.
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        internal double ScaleFactor { get; set; }

        /// <summary> Gets the status message. </summary>
        /// <value> A System.String value. </value>
        protected string StatusMessage { get; set; }

        /// <summary> Gets the list of <see cref="TextBox"/> items for this <see cref="Pane"/> </summary>
        /// <value> A reference to a <see cref="TextBoxCollection"/> collection object. </value>
        protected TextBoxCollection TextBoxes { get; private set; }

        /// <summary> Gets or sets the pane <see cref="Title"/>. </summary>
        /// <value> The title. </value>
        public Title Title { get; set; }

        /// <summary> The axis. </summary>
        private Axis _XAxis;

        /// <summary> The axis. </summary>
        private Axis _YAxis;

        /// <summary> The 2 axis. </summary>
        private Axis _Y2Axis;

        #endregion

        #region " PRIVATE  and  PROTECTED "

        /// <summary>
        /// gets a new <see cref="DrawArea"/> relative to the <see cref="PaneArea"/>
        /// based on the <see cref="InnerGap"/> scaled to the "scaleFactor" fraction.
        /// That is, ScaledGap = innerGap * scaleFactor.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="scaleFactor"> The scaling factor for the features of the graph based on the
        /// <see cref="BaseDimension"/>.  This scaling factor is calculated
        /// by the <see cref="GetScaleFactor"/> method.  The scale factor
        /// represents a linear multiple to be applied to font sizes, symbol
        /// sizes, etc. </param>
        /// <returns> The drawing area <see cref="System.Drawing.RectangleF"/> </returns>
        protected RectangleF GetDrawingArea( double scaleFactor )
        {

            // Set the rectangle to the entire pan area
            var drawingArea = this.PaneArea;

            // get the scaled margins
            var drawMargins = this.DrawAreaMargins.GetScaledMargins( scaleFactor );

            // reduce the size of the drawing area by the total horizontal and vertical
            // spans
            drawingArea.Width -= drawMargins.Horizontal;
            drawingArea.Height -= drawMargins.Vertical;

            // shift the draw rectangle by the top left margins
            drawingArea.Offset( drawMargins.TopLeft );
            return drawingArea;
        }

        /// <summary>
        /// Calculates the <see cref="InnerGap"/> scaled to the "scaleFactor" fraction.
        /// That is, ScaledGap = innerGap * scaleFactor.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="scaleFactor"> The scaling factor for the features of the graph based on the
        /// <see cref="BaseDimension"/>.  This scaling factor is calculated
        /// by the <see cref="GetScaleFactor"/> method.  The scale factor
        /// represents a linear multiple to be applied to font sizes, symbol
        /// sizes, etc. </param>
        /// <returns>
        /// A <see cref="System.Single">Single</see> in pixels, after scaling according to
        /// <paramref name="scaleFactor"/>
        /// </returns>
        protected float GetScaledGap( double scaleFactor )
        {
            return Convert.ToSingle( this._InnerGap * scaleFactor );
        }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="Pane"/> class that defines the default property values
    /// for the <see cref="Pane"/> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class PaneDefaults
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private PaneDefaults() : base()
        {
            this.BaseDimension = 8.0d;
            this.FrameFillColor = Color.White;
            this.FrameLineColor = Color.Black;
            this.FrameLineWidth = 1f;
            this.InnerGap = 4f;
            this.Filled = true;
            this.Framed = true;
            this.OuterGap = 4f;
            this.MaxAspectRatio = 2.0d;
            this.MinScaleFactor = 0.1d;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static PaneDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static PaneDefaults Get()
        {
            if ( _Instance is null )
            {
                lock ( SyncLocker )
                    _Instance = new PaneDefaults();
            }

            return _Instance;
        }

        #endregion

        /// <summary>
        /// Gets or sets the default dimension of the <see cref="Pane.PaneArea"/>, which defines a normal
        /// sized plot.  This dimension is used to scale the fonts, symbols, etc. according to the actual
        /// size of the
        /// <see cref="Pane.PaneArea"/>.
        /// </summary>
        /// <value> The base dimension. </value>
        /// <seealso cref="Pane.GetScaleFactor"/>
        public double BaseDimension { get; set; }

        /// <summary>
        /// Gets or sets the default color for the <see cref="Pane.PaneArea"/> background.
        /// (<see cref="Frame.FillColor"/> property).
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color FrameFillColor { get; set; }

        /// <summary>
        /// Gets or sets the default color for the <see cref="Pane"/> frame border.
        /// (<see cref="Frame.LineColor"/> property).
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color FrameLineColor { get; set; }

        /// <summary>
        /// Gets or sets the default pen width for the <see cref="Pane"/> frame border.
        /// (<see cref="Frame.LineWidth"/> property).
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float FrameLineWidth { get; set; }

        /// <summary>
        /// Gets or sets the default value for the <see cref="Pane.InnerGap"/> property. This is the size
        /// of the margin around the edge of the <see cref="Pane.PaneArea"/>.
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float InnerGap { get; set; }

        /// <summary>
        /// Gets or sets the default frame fill mode for the <see cref="Pane"/>.
        /// (<see cref="Frame.Filled"/> property). True to fill the <see cref="Pane.PaneArea"/> area or
        /// False to keep it transparent.
        /// </summary>
        /// <value> The filled. </value>
        public bool Filled { get; set; }

        /// <summary>
        /// Gets or sets the default frame outline mode for the <see cref="Pane"/>.
        /// (<see cref="Frame.IsOutline"/> property). True to draw a frame around the
        /// <see cref="Pane.PaneArea"/>, False otherwise.
        /// </summary>
        /// <value> The framed. </value>
        public bool Framed { get; set; }

        /// <summary>
        /// Gets or sets the default value for the <see cref="Pane.OuterGap"/> property. This is the size
        /// of the margin around the outer edge of the <see cref="Pane.PaneArea"/>.
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> in pixels. </value>
        public float OuterGap { get; set; }

        /// <summary>
        /// Gets or sets the maximum aspect ratio (x/y) for scaling charts so that Int64 charts won't
        /// have outrageous font sizes.
        /// </summary>
        /// <value> A <see cref="System.Double">Double</see> value. </value>
        public double MaxAspectRatio { get; set; }

        /// <summary>
        /// Gets or sets the minimum scale factor for scaling charts relative to the baseDimension.
        /// </summary>
        /// <value> A <see cref="System.Double">Double</see> value. </value>
        public double MinScaleFactor { get; set; }
    }
}

#endregion

