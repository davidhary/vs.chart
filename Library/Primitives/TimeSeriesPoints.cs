using System;
using System.Windows.Forms;

using isr.Core.Constructs;

namespace isr.Visuals
{

    /// <summary> Defines a <see cref="System.Single">Single</see> time series point. </summary>
    /// <remarks>
    /// David, 2004-05-12, 1.0.1593.x. Created <para>
    /// David, 2004-05-28, 1.0.1609.x. Add time series point tag </para><para>
    /// David, 2004-06-03, 1.0.1615.x. Add zero and partial copy </para><para>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public struct TimeSeriesPointF
    {

        #region " TYPES "

        /// <summary> Returns the time from seconds since 1-1-0001. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="seconds"> The time in seconds from 1-1-0001. </param>
        /// <returns> A <see cref="System.DateTime">Time</see> value. </returns>
        public static DateTime GetDateTime( double seconds )
        {
            return new DateTime( Convert.ToInt64( seconds ) * TimeSpan.TicksPerSecond );
        }

        /// <summary> gets the rate of change between two time series points. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="first">  The first. </param>
        /// <param name="second"> The second. </param>
        /// <returns> The slope of the line between to two time series points. </returns>
        public static double GetRate( TimeSeriesPointF first, TimeSeriesPointF second )
        {
            return first.T.Equals( second.T ) ? 0.0d : (second.Y - first.Y) / (second.Seconds - first.Seconds);
        }

        /// <summary> Gets the minimum time series point value. </summary>
        /// <value> A <see cref="TimeSeriesPointF"/> value. </value>
        public static TimeSeriesPointF MinValue => new TimeSeriesPointF( DateTime.MinValue, float.MinValue, 0 );

        /// <summary> Gets the zero time series point value. </summary>
        /// <value> A <see cref="TimeSeriesPointF"/> value. </value>
        public static TimeSeriesPointF Zero => new TimeSeriesPointF( DateTime.MinValue, 0f, 0 );

        #endregion

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs a <see cref="TimeSeriesPointF">time series point</see> instance by its t, y, and
        /// tag values.
        /// </summary>
        /// <remarks>   David, 2020-10-26. </remarks>
        /// <param name="time">     A <see cref="System.DateTime">Time</see> value. </param>
        /// <param name="value">    A <see cref="System.Single">Single</see> value. </param>
        /// <param name="tag">      A <see cref="isr.Visuals.TimeSeriesPointTags">time series point
        ///                         tag</see>
        ///                         value. </param>
        /// <param name="index">    The index. </param>
        public TimeSeriesPointF( DateTime time, float value, TimeSeriesPointTags tag, int index )
        {
            this.T = time;
            this.Y = value;
            this.Tag = tag;
            this.Index = index;
        }

        /// <summary>
        /// Constructs a <see cref="TimeSeriesPointF">time series point</see> instance by its t and y
        /// values.
        /// </summary>
        /// <remarks>   David, 2020-10-26. </remarks>
        /// <param name="time">     A <see cref="System.DateTime">DateTime</see> value. </param>
        /// <param name="value">    A <see cref="System.Single">Single</see> value. </param>
        /// <param name="index">    The index. </param>
        public TimeSeriesPointF( DateTime time, float value, int index )
        {
            this.T = time;
            this.Y = value;
            this.Tag = TimeSeriesPointTags.None;
            this.Index = index;
        }

        /// <summary>
        /// Constructs a <see cref="TimeSeriesPointF">time series point</see> instance by its y value for
        /// the current time.
        /// </summary>
        /// <remarks>   David, 2020-10-26. </remarks>
        /// <param name="y">        A <see cref="System.Single">Single</see> value. </param>
        /// <param name="index">    The index. </param>
        public TimeSeriesPointF( float y, int index )
        {
            this.T = DateTime.UtcNow;
            this.Y = y;
            this.Tag = TimeSeriesPointTags.None;
            this.Index = index;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="model"> The <see cref="TimeSeriesPointF">time series point</see> from which to
        /// copy. </param>
        public TimeSeriesPointF( TimeSeriesPointF model )
        {
            this.T = model.T;
            this.Y = model.Y;
            this.Tag = model.Tag;
            this.Index = model.Index;
        }

        /// <summary> The partial copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="model"> The <see cref="TimeSeriesPointF">time series point</see> from which to
        /// copy. </param>
        /// <param name="y">     The new amplitude value. </param>
        public TimeSeriesPointF( TimeSeriesPointF model, float y )
        {
            this.T = model.T;
            this.Y = y;
            this.Tag = model.Tag;
            this.Index = model.Index;
        }

        #endregion

        #region " EQUALS "

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Time series point f to be compared. </param>
        /// <param name="right"> Time series point f to be compared. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( TimeSeriesPointF left, TimeSeriesPointF right )
        {
            return Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Time series point f to be compared. </param>
        /// <param name="right"> Time series point f to be compared. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( TimeSeriesPointF left, TimeSeriesPointF right )
        {
            return !Equals( left, right );
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Time series point f to be compared. </param>
        /// <param name="right"> Time series point f to be compared. </param>
        /// <returns>
        /// <c>True</c> if arguments are the same type and represent the same value; otherwise,
        /// <c>False</c>.
        /// </returns>
        public static bool Equals( TimeSeriesPointF left, TimeSeriesPointF right )
        {
            return left.Equals( right );
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="obj"> Another object to compare to. </param>
        /// <returns>
        /// <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
        /// same value; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return obj is object && ReferenceEquals( this.GetType(), obj.GetType() ) && this.Equals( ( TimeSeriesPointF ) obj );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="other"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// The two PlanarRanges are the same if the have the same
        /// <see cref="T"/> and <see cref="Y"/> ranges.
        /// </remarks>
        /// <param name="other"> The <see cref="TimeSeriesPointF">TimeSeriesPointF</see> to compare for
        /// equality with this instance. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public bool Equals( TimeSeriesPointF other )
        {
            return this.T.Equals( other.T ) && this.Y.Equals( other.Y );
        }

        #endregion

        #region " METHODS "

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return this.T.GetHashCode() ^ this.Y.GetHashCode();
        }

        /// <summary> gets the rate of change between this and another time series point. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="referenceTime"> The first time series point. </param>
        /// <returns> The slope of the line between this and the given time series point. </returns>
        public double Rate( TimeSeriesPointF referenceTime )
        {
            return this.T.Equals( referenceTime.T ) ? 0.0d : (this.Y - referenceTime.Y) / (this.Seconds - referenceTime.Seconds);
        }

        /// <summary> Returns the default string representation of the time series point. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The fully qualified type name. </returns>
        public override string ToString()
        {
            return $"[{this.T},{this.Y}]";
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the index of the time series point in a time series array. </summary>
        /// <value> The index. </value>
        public int Index { get; set; }

        /// <summary> Returns true if the point is a Range point. </summary>
        /// <value> The is range point. </value>
        public bool IsRangePoint => (this.Tag & TimeSeriesPointTags.RangePoint) == TimeSeriesPointTags.RangePoint;

        /// <summary> gets the time value in seconds since 0001-01-01. </summary>
        /// <value> Time value in <see cref="System.Double">double precision</see> seconds. </value>
        public double Seconds => this.T.Ticks * TimeSeriesPointR.SecondsPerTick;

        /// <summary> Gets the time series <see cref="TimeSeriesPointTags">tag</see> </summary>
        /// <value> A <see cref="TimeSeriesPointTags">tag</see> </value>
        public TimeSeriesPointTags Tag { get; set; }

        /// <summary> Gets the <see cref="TimeSeriesPointF">time series point</see> time value. </summary>
        /// <value> A <see cref="System.DateTime">Time</see> value. </value>
        public DateTime T { get; set; }

        /// <summary>
        /// Gets the vertical <see cref="TimeSeriesPointF">time series point</see> value.
        /// </summary>
        /// <value> A <see cref="System.Single">Single</see> value. </value>
        public float Y { get; set; }

        #endregion

    }

    /// <summary> Defines a <see cref="System.Double">Double</see> time series point. </summary>
    /// <remarks>
    /// David, 2004-06-03, 1.0.1615. Add zero and partial. <para>
    /// David, 2004-05-28, 1.0.1609. Add time series point tag. </para><para>
    /// David, 2004-05-12, 1.0.1593. Copy </para><para>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public struct TimeSeriesPointR
    {

        #region " TYPES "

        /// <summary> Returns the time from seconds since 1-1-0001. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="seconds"> The time in seconds from 1-1-0001. </param>
        /// <returns> A <see cref="System.DateTime">Time</see> value. </returns>
        public static DateTime GetDateTime( double seconds )
        {
            return new DateTime( Convert.ToInt64( seconds ) * TimeSpan.TicksPerSecond );
        }

        /// <summary>
        /// gets the amplitude range for the <see cref="isr.Visuals.TimeSeriesPointR">Time Series</see>
        /// data array.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="timeSeries"> <see cref="isr.Visuals.TimeSeriesPointR">Time Series</see>
        /// data array. </param>
        /// <returns> The calculated amplitude range. </returns>
        public static RangeR GetAmplitudeRange( TimeSeriesPointR[] timeSeries )
        {

            // return the unit range if no data
            if ( timeSeries is null )
            {
                return RangeR.Unity;
            }

            int numPoints = timeSeries.Length;

            // initialize the values to the empty range
            double yTemp;
            yTemp = timeSeries[0].Y;
            double yMin = yTemp;
            double yMax = yTemp;

            // Loop over each point in the arrays
            for ( int i = 0, loopTo = numPoints - 1; i <= loopTo; i++ )
            {
                yTemp = timeSeries[i].Y;
                if ( yTemp < yMin )
                {
                    yMin = yTemp;
                }
                else if ( yTemp > yMax )
                {
                    yMax = yTemp;
                }
            }

            return new RangeR( yMin, yMax );
        }

        /// <summary> gets the rate of change between two time series points. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="first">  The first time series point. </param>
        /// <param name="second"> The second time series point. </param>
        /// <returns> The slope of the line between to two time series points. </returns>
        public static double GetRate( TimeSeriesPointR first, TimeSeriesPointR second )
        {
            return first.T.Equals( second.T ) ? 0.0d : (second.Y - first.Y) / (second.Seconds - first.Seconds);
        }

        /// <summary> Gets the minimum time series point value. </summary>
        /// <value> A <see cref="TimeSeriesPointF"/> value. </value>
        public static TimeSeriesPointR MinValue => new TimeSeriesPointR( DateTime.MinValue, double.MinValue, 0 );

        /// <summary> The seconds per tick. </summary>
        private const double _SecondsPerTick = 1.0d / TimeSpan.TicksPerSecond;

        /// <summary> Gets the conversion factor from ticks to seconds. </summary>
        /// <value> A <see cref="System.Double">double precision</see> value. </value>
        public static double SecondsPerTick => _SecondsPerTick;

        /// <summary> Gets the zero time series point value. </summary>
        /// <value> A <see cref="TimeSeriesPointR"/> value. </value>
        public static TimeSeriesPointR Zero => new TimeSeriesPointR( DateTime.MinValue, 0d, 0 );

        #endregion

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs a <see cref="TimeSeriesPointF">time series point</see> instance by its t, y, and
        /// tag values.
        /// </summary>
        /// <remarks>   David, 2020-10-26. </remarks>
        /// <param name="time">     A <see cref="System.DateTime">DateTime</see> value. </param>
        /// <param name="value">    A <see cref="System.Double">Double</see> value. </param>
        /// <param name="tag">      A <see cref="isr.Visuals.TimeSeriesPointTags">time series point
        ///                         tag</see>
        ///                         value. </param>
        /// <param name="index">    The index. </param>
        public TimeSeriesPointR( DateTime time, double value, TimeSeriesPointTags tag, int index )
        {
            this.T = time;
            this.Y = value;
            this.Tag = tag;
            this.Index = index;
        }

        /// <summary>
        /// Constructs a <see cref="TimeSeriesPointR">time series point</see> instance by its t and y
        /// values.
        /// </summary>
        /// <remarks>   David, 2020-10-26. </remarks>
        /// <param name="time">     A <see cref="System.DateTime">DateTime</see> value. </param>
        /// <param name="value">    A <see cref="System.Double">Double</see> value. </param>
        /// <param name="index">    The index. </param>
        public TimeSeriesPointR( DateTime time, double value, int index )
        {
            this.T = time;
            this.Y = value;
            this.Tag = TimeSeriesPointTags.None;
            this.Index = index;
        }

        /// <summary>
        /// Constructs a <see cref="TimeSeriesPointR">time series point</see> instance by its y value for
        /// the current time.
        /// </summary>
        /// <remarks>   David, 2020-10-26. </remarks>
        /// <param name="y">        A <see cref="System.Double">Double</see> value. </param>
        /// <param name="index">    The index. </param>
        public TimeSeriesPointR( double y, int index )
        {
            this.T = DateTime.UtcNow;
            this.Y = y;
            this.Tag = TimeSeriesPointTags.None;
            this.Index = index;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="model"> The <see cref="TimeSeriesPointR">time series point</see> from which to
        /// copy. </param>
        public TimeSeriesPointR( TimeSeriesPointR model )
        {
            this.T = model.T;
            this.Y = model.Y;
            this.Tag = model.Tag;
            this.Index = model.Index;
        }

        /// <summary> The partial copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="model"> The <see cref="TimeSeriesPointR">time series point</see> from which to
        /// copy. </param>
        /// <param name="y">     The new amplitude value. </param>
        public TimeSeriesPointR( TimeSeriesPointR model, double y )
        {
            this.T = model.T;
            this.Y = y;
            this.Tag = model.Tag;
            this.Index = model.Index;
        }

        #endregion

        #region " EQUALS "

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Time series point r to be compared. </param>
        /// <param name="right"> Time series point r to be compared. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( TimeSeriesPointR left, TimeSeriesPointR right )
        {
            return Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Time series point r to be compared. </param>
        /// <param name="right"> Time series point r to be compared. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( TimeSeriesPointR left, TimeSeriesPointR right )
        {
            return !Equals( left, right );
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Time series point r to be compared. </param>
        /// <param name="right"> Time series point r to be compared. </param>
        /// <returns>
        /// <c>True</c> if values are the same type and represent the same value; otherwise, <c>False</c>.
        /// </returns>
        public static bool Equals( TimeSeriesPointR left, TimeSeriesPointR right )
        {
            return left.Equals( right );
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="obj"> Another object to compare to. </param>
        /// <returns>
        /// <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
        /// same value; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return obj is object && ReferenceEquals( this.GetType(), obj.GetType() ) && this.Equals( ( TimeSeriesPointR ) obj );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="other"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// The two PlanarRanges are the same if the have the same
        /// <see cref="T"/> and <see cref="Y"/> values.
        /// </remarks>
        /// <param name="other"> The <see cref="TimeSeriesPointR">TimeSeriesPointR</see> to compare for
        /// equality with this instance. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public bool Equals( TimeSeriesPointR other )
        {
            return this.T.Equals( other.T ) && this.Y.Equals( other.Y );
        }

        #endregion

        #region " METHODS "

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return this.T.GetHashCode() ^ this.Y.GetHashCode();
        }

        /// <summary> gets the rate of change between this and another time series point. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="referenceTime"> The first time series point. </param>
        /// <returns> The slope of the line between this and the given time series point. </returns>
        public double Rate( TimeSeriesPointR referenceTime )
        {
            return this.T.Equals( referenceTime.T ) ? 0.0d : (this.Y - referenceTime.Y) / (this.Seconds - referenceTime.Seconds);
        }

        /// <summary> Returns the default string representation of the time series point. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The fully qualified type name. </returns>
        public override string ToString()
        {
            return $"[{this.T},{this.Y}]";
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the index of the time series point in a time series array. </summary>
        /// <value> The index. </value>
        public int Index { get; set; }

        /// <summary> Returns true if the point is a Range point. </summary>
        /// <value> The is range point. </value>
        public bool IsRangePoint => (this.Tag & TimeSeriesPointTags.RangePoint) == TimeSeriesPointTags.RangePoint;

        /// <summary> gets the time value in seconds since 0001-01-01. </summary>
        /// <value> Time value in <see cref="System.Double">double precision</see> seconds. </value>
        public double Seconds => this.T.Ticks * SecondsPerTick;

        /// <summary> Gets or sets the time series <see cref="TimeSeriesPointTags">tag</see> </summary>
        /// <value> A <see cref="TimeSeriesPointTags">tag</see> </value>
        public TimeSeriesPointTags Tag { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="TimeSeriesPointF">time series point</see> time value.
        /// </summary>
        /// <value> A <see cref="System.DateTime">Time</see> value. </value>
        public DateTime T { get; set; }

        /// <summary>
        /// Gets or sets the vertical <see cref="TimeSeriesPointR">time series point</see> value.
        /// </summary>
        /// <value> A <see cref="System.Double">Double</see> value. </value>
        public double Y { get; set; }

        #endregion

    }

    #region " TYPES "

    /// <summary> Enumerates the available time series tags. </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    /// <seealso cref="isr.Visuals.Axis.AxisType"/>
    [Flags()]
    public enum TimeSeriesPointTags
    {

        /// <summary>No tag</summary>
        None = 0,

        /// <summary>Tags a time series point as a range point.  Only points tagged as
        /// range points as selected when setting the range for auto scale.</summary>
        RangePoint = 1
    }

    #endregion

    /// <summary>
    /// Defines a <see cref="System.Double">double precision</see> time series book mark.
    /// </summary>
    /// <remarks>
    /// David, 2004-05-12, 1.0.1593. Created <para>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public struct TimeSeriesBookmark
    {

        #region " Constructors "

        /// <summary>
        /// Constructs a <see cref="TimeSeriesBookmark"/> instance by its
        /// <see cref="TimeSeriesPointR">time series point</see>, index,
        /// and caption.
        /// </summary>
        /// <remarks>   David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="mainPoint">    The main book mark point <see cref="TimeSeriesPointR">time series
        ///                             point</see> </param>
        /// <param name="fromPoint">    The starting <see cref="TimeSeriesPointR">time series point</see> </param>
        /// <param name="toPoint">      The ending <see cref="TimeSeriesPointR">time series point</see> </param>
        /// <param name="caption">      The book mark caption. </param>
        /// <param name="tag">          The book mark tag. </param>
        /// <param name="serialNumber"> The serial number. </param>
        public TimeSeriesBookmark( TimeSeriesPointR mainPoint, TimeSeriesPointR fromPoint, TimeSeriesPointR toPoint, string caption, string tag, int serialNumber )
        {
            if ( string.IsNullOrWhiteSpace( caption ) )
            {
                throw new ArgumentNullException( nameof( caption ) );
            }

            if ( string.IsNullOrWhiteSpace( tag ) )
            {
                throw new ArgumentNullException( nameof( tag ) );
            }

            this.MainPoint = mainPoint;
            this.FromPoint = fromPoint;
            this.ToPoint = toPoint;
            this.Tag = tag;
            this.Caption = caption;
            this.SerialNumber = serialNumber;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="model"> The <see cref="TimeSeriesBookmark">time series book mark</see> from which
        /// to copy. </param>
        public TimeSeriesBookmark( TimeSeriesBookmark model )
        {
            this.MainPoint = model.MainPoint;
            this.FromPoint = model.FromPoint;
            this.ToPoint = model.ToPoint;
            this.Caption = model.Caption;
            this.Tag = model.Tag;
            this.SerialNumber = model.SerialNumber;
        }

        #endregion

        #region " EQUALS "

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Time series bookmark to be compared. </param>
        /// <param name="right"> Time series bookmark to be compared. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( TimeSeriesBookmark left, TimeSeriesBookmark right )
        {
            return Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Time series bookmark to be compared. </param>
        /// <param name="right"> Time series bookmark to be compared. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( TimeSeriesBookmark left, TimeSeriesBookmark right )
        {
            return !Equals( left, right );
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Time series bookmark to be compared. </param>
        /// <param name="right"> Time series bookmark to be compared. </param>
        /// <returns>
        /// <c>True</c> if values are the same type and represent the same value; otherwise, <c>False</c>.
        /// </returns>
        public static bool Equals( TimeSeriesBookmark left, TimeSeriesBookmark right )
        {
            return left.Equals( right );
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="obj"> Another object to compare to. </param>
        /// <returns>
        /// <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
        /// same value; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return obj is object && ReferenceEquals( this.GetType(), obj.GetType() ) && this.Equals( ( TimeSeriesBookmark ) obj );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="other"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// Time Series Marks are the same if the have the same
        /// <see cref="FromPoint"/> and <see cref="MainPoint"/> and <see cref="ToPoint"/>
        /// and <see cref="Caption"/> and <see cref="SerialNumber"/> and <see cref="Tag"/> values.
        /// </remarks>
        /// <param name="other"> The <see cref="TimeSeriesBookmark">TimeSeriesBookmark</see> to compare
        /// for equality with this instance. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public bool Equals( TimeSeriesBookmark other )
        {
            return this.FromPoint.Equals( other.FromPoint ) && this.MainPoint.Equals( other.MainPoint ) && this.ToPoint.Equals( other.ToPoint ) && this.Caption.Equals( other.Caption ) && this.SerialNumber.Equals( other.SerialNumber ) && this.Tag.Equals( other.Tag );
        }

        #endregion

        #region " METHODS "

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return this.MainPoint.GetHashCode() ^ this.FromPoint.GetHashCode() ^ this.ToPoint.GetHashCode() ^ this.Caption.GetHashCode() ^ this.SerialNumber.GetHashCode() ^ this.Tag.GetHashCode();
        }

        /// <summary> Returns the time series index of the bookmark. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="timeSeries"> The <see cref="TimeSeriesBookmark">time series point</see> array. </param>
        /// <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
        public int FindIndex( TimeSeriesPointR[] timeSeries )
        {
            return Array.IndexOf( timeSeries, this.FromPoint );
        }

        /// <summary> Returns the default string representation of the time series book mark. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The fully qualified type name. </returns>
        public override string ToString()
        {
            return this.Caption.Length > 0 ? $"{this.Caption}: {this.FromPoint.T}" : this.FromPoint.T.ToString( Application.CurrentCulture );
        }

        /// <summary> Returns the default string representation of the time series book mark. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="dateFormat"> The date format. </param>
        /// <returns> A String that represents this object. </returns>
        public string ToString( string dateFormat )
        {
            return string.IsNullOrWhiteSpace( dateFormat )
                ? throw new ArgumentNullException( nameof( dateFormat ) )
                : string.IsNullOrWhiteSpace( this.Caption ) ? this.FromPoint.T.ToString( Application.CurrentCulture ) : $"{this.Caption}: {this.FromPoint.T.ToString( dateFormat, Application.CurrentCulture )}";
        }

        #endregion

        #region " Properties"

        /// <summary> Gets or sets the Bookmark caption. </summary>
        /// <value> A <see cref="String"/> value. </value>
        public string Caption { get; set; }

        /// <summary>
        /// Gets or sets the start <see cref="TimeSeriesPointR">time series point</see> of this Bookmark.
        /// </summary>
        /// <value> A <see cref="TimeSeriesPointR">time series point</see> value. </value>
        public TimeSeriesPointR FromPoint { get; set; }

        /// <summary>
        /// Gets or sets the main <see cref="TimeSeriesPointR">time series point</see> of this Bookmark.
        /// </summary>
        /// <value> A <see cref="TimeSeriesPointR">time series point</see> value. </value>
        public TimeSeriesPointR MainPoint { get; set; }

        /// <summary> Gets or sets the book mark serial number in the book mark collection. </summary>
        /// <value> The serial number. </value>
        public int SerialNumber { get; set; }

        /// <summary> Gets or sets the Bookmark tag. </summary>
        /// <value> A <see cref="String"/> value. </value>
        public string Tag { get; set; }

        /// <summary>
        /// Gets or sets the end <see cref="TimeSeriesPointR">time series point</see> of this Bookmark.
        /// </summary>
        /// <value> A <see cref="TimeSeriesPointR">time series point</see> value. </value>
        public TimeSeriesPointR ToPoint { get; set; }

        #endregion

    }
}
