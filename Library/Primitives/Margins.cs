using System;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary> Provides <see cref="T:System.Integer">integer</see> margins properties. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public struct Margins
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs an instance of this class with specific margins. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">   Left margin. </param>
        /// <param name="right">  Right margin. </param>
        /// <param name="top">    Top margin. </param>
        /// <param name="bottom"> Bottom margin. </param>
        public Margins( int left, int right, int top, int bottom )
        {
            this.Left = left;
            this.Right = right;
            this.Top = top;
            this.Bottom = bottom;
        }

        /// <summary>
        /// Constructs an instance of this class with specific margins to convert from
        /// <see cref="System.Double">Double</see>
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">   Left margin. </param>
        /// <param name="right">  Right margin. </param>
        /// <param name="top">    Top margin. </param>
        /// <param name="bottom"> Bottom margin. </param>
        public Margins( double left, double right, double top, double bottom ) : this( Convert.ToInt32( left ), Convert.ToInt32( right ), Convert.ToInt32( top ), Convert.ToInt32( bottom ) )
        {
        }

        #endregion

        #region " EQUALS "

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Left margin. </param>
        /// <param name="right"> Right margin. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( Margins left, Margins right )
        {
            return Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Left margin. </param>
        /// <param name="right"> Right margin. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( Margins left, Margins right )
        {
            return !Equals( left, right );
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Left margin. </param>
        /// <param name="right"> Right margin. </param>
        /// <returns>
        /// <c>True</c> if arguments are the same type and represent the same value; otherwise,
        /// <c>False</c>.
        /// </returns>
        public static bool Equals( Margins left, Margins right )
        {
            return left.Equals( right );
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="obj"> Another object to compare to. </param>
        /// <returns>
        /// <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
        /// same value; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return obj is object && ReferenceEquals( this.GetType(), obj.GetType() ) && this.Equals( ( Margins ) obj );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="other"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// Margins are the same if the have the same
        /// <see cref="Left"/>, <see cref="Right"/>, <see cref="Top"/> and <see cref="Bottom"/>  value.
        /// </remarks>
        /// <param name="other"> The <see cref="Margins">Margins</see> to compare for equality with this
        /// instance. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public bool Equals( Margins other )
        {
            return this.Left.Equals( other.Left ) && this.Right.Equals( other.Right ) && this.Top.Equals( other.Top ) && this.Bottom.Equals( other.Bottom );
        }

        #endregion

        #region " METHODS "

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return this.Left.GetHashCode() ^ this.Top.GetHashCode() ^ this.Right.GetHashCode() ^ this.Bottom.GetHashCode();
        }

        /// <summary> Calculates the margins scaled to the ScaleFactor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="scaleFactor"> The scaling factor. </param>
        /// <returns> The margins after scaling by <paramref name="scaleFactor"/> </returns>
        public Margins GetScaledMargins( double scaleFactor )
        {
            return new Margins( this.Left * scaleFactor, this.Right * scaleFactor, this.Top * scaleFactor, this.Bottom * scaleFactor );
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the bottom margin. </summary>
        /// <value> A <see cref="T:System.Integer">integer</see> </value>
        public int Bottom { get; set; }

        /// <summary> Returns the sum of the left and right margins. </summary>
        /// <value> A <see cref="T:System.Integer">integer</see> </value>
        public int Horizontal => this.Left + this.Right;

        /// <summary> Gets the left margin. </summary>
        /// <value> A <see cref="T:System.Integer">integer</see> </value>
        public int Left { get; set; }

        /// <summary> Gets the right margin. </summary>
        /// <value> A <see cref="T:System.Integer">integer</see> </value>
        public int Right { get; set; }

        /// <summary> Gets the top margin. </summary>
        /// <value> A <see cref="T:System.Integer">integer</see> </value>
        public int Top { get; set; }

        /// <summary> Returns the top left point of the margins. </summary>
        /// <value> A <see cref="System.Drawing.Point"/> </value>
        public Point TopLeft => new Point( this.Left, this.Top );

        /// <summary> Returns the sum of the top and bottom margins. </summary>
        /// <value> A <see cref="T:System.Integer">integer</see> </value>
        public int Vertical => this.Top + this.Bottom;

        #endregion

    }

    /// <summary> Provides <see cref="System.Single">Single</see> margins properties. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581. Created </para>
    /// </remarks>
    public struct MarginsF
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs an instance of this class with specific margins. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">   Left margin. </param>
        /// <param name="right">  Right margin. </param>
        /// <param name="top">    Top margin. </param>
        /// <param name="bottom"> Bottom margin. </param>
        public MarginsF( float left, float right, float top, float bottom )
        {
            this.Left = left;
            this.Right = right;
            this.Top = top;
            this.Bottom = bottom;
        }

        /// <summary>
        /// Constructs an instance of this class with specific margins to convert from
        /// <see cref="System.Double">Double</see>
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">   Left margin. </param>
        /// <param name="right">  Right margin. </param>
        /// <param name="top">    Top margin. </param>
        /// <param name="bottom"> Bottom margin. </param>
        public MarginsF( double left, double right, double top, double bottom ) : this( Convert.ToSingle( left ), Convert.ToSingle( right ), Convert.ToSingle( top ), Convert.ToSingle( bottom ) )
        {
        }

        #endregion

        #region " EQUALS "

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Left margin. </param>
        /// <param name="right"> Right margin. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( MarginsF left, MarginsF right )
        {
            return Equals( left, ( object ) right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Left margin. </param>
        /// <param name="right"> Right margin. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( MarginsF left, MarginsF right )
        {
            return !Equals( left, ( object ) right );
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="left">  Left margin. </param>
        /// <param name="right"> Right margin. </param>
        /// <returns>
        /// <c>True</c> if values are the same type and represent the same value; otherwise, <c>False</c>.
        /// </returns>
        public static bool Equals( MarginsF left, MarginsF right )
        {
            return left.Left.Equals( right.Left ) && left.Right.Equals( right.Right ) && left.Top.Equals( right.Top ) && left.Bottom.Equals( right.Bottom );
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="obj"> Another object to compare to. </param>
        /// <returns>
        /// <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
        /// same value; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return obj is object && obj is Margins margins && this.Equals( margins );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="compared"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// Margins are the same if the have the same
        /// <see cref="Left"/>, <see cref="Right"/>, <see cref="Top"/> and <see cref="Bottom"/>  value.
        /// </remarks>
        /// <param name="compared"> The <see cref="Margins">Margins</see> to compare for equality with
        /// this instance. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public bool Equals( MarginsF compared )
        {
            return Equals( this, ( object ) compared );
        }

        #endregion

        #region " METHODS "

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return this.Left.GetHashCode() ^ this.Top.GetHashCode() ^ this.Right.GetHashCode() ^ this.Bottom.GetHashCode();
        }

        /// <summary> Calculates the margins scaled to the ScaleFactor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="scaleFactor"> The scaling factor. </param>
        /// <returns> The margins after scaling by <paramref name="scaleFactor"/> </returns>
        public MarginsF GetScaledMargins( double scaleFactor )
        {
            return new MarginsF( this.Left * scaleFactor, this.Right * scaleFactor, this.Top * scaleFactor, this.Bottom * scaleFactor );
        }

        /// <summary> Returns an area adjusted by the scaled margins. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="area"> The initial <see cref="System.Drawing.RectangleF"/> area. </param>
        /// <returns>
        /// A <see cref="System.Drawing.RectangleF"/> reduced by the horizontal and vertical spans and
        /// shifted by the top-left margins.
        /// </returns>
        public RectangleF GetAdjustedArea( RectangleF area )
        {

            // reduce the size of the area by the total horizontal and vertical spans
            area.Width -= this.Horizontal;
            area.Height -= this.Vertical;

            // shift the draw rectangle by the top left margins
            area.Offset( this.TopLeft );
            return area;
        }

        /// <summary> Returns an area adjusted by the scaled margins. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="area">        The initial <see cref="System.Drawing.RectangleF"/> area. </param>
        /// <param name="scaleFactor"> The scaling factor. </param>
        /// <returns>
        /// A <see cref="System.Drawing.RectangleF"/> reduced by the horizontal and vertical spans and
        /// shifted by the scaled (<paramref name="scaleFactor"/>) top-left margins.
        /// </returns>
        public RectangleF GetAdjustedArea( RectangleF area, double scaleFactor )
        {
            return this.GetScaledMargins( scaleFactor ).GetAdjustedArea( area );
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the bottom margin. </summary>
        /// <value> A <see cref="System.Single">Single</see> </value>
        public float Bottom { get; set; }

        /// <summary> Returns the sum of the left and right margins. </summary>
        /// <value> A <see cref="System.Single">Single</see> </value>
        public float Horizontal => this.Left + this.Right;

        /// <summary> Gets the left margin. </summary>
        /// <value> A <see cref="System.Single">Single</see> </value>
        public float Left { get; set; }

        /// <summary> Gets the right margin. </summary>
        /// <value> A <see cref="System.Single">Single</see> </value>
        public float Right { get; set; }

        /// <summary> Gets the top margin. </summary>
        /// <value> A <see cref="System.Single">Single</see> </value>
        public float Top { get; set; }

        /// <summary> Returns the top left point of the margins. </summary>
        /// <value> A <see cref="System.Drawing.Point"/> </value>
        public PointF TopLeft => new PointF( this.Left, this.Top );

        /// <summary> Returns the sum of the top and bottom margins. </summary>
        /// <value> A <see cref="System.Single">Single</see> </value>
        public float Vertical => this.Top + this.Bottom;

        #endregion

    }
}
