using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary>
    /// A class that represents a text object on the graph.  A list of
    /// <see cref="TextBox"/> objects is maintained by the <see cref="TextBoxCollection"/>
    /// collection class.
    /// </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class TextBox : ICloneable, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initialize a <see cref="TextBox" /> with properties from the
        /// <see cref="TextBoxDefaults" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public TextBox( Pane drawingPane ) : base()
        {
            if ( drawingPane is null )
            {
                throw new ArgumentNullException( nameof( drawingPane ) );
            }

            this.Text = "Text";
            this.Location = new PointF( 0f, 0f );
            var textBoxDefaults = TextBoxDefaults.Get();
            this.Alignment = new Alignment( textBoxDefaults.AlignH, textBoxDefaults.AlignV );
            this.CoordinateFrame = textBoxDefaults.CoordinateFrame;
            this.Appearance = new TextAppearance( textBoxDefaults.Font, textBoxDefaults.FontColor );
            this._Pane = drawingPane;
        }

        /// <summary>
        /// Constructs a <see cref="TextBox"/> with specific values and default properties from the
        /// <see cref="TextBoxDefaults"/> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="text">        The <see cref="TextBox"/> to be displayed.  This text can be
        /// multi-line by including new line ('\n') characters between the
        /// lines. </param>
        /// <param name="x">           The x position of the text.  Units are based on the
        /// <see cref="CoordinateFrame"/> property.  The text will be aligned
        /// to this position based
        /// on the <see cref="Alignment.Horizontal"/> property. </param>
        /// <param name="y">           The y position of the text.  Units are specified by the
        /// <see cref="CoordinateFrame"/> property.  The text will be aligned
        /// to this position based
        /// on the <see cref="Alignment.Vertical"/> property. </param>
        /// <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
        public TextBox( string text, float x, float y, Pane drawingPane ) : this( drawingPane )
        {
            if ( string.IsNullOrWhiteSpace( text ) )
            {
                text = string.Empty;
            }

            this.Text = text;
            this.Location = new PointF( x, y );
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="model"> The isr.Visuals.TextBox object from which to copy. </param>
        public TextBox( TextBox model ) : base()
        {
            if ( model is null )
            {
                throw new ArgumentNullException( nameof( model ) );
            }

            this.Text = model.Text;
            this.Alignment = model.Alignment;
            this.Location = model.Location;
            this.CoordinateFrame = model.CoordinateFrame;
            this.Appearance = model.Appearance.Copy();
            this._Pane = model._Pane;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets (private) the dispose status sentinel. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called
                        this.Alignment = default;
                        if ( this.Appearance is object )
                        {
                            this.Appearance.Dispose();
                            this.Appearance = null;
                        }
                    }

                    // Free shared unmanaged resources

                }
            }
            finally
            {

                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the isr.Visuals.TextBox. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the isr.Visuals.TextBox. </returns>
        public TextBox Copy()
        {
            return new TextBox( this );
        }

        /// <summary>
        /// Renders this <see cref="TextBox"/> object to the specified
        /// <see cref="Graphics"/> device. This method is normally only called by
        /// the Draw method of the parent <see cref="TextBoxCollection"/> collection object.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        public void Draw( Graphics graphicsDevice )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            var screenLocation = this._Pane.GeneralTransform( this.Location, this.CoordinateFrame );
            if ( this._Pane.DrawArea.Contains( screenLocation ) )
            {

                // Draw the text on the screen, including any frame and background fill elements
                this.Appearance.Draw( graphicsDevice, this.Text, screenLocation.X, screenLocation.Y, this.Alignment.Horizontal, this.Alignment.Vertical, this._Pane.ScaleFactor );
            }
            else
            {
                Debug.Assert( !Debugger.IsAttached, "text box location out of draw area" );
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary>
        /// Alignment for this <see cref="TextBox"/> specified using the <see cref="Alignment"/> type.
        /// </summary>
        /// <value> The alignment. </value>
        public Alignment Alignment { get; set; }

        /// <summary>
        /// gets a reference to the <see cref="isr.Visuals.TextAppearance">TextAppearance</see> class
        /// used to render this <see cref="TextBox"/>
        /// </summary>
        /// <value> A <see cref="isr.Visuals.TextAppearance">TextAppearance</see> instance. </value>
        public TextAppearance Appearance { get; private set; }

        /// <summary>
        /// The location of the <see cref="TextBox"/>.  Units are based on the
        /// <see cref="CoordinateFrame"/> property.  Text will be aligned to this location based on the
        /// <see cref="Alignment"/> property.
        /// </summary>
        /// <value> The location. </value>
        public PointF Location { get; set; }

        /// <summary>
        /// The coordinate system to be used for defining the <see cref="TextBox"/> position.
        /// </summary>
        /// <value> A <see cref="CoordinateFrameType"/> </value>
        public CoordinateFrameType CoordinateFrame { get; set; }

        /// <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
        private readonly Pane _Pane;

        /// <summary> Gets or sets the status message. </summary>
        /// <value> A System.String value. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string StatusMessage { get; set; }

        /// <summary>The <see cref="TextBox"/> to be displayed.  This text can be multi-line by
        /// including new line ('\n') characters between the lines.</summary>
        public string Text { get; set; }

        #endregion

    }

    #region " DEFAULTS "

    /// <summary>
    /// A simple subclass of the <see cref="TextBox"/> class that defines the default property values
    /// for the <see cref="TextBox"/> class.
    /// </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public sealed class TextBoxDefaults : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs this class. This constructor is private to ensure only a single instance of this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private TextBoxDefaults() : base()
        {
            this.AlignV = VerticalAlignment.Center;
            this.AlignH = HorizontalAlignment.Center;
            this.CoordinateFrame = CoordinateFrameType.AxisXYScale;
            this.Font = new Font( "Arial", 14f, FontStyle.Regular | FontStyle.Bold );
            this.FontColor = Color.Black;
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new object();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static TextBoxDefaults _Instance;

        /// <summary>
        /// Instantiates the class.
        /// </summary>
        /// <returns>
        /// A new or existing instance of the class.
        /// </returns>
        /// <remarks>
        /// Use this property to instantiate a single instance of this class.
        /// This class uses lazy instantiation, meaning the instance isn't
        /// created until the first time it's retrieved.
        /// </remarks>
        public static TextBoxDefaults Get()
        {
            if ( _Instance is null || _Instance.IsDisposed )
            {
                lock ( SyncLocker )
                    _Instance = new TextBoxDefaults();
            }

            return _Instance;
        }

        #region "IDisposable Support"

        /// <summary> Gets or sets a value indicating whether this instance is disposed. </summary>
        /// <value> <c>True</c> if this instance is disposed; otherwise, <c>False</c>. </value>
        private bool IsDisposed { get; set; } // To detect redundant calls

        /// <summary> Releases unmanaged and - optionally - managed resources. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged resources. </param>
        private void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        // dispose managed state (managed objects).
                        if ( this.Font is object )
                        {
                            this.Font.Dispose();
                            this.Font = null;
                        }
                    }
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }
        #endregion

        #endregion

        /// <summary>
        /// Default value for the vertical <see cref="TextBox"/>
        /// text alignment (<see cref="Alignment.Vertical"/> property).
        /// </summary>
        /// <value> A  <see cref="VerticalAlignment"/> value. </value>
        public VerticalAlignment AlignV { get; set; }

        /// <summary>
        /// Default value for the horizontal <see cref="TextBox"/>
        /// text alignment (<see cref="Alignment.Horizontal"/> property).
        /// </summary>
        /// <value> A  <see cref="HorizontalAlignment"/> value. </value>
        public HorizontalAlignment AlignH { get; set; }

        /// <summary>
        /// Gets or sets the default coordinate system to be used for defining the
        /// <see cref="TextBox"/> location coordinates
        /// (<see cref="TextBox.CoordinateFrame"/> property).
        /// </summary>
        /// <value> A <see cref="CoordinateFrameType"/> </value>
        public CoordinateFrameType CoordinateFrame { get; set; }

        /// <summary>
        /// Gets or sets the default <see cref="System.Drawing.Font">Font</see>
        /// of the <see cref="Title"/> caption.
        /// </summary>
        /// <value> The font. </value>
        public Font Font { get; set; }

        /// <summary>
        /// Gets or sets the default font color for the <see cref="Legend"/> entries
        /// (<see cref="TextAppearance.FontColor"/> property).
        /// </summary>
        /// <value> A <see cref="System.Drawing.Color">Color</see> </value>
        public Color FontColor { get; set; }
    }
}

#endregion

