using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

using isr.Core.TimeSpanExtensions;

namespace isr.Visuals
{

    /// <summary>Draws a simple line strip chart.</summary>
    /// <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2005-07-27, 1.0.2034.x">
    /// Created
    /// </para></remarks>
    [Description( "Line Recorder - Windows Forms Custom Control" )]
    [ToolboxBitmap( typeof( LineRecorder ) )]
    public partial class LineRecorder : UserControl
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs the class initializing components. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public LineRecorder() : base()
        {

            // Initialize user components that might be affected by resize or paint actions
            // onInitialize()

            // This method is required by the Windows Form Designer.
            this.InitializeComponent();
            this._GridSize = 12;
            this._Amplitude = 50;
            this._LowerLimit = 25;
            this._Maximum = 100;
            this._RefreshInterval = 1000;
            this._UpperLimit = 75;

            // Add any initialization after the InitializeComponent() call
            this.SetStyle( ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer | ControlStyles.CacheText | ControlStyles.Opaque | ControlStyles.ResizeRedraw, true );
            this.UpdateStyles();
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
        /// resources; <see langword="false" /> to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    if ( this._Thread is object )
                        this._Thread.Abort();
                    if ( this.components is object )
                        this.components.Dispose();
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }


        #endregion

        #region " METHODS  and  PROPERTIES "

        /// <summary> The amplitude. </summary>
        private int _Amplitude;

        /// <summary> The current amplitude to plot. </summary>
        /// <value> The amplitude. </value>
        [Category( "Behavior" )]
        [DefaultValue( 50 )]
        [Description( "The current amplitude to plot." )]
        public int Amplitude
        {
            get => this._Amplitude;

            set {
                if ( value > this.Maximum )
                {
                    this._Amplitude = this._Maximum;
                }
                else if ( value < this._Minimum )
                {
                    this._Amplitude = this._Minimum;
                }
                else
                {
                    this._Amplitude = value;
                }
            }
        }

        /// <summary>Gets or sets the amplitudes stored.</summary>
        private readonly ArrayList _Amplitudes = new ArrayList();

        /// <summary> Returns the default size for this control. </summary>
        /// <value> The default <see cref="T:System.Drawing.Size" /> of the control. </value>
        protected override Size DefaultSize => new Size( 200, 100 );

        /// <summary>Gets or sets the vertical and horizontal step size in pixels.</summary>
        private const int _DivisionSize = 2;

        /// <summary> Size of the grid. </summary>
        private int _GridSize;

        /// <summary> The grid size in pixels. </summary>
        /// <value> The size of the grid. </value>
        [Category( "Behavior" )]
        [DefaultValue( 12 )]
        [Description( "The grid size in pixels." )]
        public int GridSize
        {
            get => this._GridSize;

            set {
                if ( value < 2 )
                {
                    value = _DivisionSize;
                }

                this._GridSize = value;
            }
        }

        /// <summary> Name of the instance. </summary>
        private readonly string _InstanceName = "LineRecorder";

        /// <summary> Gets the instance name of the instance of this class. </summary>
        /// <remarks>
        /// Use this property to identify the instance of the class.  If instance name is not set,
        /// returns .ToString.
        /// </remarks>
        /// <value> <c>InstanceName</c> is a String property that can be read from (read only). </value>
        public string InstanceName => string.IsNullOrWhiteSpace( this._InstanceName ) ? this._InstanceName : base.ToString();

        /// <summary> The lower limit. </summary>
        private int _LowerLimit;

        /// <summary> The lower value of the normal range. </summary>
        /// <value> The lower limit. </value>
        [Category( "Behavior" )]
        [DefaultValue( 25 )]
        [Description( "The lower value of the normal range." )]
        public int LowerLimit
        {
            get => this._LowerLimit;

            set {
                if ( value > this.UpperLimit )
                {
                    this._LowerLimit = this._UpperLimit;
                }
                else if ( value < this._Minimum )
                {
                    this._LowerLimit = this._Minimum;
                }
                else
                {
                    this._LowerLimit = value;
                }

                this.Invalidate();
            }
        }

        /// <summary> The maximum. </summary>
        private int _Maximum;

        /// <summary> The upper bound of the amplitude range. </summary>
        /// <value> The maximum value. </value>
        [Category( "Behavior" )]
        [DefaultValue( 100 )]
        [Description( "The upper bound of the amplitude range." )]
        public int Maximum
        {
            get => this._Maximum;

            set {
                this._Maximum = value < this._UpperLimit ? this._UpperLimit : value;
                this.Invalidate();
            }
        }

        /// <summary> The minimum. </summary>
        private int _Minimum;

        /// <summary> The lower bound of the amplitude range. </summary>
        /// <value> The minimum value. </value>
        [Category( "Behavior" )]
        [DefaultValue( 0 )]
        [Description( "The lower bound of the amplitude range." )]
        public int Minimum
        {
            get => this._Minimum;

            set {
                this._Minimum = value > this.LowerLimit ? this._LowerLimit : value;
                this.Invalidate();
            }
        }

        /// <summary>Gets or sets the strip chart position within the grid box.</summary>
        private int _Mover;

        /// <summary> Starts or resumes recording. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void Play()
        {
            if ( this._Thread is null || this._Thread.ThreadState == System.Threading.ThreadState.Aborted )
            {
                this._Thread = new Thread( new ThreadStart( this.ProcessThread ) );
                this._Thread.Start();
            }
            else if ( this._Thread.ThreadState == System.Threading.ThreadState.Suspended )
            {
                this._Thread.Start();
            }
            else if ( this._Thread.ThreadState == System.Threading.ThreadState.Stopped )
            {
                this._Thread.Start();
            }
        }

        /// <summary> Pauses the recorder. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void Pause()
        {
            if ( this._Thread is object && this._Thread.ThreadState != System.Threading.ThreadState.Suspended )
            {
                this._Thread.Start();
            }
        }

        /// <summary>Stops the recorder.</summary>
        public void Stop()
        {
            if ( this._Thread is object )
            {
                this._Thread.Abort();
            }
        }

        /// <summary> Processes the data. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private void ProcessThread()
        {
            while ( this._Thread.ThreadState == System.Threading.ThreadState.Running )
            {
                if ( this._Mover >= this._GridSize - _DivisionSize )
                {
                    this._Mover = 0;
                }
                else
                {
                    this._Mover += _DivisionSize;
                }

                if ( this._Amplitudes.Count < this.Width / ( double ) _DivisionSize )
                {

                    // add a new amplitude
                    _ = this._Amplitudes.Add( this._Amplitude );
                }
                else
                {

                    // shift the amplitudes
                    for ( int i = 0, loopTo = this._Amplitudes.Count - 2; i <= loopTo; i++ )
                        this._Amplitudes[i] = this._Amplitudes[i + 1];
                    this._Amplitudes[this._Amplitudes.Count - 1] = this._Amplitude;
                }

                // redraw
                this.Invalidate();

                // wait before the next redraw.
                TimeSpan.FromMilliseconds( this._RefreshInterval ).SpinWait();
            }
        }

        /// <summary> The refresh interval. </summary>
        private int _RefreshInterval;

        /// <summary> The refreshing interval of the recorder between 1 and 1000 milliseconds. </summary>
        /// <value> The refresh interval. </value>
        [Category( "Behavior" )]
        [DefaultValue( 1000 )]
        [Description( "The refreshing interval of the recorder between 1 and 1000 milliseconds." )]
        public int RefreshInterval
        {
            get => this._RefreshInterval;

            set {
                if ( value < 1 )
                {
                    value = 1;
                }
                else if ( value > 1000 )
                {
                    value = 1000;
                }

                this._RefreshInterval = value;
            }
        }

        /// <summary>Reference to the thread the runs the strip chart.</summary>
        private Thread _Thread;

        /// <summary> The upper limit. </summary>
        private int _UpperLimit;

        /// <summary> The upper value of the normal range. </summary>
        /// <value> The upper limit. </value>
        [Category( "Behavior" )]
        [DefaultValue( 75 )]
        [Description( "The upper value of the normal range." )]
        public int UpperLimit
        {
            get => this._UpperLimit;

            set {
                if ( value > this.Maximum )
                {
                    this._UpperLimit = this._Maximum;
                }
                else if ( value < this._LowerLimit )
                {
                    this._UpperLimit = this._LowerLimit;
                }
                else
                {
                    this._UpperLimit = value;
                }

                this.Invalidate();
            }
        }

        /// <summary> Hold True to highlight out of bounds values. </summary>
        /// <value> The use limits. </value>
        [Category( "Behavior" )]
        [DefaultValue( false )]
        [Description( "Hold True to highlight out of bounds values." )]
        public bool UseLimits { get; set; }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Draw the chart and the grid. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaint( PaintEventArgs e )
        {
            if ( e is null )
                return;
            base.OnPaint( e );
            var graphicsDevice = e.Graphics;

            // draw the horizontal grid.
            int position = 0;
            for ( int i = 1, loopTo = Convert.ToInt32( this.Height / ( double ) this._GridSize ); i <= loopTo; i++ )
            {
                position += this._GridSize;
                graphicsDevice.DrawLine( Pens.DarkGreen, 0, position, this.Width, position );
            }

            // draw the vertical grid.
            position = -this._Mover;
            for ( int i = 1, loopTo1 = Convert.ToInt32( this.Width / ( double ) this._GridSize ); i <= loopTo1; i++ )
            {
                position += this._GridSize;
                graphicsDevice.DrawLine( Pens.DarkGreen, position, 0, position, this.Height );
            }

            double scale = this.Height / ( double ) (this._Maximum - this._Minimum);
            var drawingPen = Pens.Yellow;
            int currentX = this.Width - _DivisionSize * this._Amplitudes.Count;
            int previousX;
            int currentY;
            int previousY;
            int currentAmplitude;
            int previousAmplitude;
            if ( this.UseLimits && this._Maximum > this.UpperLimit && this._Minimum < this._LowerLimit )
            {
                for ( int i = 1, loopTo2 = this._Amplitudes.Count - 1; i <= loopTo2; i++ )
                {
                    previousX = currentX;
                    currentX += _DivisionSize;
                    currentAmplitude = Convert.ToInt32( this._Amplitudes[i], System.Globalization.CultureInfo.CurrentCulture );
                    previousAmplitude = Convert.ToInt32( this._Amplitudes[i - 1], System.Globalization.CultureInfo.CurrentCulture );
                    currentY = Convert.ToInt32( scale * (this._Maximum - Convert.ToInt32( this._Amplitudes[i], System.Globalization.CultureInfo.CurrentCulture )) );
                    previousY = Convert.ToInt32( scale * (this._Maximum - previousAmplitude), System.Globalization.CultureInfo.CurrentCulture );
                    drawingPen = currentAmplitude > this.UpperLimit | currentAmplitude < this._LowerLimit ? Pens.Red : Pens.Yellow;
                    graphicsDevice.DrawLine( drawingPen, currentX, currentY, previousX, previousY );
                }
            }
            else
            {
                for ( int i = 1, loopTo3 = this._Amplitudes.Count - 1; i <= loopTo3; i++ )
                {
                    currentAmplitude = Convert.ToInt32( this._Amplitudes[i], System.Globalization.CultureInfo.CurrentCulture );
                    previousAmplitude = Convert.ToInt32( this._Amplitudes[i - 1], System.Globalization.CultureInfo.CurrentCulture );
                    previousX = currentX;
                    currentX += _DivisionSize;
                    currentY = Convert.ToInt32( scale * (this._Maximum - currentAmplitude), System.Globalization.CultureInfo.CurrentCulture );
                    previousY = Convert.ToInt32( scale * (this._Maximum - previousAmplitude), System.Globalization.CultureInfo.CurrentCulture );
                    graphicsDevice.DrawLine( drawingPen, currentX, currentY, previousX, previousY );
                }
            }

            ControlPaint.DrawBorder3D( graphicsDevice, 0, 0, this.Width, this.Height, Border3DStyle.Sunken );
        }

        /// <summary> Paints the background of the control. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaintBackground( PaintEventArgs e )
        {
            if ( e is object )
            {
                base.OnPaintBackground( e );
                if ( this.UseLimits && this._Maximum > this.UpperLimit && this._Minimum < this._LowerLimit )
                {
                    var myColor = Color.FromArgb( 64, Color.White );
                    using var myBrush = new SolidBrush( myColor );
                    double scale = this.Height / ( double ) (this.Maximum - this.Minimum);
                    var range = new Rectangle( 0, Convert.ToInt32( scale * (this.Maximum - this.UpperLimit) ), this.Width, Convert.ToInt32( scale * (this.UpperLimit - this.LowerLimit) ) );
                    var graphicsDevice = e.Graphics;
                    graphicsDevice.FillRectangle( myBrush, range );
                }
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.Resize" /> event. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnResize( EventArgs e )
        {
            if ( e is object )
            {
                base.OnResize( e );
                if ( this._Amplitudes.Count > this.Width / ( double ) _DivisionSize )
                {
                    this._Amplitudes.RemoveRange( 0, this._Amplitudes.Count - Convert.ToInt32( this.Width / ( double ) _DivisionSize ) );
                }
            }
        }

        #endregion

    }
}
