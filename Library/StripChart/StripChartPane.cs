using System;
using System.Drawing;

namespace isr.Visuals
{

    /// <summary>
    /// Draws a strip chart.  It is preferable to use the strip chart control because it has embedded
    /// double buffering whereas with this double buffering must be set.
    /// </summary>
    /// <remarks>
    /// For optimal graphics, leverage the power of the .Net Framework. Some
    /// controls, such as the PictureBox are double buffered automatically. Also, pay attention to
    /// how the ResizeRedraw, Opaque, DoubleBuffer, and other control styles can benefit you.
    /// Consolidate painting code. The basic logic used to render your Windows Forms application
    /// should exist in the OnPaint and OnPaintBackground methods. Avoid creating drawing code in
    /// methods responding to Resize, Load, or Show events. Think before you paint. Obviously, draw
    /// as little as possible for the best performance. Make use of Regions and clipping
    /// rectangles.  Employ a double buffering technique if feasible. Know your trade offs. For
    /// example: keeping global Brush objects may benefit the speed of your application, however
    /// the memory footprint will be larger. Also, even though the floating point data type is
    /// larger than an integer, the use of floats will provide more accurate scaling. <para>
    /// David, 2004-05-12, 1.0.1593.  Created </para><para>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class StripChartPane : Pane, ICloneable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs a <see cref="StripChartPane"/> with default values, unit drawing area, and empty
        /// labels.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public StripChartPane() : base()
        {
        }

        /// <summary>
        /// Constructs a <see cref="StripChartPane"/> object with the specified drawing area and axis and
        /// chart labels.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="chartArea"> A rectangular screen area where the chart is to be displayed. This
        /// area can be any size, and can be resize at any time using the
        /// <see cref="isr.Visuals.Pane.PaneArea"/> property. </param>
        public StripChartPane( RectangleF chartArea ) : base( chartArea )
        {
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="model"> The StripChartPane object from which to copy. </param>
        public StripChartPane( StripChartPane model ) : base( model )
        {
        }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// <c>False</c> if this method releases only unmanaged resources.
        /// </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {

                        // Free managed resources when explicitly called

                    }

                    // Free shared unmanaged resources
                }
            }
            finally
            {

                // Invoke the base class dispose method

                base.Dispose( disposing );
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> Updates the time series curve data. </summary>
        /// <remarks>
        /// Use this method to add a new value to the strip chart.  Shifts the buffer one notch every
        /// time a new data points comes in thus 'scrolling' the data along.
        /// </remarks>
        /// <param name="time">      The time (X or horizontal axis) value. </param>
        /// <param name="amplitude"> The amplitude (Y, or vertical axis) value. </param>
        public void AddDataPoint( DateTime time, double amplitude )
        {
            _ = this.Curves[0].AddDataPoint( time, amplitude );
        }

        /// <summary> Updates the time series curve data. </summary>
        /// <remarks>
        /// Use this method to add a new value to the strip chart.  Shifts the buffer one notch every
        /// time a new data points comes in thus 'scrolling' the data along.
        /// </remarks>
        /// <param name="time">      The time (X or horizontal axis) value. </param>
        /// <param name="amplitude"> The amplitude (Y, or vertical axis) value. </param>
        public void AddDataPoint( DateTimeOffset time, double amplitude )
        {
            _ = this.Curves[0].AddDataPoint( time.DateTime, amplitude );
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the StripChartPane. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new, independent copy of the StripChartPane. </returns>
        public StripChartPane Copy()
        {
            return new StripChartPane( this );
        }

        /// <summary>
        /// Creates a sample demo chart with three curves, titles, text boxes, arrows and legend.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="clientRectangle"> The client area to use for displaying the chart. </param>
        public void CreateSampleOne( Rectangle clientRectangle )
        {

            // set chart area and titles.
            this.PaneArea = new RectangleF( 10f, 10f, 10f, 10f );
            this.Title.Visible = false;
            this.Legend.Visible = false;
            this.AxisFrame.FillColor = Color.WhiteSmoke;

            // amplitude Y-axis is first so that the vertical grid labels (x grid) are
            // drawn over the Y axis labels
            var yAxis = this.AddAxis( "Amplitude", AxisType.Y );
            yAxis.Grid.Visible = true;
            yAxis.Grid.LineColor = Color.DarkGray;
            yAxis.TickLabels.Visible = true;
            yAxis.TickLabels.DecimalPlaces = new AutoValue( 0, true );
            yAxis.Title.Visible = false;
            yAxis.Max = new AutoValueR( 1d, false );
            yAxis.Min = new AutoValueR( -1, false );

            // time series X axis
            var xAxis = this.AddAxis( "Time", AxisType.X );
            xAxis.Visible = false;
            xAxis.CoordinateScale = new CoordinateScale( CoordinateScaleType.StripChart );
            xAxis.Max = new AutoValueR( 1d, false );
            xAxis.Min = new AutoValueR( 0d, false );
            xAxis.Grid.Visible = true;
            xAxis.Grid.LineColor = Color.DarkGray;
            xAxis.MajorTick.Visible = true;
            xAxis.MajorTick.ScaleFormat = "T";
            xAxis.MinorTick.Visible = false;
            xAxis.TickLabels.Visible = true;
            xAxis.TickLabels.Appearance.Angle = -90.0f;
            xAxis.TickLabels.Appearance.FontColor = Color.DarkGray;
            xAxis.TickLabels.Appearance.Frame.Visible = true;
            xAxis.TickLabels.Appearance.Frame.IsOutline = false;
            xAxis.TickLabels.Appearance.Frame.Filled = true;
            xAxis.TickLabels.Appearance.Frame.FillColor = this.AxisFrame.FillColor;
            xAxis.Title.Visible = false;
            this.SetSize( clientRectangle );
            Curve curve;
            curve = this.AddCurve( CurveType.StripChart, "time series", xAxis, yAxis );
            curve.Cord.LineColor = Color.Red;
            curve.Symbol.Shape = ShapeType.Star;
            curve.Symbol.Size = 1f;
            curve.Symbol.Visible = false;
            this.Rescale();
        }

        /// <summary>
        /// Draw all elements in the <see cref="StripChartPane"/> to the specified graphics device.  This
        /// routine should be part of the Paint() update process.  Calling This method will redraw all
        /// features of the graph.  No preparation is required other than an instantiated
        /// <see cref="StripChartPane"/> object.
        /// </summary>
        /// <remarks>
        /// In a strip chart it is presumed that, at each chart speed, pixel spacing correspond to a fix
        /// time scale, e.graphicsDevice., 1 per 100 ms or 1 per 20 ms. Therefore, each new data point
        /// corresponds to a screen point.  What is different as the speed changes are the time marks on
        /// the chart.  Thus, to draw the curve time wise requires to translate the curve index back from
        /// the current point to the left side of the chart.  To draw the grid requires to locate the
        /// index which most closely corresponds to the current chart spacing. For drawing the grid, we
        /// assume a fixed grid count of 10 major divisions per window and 5 minor divisions per major
        /// division.  This corresponds to the standard charting when changing speed does not affect the
        /// divisions of the paper but rather how the paper is labeled.  At higher speed, each division
        /// corresponds to a Int16er time span.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
        /// <see cref="System.Windows.Forms.PaintEventArgs.Graphics"/> of the
        /// <see cref="M:Paint"/> method. </param>
        public override void Draw( Graphics graphicsDevice )
        {

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // clear and exit if not visible.
            if ( !this.Visible )
            {
                graphicsDevice.Clear( this.PaneFrame.FillColor );
                return;
            }

            // set the scale factor relative to the base dimension)
            this.ScaleFactor = this.GetScaleFactor( graphicsDevice );

            // allocate the drawing area
            this.SetDrawArea();

            // Calculate the axis area, deducting the area for the scales, titles, legend, etc.
            this.SetAxisArea( graphicsDevice );

            // Frame the pane and axis
            this.DrawFrames( graphicsDevice );

            // Draw the Pane Title
            this.Title.Draw( graphicsDevice );

            // set the time series scale
            this.Curves.ScaleTimeSeriesAxis();

            // Draw the Axes
            this.Axes.Draw( graphicsDevice );

            // draw the curve on top of the label and grids.
            this.Curves.Draw( graphicsDevice );

            // Reset the clipping
            graphicsDevice.ResetClip();
        }

        /// <summary>
        /// Prints all elements in the <see cref="Pane"/> to the specified graphics device.  This routine
        /// should be overridden by the inheriting classes.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="graphicsDevice"> Reference to a graphic device to print into.  This is normally
        /// <see cref="System.Drawing.Printing.PrintPageEventArgs.Graphics">graphics</see>
        /// of the <see cref="M:PrintPage"/> delegate. </param>
        /// <param name="printArea">      The <see cref="System.Drawing.RectangleF">area</see> on the print
        /// document allotted for the chart. </param>
        public override void Print( Graphics graphicsDevice, RectangleF printArea )
        {

            // exit if not visible.
            if ( !this.Visible )
            {
                return;
            }

            // validate argument.
            if ( graphicsDevice is null )
            {
                throw new ArgumentNullException( nameof( graphicsDevice ) );
            }

            // save the pane area
            var tempPaneArea = this.PaneArea;

            // set the pane area to the print window
            this.PaneArea = new RectangleF( printArea.Location, printArea.Size );
            this.Draw( graphicsDevice );

            // restore the pane area
            this.PaneArea = tempPaneArea;
        }

        #endregion

    }
}
