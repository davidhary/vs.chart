using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace isr.Visuals
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class LineRecorder
    {

        // Required by the Windows Form Designer
        private IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new Container();
            AutoScaleMode = AutoScaleMode.Font;
        }
    }
}
