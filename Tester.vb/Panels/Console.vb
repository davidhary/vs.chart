''' <summary> Test console. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2/15/2014, Documented. </para>
''' </remarks>
Public Class Console
    Inherits isr.Core.Forma.UserFormBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()
        '
        ' Required for Windows Form Designer support
        Me.CreateChart()
        Me.InitializeComponent()
        '         Me._graphicsBuffer = New BufferedGraphics()
    End Sub 'New

#End Region

    ''' <summary> Buffer for graphics data. </summary>
    Private _GraphicsBuffer As BufferedGraphics

    ''' <summary> The chart pane. </summary>
    Private _ChartPane As ChartPane

    ''' <summary> Creates the chart. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub CreateChart()

        Me._GraphicsBuffer = New isr.Visuals.BufferedGraphics(Me.ClientRectangle.Width, Me.ClientRectangle.Height)
        Me._GraphicsBuffer.CreateDoubleBuffer(Me.ClientRectangle.Width, Me.ClientRectangle.Height)
        Me._ChartPane = New isr.Visuals.ChartPane
        Me._ChartPane.CreateSampleOne(Me.ClientRectangle)

    End Sub

    ''' <summary> Paints the background of the control. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="pevent"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                       event data. </param>
    Protected Overrides Sub OnPaintBackground(ByVal pevent As PaintEventArgs)
    End Sub 'OnPaintBackground

    ''' <summary> Form 1 paint. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Paint event information. </param>
    Private Sub Form1_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MyBase.Paint

        If Me._GraphicsBuffer.CanDoubleBuffer() Then

            ' Fill in Background (for efficiency only the area that has been clipped)
            Using SB As New SolidBrush(SystemColors.Window)
                Me._GraphicsBuffer.GraphicsDevice.FillRectangle(SB, e.ClipRectangle.X, e.ClipRectangle.Y, e.ClipRectangle.Width, e.ClipRectangle.Height)
            End Using

            ' clear the client area
            Using SB As New SolidBrush(SystemColors.Window)
                Me._GraphicsBuffer.GraphicsDevice.FillRectangle(SB, Me.ClientRectangle)
            End Using

            ' Drawing using Me._graphicsBuffer.g 
            Me._ChartPane.Draw(Me._GraphicsBuffer.GraphicsDevice)

            ' Render to the form
            Me._GraphicsBuffer.Render(e.Graphics)

            ' if double buffer is not available, do without it
        Else
            ' Drawing using e.Graphics

            ' clear
            Using SB As New SolidBrush(Color.Gray)
                e.Graphics.FillRectangle(SB, Me.ClientRectangle)
            End Using

            ' draw
            Me._ChartPane.Draw(e.Graphics)
        End If

    End Sub 'Form1_Paint

    ''' <summary> Form 1 resize. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Form1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        Me._GraphicsBuffer.CreateDoubleBuffer(Me.ClientRectangle.Width, Me.ClientRectangle.Height)
        Me.SetSize()
        Me.Invalidate()
    End Sub 'Form1_Resize

    ''' <summary> Sets the size. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub SetSize()
        Dim paneArea As RectangleF = RectangleF.op_Implicit(Me.ClientRectangle)
        PaneArea.Inflate(-10, -10)
        Me._chartPane.PaneArea = PaneArea
    End Sub 'SetSize

End Class
