Imports isr.Visuals.Testers.ExceptionExtensions

''' <summary> A chart control panel. </summary>
''' <remarks>
''' Launch this form by calling its Show or ShowDialog method from its default instance. <para>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved.  </para><para>
''' Licensed under The MIT License. </para><para>  
''' David, 2/15/2014, Documented. </para>
''' </remarks>
Public Class ChartControlPanel
    Inherits isr.Core.Forma.UserFormBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="ChartControlPanel" /> class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions

        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        ' instantiate a main context menu
        Dim mainContextMenu As ContextMenu
        mainContextMenu = New ContextMenu
        mainContextMenu.MenuItems.Add("Print", AddressOf Me.OnPrintMenuItem)
        Me.ContextMenu = mainContextMenu

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> The instantiated. </value>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        Get
            Return My.Application.OpenForms.Count > 0 AndAlso
                My.Application.OpenForms.Item(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name) IsNot Nothing
        End Get
    End Property

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Occurs before the form is closed. </summary>
    ''' <remarks>
    ''' Use this method to optionally cancel the closing of the form. Because the form is not yet
    ''' closed at this point, this is also the best place to serialize a form's visible properties,
    ''' such as size and location. Finally, dispose of any form level objects especially those that
    ''' might needs access to the form and thus should not be terminated after the form closed.
    ''' </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                         <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      <see cref="System.ComponentModel.CancelEventArgs"/> </param>
    Private Sub Form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        Windows.Forms.Application.DoEvents()

        ' set module objects that reference other objects to Nothing

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            ' terminate form-level objects
            ' Me.terminateObjects()
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary> Occurs when the form is loaded. </summary>
    ''' <remarks>
    ''' Use this method for doing any final initialization right before the form is shown.  This is a
    ''' good place to change the Visible and ShowInTaskbar properties to start the form as hidden.  
    ''' Starting a form as hidden is useful for forms that need to be running but that should not
    ''' show themselves right away, such as forms with a notify icon in the task bar.
    ''' </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                         <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      <see cref="System.EventArgs"/> </param>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            'Me.instantiateObjects()

            ' set the form caption
            Me.Text = My.Application.Info.BuildDefaultCaption(": CHART CONTROL PANEL")

            ' set tool tips
            'initializeUserInterface()

            ' center the form
            Me.CenterToScreen()

            ' Create the chart
            Me.ChartControl1.ChartPane.CreateSampleOne(Me.ChartControl1.ClientRectangle)

            ' turn on the loaded flag
            '      loaded = true

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " PRINTING "

    ''' <summary> The print window area includes all the drawing objects. </summary>
    Private _PrintWindow As RectangleF = New RectangleF(100, 200, 650, 400)

    ''' <summary> The print document handles the printing. </summary>
    Private _PrintDoc As System.Drawing.Printing.PrintDocument

    ''' <summary> Handles the print menu item delegate. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Reference to a <see cref="System.EventArgs"></see> </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnPrintMenuItem(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Me.PrintChart()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show($"{Me.Name} failed printing. {ex.ToFullBlownString}.", "Exception", MessageBoxButtons.OK, MessageBoxIcon.Exclamation,
                                                 MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary> Prints the chart. </summary>
    ''' <remarks> Use this method to print the strip chart. </remarks>
    Private Sub PrintChart()

        ' instantiate the print document object to handle the printing
        Me._PrintDoc = New System.Drawing.Printing.PrintDocument

        ' add handler to handle printing
        AddHandler Me._PrintDoc.PrintPage, AddressOf Me.PrintChartHandler

        ' set the cursor to wait
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        ' do the printing
        Me._PrintDoc.Print()

    End Sub

    ''' <summary> Prints the chart header. </summary>
    ''' <remarks> Prints the chart header. </remarks>
    ''' <param name="g">            specifies the graphics context for
    '''                               printing. </param>
    ''' <param name="headerHeight"> [in,out] is a Single value returning the height of the header. </param>
    Private Sub PrintChartHeader(ByVal g As Graphics, ByRef headerHeight As Single)

        Using dateFont As New Font("Ariel", 10, FontStyle.Regular)

            Dim chartDate As String = $"{DateTime.Now.ToLongDateString} {DateTime.Now.ToLongTimeString}"
            Dim dateSize As SizeF = g.MeasureString(chartDate, dateFont)

            'Dim headerFont As New Font("Ariel", 14, FontStyle.Bold)
            'Dim chartTitle As String = "Chart One"
            ' Dim titleSize As SizeF = g.MeasureString(chartTitle, headerFont)

            ' set text rendering to anti-alias
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias

            If Me._PrintDoc.DefaultPageSettings.Landscape Then
                ' We cannot handle landscape printing at this time!  This requires
                ' rotating the printing not just assuming that the printer already done
                ' that.
                ' print the chart date and time at the top right
                g.DrawString(chartDate, dateFont, Brushes.Black, Me._PrintWindow.Right - dateSize.Width, Me._PrintWindow.Top - dateSize.Height)

                headerHeight = dateSize.Height

            Else
                ' print the chart date and time at the top right
                g.DrawString(chartDate, dateFont, Brushes.Black, Me._PrintWindow.Right - dateSize.Width, Me._PrintWindow.Top - dateSize.Height)
                headerHeight = dateSize.Height

            End If

        End Using

    End Sub

    ''' <summary> Prints the chart. </summary>
    ''' <remarks> Serves to handle the PrintPage event of the Print document. </remarks>
    ''' <param name="sender"> specifies the printing object sending
    '''                         the event message. </param>
    ''' <param name="e">      specifies an instance of the
    '''                         PrintPageEventArgs. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PrintChartHandler(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
        Try
            Me.OnPrintChart(e)
        Catch ex As Exception
            ' throw an exception
            System.Windows.Forms.MessageBox.Show($"{Me.Name} failed printing. {ex.ToFullBlownString}", "Exception", MessageBoxButtons.OK,
                                      MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary> Prints the chart. </summary>
    ''' <remarks> Serves to handle the PrintPage event of the Print document. </remarks>
    ''' <param name="e"> specifies an instance of the
    '''                    PrintPageEventArgs. </param>
    Protected Overridable Sub OnPrintChart(ByVal e As System.Drawing.Printing.PrintPageEventArgs)

        If e Is Nothing Then Return

        Dim headerHeight As Single

        ' print the chart header
        Me.PrintChartHeader(e.Graphics, headerHeight)

        ' print the chart
        Me.ChartControl1.ChartPane.Print(e.Graphics, Me._PrintWindow)

        ' specify that this is the last page to print
        e.HasMorePages = False

        ' restore the mouse cursor
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

#End Region

End Class
