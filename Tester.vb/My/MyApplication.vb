
Namespace My

    ''' <summary> my application. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.ProjectTraceEventId.Visuals + &HA

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Chart User Control Tester"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Chart User Control Tester"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Chart.User.Control.Tester"

    End Class

End Namespace



