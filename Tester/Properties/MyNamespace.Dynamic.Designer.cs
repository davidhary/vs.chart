﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.Visuals.Testers.My
{
    internal static partial class MyProject
    {
        internal partial class MyForms
        {
            [EditorBrowsable(EditorBrowsableState.Never)]
            public ChartControlPanel m_ChartControlPanel;

            public ChartControlPanel ChartControlPanel
            {
                [DebuggerHidden]
                get
                {
                    m_ChartControlPanel = Create__Instance__(m_ChartControlPanel);
                    return m_ChartControlPanel;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_ChartControlPanel))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_ChartControlPanel);
                }
            }

            [EditorBrowsable(EditorBrowsableState.Never)]
            public ChartPanel m_ChartPanel;

            public ChartPanel ChartPanel
            {
                [DebuggerHidden]
                get
                {
                    m_ChartPanel = Create__Instance__(m_ChartPanel);
                    return m_ChartPanel;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_ChartPanel))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_ChartPanel);
                }
            }

            [EditorBrowsable(EditorBrowsableState.Never)]
            public Console m_Console;

            public Console Console
            {
                [DebuggerHidden]
                get
                {
                    m_Console = Create__Instance__(m_Console);
                    return m_Console;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_Console))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_Console);
                }
            }

            [EditorBrowsable(EditorBrowsableState.Never)]
            public LineRecorderPanel m_LineRecorderPanel;

            public LineRecorderPanel LineRecorderPanel
            {
                [DebuggerHidden]
                get
                {
                    m_LineRecorderPanel = Create__Instance__(m_LineRecorderPanel);
                    return m_LineRecorderPanel;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_LineRecorderPanel))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_LineRecorderPanel);
                }
            }

            [EditorBrowsable(EditorBrowsableState.Never)]
            public StripChartPanel m_StripChartPanel;

            public StripChartPanel StripChartPanel
            {
                [DebuggerHidden]
                get
                {
                    m_StripChartPanel = Create__Instance__(m_StripChartPanel);
                    return m_StripChartPanel;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_StripChartPanel))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_StripChartPanel);
                }
            }

            [EditorBrowsable(EditorBrowsableState.Never)]
            public Switchboard m_Switchboard;

            public Switchboard Switchboard
            {
                [DebuggerHidden]
                get
                {
                    m_Switchboard = Create__Instance__(m_Switchboard);
                    return m_Switchboard;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_Switchboard))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_Switchboard);
                }
            }
        }
    }
}