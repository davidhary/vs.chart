﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Visuals.Testers
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class ChartPanel
    {

        // Form overrides dispose to clean up the component list.
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {

                    // Free managed resources when explicitly called
                    if (_GraphicsBuffer is object)
                    {
                        _GraphicsBuffer.Dispose();
                        _GraphicsBuffer = null;
                    }

                    if (_ChartPane is object)
                    {
                        _ChartPane.Dispose();
                        _ChartPane = null;
                    }

                    if (components is object)
                    {
                        components.Dispose();
                    }
                }
            }

            // Free shared unmanaged resources

            finally
            {

                // Invoke the base class dispose method
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            SuspendLayout();
            // 
            // ChartPanel
            // 
            ClientSize = new Size(584, 350);
            Name = "ChartPanel";
            Text = "ChartPanel";
            Closing += new System.ComponentModel.CancelEventHandler(Form_Closing);
            Load += new EventHandler(Form_Load);
            Paint += new PaintEventHandler(Form_Paint);
            Resize += new EventHandler(Form1_Resize);
            ResumeLayout(false);
        }
    }
}