﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Visuals.Testers
{
    [DesignerGenerated()]
    public partial class ChartControlPanel
    {

        // Form overrides dispose to clean up the component list.
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {

                    // Free managed resources when explicitly called
                    // onDisposeManagedResources()

                    if (components is object)
                    {
                        components.Dispose();
                    }
                }
            }

            // Free shared unmanaged resources
            // onDisposeUnManagedResources()

            finally
            {

                // Invoke the base class dispose method
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var chartPane1 = new ChartPane();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ChartControlPanel));
            var frame1 = new Frame();
            ChartControl1 = new ChartControl();
            SuspendLayout();
            // 
            // ChartControl1
            // 
            chartPane1.PaneArea = (RectangleF)resources.GetObject("ChartPane1.PaneArea");
            frame1.FillColor = Color.White;
            frame1.Filled = true;
            frame1.IsOutline = true;
            frame1.LineColor = Color.Black;
            frame1.LineWidth = 1.0f;
            frame1.StatusMessage = string.Empty;
            frame1.Visible = true;
            chartPane1.PaneFrame = frame1;
            chartPane1.Visible = true;
            ChartControl1.ChartPane = chartPane1;
            ChartControl1.Dock = DockStyle.Fill;
            ChartControl1.Location = new Point(0, 0);
            ChartControl1.Name = "ChartControl1";
            ChartControl1.Size = new Size(504, 358);
            ChartControl1.TabIndex = 1;
            // 
            // ChartControlPanel
            // 
            ClientSize = new Size(504, 358);
            Controls.Add(ChartControl1);
            Name = "ChartControlPanel";
            Text = "ChartControlPanel";
            Closing += new System.ComponentModel.CancelEventHandler(Form_Closing);
            Load += new EventHandler(Form_Load);
            ResumeLayout(false);
        }

        private ChartControl ChartControl1;
    }
}