using System;
using System.Drawing;
using System.Windows.Forms;

using isr.Visuals.Testers.ExceptionExtensions;

namespace isr.Visuals.Testers
{

    /// <summary> A chart control panel. </summary>
    /// <remarks>
    /// Launch this form by calling its Show or ShowDialog method from its default instance. <para>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved.  </para><para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2014-02-15, Documented. </para>
    /// </remarks>
    public partial class ChartControlPanel : Core.Forma.UserFormBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="ChartControlPanel" /> class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public ChartControlPanel() : base()
        {

            // Initialize user components that might be affected by resize or paint actions

            // This method is required by the Windows Form Designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call
            // instantiate a main context menu
            ContextMenu mainContextMenu;
            mainContextMenu = new ContextMenu();
            _ = mainContextMenu.MenuItems.Add( "Print", this.OnPrintMenuItem );
            this.ContextMenu = mainContextMenu;
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> The instantiated. </value>
        internal static bool Instantiated => My.MyProject.Application.OpenForms.Count > 0 &&
                                             My.MyProject.Application.OpenForms[System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name] is object;

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary> Occurs before the form is closed. </summary>
        /// <remarks>
        /// Use this method to optionally cancel the closing of the form. Because the form is not yet
        /// closed at this point, this is also the best place to serialize a form's visible properties,
        /// such as size and location. Finally, dispose of any form level objects especially those that
        /// might needs access to the form and thus should not be terminated after the form closed.
        /// </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      <see cref="System.ComponentModel.CancelEventArgs"/> </param>
        private void Form_Closing( object sender, System.ComponentModel.CancelEventArgs e )
        {

            // disable the timer if any
            // actionTimer.Enabled = False
            Application.DoEvents();

            // set module objects that reference other objects to Nothing

            this.Cursor = Cursors.WaitCursor;
            try
            {
            }
            // terminate form-level objects
            // Me.terminateObjects()
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Occurs when the form is loaded. </summary>
        /// <remarks>
        /// Use this method for doing any final initialization right before the form is shown.  This is a
        /// good place to change the Visible and ShowInTaskbar properties to start the form as hidden.
        /// Starting a form as hidden is useful for forms that need to be running but that should not
        /// show themselves right away, such as forms with a notify icon in the task bar.
        /// </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      <see cref="System.EventArgs"/> </param>
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;

                // instantiate form objects
                // Me.instantiateObjects()

                // set the form caption
                this.Text = My.MyApplication.Appliance.Info.BuildDefaultCaption( ": CHART CONTROL PANEL" );

                // set tool tips
                // initializeUserInterface()

                // center the form
                this.CenterToScreen();

                // Create the chart
                this.ChartControl1.ChartPane.CreateSampleOne( this.ChartControl1.ClientRectangle );
            }

            // turn on the loaded flag
            // loaded = true

            catch
            {

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " PRINTING "

        /// <summary> The print window area includes all the drawing objects. </summary>
        private RectangleF _PrintWindow = new RectangleF( 100f, 200f, 650f, 400f );

        /// <summary> The print document handles the printing. </summary>
        private System.Drawing.Printing.PrintDocument _PrintDoc;

        /// <summary> Handles the print menu item delegate. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Reference to a <see cref="System.EventArgs"></see> </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OnPrintMenuItem( object sender, EventArgs e )
        {
            try
            {
                this.PrintChart();
            }
            catch ( Exception ex )
            {
                _ = MessageBox.Show( $"{this.Name} failed printing. {ex.ToFullBlownString()}.", "Exception", MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
            }
        }

        /// <summary> Prints the chart. </summary>
        /// <remarks> Use this method to print the strip chart. </remarks>
        private void PrintChart()
        {

            // instantiate the print document object to handle the printing
            this._PrintDoc = new System.Drawing.Printing.PrintDocument();

            // add handler to handle printing
            this._PrintDoc.PrintPage += this.PrintChartHandler;

            // set the cursor to wait
            this.Cursor = Cursors.WaitCursor;

            // do the printing
            this._PrintDoc.Print();
        }

        /// <summary> Prints the chart header. </summary>
        /// <remarks> Prints the chart header. </remarks>
        /// <param name="g">            specifies the graphics context for
        /// printing. </param>
        /// <param name="headerHeight"> [in,out] is a Single value returning the height of the header. </param>
        private void PrintChartHeader( Graphics g, ref float headerHeight )
        {
            using var dateFont = new Font( "Ariel", 10f, FontStyle.Regular );
            string chartDate = $"{DateTime.Now.ToLongDateString()} {DateTime.Now.ToLongTimeString()}";
            var dateSize = g.MeasureString( chartDate, dateFont );

            // Dim headerFont As New Font("Ariel", 14, FontStyle.Bold)
            // Dim chartTitle As String = "Chart One"
            // Dim titleSize As SizeF = g.MeasureString(chartTitle, headerFont)

            // set text rendering to anti-alias
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            if ( this._PrintDoc.DefaultPageSettings.Landscape )
            {
                // We cannot handle landscape printing at this time!  This requires
                // rotating the printing not just assuming that the printer already done
                // that.
                // print the chart date and time at the top right
                g.DrawString( chartDate, dateFont, Brushes.Black, this._PrintWindow.Right - dateSize.Width, this._PrintWindow.Top - dateSize.Height );
                headerHeight = dateSize.Height;
            }
            else
            {
                // print the chart date and time at the top right
                g.DrawString( chartDate, dateFont, Brushes.Black, this._PrintWindow.Right - dateSize.Width, this._PrintWindow.Top - dateSize.Height );
                headerHeight = dateSize.Height;
            }
        }

        /// <summary> Prints the chart. </summary>
        /// <remarks> Serves to handle the PrintPage event of the Print document. </remarks>
        /// <param name="sender"> specifies the printing object sending
        /// the event message. </param>
        /// <param name="e">      specifies an instance of the
        /// PrintPageEventArgs. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void PrintChartHandler( object sender, System.Drawing.Printing.PrintPageEventArgs e )
        {
            try
            {
                this.OnPrintChart( e );
            }
            catch ( Exception ex )
            {
                // throw an exception
                _ = MessageBox.Show( $"{this.Name} failed printing. {ex.ToFullBlownString()}", "Exception", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
            }
        }

        /// <summary> Prints the chart. </summary>
        /// <remarks> Serves to handle the PrintPage event of the Print document. </remarks>
        /// <param name="e"> specifies an instance of the
        /// PrintPageEventArgs. </param>
        protected virtual void OnPrintChart( System.Drawing.Printing.PrintPageEventArgs e )
        {
            if ( e is null )
                return;
            var headerHeight = default( float );

            // print the chart header
            this.PrintChartHeader( e.Graphics, ref headerHeight );

            // print the chart
            this.ChartControl1.ChartPane.Print( e.Graphics, this._PrintWindow );

            // specify that this is the last page to print
            e.HasMorePages = false;

            // restore the mouse cursor
            this.Cursor = Cursors.Default;
        }

        #endregion

    }
}
