/// <summary> A tester for the line recorder control. </summary>
/// <remarks>
/// Launch this form by calling its Show or ShowDialog method from its default instance. <para>
/// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved.  </para><para>
/// Licensed under The MIT License. </para><para>
/// David, 2014-02-15, Documented. </para>
/// </remarks>
using System;
using System.Windows.Forms;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Visuals.Testers
#pragma warning restore IDE1006 // Naming Styles
{
    public partial class LineRecorderPanel : Core.Forma.UserFormBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public LineRecorderPanel() : base()
        {

            // Initialize user components that might be affected by resize or paint actions

            // This method is required by the Windows Form Designer.
            this.InitializeComponent();
            this.__SpeedBar.Name = "_SpeedBar";
            this.__AmplitudeBar.Name = "_AmplitudeBar";
            this.__IntervalNumericUpDown.Name = "_IntervalNumericUpDown";

            // Add any initialization after the InitializeComponent() call

        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> The instantiated. </value>
        internal static bool Instantiated => My.MyProject.Application.OpenForms.Count > 0 && My.MyProject.Application.OpenForms[System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name] is object;

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary> Occurs before the form is closed. </summary>
        /// <remarks>
        /// Use this method to optionally cancel the closing of the form. Because the form is not yet
        /// closed at this point, this is also the best place to serialize a form's visible properties,
        /// such as size and location. Finally, dispose of any form level objects especially those that
        /// might needs access to the form and thus should not be terminated after the form closed.
        /// </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      <see cref="System.ComponentModel.CancelEventArgs"/> </param>
        private void Form_Closing( object sender, System.ComponentModel.CancelEventArgs e )
        {

            // disable the timer if any
            // actionTimer.Enabled = False
            Application.DoEvents();

            // set module objects that reference other objects to Nothing

            this.Cursor = Cursors.WaitCursor;
            try
            {
            }
            // terminate form-level objects
            // Me.terminateObjects()
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Occurs when the form is loaded. </summary>
        /// <remarks>
        /// Use this method for doing any final initialization right before the form is shown.  This is a
        /// good place to change the Visible and ShowInTaskbar properties to start the form as hidden.
        /// Starting a form as hidden is useful for forms that need to be running but that should not
        /// show themselves right away, such as forms with a notify icon in the task bar.
        /// </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      <see cref="System.EventArgs"/> </param>
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;

                // instantiate form objects
                // Me.instantiateObjects()

                // set the form caption
                this.Text = My.MyApplication.Appliance.Info.BuildDefaultCaption( ": LINE RECORDER PANEL" );

                // Initialize and set the user interface
                // initializeUserInterface()
                this._LineRecorder.Play();

                // center the form
                this.CenterToScreen();
            }

            // turn on the loaded flag
            // loaded = True

            catch
            {

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Amplitude bar value changed. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        private void AmplitudeBar_ValueChanged( object sender, EventArgs e )
        {
            this._LineRecorder.Amplitude = this._AmplitudeBar.Value;
            this._ToolTip.SetToolTip( this._AmplitudeBar, this._AmplitudeBar.Value.ToString( System.Globalization.CultureInfo.CurrentCulture ) );
        }

        /// <summary> Speed bar scroll. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        private void SpeedBar_Scroll( object sender, EventArgs e )
        {
            this._LineRecorder.RefreshInterval = this._SpeedBar.Value;
            this._ToolTip.SetToolTip( this._SpeedBar, this._SpeedBar.Value.ToString( System.Globalization.CultureInfo.CurrentCulture ) );
        }

        #endregion

        /// <summary> Interval numeric up down value changed. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        private void IntervalNumericUpDown_ValueChanged( object sender, EventArgs e )
        {
            this._LineRecorder.RefreshInterval = ( int ) this._IntervalNumericUpDown.Value;
        }
    }
}
