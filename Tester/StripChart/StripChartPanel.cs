
/// <summary>
/// Tests the strip chart control using a run-time created chart with random amplitude.
/// </summary>
/// <remarks>
/// Launch this form by calling its Show or ShowDialog method from its default instance. <para>
/// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved.  </para><para>
/// Licensed under The MIT License. </para><para>
/// David, 2014-02-15, Documented. </para>
/// </remarks>
using System;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Visuals.Testers
{
    public partial class StripChartPanel : Core.Forma.UserFormBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="StripChartPanel" /> class. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public StripChartPanel() : base()
        {

            // Initialize user components that might be affected by resize or paint actions
            this.CreateChart();

            // This method is required by the Windows Form Designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call
            // instantiate the action timer
            this.StripChartTimer = new Timer() { Enabled = false, Interval = 50 };
            this.StripChartTimer.Tick += this.OnTimerTick;
        }

        /// <summary> Creates the chart. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private void CreateChart()
        {
            this._GraphicsBuffer = new BufferedGraphics( this.ClientRectangle.Width, this.ClientRectangle.Height );
            this._ChartPane = new StripChartPane();
            this._ChartPane.CreateSampleOne( this.ClientRectangle );
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> The instantiated. </value>
        internal static bool Instantiated => My.MyProject.Application.OpenForms.Count > 0 && My.MyProject.Application.OpenForms[System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name] is object;

        #endregion

        #region " PROPERTIES "

        /// <summary> Buffer for graphics data. </summary>
        private BufferedGraphics _GraphicsBuffer;

        /// <summary> The chart pane. </summary>
        private StripChartPane _ChartPane;

        /// <summary> The strip chart timer. </summary>
        internal Timer StripChartTimer { get; set; }

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary> Occurs before the form is closed. </summary>
        /// <remarks>
        /// Use this method to optionally cancel the closing of the form. Because the form is not yet
        /// closed at this point, this is also the best place to serialize a form's visible properties,
        /// such as size and location. Finally, dispose of any form level objects especially those that
        /// might needs access to the form and thus should not be terminated after the form closed.
        /// </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      <see cref="System.ComponentModel.CancelEventArgs"/> </param>
        private void Form_Closing( object sender, System.ComponentModel.CancelEventArgs e )
        {

            // disable the timer if any
            if ( this.StripChartTimer is object )
            {
                this.StripChartTimer.Enabled = false;
            }

            // actionTimer.Enabled = False
            Application.DoEvents();

            // set module objects that reference other objects to Nothing

            this.Cursor = Cursors.WaitCursor;
            try
            {
            }
            // terminate form-level objects
            // Me.terminateObjects()
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Occurs when the form is loaded. </summary>
        /// <remarks>
        /// Use this method for doing any final initialization right before the form is shown. This is a
        /// good place to change the Visible and ShowInTaskbar properties to start the form as hidden.
        /// Starting a form as hidden is useful for forms that need to be running but that should not
        /// show themselves right away, such as forms with a notify icon in the task bar.
        /// </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      <see cref="System.EventArgs"/> </param>
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;

                // instantiate form objects
                // Me.instantiateObjects()

                // set the form caption
                this.Text = My.MyApplication.Appliance.Info.BuildDefaultCaption( ": STRIP CHART PANEL" );

                // set tool tips
                // initializeUserInterface()

                // center the form
                this.CenterToScreen();

                // enable the timer
                this.StripChartTimer.Enabled = true;

                // required for C#
                this._ChartPane.SetSize( this.ClientRectangle );
            }

            // turn on the loaded flag
            // loaded = True

            catch
            {

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Occurs when the form is redrawn. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      <see cref="System.Windows.Forms.PaintEventArgs"/> </param>
        private void Form_Paint( object sender, PaintEventArgs e )
        {
            if ( this._GraphicsBuffer.CanDoubleBuffer() )
            {

                // clear the client area
                using ( var sb = new SolidBrush( Color.Gray ) )
                {
                    this._GraphicsBuffer.GraphicsDevice.FillRectangle( sb, this.ClientRectangle );
                }

                // drawing using Me._graphicsBuffer.g instead e.Graphics
                this._ChartPane.Draw( this._GraphicsBuffer.GraphicsDevice );

                // Render to the form
                this._GraphicsBuffer.Render( e.Graphics );
            }
            else
            {

                // if double buffer is not available, draw to e.Graphics
                using ( var sb = new SolidBrush( Color.Gray ) )
                {
                    e.Graphics.FillRectangle( sb, this.ClientRectangle );
                }

                this._ChartPane.Draw( e.Graphics );
            }
        }

        /// <summary>
        /// Paints the background of the control.  The OnPaintBackground method allows derived classes to
        /// handle the event without attaching a delegate. This is the preferred technique for handling
        /// the event in a derived class.  Inheriting classes should override this method to handle the
        /// erase background request from windows. When overriding OnPaintBackground in a derived class
        /// it is not necessary to call the base class's OnPaintBackground method.  By overriding this
        /// method, the paint event of the parent class is disabled and handled by the double buffering
        /// method, which smooths the refreshing of the chart.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="pevent"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaintBackground( PaintEventArgs pevent )
        {
        } // OnPaintBackground

        /// <summary> Occurs when the form is resized. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      <see cref="System.EventArgs"/> </param>
        private void Form_Resize( object sender, EventArgs e )
        {
            _ = this._GraphicsBuffer.CreateDoubleBuffer( this.ClientRectangle.Width, this.ClientRectangle.Height );
            this._ChartPane.SetSize( this.ClientRectangle );
            this.Invalidate();
        }

        #endregion

        #region " EVENT HANDLERS "

        /// <summary> The last value. </summary>
        private double _LastValue;

        /// <summary> Handles the timer event. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Reference to a <see cref="System.EventArgs"></see> </param>
        private void OnTimerTick( object sender, EventArgs e )
        {
            var randomValue = new Random();

            // stop the timer
            this.StripChartTimer.Enabled = false;

            // add a new data point
            this._LastValue = 0.9d * this._LastValue + 0.1d * randomValue.NextDouble();
            this._ChartPane.AddDataPoint( DateTimeOffset.Now.DateTime, 0.1d * this._LastValue );

            // refresh the chart
            this.Invalidate();

            // re-able the timer
            this.StripChartTimer.Enabled = true;
        }

        #endregion

    }
}
