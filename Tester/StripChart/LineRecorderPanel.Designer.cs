﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Visuals.Testers
{
    [DesignerGenerated()]
    public partial class LineRecorderPanel
    {

        // Form overrides dispose to clean up the component list.
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {

                    // Free managed resources when explicitly called

                    if (components is object)
                    {
                        components.Dispose();
                    }
                }
            }

            // Free shared unmanaged resources

            finally
            {

                // Invoke the base class dispose method
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _LineRecorder = new LineRecorder();
            __SpeedBar = new TrackBar();
            __SpeedBar.Scroll += new EventHandler(SpeedBar_Scroll);
            __AmplitudeBar = new TrackBar();
            __AmplitudeBar.ValueChanged += new EventHandler(AmplitudeBar_ValueChanged);
            _ToolTip = new ToolTip(components);
            _Layout = new TableLayoutPanel();
            __IntervalNumericUpDown = new NumericUpDown();
            __IntervalNumericUpDown.ValueChanged += new EventHandler(IntervalNumericUpDown_ValueChanged);
            ((System.ComponentModel.ISupportInitialize)__SpeedBar).BeginInit();
            ((System.ComponentModel.ISupportInitialize)__AmplitudeBar).BeginInit();
            _Layout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)__IntervalNumericUpDown).BeginInit();
            SuspendLayout();
            // 
            // _LineRecorder
            // 
            _LineRecorder.BackColor = SystemColors.WindowText;
            _LineRecorder.Dock = DockStyle.Fill;
            _LineRecorder.Location = new Point(61, 10);
            _LineRecorder.Margin = new Padding(10);
            _LineRecorder.Name = "_LineRecorder";
            _LineRecorder.Size = new Size(677, 302);
            _LineRecorder.TabIndex = 3;
            // 
            // _SpeedBar
            // 
            __SpeedBar.Dock = DockStyle.Bottom;
            __SpeedBar.Location = new Point(54, 325);
            __SpeedBar.Maximum = 1000;
            __SpeedBar.Minimum = 10;
            __SpeedBar.Name = "__SpeedBar";
            __SpeedBar.Size = new Size(691, 45);
            __SpeedBar.TabIndex = 5;
            __SpeedBar.TickFrequency = 100;
            __SpeedBar.Value = 1000;
            // 
            // _AmplitudeBar
            // 
            __AmplitudeBar.Dock = DockStyle.Left;
            __AmplitudeBar.Location = new Point(3, 3);
            __AmplitudeBar.Maximum = 100;
            __AmplitudeBar.Name = "__AmplitudeBar";
            __AmplitudeBar.Orientation = Orientation.Vertical;
            __AmplitudeBar.Size = new Size(45, 316);
            __AmplitudeBar.TabIndex = 4;
            __AmplitudeBar.TickFrequency = 10;
            __AmplitudeBar.Value = 50;
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 2;
            _Layout.ColumnStyles.Add(new ColumnStyle());
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            _Layout.Controls.Add(_LineRecorder, 1, 0);
            _Layout.Controls.Add(__SpeedBar, 1, 1);
            _Layout.Controls.Add(__AmplitudeBar, 0, 0);
            _Layout.Controls.Add(__IntervalNumericUpDown, 0, 1);
            _Layout.Dock = DockStyle.Fill;
            _Layout.Location = new Point(0, 0);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 2;
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.Size = new Size(748, 373);
            _Layout.TabIndex = 6;
            // 
            // _IntervalNumericUpDown
            // 
            __IntervalNumericUpDown.Anchor = AnchorStyles.None;
            __IntervalNumericUpDown.Font = new Font(Font, FontStyle.Bold);
            __IntervalNumericUpDown.Increment = new decimal(new int[] { 10, 0, 0, 0 });
            __IntervalNumericUpDown.Location = new Point(3, 337);
            __IntervalNumericUpDown.Maximum = new decimal(new int[] { 1000, 0, 0, 0 });
            __IntervalNumericUpDown.Minimum = new decimal(new int[] { 10, 0, 0, 0 });
            __IntervalNumericUpDown.Name = "__IntervalNumericUpDown";
            __IntervalNumericUpDown.Size = new Size(45, 20);
            __IntervalNumericUpDown.TabIndex = 6;
            __IntervalNumericUpDown.Value = new decimal(new int[] { 1000, 0, 0, 0 });
            // 
            // LineRecorderPanel
            // 
            ClientSize = new Size(748, 373);
            Controls.Add(_Layout);
            Name = "LineRecorderPanel";
            Text = "Line Recorder Test Panel";
            ((System.ComponentModel.ISupportInitialize)__SpeedBar).EndInit();
            ((System.ComponentModel.ISupportInitialize)__AmplitudeBar).EndInit();
            _Layout.ResumeLayout(false);
            _Layout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)__IntervalNumericUpDown).EndInit();
            Closing += new System.ComponentModel.CancelEventHandler(Form_Closing);
            Load += new EventHandler(Form_Load);
            ResumeLayout(false);
        }

        private LineRecorder _LineRecorder;
        private TrackBar __SpeedBar;

        private TrackBar _SpeedBar
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SpeedBar;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SpeedBar != null)
                {
                    __SpeedBar.Scroll -= SpeedBar_Scroll;
                }

                __SpeedBar = value;
                if (__SpeedBar != null)
                {
                    __SpeedBar.Scroll += SpeedBar_Scroll;
                }
            }
        }

        private TrackBar __AmplitudeBar;

        private TrackBar _AmplitudeBar
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AmplitudeBar;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AmplitudeBar != null)
                {
                    __AmplitudeBar.ValueChanged -= AmplitudeBar_ValueChanged;
                }

                __AmplitudeBar = value;
                if (__AmplitudeBar != null)
                {
                    __AmplitudeBar.ValueChanged += AmplitudeBar_ValueChanged;
                }
            }
        }

        private ToolTip _ToolTip;
        private TableLayoutPanel _Layout;
        private NumericUpDown __IntervalNumericUpDown;

        private NumericUpDown _IntervalNumericUpDown
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __IntervalNumericUpDown;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__IntervalNumericUpDown != null)
                {
                    __IntervalNumericUpDown.ValueChanged -= IntervalNumericUpDown_ValueChanged;
                }

                __IntervalNumericUpDown = value;
                if (__IntervalNumericUpDown != null)
                {
                    __IntervalNumericUpDown.ValueChanged += IntervalNumericUpDown_ValueChanged;
                }
            }
        }
    }
}