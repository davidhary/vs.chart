﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Visuals.Testers
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class Console
    {

        // Form overrides dispose to clean up the component list.
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components is object)
                    {
                        components.Dispose();
                    }

                    if (_GraphicsBuffer is object)
                    {
                        _GraphicsBuffer.Dispose();
                        _GraphicsBuffer = null;
                    }

                    if (_ChartPane is object)
                    {
                        _ChartPane.Dispose();
                        _ChartPane = null;
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            SuspendLayout();
            // 
            // Form1
            // 
            ClientSize = new Size(448, 358);
            Name = "Form1";
            Text = "Form1";
            Paint += new PaintEventHandler(Form1_Paint);
            Resize += new EventHandler(Form1_Resize);
            ResumeLayout(false);
        }
    }
}