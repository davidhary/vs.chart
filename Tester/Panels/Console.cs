using System;
using System.Drawing;
using System.Windows.Forms;

namespace isr.Visuals.Testers
{

    /// <summary> Test console. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2014-02-15, Documented. </para>
    /// </remarks>
    public partial class Console : Core.Forma.UserFormBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public Console()
        {
            // 
            // Required for Windows Form Designer support
            this.CreateChart();
            this.InitializeComponent();
            // Me._graphicsBuffer = New BufferedGraphics()
        } // New

        #endregion

        /// <summary> Buffer for graphics data. </summary>
        private BufferedGraphics _GraphicsBuffer;

        /// <summary> The chart pane. </summary>
        private ChartPane _ChartPane;

        /// <summary> Creates the chart. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private void CreateChart()
        {
            this._GraphicsBuffer = new BufferedGraphics( this.ClientRectangle.Width, this.ClientRectangle.Height );
            _ = this._GraphicsBuffer.CreateDoubleBuffer( this.ClientRectangle.Width, this.ClientRectangle.Height );
            this._ChartPane = new ChartPane();
            this._ChartPane.CreateSampleOne( this.ClientRectangle );
        }

        /// <summary> Paints the background of the control. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="pevent"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnPaintBackground( PaintEventArgs pevent )
        {
        } // OnPaintBackground

        /// <summary> Form 1 paint. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Paint event information. </param>
        private void Form1_Paint( object sender, PaintEventArgs e )
        {
            if ( this._GraphicsBuffer.CanDoubleBuffer() )
            {

                // Fill in Background (for efficiency only the area that has been clipped)
                using ( var SB = new SolidBrush( SystemColors.Window ) )
                {
                    this._GraphicsBuffer.GraphicsDevice.FillRectangle( SB, e.ClipRectangle.X, e.ClipRectangle.Y, e.ClipRectangle.Width, e.ClipRectangle.Height );
                }

                // clear the client area
                using ( var SB = new SolidBrush( SystemColors.Window ) )
                {
                    this._GraphicsBuffer.GraphicsDevice.FillRectangle( SB, this.ClientRectangle );
                }

                // Drawing using Me._graphicsBuffer.g 
                this._ChartPane.Draw( this._GraphicsBuffer.GraphicsDevice );

                // Render to the form
                this._GraphicsBuffer.Render( e.Graphics );
            }

            // if double buffer is not available, do without it
            else
            {
                // Drawing using e.Graphics

                // clear
                using ( var SB = new SolidBrush( Color.Gray ) )
                {
                    e.Graphics.FillRectangle( SB, this.ClientRectangle );
                }

                // draw
                this._ChartPane.Draw( e.Graphics );
            }
        } // Form1_Paint

        /// <summary> Form 1 resize. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Form1_Resize( object sender, EventArgs e )
        {
            _ = this._GraphicsBuffer.CreateDoubleBuffer( this.ClientRectangle.Width, this.ClientRectangle.Height );
            this.SetSize();
            this.Invalidate();
        } // Form1_Resize

        /// <summary> Sets the size. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private void SetSize()
        {
            RectangleF paneArea = this.ClientRectangle;
            paneArea.Inflate( -10, -10 );
            this._ChartPane.PaneArea = paneArea;
        } // SetSize
    }
}
