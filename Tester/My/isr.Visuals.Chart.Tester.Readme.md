## ISR Visuals Chart Libraries Tester<sub>&trade;</sub>
### Revision History

*3.0.6166 2016-11-18*  
Uses new core libraries and VS 2015.

*2.2.4707 2012-11-20*  
Converted to VS10.

*2.1.4232 2011-08-03*  
Standardize code elements and documentation.

*2.1.4213 2011-07-15*  
Simplifies the assembly information.

*2.1.2961 2008-02-09*  
Update to .NET 3.5.

*2.0.2789 2007-08-21*  
Update to Visual Studio 8.

*1.0.2219 2006-01-28*  
Remove Visual Basic import.

*1.0.2206 2006-01-15*  
New support and exceptions libraries. Use INt32,
Int64, and Int16 instead of Integer, Long, and Short.

*1.0.2034 2005-07-27 Add line recorder and a new graphics double buffer.
The double buffer slows down the line recorder significantly.

*1.0.1615 2004-06-03*  
Add a print context menu to chart control pane and use
to print the chart..

*1.0.1581 2004-04-30*  
New library.

\(C\) 2004 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Chart Libraries](https://bitbucket.org/davidhary/vs.chart):
