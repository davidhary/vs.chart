''' <summary> Handles the drawing of the strip chart pen <see cref="Stylus"/>. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public Class Stylus

    Implements ICloneable

#Region " TYPES "

#End Region

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a <see cref="Stylus"/> with default values as defined in the
    ''' <see cref="StylusDefaults"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
    Public Sub New(ByVal drawingPane As Pane)

        MyBase.New()
        If drawingPane Is Nothing Then
            Throw New ArgumentNullException(NameOf(drawingPane))
        End If

        Dim stylusDefaults As StylusDefaults = StylusDefaults.Get()
        Me._Size = stylusDefaults.Size
        Me._LineWidth = stylusDefaults.LineWidth
        Me._LineColor = stylusDefaults.LineColor
        Me._Visible = stylusDefaults.Visible
        Me._Filled = stylusDefaults.Filled
        Me._Pane = drawingPane

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The Stylus object from which to copy. </param>
    Public Sub New(ByVal model As Stylus)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Me._Size = model._Size
        Me._LineWidth = model._LineWidth
        Me._LineColor = model._LineColor
        Me._Visible = model._Visible
        Me._Filled = model._Filled
        Me._Pane = model._Pane

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the Stylus. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the Stylus. </returns>
    Public Function Copy() As Stylus
        Return New Stylus(Me)
    End Function

    ''' <summary>
    ''' Draw the <see cref="Stylus"/> to the specified <see cref="graphics"/> device at the specified
    ''' location.  This method draws a single Stylus.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="y">              The y position of the tip of the Stylus in screen pixels. </param>
    ''' <param name="axisArea">       The rectangle that contains the area bounded by the axes, in
    '''                               pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso> </param>
    ''' <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
    '''                               <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                               calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                               The scale factor is applied to fonts, symbols, etc. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal y As Single,
                    ByVal axisArea As RectangleF, ByVal scaleFactor As Double)

        ' Only draw if the Stylus is visible
        If Me._Visible Then

            ' validate argument.
            If graphicsDevice Is Nothing Then
                Throw New ArgumentNullException(NameOf(graphicsDevice))
            End If

            Dim scaledSize As Single = Convert.ToSingle(Me.Size * scaleFactor)
            Dim x As Single = axisArea.X + axisArea.Width

            Using sb As New SolidBrush(Color.Black)
                ' draw the stylus background
                graphicsDevice.FillRectangle(sb, x, axisArea.Y - scaledSize / 2,
                                             scaledSize, axisArea.Height + scaledSize)
            End Using

            Using brush As New SolidBrush(Me._LineColor)
                Using pen As New Pen(Me._LineColor, Me._LineWidth)
                    ' Fill or draw the Stylus as required
                    If Me._Filled Then
                        Me.Fill(graphicsDevice, x, y, scaleFactor, brush)
                    Else
                        Me.Outline(graphicsDevice, x, y, scaleFactor, pen)
                    End If
                End Using
            End Using
        End If

    End Sub

    ''' <summary>
    ''' Draw the <see cref="Symbol"/> (outline only) to the specified
    ''' <see cref="graphics"/> device at the specified location.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="x">              The x position of the center of the symbol in screen pixels. </param>
    ''' <param name="y">              The y position of the center of the symbol in screen pixels. </param>
    ''' <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
    '''                               <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                               calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                               The scale factor is applied to fonts, symbols, etc. </param>
    ''' <param name="pen">            A pen with attributes of <see cref="Color"/> and
    '''                               <see cref="LineWidth"/> for this symbol. </param>
    Private Sub Outline(ByVal graphicsDevice As Graphics, ByVal x As Single, ByVal y As Single,
                        ByVal scaleFactor As Double, ByVal pen As Pen)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        graphicsDevice.DrawPolygon(pen, Me.GetStylusPolygon(x, y, scaleFactor))

    End Sub

    ''' <summary>
    ''' Renders the filled <see cref="Stylus"/> to the specified <see cref="graphics"/>
    ''' device at the specified location.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="x">              The x position of the center of the Stylus in screen pixels. </param>
    ''' <param name="y">              The y position of the center of the Stylus in screen pixels. </param>
    ''' <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
    '''                               <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                               calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                               The scale factor is applied to fonts, symbols, etc. </param>
    ''' <param name="brush">          A brush with the <see cref="Color"/> attribute for this Stylus. </param>
    Private Sub Fill(ByVal graphicsDevice As Graphics, ByVal x As Single, ByVal y As Single,
                     ByVal scaleFactor As Double, ByVal brush As Brush)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        graphicsDevice.FillPolygon(brush, Me.GetStylusPolygon(x, y, scaleFactor))

    End Sub

    ''' <summary> Returns the pen stylus polygon. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x">           The x position of the center of the Stylus in screen pixels. </param>
    ''' <param name="y">           The y position of the center of the Stylus in screen pixels. </param>
    ''' <param name="scaleFactor"> The scaling factor for the chart with reference to the chart
    '''                            <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                            calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                            The scale factor is applied to fonts, symbols, etc. </param>
    ''' <returns> The stylus polygon. </returns>
    Private Function GetStylusPolygon(ByVal x As Single, ByVal y As Single, ByVal scaleFactor As Double) As PointF()

        Dim scaledSize As Single = Convert.ToSingle(Me._Size * scaleFactor)
        Dim hsize As Single = scaledSize / 2
        Dim polyPt(3) As PointF

        polyPt(0).X = x
        polyPt(0).Y = y
        polyPt(1).X = x + scaledSize
        polyPt(1).Y = y + hsize
        polyPt(2).X = polyPt(1).X
        polyPt(2).Y = y - hsize
        polyPt(3) = polyPt(0)
        Return polyPt

    End Function

    ''' <summary>
    ''' Adjusts the <see cref="Pane.AxisArea"/> for the
    ''' <see cref="isr.Visuals.Stylus">Stylus</see> size.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="scaleFactor"> The scaling factor for the chart with reference to the chart
    '''                            <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                            calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                            The scale factor is applied to fonts, symbols, etc. </param>
    ''' <param name="axisArea">    The rectangle that contains the area bounded by the axes, in
    '''                            pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso> </param>
    ''' <returns> Adjusted <see cref="Pane.AxisArea"/> </returns>
    Public Function GetAxisArea(ByVal scaleFactor As Double, ByVal axisArea As RectangleF) As RectangleF

        ' Leave room for the stylus
        If Me._Visible Then

            ' Leave room for the stylus 
            axisArea.Width -= Convert.ToSingle(Me.Size * scaleFactor)

        End If

        ' return adjusted axis rectangle
        Return axisArea

    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Gets or sets the filled mode of the <see cref="Stylus"/> shape.  Set to True to fill the
    ''' shape with color, or False for an outline Stylus.  Note that symbols that are not closed,
    ''' such as <see cref="isr.Visuals.ShapeType.Plus"/>
    ''' cannot be filled.
    ''' </summary>
    ''' <value> The filled. </value>
    Public Property Filled() As Boolean

    ''' <summary> Gets or sets a property that shows or hides the <see cref="Stylus"/>. </summary>
    ''' <value> True to show the Stylus, False to hide it. </value>
    Public Property Visible() As Boolean

    ''' <summary> Gets or sets the pen width for drawing the <see cref="Stylus"/> outline. </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property LineWidth() As Single

    ''' <summary> Gets or sets the line color of the <see cref="Stylus"/> </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property LineColor() As Color

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
    Private ReadOnly _Pane As Pane

    ''' <summary> Gets or sets the size of the <see cref="Stylus"/> </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property Size() As Single

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> A System.String value. </value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="Stylus"/> class that defines the default property values
''' for the <see cref="isr.Visuals.Stylus"/> class.
''' </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public NotInheritable Class StylusDefaults

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs this class. This constructor is private to ensure only a single instance of this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub New()
        MyBase.New()
        Me._Filled = True
        Me._Visible = True
        Me._LineColor = Color.Red
        Me._LineWidth = 1.0F
        Me._Size = 8
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _Instance As StylusDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As StylusDefaults
        If StylusDefaults._Instance Is Nothing Then
            SyncLock StylusDefaults.SyncLocker
                StylusDefaults._Instance = New StylusDefaults()
            End SyncLock
        End If
        Return StylusDefaults._Instance
    End Function

#End Region

    ''' <summary>
    ''' Gets or sets the default fill mode for symbols (<see cref="Stylus.Filled"/> property). true
    ''' to have symbols filled in with color, false to leave them as outlines.
    ''' </summary>
    ''' <value> The filled. </value>
    Public Property Filled() As Boolean

    ''' <summary>
    ''' Gets or sets the default display mode for symbols (<see cref="Stylus.Visible"/> property).
    ''' true to display symbols, false to hide them.
    ''' </summary>
    ''' <value> The visible. </value>
    Public Property Visible() As Boolean

    ''' <summary>
    ''' Gets or sets the default color for drawing symbols (<see cref="Stylus.LineColor"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property LineColor() As Color

    ''' <summary>
    ''' Gets or sets the default pen width to be used for drawing curve symbols
    ''' (<see cref="Stylus.LineWidth"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property LineWidth() As Single

    ''' <summary>
    ''' Gets or sets the default size for curve symbols (<see cref="Stylus.Size"/>.
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property Size() As Single

End Class
#End Region

