''' <summary>
''' A class representing all the characteristics of the <see cref="Cord"/>
''' segments that make up a curve on the graph.
''' </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public Class Cord
    Implements ICloneable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a <see cref="Cord"/> with default property values values as defined in the
    ''' <see cref="CordDefaults"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()

        MyBase.New()

        Dim cordDefaults As CordDefaults = CordDefaults.Get()
        Me._LineWidth = cordDefaults.LineWidth
        Me._DashStyle = cordDefaults.DashStyle
        Me._Visible = cordDefaults.Visible
        Me._LineColor = cordDefaults.LineColor
        Me._CordType = cordDefaults.CordType

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The Cord object from which to copy. </param>
    Public Sub New(ByVal model As Cord)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Me._LineWidth = model._LineWidth
        Me._DashStyle = model._DashStyle
        Me._Visible = model._Visible
        Me._LineColor = model._LineColor
        Me._CordType = model.CordType
    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the Cord. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the Cord. </returns>
    Public Function Copy() As Cord
        Return New Cord(Me)
    End Function

    ''' <summary>
    ''' Renders a single <see cref="Cord"/> segment to the specified
    ''' <see cref="graphics"/> device.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="x1">             The x position of the starting point that defines the line
    '''                               segment in screen pixels. </param>
    ''' <param name="y1">             The y position of the starting point that defines the line
    '''                               segment in screen pixels. </param>
    ''' <param name="x2">             The x position of the ending point that defines the line segment
    '''                               in screen pixels. </param>
    ''' <param name="y2">             The y position of the ending point that defines the line segment
    '''                               in screen pixels. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal x1 As Single, ByVal y1 As Single,
                    ByVal x2 As Single, ByVal y2 As Single)

        If Not Me._Visible Then Return

        ' validate argument.
        If graphicsDevice Is Nothing Then Throw New ArgumentNullException(NameOf(graphicsDevice))

        Using pen As New Pen(Me._LineColor, Me._LineWidth)
            pen.DashStyle = Me._DashStyle
            graphicsDevice.DrawLine(pen, x1, y1, x2, y2)
        End Using

    End Sub

    ''' <summary>
    ''' Renders a series of <see cref="Cord"/> segments to the specified
    ''' <see cref="graphics"/> device.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="x">              The array of x position values that define the line segments in
    '''                               screen pixels. </param>
    ''' <param name="y">              The array of y position values that define the line segments in
    '''                               screen pixels. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal x() As Single, ByVal y() As Single)

        If Not Me._Visible Then
            Return
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If
        If x Is Nothing Then
            Throw New ArgumentNullException(NameOf(x))
        End If
        If y Is Nothing Then
            Throw New ArgumentNullException(NameOf(y))
        End If

        Using pen As New Pen(Me._LineColor, Me._LineWidth)
            pen.DashStyle = Me._DashStyle
            Dim nSeg As Integer = x.Length - 1
            Dim iPlus As Integer = 0

            For i As Integer = 0 To nSeg - 1
                iPlus += 1
                If x(i) <> System.Single.MaxValue AndAlso x(iPlus) <> System.Single.MaxValue AndAlso y(i) <> System.Single.MaxValue AndAlso y(iPlus) <> System.Single.MaxValue Then
                    Select Case Me.CordType
                        Case CordType.HoldStep
                            graphicsDevice.DrawLine(pen, x(i), y(i), x(i), y(iPlus))
                            graphicsDevice.DrawLine(pen, x(i), y(iPlus), x(iPlus), y(iPlus))
                        Case CordType.Linear
                            graphicsDevice.DrawLine(pen, x(i), y(i), x(iPlus), y(iPlus))
                        Case CordType.StepHold
                            graphicsDevice.DrawLine(pen, x(i), y(i), x(iPlus), y(i))
                            graphicsDevice.DrawLine(pen, x(iPlus), y(i), x(iPlus), y(iPlus))
                        Case Else
                            Debug.Assert(Not Debugger.IsAttached, "Unhandled cord type")
                    End Select
                End If
            Next i
        End Using
    End Sub

    ''' <summary>
    ''' Renders a series of <see cref="Cord"/> segments to the specified
    ''' <see cref="graphics"/> device.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="x">              The array of x position values that define the line segments in
    '''                               screen pixels. </param>
    ''' <param name="y">              The array of y position values that define the line segments in
    '''                               screen pixels. </param>
    ''' <param name="ignoreMissing">  True to ignore missing values. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal x() As Single, ByVal y() As Single, ByVal ignoreMissing As Boolean)

        If Not Me._Visible Then
            Return
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If
        If x Is Nothing Then
            Throw New ArgumentNullException(NameOf(x))
        End If
        If y Is Nothing Then
            Throw New ArgumentNullException(NameOf(y))
        End If

        Using pen As New Pen(Me._LineColor, Me._LineWidth)
            pen.DashStyle = Me._DashStyle
            Dim nSeg As Integer = x.Length - 1
            Dim iPlus As Integer = 0
            For i As Integer = 0 To nSeg - 1
                iPlus += 1
                If ignoreMissing OrElse
                           (x(i) <> System.Single.MaxValue AndAlso x(iPlus) <> System.Single.MaxValue AndAlso
                            y(i) <> System.Single.MaxValue AndAlso y(iPlus) <> System.Single.MaxValue) Then
                    Select Case Me.CordType
                        Case CordType.HoldStep
                            graphicsDevice.DrawLine(pen, x(i), y(i), x(i), y(iPlus))
                            graphicsDevice.DrawLine(pen, x(i), y(iPlus), x(iPlus), y(iPlus))
                        Case CordType.Linear
                            graphicsDevice.DrawLine(pen, x(i), y(i), x(iPlus), y(iPlus))
                        Case CordType.StepHold
                            graphicsDevice.DrawLine(pen, x(i), y(i), x(iPlus), y(i))
                            graphicsDevice.DrawLine(pen, x(iPlus), y(i), x(iPlus), y(iPlus))
                        Case Else
                            Debug.Assert(Not Debugger.IsAttached, "Unhandled cord type")
                    End Select
                End If
            Next i
        End Using

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Gets or sets the <see cref="System.Drawing.Drawing2D.DashStyle"/> of the <see cref="Cord"/>
    ''' allowing to draw solid, dashed, or dotted cords.
    ''' </summary>
    ''' <value> The dash style. </value>
    Public Property DashStyle() As System.Drawing.Drawing2D.DashStyle

    ''' <summary> Gets or sets a property that shows or hides the <see cref="Cord"/>. </summary>
    ''' <value> True to show the Cord, false to hide it. </value>
    Public Property Visible() As Boolean

    ''' <summary> The color of the <see cref="Cord"/> </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property LineColor() As Color

    ''' <summary> The pen width used to draw the <see cref="Cord"/> </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property LineWidth() As Single

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> A System.String value. </value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

    ''' <summary>
    ''' Determines how cords connect <see cref="Curve"/> points.  Points can be connected by directly
    ''' connecting the points from the <see cref="Curve.X"/> and
    ''' <see cref="Curve.Y"/> data arrays.  If the cord curve is a stair type of either
    ''' <see cref="CordType.StepHold"/> or <see cref="CordType.StepHold"/> points
    ''' are connected by a series of horizontal and vertical lines that represent discrete, constant
    ''' values.  Thus, values can be forward oriented <c>StepHold</c>
    ''' (<see cref="isr.Visuals.CordType"/>) or rearward oriented <c>HoldStep</c>. That is, the
    ''' points are defined at the beginning or end of the constant value for which they apply,
    ''' respectively.
    ''' </summary>
    ''' <remarks> David, 10/15/07, 1.0.2844.x Rename from Type to Cord Type. </remarks>
    ''' <value> A <see cref="isr.Visuals.CordType"/> value. </value>
    Public Property CordType() As CordType

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="Cord"/> class that defines the default property values
''' for the <see cref="isr.Visuals.Cord"/> class.
''' </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public NotInheritable Class CordDefaults

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs this class. This constructor is private to ensure only a single instance of this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub New()
        MyBase.New()
        Me._LineColor = Color.Red
        Me._Visible = True
        Me._LineWidth = 1.0F
        Me._DashStyle = System.Drawing.Drawing2D.DashStyle.Solid
        Me._CordType = isr.Visuals.CordType.Linear
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary> The instance. </summary>
    Private Shared _Instance As CordDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As CordDefaults
        If CordDefaults._Instance Is Nothing Then
            SyncLock CordDefaults.SyncLocker
                CordDefaults._Instance = New CordDefaults()
            End SyncLock
        End If
        Return CordDefaults._Instance
    End Function

#End Region

    ''' <summary>
    ''' Gets or sets the default color for curves (Cord segments connecting the points). This is the
    ''' default value for the <see cref="Cord.LineColor"/> property.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property LineColor() As Color

    ''' <summary>
    ''' Gets or sets the default mode for displaying line segments (<see cref="Cord.Visible"/>
    ''' property).  True to show the line segments, false to hide them.
    ''' </summary>
    ''' <value> The visible. </value>
    Public Property Visible() As Boolean

    ''' <summary> The (<see cref="Cord.LineWidth"/> of line segments. </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property LineWidth() As Single

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Drawing2D.DashStyle"/> style for line
    ''' segments (<see cref="Cord.DashStyle"/> property).
    ''' </summary>
    ''' <value> The dash style. </value>
    Public Property DashStyle() As System.Drawing.Drawing2D.DashStyle

    ''' <summary>
    ''' Default value for the cord type property (<see cref="CordType"/>).  
    ''' This determines if the curve will be drawn by directly connecting the points from the
    ''' <see cref="Curve.X"/> and <see cref="Curve.Y"/> data arrays, or if the curve will be a "stair-
    ''' step" in which the points are connected by a series of horizontal and vertical lines that
    ''' represent discrete values.  Discrete values can be forward oriented
    ''' <code>StepHold</code> (<see cref="CordType"/>) or backward oriented
    ''' <code>HoldStep</code>. That is, the points are defined at the beginning or end
    ''' of the value for which they apply, respectively.
    ''' </summary>
    ''' <value> A <see cref="CordType"/> value. </value>
    Public Property CordType() As isr.Visuals.CordType

End Class

#End Region

