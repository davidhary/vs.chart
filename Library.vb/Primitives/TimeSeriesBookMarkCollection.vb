''' <summary>
''' Contains a list of <see cref="TimeSeriesBookmark"/> objects, which tag a time series.
''' </summary>
''' <remarks>
''' Declare <see cref="A:NotInheritable"/> so as to allow calling base methods in the
''' constructor. <para>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para><para>  
''' David, 06/02/04, 1.0.1614. Created </para>
''' </remarks>
Public NotInheritable Class TimeSeriesBookmarkCollection
    Inherits System.Collections.ObjectModel.Collection(Of TimeSeriesBookmark)
    Implements ICloneable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Default constructor for the <see cref="TimeSeriesBookmarkCollection"/> collection class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="curve"> Reference to the <see cref="isr.Visuals.Curve">Curve</see> </param>
    Public Sub New(ByVal curve As Curve)
        MyBase.New()
        Me._Curve = curve
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are
    '''                                          null. </exception>
    ''' <param name="model"> The TimeSeriesBookmarkCollection object from which to copy. </param>
    Public Sub New(ByVal model As TimeSeriesBookmarkCollection)
        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Dim item As isr.Visuals.TimeSeriesBookmark
        For Each item In model
            Me.Add(New isr.Visuals.TimeSeriesBookmark(item))
        Next item
        Me._Curve = model._Curve
    End Sub

#End Region

#Region " CUSTOM COLLECTION METHODS "

    ''' <summary> Add a <see cref="TimeSeriesBookmark"/> to the collection. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="bookmark"> A reference to the <see cref="TimeSeriesBookmark"/> object to add. </param>
    Public Overloads Sub Add(ByVal bookmark As TimeSeriesBookmark)
        MyBase.Add(bookmark)
        bookmark.SerialNumber = MyBase.Count
    End Sub

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the TimeSeriesBookmarkCollection. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the TimeSeriesBookmarkCollection. </returns>
    Public Function Copy() As TimeSeriesBookmarkCollection
        Return New TimeSeriesBookmarkCollection(Me)
    End Function

    ''' <summary>
    ''' Creates a one-dimensional <see cref="T:System.Array">Array</see>
    ''' instance containing the collection items.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> Array of type <see cref="TimeSeriesBookmark">time series Bookmark</see> </returns>
    Public Function ToArray() As TimeSeriesBookmark()
        Dim bookmarks As TimeSeriesBookmark() = New TimeSeriesBookmark(Me.Count - 1) {}
        Me.CopyTo(bookmarks, 0)
        Return bookmarks
    End Function

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Curve">curve</see></summary>
    Private ReadOnly _Curve As Curve

#End Region

End Class

