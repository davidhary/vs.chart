''' <summary> Renders frames for text and chart boxes. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581.Created </para>
''' </remarks>
Public Class Frame

    Implements ICloneable, IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a <see cref="Frame"/> with
    ''' <see cref="FrameDefaults">default property values</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()

        MyBase.New()

        Dim frameDefaults As FrameDefaults = FrameDefaults.Get()
        Me._FillColor = frameDefaults.FillColor
        Me._Filled = frameDefaults.Filled
        Me.IsOutline = frameDefaults.IsOutline
        Me._Visible = frameDefaults.Visible
        Me._LineColor = frameDefaults.LineColor
        Me._LineWidth = frameDefaults.LineWidth

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The Frame object from which to copy. </param>
    Public Sub New(ByVal model As Frame)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Me._StatusMessage = model.StatusMessage
        Me._FillColor = model._FillColor
        Me._Filled = model._Filled
        Me.IsOutline = model._IsOutline
        Me._Visible = model._Visible
        Me._LineColor = model._LineColor
        Me._LineWidth = model._LineWidth

    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> True if disposed. </summary>
    Private _Disposed As Boolean

    ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._Disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._Disposed = value
        End Set
    End Property

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the Frame. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of Frame. </returns>
    Public Function Copy() As Frame
        Return New Frame(Me)
    End Function

    ''' <summary> Renders a <see cref="Frame"/>. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="frameArea">      The frame <see cref="RectangleF"/>. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal frameArea As RectangleF)

        If Me._Visible Then

            ' validate argument.
            If graphicsDevice Is Nothing Then Throw New ArgumentNullException(NameOf(graphicsDevice))

            ' If the background is to be filled, fill it
            If Me._Filled Then
                Using fillBrush As New SolidBrush(Me._FillColor)
                    graphicsDevice.FillRectangle(fillBrush, Rectangle.Round(frameArea))
                End Using
            End If

            ' Draw the outline around 
            If Me._IsOutline Then
                Using pen As New Pen(Me._LineColor, Me._LineWidth)
                    graphicsDevice.DrawRectangle(pen, Rectangle.Round(frameArea))
                End Using
            End If
        End If

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Gets or sets the background color of the frame. Background fill is turned on or off using the
    ''' <see cref="Filled"/> property.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> value. </value>
    Public Property FillColor() As Color

    ''' <summary>
    ''' Gets or sets the fill mode of the <see cref="Frame"/>.  Set to True to fill the frame with
    ''' color, or False otherwise.
    ''' </summary>
    ''' <value>
    ''' True to fill the <see cref="Frame"/> background with the <see cref="FillColor"/>, False to
    ''' leave the background transparent.
    ''' </value>
    Public Property Filled() As Boolean

    ''' <summary>
    ''' Gets or sets the outline mode of the <see cref="Frame"/>.  Set to True to outline the frame
    ''' with color, or False otherwise.
    ''' </summary>
    ''' <value> The is outline. </value>
    Public Property IsOutline() As Boolean

    ''' <summary> Gets or sets a property that shows or hides the <see cref="Symbol"/>. </summary>
    ''' <value> True to show the symbol, False to hide it. </value>
    Public Property Visible() As Boolean

    ''' <summary> Gets or sets the pen width for drawing the <see cref="Symbol"/> outline. </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property LineWidth() As Single

    ''' <summary> Gets or sets the line color of the <see cref="Symbol"/> </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property LineColor() As Color

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> A System.String value. </value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="Frame"/> class that defines the default property values
''' for the <see cref="Frame"/> class.
''' </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public NotInheritable Class FrameDefaults

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs this class. This constructor is private to ensure only a single instance of this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub New()
        MyBase.New()
        Me._FillColor = Color.White
        Me._Visible = True
        Me._LineWidth = 1.0F
        Me._IsOutline = True
        Me._Filled = True
        Me._LineColor = Color.Black
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _Instance As FrameDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As FrameDefaults
        If FrameDefaults._Instance Is Nothing Then
            SyncLock FrameDefaults.SyncLocker
                FrameDefaults._Instance = New FrameDefaults()
            End SyncLock
        End If
        Return FrameDefaults._Instance
    End Function

#End Region

    ''' <summary>
    ''' Gets or sets the default color for filling the frame (<see cref="Frame.FillColor"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property FillColor() As Color

    ''' <summary>
    ''' Gets or sets the default display mode for Frame (<see cref="Frame.Visible"/> property). True
    ''' to display the frame, False to hide the frame.
    ''' </summary>
    ''' <value> The visible. </value>
    Public Property Visible() As Boolean

    ''' <summary>
    ''' Gets or sets the default pen width to be used for drawing frame outline
    ''' (<see cref="Frame.LineWidth"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property LineWidth() As Single

    ''' <summary>
    ''' Gets or sets the default outline draw mode for Frame (<see cref="Frame.IsOutline"/>
    ''' property). True to draw the frame outline, False otherwise.
    ''' </summary>
    ''' <value> The is outline. </value>
    Public Property IsOutline() As Boolean

    ''' <summary>
    ''' Gets or sets the default fill mode for the frame (<see cref="Frame.Filled"/> property). True
    ''' to have Frame filled in with color, False to leave frame as outline.
    ''' </summary>
    ''' <value> The filled. </value>
    Public Property Filled() As Boolean

    ''' <summary>
    ''' Gets or sets the default color for drawing frame outline (<see cref="Frame.LineColor"/>
    ''' property).
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property LineColor() As Color

End Class

#End Region

