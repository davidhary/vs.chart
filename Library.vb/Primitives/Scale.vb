Imports isr.Core.Constructs

''' <summary> Handles axis and data scaling. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public Class Scale

    Implements ICloneable, IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Default constructor for <see cref="Axis"/> scale that sets all properties to default values
    ''' as defined in the <see cref="ScaleDefaults"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()

        MyBase.New()

        Dim scaleDefaults As ScaleDefaults = ScaleDefaults.Get()
        Me._Reversed = scaleDefaults.Reversed
        Me._Visible = scaleDefaults.Visible
        Me._Max = scaleDefaults.Max.Copy()
        Me._Min = scaleDefaults.Min.Copy()
        Me._CoordinateScale.CoordinateScaleType = scaleDefaults.CoordinateScale
        Me._ScaleExponent = scaleDefaults.ScaleExponent.Copy()

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The Scale object from which to copy. </param>
    Public Sub New(ByVal model As Scale)

        Me.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Me._Visible = model._Visible
        Me._Reversed = model._Reversed
        Me._Min = model._Min
        Me._Max = model._Max
        Me._CoordinateScale = model._CoordinateScale
        Me._ScaleExponent = model._ScaleExponent

    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> True if disposed. </summary>
    Private _Disposed As Boolean

    ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._Disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._Disposed = value
        End Set
    End Property

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the Scale. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of Scale. </returns>
    Public Function Copy() As Scale
        Return New Scale(Me)
    End Function

    ''' <summary>
    ''' Calculate a date that is close to the specified date and an
    '''   even multiple of the selected <see cref="TimeIntervalType"/> for a
    '''   <see cref="CoordinateScaleType.Date"/> scale. This method is used by
    ''' <see cref="Axis.Rescale"/>.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="dateValue">        The date which the calculation should be close to. </param>
    ''' <param name="direction">        The desired direction for the date to take. 1 indicates the
    '''                                 result date should be greater than the specified date
    '''                                 parameter.  -1 indicates the other direction. </param>
    ''' <param name="timeIntervalType"> The<see cref="TimeIntervalType"/> </param>
    ''' <returns> The even date spacing. </returns>
    Protected Shared Function GetEvenDateSpacing(ByVal dateValue As Double, ByVal direction As Integer, ByVal timeIntervalType As TimeIntervalType) As Double

        Dim year, month, day, hour, minute, second, milliseconds As Integer

        JulianDate.ToCalendarDate(dateValue, year, month, day, hour, minute, second, milliseconds)

        ' If the direction is -1, then it is sufficient to go to the beginning of
        ' the current time period, .e.graphicsDevice., for 15-May-95, and monthly spacing, we
        ' can just back up to 1-May-95
        If direction < 0 Then
            direction = 0
        End If
        Select Case timeIntervalType
            Case TimeIntervalType.Year
                ' If the date is already an exact year, then don't space to the next year
                Return If(direction = 1 AndAlso month = 1 AndAlso day = 1 AndAlso hour = 0 AndAlso minute = 0 AndAlso second = 0,
                    dateValue,
                    JulianDate.FromArbitraryCalendarDate(year + direction, 1, 1, 0, 0, 0, 0))
            Case TimeIntervalType.Month
                ' If the date is already an exact month, then don't space to the next month
                Return If(direction = 1 AndAlso day = 1 AndAlso hour = 0 AndAlso minute = 0 AndAlso second = 0,
                    dateValue,
                    JulianDate.FromArbitraryCalendarDate(year, month + direction, 1, 0, 0, 0, 0))
            Case TimeIntervalType.Day
                ' If the date is already an exact Day, then don't space to the next day
                Return If(direction = 1 AndAlso hour = 0 AndAlso minute = 0 AndAlso second = 0,
                    dateValue,
                    JulianDate.FromArbitraryCalendarDate(year, month, day + direction, 0, 0, 0, 0))
            Case TimeIntervalType.Hour
                ' If the date is already an exact hour, then don't space to the next hour
                Return If(direction = 1 AndAlso minute = 0 AndAlso second = 0,
                    dateValue,
                    JulianDate.FromArbitraryCalendarDate(year, month, day, hour + direction, 0, 0, 0))
            Case TimeIntervalType.Minute
                ' If the date is already an exact minute, then don't space to the next minute
                Return If(direction = 1 AndAlso second = 0,
                    dateValue,
                    JulianDate.FromArbitraryCalendarDate(year, month, day, hour, minute + direction, 0, 0))
            Case TimeIntervalType.Second
                Return JulianDate.FromArbitraryCalendarDate(year, month, day, hour, minute, second + direction, 0)
        End Select
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Gets the <see cref="isr.Visuals.CoordinateScale"/> for this <see cref="Scale"/>.
    ''' </summary>
    ''' <value> The coordinate scale. </value>
    Public Property CoordinateScale() As isr.Visuals.CoordinateScale

    ''' <summary> Determines if the scale values are reversed for this <see cref="Scale"/> </summary>
    ''' <value>
    ''' True for the X values to decrease to the right or the Y values to decrease upwards, false
    ''' otherwise.
    ''' </value>
    Public Property Reversed() As Boolean

    ''' <summary> Determines if the value <see cref="Labels"/> will be drawn. </summary>
    ''' <value> A <see cref="System.Boolean">Boolean</see> </value>
    Public Property Visible() As Boolean

    ''' <summary>
    ''' Gets maximum auto value for this axis.  This value can be set automatically based on the
    ''' state of <see cref="AutoValueR.AutoScale"/>.  If this value is set manually, then
    ''' <see cref="AutoValueR.AutoScale"/> is set to False.
    ''' </summary>
    ''' <value>
    ''' A <see cref="AutoValueR"/> value in user scale units for
    ''' <see cref="CoordinateScaleType.Log"/> and <see cref="CoordinateScaleType.Linear"/> axes. For
    ''' <see cref="CoordinateScaleType.Text"/> axes, this value is an ordinal starting with 1.0.
    ''' For <see cref="CoordinateScaleType.Date"/> axes, this value is in an internal
    ''' (<see cref="System.Double"/> date format based on the <see cref="JulianDate"/>.
    ''' </value>
    Public Property Max() As AutoValueR

    ''' <summary>
    ''' Gets minimum auto value for this axis.  The value of this auto value automatically based on
    ''' the state of <see cref="AutoValueR.AutoScale"/>.  If this value is set manually, then
    ''' <see cref="AutoValueR.AutoScale"/> is set to false.
    ''' </summary>
    ''' <value>
    ''' A <see cref="AutoValueR"/> value in user scale units for
    ''' <see cref="CoordinateScaleType.Log"/> and <see cref="CoordinateScaleType.Linear"/> axes. For
    ''' <see cref="CoordinateScaleType.Text"/> axes, this value is an ordinal starting with 1.0.
    ''' For <see cref="CoordinateScaleType.Date"/> axes, this value is in an internal
    ''' (<see cref="System.Double"/> date format based on the <see cref="JulianDate"/>.
    ''' </value>
    Public Property Min() As AutoValueR

    ''' <summary> Gets the axis range. </summary>
    ''' <value> Max - Min values. </value>
    Public ReadOnly Property Range() As Double
        Get
            Return Me._Max.Value - Me._Min.Value
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the magnitude multiplier decade for scale values. This is used to limit the size
    ''' of the displayed value labels.  For example, if the value is really 2000000, then the graph
    ''' could instead display 2000 with a magnitude multiplier decade of 3 (10^3).  The Value can be
    ''' determined automatically depending on the state of <see cref="AutoValue.AutoScale"/>. If the
    ''' Value is set manually by the user, then <see cref="AutoValue.AutoScale"/>
    ''' is set to False. The magnitude multipliers.
    ''' </summary>
    ''' <value> The magnitude multiplier (power of 10) for the scale value labels. </value>
    Public Property ScaleExponent() As AutoValue

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> A System.String value. </value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="Scale"/> class that defines the default property values
''' for the <see cref="Scale"/> class.
''' </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public NotInheritable Class ScaleDefaults

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs this class. This constructor is private to ensure only a single instance of this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub New()
        MyBase.New()
        Me._CoordinateScale = isr.Visuals.CoordinateScaleType.Linear
        Me._IgnoreInitial = False
        Me._Reversed = False
        Me._Visible = True
        Me._Max = New AutoValueR(1, True)
        Me._Min = New AutoValueR(0, True)
        Me._MinRange = 1.0E-20
        Me._ScaleExponent = New AutoValue(0, False)
        Me._ZeroLever = 0.25
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _Instance As ScaleDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As ScaleDefaults
        If ScaleDefaults._Instance Is Nothing Then
            SyncLock ScaleDefaults.SyncLocker
                ScaleDefaults._Instance = New ScaleDefaults()
            End SyncLock
        End If
        Return ScaleDefaults._Instance
    End Function

#End Region

    ''' <summary>
    ''' Gets or sets the default setting for the <see cref="Axis"/> coordinate scale type
    ''' (<see cref="Scale.CoordinateScale"/> property).  This value is set as per the
    ''' <see cref="CoordinateScaleType"/> enumeration.
    ''' </summary>
    ''' <value> The coordinate scale. </value>
    Public Property CoordinateScale() As isr.Visuals.CoordinateScaleType

    ''' <summary>
    ''' Gets or sets the default settings for the <see cref="Axis"/> scale ignore initial zero values
    ''' option (<see cref="Pane.IgnoreInitial"/> property). true to have the auto-scale-range code
    ''' ignore the initial data points until the first non-zero Y value, false otherwise.
    ''' </summary>
    ''' <value> The ignore initial. </value>
    Public Property IgnoreInitial() As Boolean

    ''' <summary>
    ''' Gets or sets the default reverse mode for the <see cref="Axis"/> scale
    ''' (<see cref="Axis.Reversed"/> property). true for a reversed scale (X decreasing to the left,
    ''' Y/Y2 decreasing upwards), false otherwise.
    ''' </summary>
    ''' <value> The reversed. </value>
    Public Property Reversed() As Boolean

    ''' <summary>
    ''' Gets or sets the default display mode for the axis <see cref="Scale"/>
    ''' (<see cref="Scale.Visible"/> property). True to show the scale, False to hide it.
    ''' </summary>
    ''' <value> The visible. </value>
    Public Property Visible() As Boolean

    ''' <summary>
    ''' Gets or sets the default maximum point for the axis <see cref="Scale"/>
    ''' (<see cref="Scale.Max"/> property).
    ''' </summary>
    ''' <value> The maximum value. </value>
    Public Property Max() As AutoValueR

    ''' <summary>
    ''' Gets or sets the default minimum point for the axis <see cref="Scale"/>
    ''' (<see cref="Scale.Min"/> property).
    ''' </summary>
    ''' <value> The minimum value. </value>
    Public Property Min() As AutoValueR

    ''' <summary> Gets or sets the default minimum range for the axis <see cref="Scale"/>. </summary>
    ''' <value> The minimum range. </value>
    Public Property MinRange() As Double

    ''' <summary>
    ''' Gets or sets the default scale decade for the axis value <see cref="Labels"/>
    ''' (<see cref="Scale.ScaleExponent"/> property).
    ''' </summary>
    ''' <value> The scale exponent. </value>
    Public Property ScaleExponent() As AutoValue

    ''' <summary>
    ''' Gets or sets the default "zero lever" for automatically selecting the axis scale range (see
    ''' <see cref="Axis.Rescale"/>). This number is used to determine when an axis scale range should
    ''' be extended to include the zero value.  This value is maintained only in the
    ''' <see cref="ScaleDefaults"/> class, and can be changed after compilation.
    ''' The Zero Lever is the allowable fraction that the scale range can be extended to include the
    ''' zero value. For example, if the Zero Lever is 0.25 and the data range is from 2 to 12, then
    ''' the scale would be extended to the range of 0.0 to 12 because the zero value lies 20% (2/(12-
    ''' 2)) outside the actual data range.
    ''' </summary>
    ''' <value> The zero lever. </value>
    Public Property ZeroLever() As Double

End Class
#End Region


