''' <summary>
''' A collection class containing a list of <see cref="isr.Visuals.Arrow"/> type graphic objects
''' to be displayed on the graph.
''' </summary>
''' <remarks>
''' Declare <see cref="A:NotInheritable"/> so as to allow calling base methods in the
''' constructor. <para>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public NotInheritable Class ArrowCollection
    Inherits System.Collections.ObjectModel.Collection(Of Arrow)
    Implements ICloneable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor for the collection class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The ArrowCollection object from which to copy. </param>
    Public Sub New(ByVal model As ArrowCollection)
        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Dim item As Arrow
        For Each item In model
            Me.Add(New Arrow(item))
        Next item
        Me._Pane = model._Pane
    End Sub

#End Region

#Region " CUSTOM COLLECTION METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the ArrowCollection. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the ArrowCollection. </returns>
    Public Function Copy() As ArrowCollection
        Return New ArrowCollection(Me)
    End Function

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
    Private ReadOnly _Pane As Pane

    ''' <summary>
    ''' Renders all the <see cref="Arrow">Arrows</see> to the specified <see cref="Graphics"/> device.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.Graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        ' Loop for each curve
        For Each arrow As Arrow In Me
            arrow.Draw(graphicsDevice)
        Next arrow

    End Sub

#End Region

End Class
