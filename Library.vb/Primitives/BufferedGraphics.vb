''' <summary>
''' Implements Double Buffering rendering of the graph to provide smooth refreshing. The double
''' buffer slows down the line recorder significantly.
''' </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public Class BufferedGraphics

    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="canvasWidth">  The canvas width. </param>
    ''' <param name="canvasHeight"> The canvas height. </param>
    Public Sub New(ByVal canvasWidth As Integer, ByVal canvasHeight As Integer)
        Me.CreateDoubleBuffer(canvasWidth, canvasHeight)
        '_graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> True if disposed. </summary>
    Private _Disposed As Boolean

    ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._Disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._Disposed = value
        End Set
    End Property

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Me._Canvas IsNot Nothing Then
                        Me._Canvas.Dispose()
                        Me._Canvas = Nothing
                    End If

                    If Me._Graphics IsNot Nothing Then
                        Me._Graphics.Dispose()
                        Me._Graphics = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Creates double buffer object. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="width">  width of paint area. </param>
    ''' <param name="height"> height of paint area. </param>
    ''' <returns> true/false if double buffer is created. </returns>
    Public Function CreateDoubleBuffer(ByVal width As Integer, ByVal height As Integer) As Boolean

        If width = 0 OrElse height = 0 Then
            Return False
        End If

        If width <> width OrElse height <> height Then

            If Me._Canvas IsNot Nothing Then
                Me._Canvas.Dispose()
                Me._Canvas = Nothing
            End If

            If Me._Graphics IsNot Nothing Then
                Me._Graphics.Dispose()
                Me._Graphics = Nothing
            End If

            Me._Width = width
            Me._Height = height

            Me._Canvas = New Bitmap(width, height)
            Me._Graphics = Graphics.FromImage(Me._Canvas)

        End If

        Return True

    End Function

    ''' <summary> Renders the double buffer to the screen. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="graphicsDevice"> Window forms graphics Object. </param>
    Public Sub Render(ByVal graphicsDevice As Graphics)
        If graphicsDevice IsNot Nothing AndAlso Me._Canvas IsNot Nothing Then
            graphicsDevice.DrawImage(Me._Canvas, New Rectangle(0, 0, Me._Width, Me._Height), 0, 0, Me._Width, Me._Height, GraphicsUnit.Pixel)
        End If
    End Sub

    ''' <summary> Returns true if double buffering can be achieved. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> True if we can double buffer, false if not. </returns>
    Public Function CanDoubleBuffer() As Boolean
        Return Me._Graphics IsNot Nothing
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets the status message. </summary>
    ''' <value> A System.String value. </value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

    ''' <summary> The canvas. </summary>
    Private _Canvas As Bitmap

    ''' <summary> The width. </summary>
    Private _Width As Integer

    ''' <summary> The height. </summary>
    Private _Height As Integer

    ''' <summary> The graphics. </summary>
    Private _Graphics As Graphics

    ''' <summary> Reference to graphics context. </summary>
    ''' <value> The graphics device. </value>
    Public ReadOnly Property GraphicsDevice() As Graphics
        Get
            Return Me._Graphics
        End Get
    End Property

#End Region

End Class
