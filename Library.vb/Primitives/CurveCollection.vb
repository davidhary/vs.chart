''' <summary>
''' A collection class containing a list of <see cref="Curve"/> objects that define the set of
''' curves to be displayed on the graph.
''' </summary>
''' <remarks>
''' Declare <see cref="A:NotInheritable"/> so as to allow calling base methods in the
''' constructor. <para>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public NotInheritable Class CurveCollection
    Inherits System.Collections.ObjectModel.Collection(Of Curve)

    Implements ICloneable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor for the collection class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
    Public Sub New(ByVal drawingPane As Pane)
        MyBase.New()
        Me._Pane = drawingPane
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The XAxis object from which to copy. </param>
    Public Sub New(ByVal model As CurveCollection)
        MyBase.New()
        If model Is Nothing Then Throw New ArgumentNullException(NameOf(model))
        Dim item As Curve
        For Each item In model
            Me.Add(New Curve(item))
        Next item
        Me._Pane = Me._Pane
    End Sub

#End Region

#Region " CUSTOM COLLECTION METHODS "

    ''' <summary> Add a <see cref="Curve"/> to the collection. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="curve"> A reference to the <see cref="Curve"/> object to add. </param>
    Public Overloads Sub Add(ByVal curve As Curve)
        If curve Is Nothing Then
            Throw New ArgumentNullException(NameOf(curve))
        End If
        MyBase.Add(curve)
        curve.SerialNumber = MyBase.Count
    End Sub

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the CurveCollection. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the CurveCollection. </returns>
    Public Function Copy() As CurveCollection
        Return New CurveCollection(Me)
    End Function

    ''' <summary>
    ''' Renders all the <see cref="Curve">Curves</see> to the specified <see cref="graphics"/> device.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        ' Loop for each curve
        For Each curve As Curve In Me
            ' Render the curve
            curve.Draw(graphicsDevice) ' , axisArea, scaleFactor)
        Next curve

    End Sub

    ''' <summary>
    ''' Adjusts the <see cref="Pane.AxisArea"/> for the
    ''' <see cref="isr.Visuals.Stylus">Stylus</see> size.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="scaleFactor"> The scaling factor for the chart with reference to the chart
    '''                            <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                            calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                            The scale factor is applied to fonts, symbols, etc. </param>
    ''' <param name="axisArea">    The rectangle that contains the area bounded by the axes, in
    '''                            pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso> </param>
    ''' <returns> Adjusted <see cref="Pane.AxisArea"/> </returns>
    Public Function GetAxisArea(ByVal scaleFactor As Double, ByVal axisArea As RectangleF) As RectangleF

        ' Loop for each curve
        For Each curve As Curve In Me
            ' Render the curve
            axisArea = curve.GetAxisArea(scaleFactor, axisArea)
        Next curve

        ' return adjusted axis rectangle
        Return axisArea

    End Function

    ''' <summary> get the extended range of all specified curves. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="ignoreInitial"> Affects how initial zero y values are treated for setting the
    '''                              range of X values.  If True, then initial zero data points are
    '''                              ignored when determining the X range.  All data after the first
    '''                              non-zero Y value are included. </param>
    ''' <param name="isY2Axis">      True to use Y2 Axis.  Otherwise Y Axis. </param>
    ''' <returns> The calculated range. </returns>
    Public Function GetRange(ByVal ignoreInitial As Boolean, ByVal isY2Axis As Boolean) As PlanarRangeR

        ' get the empty range
        Dim range As PlanarRangeR = PlanarRangeR.Empty

        ' Loop over each curve in the collection
        For Each curve As Curve In Me

            If (isY2Axis AndAlso curve.IsY2Axis) OrElse
        ((Not isY2Axis) AndAlso (Not curve.IsY2Axis)) Then

                range.ExtendRange(curve.GetRange(ignoreInitial))

            End If

        Next

        If range.Equals(PlanarRangeR.Empty) Then
            ' Use unit range if no data were available
            Return PlanarRangeR.Unity
        Else
            Return range
        End If

    End Function

    ''' <summary>
    ''' Determine if there is any data in any of the <see cref="Curve"/>
    ''' objects for this graph.  This method does not verify valid data, it only checks to see if
    ''' <see cref="Curve.PointCount"/> &gt; 0.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> <c>True</c> if there is any data, false otherwise. </returns>
    Public Function HasData() As Boolean
        For Each curve As Curve In Me
            If curve.PointCount > 0 Then
                Return True
            End If
        Next curve
        Return False
    End Function

    ''' <summary>
    ''' Rescale all the time series <see cref="Axis"/> objects in the list to the specified
    ''' <see cref="CurveCollection"/> by calling the <see cref="Axis.Rescale"/>
    ''' method of each <see cref="Axis"/> object.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub ScaleTimeSeriesAxis()

        ' Loop for each curve
        For Each curve As Curve In Me
            curve.ScaleTimeSeriesAxis()
        Next curve

    End Sub

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
    Private ReadOnly _Pane As Pane

#End Region

End Class

