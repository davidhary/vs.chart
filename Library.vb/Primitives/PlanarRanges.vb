Imports isr.Core.Constructs

''' <summary>
''' Defines a <see cref="System.Double">Double</see> two-dimensional range structure.
''' </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public Structure PlanarRangeR

#Region " SHARED "

    ''' <summary>Gets or sets the empty range</summary>
    ''' <value>A <see cref="PlanarRangeR"/> value with empty X and Y ranges</value>
    Public Shared ReadOnly Property [Empty]() As PlanarRangeR
        Get
            Return New PlanarRangeR(RangeR.Empty, RangeR.Empty)
        End Get
    End Property

    ''' <summary>
    ''' gets the planar range for the <see cref="X"/> and <see cref="Y"/> data arrays.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x">             The x data array. </param>
    ''' <param name="y">             The y data array. </param>
    ''' <param name="ignoreInitial"> Affects how initial zero y values are treated for setting the
    '''                              range of X values.  If True, then initial zero data points are
    '''                              ignored when determining the X range.  All data after the first
    '''                              non-zero Y value are included. </param>
    ''' <returns> The calculated range. </returns>
    Public Shared Function GetRange(ByVal x() As Double, ByVal y() As Double, ByVal ignoreInitial As Boolean) As PlanarRangeR

        ' return the unit range if no data
        If x Is Nothing OrElse y Is Nothing OrElse x.Length = 0 OrElse y.Length = 0 Then
            Return PlanarRangeR.Unity
        End If

        Dim numPoints As Integer = Convert.ToInt32(Math.Min(x.Length, y.Length))

        ' initialize the values to the empty range
        Dim yTemp As Double
        Dim xTemp As Double
        yTemp = y(0)
        xTemp = x(0)
        Dim xMin As Double = xTemp
        Dim yMin As Double = yTemp
        Dim xMax As Double = xTemp
        Dim yMax As Double = yTemp

        ' Loop over each point in the arrays
        For i As Integer = 0 To numPoints - 1

            yTemp = y(i)
            xTemp = x(i)

            ' ignoreInitial becomes false at the first non-zero Y value
            If ignoreInitial AndAlso (yTemp <> 0) AndAlso (yTemp <> System.Double.MaxValue) Then
                ignoreInitial = False
            End If

            If Not ignoreInitial AndAlso (xTemp <> System.Double.MaxValue) AndAlso (yTemp <> System.Double.MaxValue) Then
                If xTemp < xMin Then
                    xMin = xTemp
                ElseIf xTemp > xMax Then
                    xMax = xTemp
                End If
                If yTemp < yMin Then
                    yMin = yTemp
                ElseIf yTemp > yMax Then
                    yMax = yTemp
                End If
            End If

        Next i

        Return New PlanarRangeR(New RangeR(xMin, xMax), New RangeR(yMin, yMax))

    End Function

    ''' <summary> Gets the unit range. </summary>
    ''' <value> A <see cref="PlanarRangeR"/> value with unity X and Y ranges. </value>
    Public Shared ReadOnly Property Unity() As PlanarRangeR
        Get
            Return New PlanarRangeR(RangeR.Unity, RangeR.Unity)
        End Get
    End Property

    ''' <summary> Gets the zero range value. </summary>
    ''' <value> A <see cref="PlanarRangeR"/> value. </value>
    Public Shared ReadOnly Property Zero() As PlanarRangeR
        Get
            Return New PlanarRangeR(RangeR.Zero, RangeR.Zero)
        End Get
    End Property

#End Region

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Contracts a planar range based on the X and Y ranges. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x"> The horizontal <see cref="RangeR"/> </param>
    ''' <param name="y"> The vertical <see cref="RangeR"/> </param>
    Public Sub New(ByVal x As RangeR, ByVal y As RangeR)

        Me.SetPlanarRange(x, y)

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="model"> The PlanarRangeR object from which to copy. </param>
    Public Sub New(ByVal model As PlanarRangeR)

        Me._X = model._X
        Me._Y = model._Y

    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Planar range r to be compared. </param>
    ''' <param name="right"> Planar range r to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As PlanarRangeR, ByVal right As PlanarRangeR) As Boolean
        Return PlanarRangeR.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Planar range r to be compared. </param>
    ''' <param name="right"> Planar range r to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As PlanarRangeR, ByVal right As PlanarRangeR) As Boolean
        Return Not PlanarRangeR.Equals(left, right)
    End Operator

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Planar range r to be compared. </param>
    ''' <param name="right"> Planar range r to be compared. </param>
    ''' <returns>
    ''' <c>True</c> if values are the same type and represent the same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Shared Function Equals(ByVal left As PlanarRangeR, ByVal right As PlanarRangeR) As Boolean
        Return left.Equals(right)
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="obj"> Another object to compare to. </param>
    ''' <returns>
    ''' <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso Me.GetType() Is obj.GetType() AndAlso Me.Equals(CType(obj, PlanarRangeR))
    End Function

    ''' <summary>
    ''' Returns True if the value of the <paramref name="other"/> equals to the instance value.
    ''' </summary>
    ''' <remarks>
    ''' Planar Ranges are the same if the have the same
    ''' <see cref="X"/> and <see cref="Y"/> ranges.
    ''' </remarks>
    ''' <param name="other"> The <see cref="PlanarRangeR">PlanarRangeR</see> to compare for equality
    '''                      with this instance. </param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    Public Overloads Function Equals(ByVal other As PlanarRangeR) As Boolean
        Return Me._X.Equals(other._X) AndAlso Me._Y.Equals(other._Y)
    End Function

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me._X.GetHashCode Xor Me._Y.GetHashCode
    End Function

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Returns true if the <see cref="PlanarRangeR"/> contains the
    ''' <see cref="PointF">Point</see>
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="point"> A <see cref="PointF">Point</see> </param>
    ''' <returns> True if the object is in this collection, false if not. </returns>
    Public Function Contains(ByVal point As PointF) As Boolean
        If point Is Nothing Then Throw New ArgumentNullException(NameOf(point))
        Return Me._X.Contains(point.X) AndAlso Me._Y.Contains(point.Y)
    End Function

    ''' <summary>
    ''' Returns true if the <see cref="PlanarRangeR"/> contains the
    ''' <see cref="PointF">Point</see>
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="point">     A <see cref="PointF">Point</see> </param>
    ''' <param name="tolerance"> A <see cref="SizeF">Tolerance</see> around the point. </param>
    ''' <returns> True if the object is in this collection, false if not. </returns>
    Public Function Contains(ByVal point As PointF, ByVal tolerance As SizeF) As Boolean
        If point Is Nothing Then Throw New ArgumentNullException(NameOf(point))
        Return Me._X.Contains(point.X, tolerance.Width) AndAlso Me._Y.Contains(point.Y, tolerance.Height)
    End Function

    ''' <summary>
    ''' Extend this range to include both its present values and the specified range.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="range"> A <see cref="planarRangeR"/> value. </param>
    ''' <returns> A PlanarRangeR. </returns>
    Public Function ExtendRange(ByVal range As PlanarRangeR) As PlanarRangeR
        Me._X.ExtendRange(range.X)
        Me._Y.ExtendRange(range.Y)
        Return Me

    End Function

    ''' <summary> Sets the PlanarRangeR based on the extrema. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x"> The horizontal <see cref="RangeR"/>. </param>
    ''' <param name="y"> The vertical <see cref="RangeR"/>. </param>
    Public Overloads Sub SetPlanarRange(ByVal x As RangeR, ByVal y As RangeR)
        Me._X = x
        Me._Y = y
    End Sub

    ''' <summary> Returns the default string representation of the PlanarRangeR. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The fully qualified type name. </returns>
    Public Overrides Function ToString() As String
        Return $"[{Me._X},{Me._Y}]"
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets the horizontal range. </summary>
    ''' <value> A <see cref="RangeR"/> value. </value>
    Public Property X() As RangeR

    ''' <summary> Gets or sets the vertical range. </summary>
    ''' <value> A <see cref="RangeR"/> value. </value>
    Public Property Y() As RangeR

#End Region

End Structure

