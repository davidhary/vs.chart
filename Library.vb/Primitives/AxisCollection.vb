Imports isr.Core.Constructs

''' <summary>
''' A collection class containing a list of <see cref="Axis"/> objects that define the set of
''' Axes to be displayed on the graph.
''' </summary>
''' <remarks>
''' Declare <see cref="A:NotInheritable"/> so as to allow calling base methods in the
''' constructor. <para>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public NotInheritable Class AxisCollection
    Inherits System.Collections.ObjectModel.Collection(Of Axis)
    Implements ICloneable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor for the collection class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
    Public Sub New(ByVal drawingPane As Pane)
        MyBase.New()
        If drawingPane Is Nothing Then Throw New ArgumentNullException(NameOf(drawingPane))
        Me._Pane = drawingPane
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The Axis Collection object from which to copy. </param>
    Public Sub New(ByVal model As AxisCollection)
        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        For Each item As Axis In model
            Me.Add(item)
        Next item
        Me._Pane = model._Pane
    End Sub

#End Region

#Region " CUSTOM COLLECTION METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the AxisCollection. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the AxisCollection. </returns>
    Public Function Copy() As AxisCollection
        Return New AxisCollection(Me)
    End Function

    ''' <summary>
    ''' Renders all the <see cref="Axis"/> objects in the list to the specified
    ''' <see cref="Graphics"/> device by calling the <see cref="Axis.Draw"/>
    ''' method of each <see cref="Axis"/> object.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.Graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        ' Clip everything to the pane area
        graphicsDevice.SetClip(Me._Pane.PaneArea)

        ' Loop for each axis
        For Each axis As Axis In Me

            ' Render the axis
            axis.Draw(graphicsDevice)

        Next axis

    End Sub

    ''' <summary> Adjusts the <see cref="Pane.AxisArea"/> for the axis space. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.Graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
    '''                               <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                               calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                               The scale factor is applied to fonts, symbols, etc. </param>
    ''' <param name="axisArea">       The rectangle that contains the area bounded by the axes, in
    '''                               pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso> </param>
    ''' <returns> Adjusted <see cref="Pane.AxisArea"/> </returns>
    Public Function GetAxisArea(ByVal graphicsDevice As Graphics, ByVal scaleFactor As Double,
                                ByVal axisArea As RectangleF) As RectangleF

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        ' calculate the margins required for the X, Y, and Y2 axes
        Dim margins As MarginsF = New MarginsF(0, 0, 0, 0)
        Dim axisSpace As SizeF

        ' Loop for each axis
        For Each axis As Axis In Me

            ' get width and height required for axis space
            axisSpace = axis.GetSpace(graphicsDevice, scaleFactor)

            ' Set margins based on the axis
            Select Case axis.AxisType
                Case AxisType.X
                    margins.Bottom = Math.Max(margins.Bottom, axisSpace.Height)
                    margins.Left = Math.Max(margins.Left, axisSpace.Width)
                    margins.Right = Math.Max(margins.Right, axisSpace.Width)
                Case AxisType.Y
                    margins.Left = Math.Max(margins.Left, axisSpace.Width)
                    margins.Bottom = Math.Max(margins.Bottom, axisSpace.Height)
                    margins.Top = Math.Max(margins.Top, axisSpace.Height)
                Case AxisType.Y2
                    margins.Right = Math.Max(margins.Right, axisSpace.Width)
                    margins.Bottom = Math.Max(margins.Bottom, axisSpace.Height)
                    margins.Top = Math.Max(margins.Top, axisSpace.Height)
                Case Else
                    Debug.Assert(Not Debugger.IsAttached, "Unhandled axis type")
            End Select
        Next axis

        ' return adjusted axis rectangle
        Return margins.GetAdjustedArea(axisArea)

    End Function

    ''' <summary>
    ''' Rescale all the <see cref="Axis"/> objects in the list to the specified
    ''' <see cref="CurveCollection"/> by calling the <see cref="Axis.Rescale"/>
    ''' method of each <see cref="Axis"/> object.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="curves">        The curves. </param>
    ''' <param name="ignoreInitial"> True to ignore initial. </param>
    Public Sub Rescale(ByVal curves As CurveCollection, ByVal ignoreInitial As Boolean)

        ' validate argument.
        If curves Is Nothing Then
            Throw New ArgumentNullException(NameOf(curves))
        End If

        ' Get the scale range of the X-Y curves
        Dim xyRange As PlanarRangeR = curves.GetRange(ignoreInitial, False)

        ' Get the scale range of the X-Y2 curves
        Dim xy2Range As PlanarRangeR = curves.GetRange(ignoreInitial, True)

        ' set the X range to include both ranges.
        Dim xRange As RangeR = xyRange.X.ExtendedRange(xy2Range.X)

        ' Loop for each axis
        For Each item As Axis In Me
            ' Pick new scales based on the range
            Select Case item.AxisType
                Case AxisType.X
                    item.Rescale(xRange.Min, xRange.Max)
                Case AxisType.Y
                    item.Rescale(xyRange.Y.Min, xyRange.Y.Max)
                Case AxisType.Y2
                    item.Rescale(xy2Range.Y.Min, xy2Range.Y.Max)
                Case Else
                    Debug.Assert(Not Debugger.IsAttached, "Unhandled axis type")
            End Select
        Next item

    End Sub

    ''' <summary> Sets the screen ranges for rendering the <see cref="Axis"/>. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="axisArea"> The <see cref="System.Drawing.RectangleF"/> that that contains the
    '''                         area bounded by the axes. </param>
    Public Sub SetScreenRange(ByVal axisArea As RectangleF)

        ' Loop for each axis
        For Each axis As Axis In Me
            ' Render the axis
            axis.SetScreenRange(axisArea)
        Next axis

    End Sub

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
    Private ReadOnly _Pane As Pane

#End Region

End Class

