''' <summary> Handles coordinate scale type and values. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public Structure CoordinateScale

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="type"> The type. </param>
    Public Sub New(ByVal type As CoordinateScaleType)
        Me._CoordinateScaleType = type
    End Sub

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As CoordinateScale, ByVal right As CoordinateScale) As Boolean
        Return CoordinateScale.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As CoordinateScale, ByVal right As CoordinateScale) As Boolean
        Return Not CoordinateScale.Equals(left, right)
    End Operator

    ''' <summary> Returns True if equal. </summary>
    ''' <remarks>
    ''' The two Coordinate Scales are the same if the have the same
    ''' <see cref="Type">type</see>.
    ''' </remarks>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    Public Overloads Shared Function Equals(ByVal left As CoordinateScale, ByVal right As CoordinateScale) As Boolean
        Return left.Equals(right)
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="obj"> Another object to compare to. </param>
    ''' <returns>
    ''' <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso Me.GetType() Is obj.GetType() AndAlso Me.Equals(CType(obj, CoordinateScale))
    End Function

    ''' <summary>
    ''' Returns True if the value of the <paramref name="other"/> equals to the instance value.
    ''' </summary>
    ''' <remarks>
    ''' Coordinate Scales are the same if the have the same
    ''' <see cref="Type">type</see>.
    ''' </remarks>
    ''' <param name="other"> The <see cref="CoordinateScale">Coordinate Scale</see> to compare for
    '''                      equality with this instance. </param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    Public Overloads Function Equals(ByVal other As CoordinateScale) As Boolean
        Return Me.CoordinateScaleType.Equals(other.CoordinateScaleType)
    End Function

#End Region

    ''' <summary>
    ''' Returns the hash code for this <see cref="CoordinateScale"/> structure.  
    ''' In this case, the hash code is simply the equivalent hash code for the
    ''' <see cref="CoordinateScaleType.Linear"/> <see cref="Type"/>.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me._CoordinateScaleType.GetHashCode()
    End Function

    ''' <summary> Gets the <see cref="CoordinateScaleType"/> </summary>
    ''' <remarks> David, 10/15/07, 1.0.2844 Rename to CoordinateScaleType. </remarks>
    ''' <value> The type of the coordinate scale. </value>
    Public Property CoordinateScaleType() As CoordinateScaleType

    ''' <summary>
    ''' Determines if the <see cref="Scale"/> is date-time type.  To make this property True, set
    ''' <see cref="Type"/> to <see cref="CoordinateScaleType.Date"/>.
    ''' </summary>
    ''' <value> True for a date axis, False otherwise. </value>
    Public ReadOnly Property IsDate() As Boolean
        Get
            Return CoordinateScaleType.Date = Me.CoordinateScaleType
        End Get
    End Property

    ''' <summary>
    ''' Determines if the <see cref="Scale"/> is linear.  To make this property True, set
    ''' <see cref="Type"/> to <see cref="CoordinateScaleType.Log"/>.
    ''' </summary>
    ''' <value> True for a linear axis, False otherwise. </value>
    Public ReadOnly Property IsLinear() As Boolean
        Get
            Return CoordinateScaleType.Linear = Me.CoordinateScaleType
        End Get
    End Property

    ''' <summary>
    ''' Determines if the <see cref="Scale"/> is logarithmic (base 10).  To make this property True,
    ''' set <see cref="Type"/> to <see cref="CoordinateScaleType.Log"/>.
    ''' </summary>
    ''' <value> True for a logarithmic axis, False otherwise. </value>
    Public ReadOnly Property IsLog() As Boolean
        Get
            Return CoordinateScaleType.Log = Me.CoordinateScaleType
        End Get
    End Property

    ''' <summary>
    ''' Determines if the <see cref="Scale"/> is strip-chart type.  To make this property True, set
    ''' <see cref="Type"/> to <see cref="CoordinateScaleType.StripChart"/>.
    ''' </summary>
    ''' <value> True for a strip-chart axis, False otherwise. </value>
    Public ReadOnly Property IsStripChart() As Boolean
        Get
            Return CoordinateScaleType.StripChart = Me.CoordinateScaleType
        End Get
    End Property

    ''' <summary>
    ''' Returns true if the <see cref="Scale"/> is labeled with user provided text labels rather than
    ''' calculated numeric values.  The text labels are provided via the
    ''' <see cref="Type"/> property.  Internally, the axis is still handled with
    ''' ordinal values such that the axis <see cref="Axis.Min"/> is set to 1.0, and the axis
    ''' <see cref="Axis.Max"/> is set to the number of labels.  To make this property True,
    ''' set <see cref="Type"/> to <see cref="CoordinateScaleType.Text"/>.
    ''' </summary>
    ''' <value>
    ''' True for a text-based axis, False otherwise.  If this property is true, then an array of
    ''' labels must be provided via <see cref="Axis.TickLabels"/>.
    ''' </value>
    Public ReadOnly Property IsText() As Boolean
        Get
            Return CoordinateScaleType.Text = Me.CoordinateScaleType
        End Get
    End Property

End Structure

