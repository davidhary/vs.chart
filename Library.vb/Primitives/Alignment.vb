''' <summary> Provides alignment properties. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created. </para>
''' </remarks>
Public Structure Alignment

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs an instance of this class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="horizontal"> The <see cref="HorizontalAlignment"/>  setting. </param>
    ''' <param name="vertical">   <see cref="VerticalAlignment"/> setting. </param>
    Public Sub New(ByVal horizontal As HorizontalAlignment, ByVal vertical As VerticalAlignment)

        Me._Horizontal = horizontal
        Me._Vertical = vertical

    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As Alignment, ByVal right As Alignment) As Boolean
        Return Alignment.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As Alignment, ByVal right As Alignment) As Boolean
        Return Not Alignment.Equals(left, right)
    End Operator

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns>
    ''' <c>True</c> if values are the same type and represent the same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Shared Function Equals(ByVal left As Alignment, ByVal right As Alignment) As Boolean
        Return left.Equals(right)
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="obj"> Another object to compare to. </param>
    ''' <returns>
    ''' <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso Me.GetType() Is obj.GetType() AndAlso Me.Equals(CType(obj, Alignment))
    End Function

    ''' <summary>
    ''' Returns True if the value of the <paramref name="other"/> equals to the instance value.
    ''' </summary>
    ''' <remarks>
    ''' Alignments are the same if the have the same
    ''' <see cref="Horizontal"/> and <see cref="Horizontal"/> ranges.
    ''' </remarks>
    ''' <param name="other"> The <see cref="Alignment">Alignment</see> to compare for equality with
    '''                      this instance. </param>
    ''' <returns>
    ''' <c>True</c> if <paramref name="other" /> and this instance are the same type and represent
    ''' the same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Function Equals(ByVal other As Alignment) As Boolean
        Return Me._Horizontal.Equals(other._Horizontal) AndAlso Me._Vertical.Equals(other._Vertical)
    End Function

#End Region

#Region " METHODS "

    ''' <summary> Returns the hash code for this <see cref="Alignment"/> structure. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> An <see cref="T:System.integer">integer</see> value. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me._Horizontal.GetHashCode() Xor Me._Vertical.GetHashCode()
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets the horizontal alignment setting. </summary>
    ''' <value> A <see cref="HorizontalAlignment"/> value. </value>
    Public Property Horizontal() As HorizontalAlignment

    ''' <summary> Gets or sets the vertical alignment setting. </summary>
    ''' <value> A <see cref="VerticalAlignment"/> value. </value>
    Public Property Vertical() As VerticalAlignment

#End Region

End Structure

