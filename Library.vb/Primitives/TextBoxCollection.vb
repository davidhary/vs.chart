''' <summary> Contains a list of <see cref="TextBox"/> objects to display on the graph. </summary>
''' <remarks>
''' Declare <see cref="A:NotInheritable"/> so as to allow calling base methods in the
''' constructor. <para>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public NotInheritable Class TextBoxCollection
    Inherits System.Collections.ObjectModel.Collection(Of TextBox)
    Implements ICloneable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Default constructor for the <see cref="TextBoxCollection"/> collection class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are
    '''                                          null. </exception>
    ''' <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
    Public Sub New(ByVal drawingPane As Pane)
        MyBase.New()
        If drawingPane Is Nothing Then
            Throw New ArgumentNullException(NameOf(drawingPane))
        End If
        Me._Pane = drawingPane
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The TextBoxCollection object from which to copy. </param>
    Public Sub New(ByVal model As TextBoxCollection)
        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Dim item As isr.Visuals.TextBox
        For Each item In model
            Me.Add(New isr.Visuals.TextBox(item))
        Next item
        Me._Pane = model._Pane
    End Sub

#End Region

#Region " CUSTOM COLLECTION METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the TextBoxCollection. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the TextBoxCollection. </returns>
    Public Function Copy() As TextBoxCollection
        Return New TextBoxCollection(Me)
    End Function

    ''' <summary>
    ''' Renders all the <see cref="TextBox">Test Boxes</see> to the specified <see cref="graphics"/>
    ''' device.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        ' Clip everything to the panel area
        graphicsDevice.SetClip(Me._Pane.PaneArea)

        ' Loop for each curve
        For Each textBox As TextBox In Me
            textBox.Draw(graphicsDevice)
        Next textBox

    End Sub

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
    Private ReadOnly _Pane As Pane

#End Region

End Class

