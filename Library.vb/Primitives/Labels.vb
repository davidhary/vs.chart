Imports isr.Core.Constructs

''' <summary> Handles specification and drawing of axis value labels. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public Class Labels

    Implements ICloneable, IDisposable

    ' update:  with getSpace, allocated an area for the label and use the area to 
    ' display the labels aligned to the axis.
    ' add Tick Label class to handle generating tick labels.

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Default constructor for <see cref="Axis"/> Labels that sets all properties to default values
    ''' as defined in the <see cref="LabelsDefaults"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
    Public Sub New(ByVal drawingPane As Pane)

        MyBase.New()
        If drawingPane Is Nothing Then Throw New ArgumentNullException(NameOf(drawingPane))

        Dim labelDefaults As LabelsDefaults = LabelsDefaults.Get()
        Me._Appearance = New TextAppearance(labelDefaults.Font, labelDefaults.FontColor)
        Me._Appearance.Frame.Filled = labelDefaults.Filled
        Me._Appearance.Frame.IsOutline = labelDefaults.Framed
        Me._Appearance.Frame.Visible = labelDefaults.Framed Or labelDefaults.Filled
        Me._DecimalPlaces = labelDefaults.DecimalPlaces.Copy()
        Me._TextLabels = Array.Empty(Of String)()
        Me._Pane = drawingPane
        Me._ScaleFormat = labelDefaults.ScaleFormat
        Me._Visible = labelDefaults.Visible

    End Sub

    ''' <summary>
    ''' Default constructor for <see cref="Axis"/> Labels that sets all properties to default values
    ''' as defined in the <see cref="LabelsDefaults"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="textLabels">  The array of text labels. </param>
    ''' <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
    Public Sub New(ByVal textLabels() As String, ByVal drawingPane As Pane)

        Me.New(drawingPane)

        If textLabels Is Nothing Then
            Throw New ArgumentNullException(NameOf(textLabels))
        End If

        Me._TextLabels = textLabels

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The Labels object from which to copy. </param>
    Public Sub New(ByVal model As Labels)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Me._Appearance = model._Appearance.Copy
        Me._DecimalPlaces = model._DecimalPlaces
        Me._Visible = model._Visible
        Me._ScaleFormat = model._ScaleFormat
        If model._TextLabels IsNot Nothing Then
            Me._TextLabels = CType(model._TextLabels.Clone, String())
        End If
        Me._Pane = model._Pane

    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> True if disposed. </summary>
    Private _Disposed As Boolean

    ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._Disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._Disposed = value
        End Set
    End Property

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._TextLabels = Nothing
                    If Me._Appearance IsNot Nothing Then
                        Me._Appearance.Dispose()
                        Me._Appearance = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the Labels. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of Labels. </returns>
    Public Function Copy() As Labels
        Return New Labels(Me)
    End Function

    ''' <summary> Renders the specified tick label to the <see cref="graphics"/> device. </summary>
    ''' <remarks> This method centers the label vertical and horizontally. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="[text]">         A <see cref="System.String">String</see> value containing the
    '''                               text to be displayed.  This can be multiple lines, separated
    '''                               by new line ('\n')
    '''                               characters. </param>
    ''' <param name="x">              The X location to display the text, in screen coordinates,
    '''                               relative to the horizontal (<see cref="HorizontalAlignment"/>)
    '''                               alignment parameter. </param>
    ''' <param name="y">              The Y location to display the text, in screen coordinates,
    '''                               relative to the vertical (<see cref="VerticalAlignment"/>
    '''                               alignment parameter. </param>
    ''' <param name="scaleFactor">    The scaling factor to be used for rendering objects.  This is
    '''                               calculated and passed down by the parent
    '''                               <see cref="Pane"/> object using the
    '''                               <see cref="Pane.getScaleFactor"/>
    '''                               method, and is used to proportionally adjust font sizes, etc.
    '''                               according to the actual size of the graph. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal [text] As String,
                    ByVal x As Single, ByVal y As Single, ByVal scaleFactor As Double)

        If Not Me._Visible OrElse String.IsNullOrWhiteSpace(text) Then
            Return
        End If

        ' validate arguments.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        Me._Appearance.Draw(graphicsDevice, [text], x, y, HorizontalAlignment.Center, VerticalAlignment.Center, scaleFactor)

    End Sub

    ''' <summary> Draw the major tick labels. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="axis">           Reference to the <see cref="Axis"/> </param>
    ''' <param name="tick">           The tick. </param>
    ''' <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
    '''                               <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                               calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                               The scale factor is applied to fonts, symbols, etc. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal axis As Axis, ByVal tick As Tick, ByVal scaleFactor As Double)

        If Not Me._Visible Then
            Return
        End If

        ' validate arguments.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If
        If axis Is Nothing Then
            Throw New ArgumentNullException(NameOf(axis))
        End If
        If tick Is Nothing Then
            Throw New ArgumentNullException(NameOf(tick))
        End If

        Dim scaleMultplier As Double
        Dim textCenter As Single

        ' save the current clip bounds
        Dim clipBounds As RectangleF = graphicsDevice.ClipBounds

        If axis.CoordinateScale.IsStripChart Then

            ' clip to transformed axis area
            Dim labelArea As Rectangle = Rectangle.Round(axis.TransformedArea)
            'graphicsDevice.SetClip(axis.TransformedArea)
            labelArea.Offset(1, 0)
            labelArea.Inflate(-2, 0)
            graphicsDevice.SetClip(labelArea)

            ' get the Y position of the center of the axis labels
            ' (the axis itself is referenced at zero)
            textCenter = -0.75F * Me._Pane.AxisArea.Height
            scaleMultplier = 1

        Else
            ' get the Y position of the center of the axis labels
            ' (the axis itself is referenced at zero)
            textCenter = tick.GetScaledLength(scaleFactor) + Me.BoundingBox.Height / 2.0F
            scaleMultplier = Math.Pow(10.0R, axis.ScaleExponent.Value)

        End If

        ' loop for each tick
        For i As Integer = 0 To tick.ValidTickCount - 1

            ' draw the label
            Dim tmpStr As String = Me.GetTickLabel(i, tick.GetValue(i), axis.CoordinateScale, scaleMultplier)
            If Not String.IsNullOrWhiteSpace(tmpStr) Then
                Me.Draw(graphicsDevice, tmpStr, tick.GetLocation(i), 0.0F + textCenter, scaleFactor)
            End If

        Next

        ' reset the strip chart clipping area
        If axis.CoordinateScale.IsStripChart Then
            ' restore the clip bounds
            graphicsDevice.SetClip(clipBounds)
        End If

    End Sub

    ''' <summary> Returns a value label at the specified ordinal position. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="index">           The zero-based, ordinal index of the label.  For example, a
    '''                                value of 2 returns the third value label. </param>
    ''' <param name="tickValue">       The numeric value associated with the label.  This value is
    '''                                ignored for log (<see cref="Coordinatescale.IsLog"/>) and
    '''                                text (<see cref="Coordinatescale.IsText"/>) types. </param>
    ''' <param name="coordinateScale"> The <see cref="isr.Visuals.CoordinateScale"/> for the axis
    '''                                <see cref="Scale"/> </param>
    ''' <param name="scaleExponent">   The <see cref="Scale.ScaleExponent"/> value. </param>
    ''' <returns> The tick label. </returns>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Function GetTickLabel(ByVal index As Integer, ByVal tickValue As Double,
                                  ByVal coordinateScale As CoordinateScale, ByVal scaleExponent As Integer) As String

        Return Me.GetTickLabel(index, tickValue, coordinateScale, Math.Pow(10.0R, scaleExponent))

    End Function

    ''' <summary> Returns a value label at the specified ordinal position. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="index">           The zero-based, ordinal index of the label.  For example, a
    '''                                value of 2 returns the third value label. </param>
    ''' <param name="tickValue">       The numeric value associated with the label.  This value is
    '''                                ignored for log (<see cref="Coordinatescale.IsLog"/>) and
    '''                                text (<see cref="Coordinatescale.IsText"/>) types. </param>
    ''' <param name="coordinateScale"> The <see cref="isr.Visuals.CoordinateScale"/> for the axis
    '''                                <see cref="Scale"/> </param>
    ''' <param name="scaleMultiplier"> The 10^<see cref="Scale.ScaleExponent"/> value. </param>
    ''' <returns> The tick label. </returns>
    Private Function GetTickLabel(ByVal index As Integer, ByVal tickValue As Double,
                                  ByVal coordinateScale As CoordinateScale, ByVal scaleMultiplier As Double) As String

        Select Case coordinateScale.CoordinateScaleType

            Case CoordinateScaleType.Date

                Return JulianDate.ToString(tickValue, Me._ScaleFormat)

            Case CoordinateScaleType.Linear

                Dim tmpStr As String = "{0:F*}"
                tmpStr = tmpStr.Replace("*", Me._DecimalPlaces.ToString("D"))
                Return String.Format(Globalization.CultureInfo.CurrentCulture, tmpStr, tickValue / scaleMultiplier)

            Case CoordinateScaleType.Log

                Return If(index >= -3 AndAlso index <= 4, Math.Pow(10.0, index).ToString, $"1e{index}")

            Case CoordinateScaleType.StripChart

                Dim timeValue As DateTime = TimeSeriesPointR.GetDateTime(tickValue)
                Return timeValue.ToString(Me._ScaleFormat, Globalization.CultureInfo.CurrentCulture)

            Case CoordinateScaleType.Text

                Return If(Me._TextLabels Is Nothing OrElse index < 0 OrElse index >= Me._TextLabels.Length, String.Empty, Me._TextLabels(index))

            Case Else

                Debug.Assert(Not Debugger.IsAttached, "Unhandled coordinate scale type")
                Return String.Empty

        End Select

    End Function

    ''' <summary> Set the decimal places based on the <see paramref="CoordinateScale"/>. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="axis">            Reference to the <see cref="Axis"/> </param>
    ''' <param name="baseValue">       The value to use to figuring auto decimal places. </param>
    ''' <param name="scaleExponent">   The <see cref="Scale.ScaleExponent"/> value. </param>
    ''' <param name="coordinateScale"> The <see cref="isr.Visuals.CoordinateScale"/> for the axis
    '''                                <see cref="Scale"/> </param>
    Friend Sub SetDecimalPlaces(ByVal axis As Axis, ByVal baseValue As Double, ByVal scaleExponent As Integer,
                                ByVal coordinateScale As CoordinateScale)

        Select Case coordinateScale.CoordinateScaleType

            Case CoordinateScaleType.Date

                ' Date Scale.  the number of decimal places to display is not used
                Me._DecimalPlaces.Value = 0

            Case CoordinateScaleType.Linear

                ' Calculate the appropriate number of decimal places to display if required
                If Me.DecimalPlaces.AutoScale Then
                    Me._DecimalPlaces.Value = Math.Max(axis.DecimalPlaces, baseValue.DecimalPlaces - scaleExponent)
                End If

            Case CoordinateScaleType.Log

                ' Log Scale
                If Me._DecimalPlaces.AutoScale Then
                    ' The number of decimal places to display is not used
                    Me._DecimalPlaces.Value = 0
                End If

            Case CoordinateScaleType.StripChart

                ' if this is a strip-chart axis, then ignore as we have ordinal setting
                Me._DecimalPlaces.Value = 0

            Case CoordinateScaleType.Text

                ' if this is a text-based axis, then ignore as we have ordinal setting
                Me._DecimalPlaces.Value = 0

            Case Else

                Debug.Assert(Not Debugger.IsAttached, "Unhandled coordinate scale type")

        End Select

    End Sub

    ''' <summary>
    ''' Set the bounding box fitting the maximum size of the scale value text that is required to
    ''' label the <see cref="Axis"/>. Use <see cref="P:BoundingBox">Bounding Box</see>
    ''' to determine how much space is required for the axis labels.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="axis">           Reference to the <see cref="Axis"/> </param>
    ''' <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
    '''                               <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                               calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                               The scale factor is applied to fonts, symbols, etc. </param>
    Friend Sub SetBoundingBox(ByVal graphicsDevice As Graphics, ByVal axis As Axis, ByVal scaleFactor As Double)

        If axis Is Nothing Then Throw New ArgumentNullException(NameOf(axis))

        Dim scaleMultplier As Double = Math.Pow(10.0R, axis.ScaleExponent.Value)
        Me._BoundingBox = New SizeF(0, 0)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        ' loop for each major tick
        For i As Integer = 0 To axis.MajorTick.TickCount - 1

            ' estimate the label size
            Dim tmpStr As String = Me.GetTickLabel(i, axis.MajorTick.GetValue(i), axis.CoordinateScale, scaleMultplier)
            Dim sizeF As SizeF = Me.Appearance.BoundingBox(graphicsDevice, tmpStr, scaleFactor)
            If sizeF.Height > Me.BoundingBox.Height Then
                Me._BoundingBox.Height = sizeF.Height
            End If
            If sizeF.Width > Me.BoundingBox.Width Then
                Me._BoundingBox.Width = sizeF.Width
            End If

        Next i

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> The appearance. </summary>
    Private _Appearance As TextAppearance

    ''' <summary>
    ''' gets a reference to the <see cref="isr.Visuals.TextAppearance">TextAppearance</see> class
    ''' used to render the axis value labels.
    ''' </summary>
    ''' <value> A <see cref="isr.Visuals.TextAppearance">TextAppearance</see> instance. </value>
    Public ReadOnly Property Appearance() As TextAppearance
        Get
            Return Me._Appearance
        End Get
    End Property

    ''' <summary> The bounding box. </summary>
    Private _BoundingBox As SizeF = New SizeF(0, 0)

    ''' <summary>
    ''' Gets a <see cref="System.Drawing.SizeF">size</see> structure representing the width and
    ''' height of the bounding box for the axis label.
    ''' </summary>
    ''' <value> The bounding box. </value>
    Public ReadOnly Property BoundingBox() As SizeF
        Get
            Return Me._BoundingBox
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the <see cref="AutoValue"/> for the number of decimal places displayed for axis
    ''' value labels.  The value can be determined automatically depending on
    ''' <see cref="AutoValue.AutoScale"/>.
    ''' </summary>
    ''' <value> The number of decimal places to be displayed for axis value labels. </value>
    Public Property DecimalPlaces() As AutoValue

    ''' <summary> Determines if the value <see cref="Labels"/> will be drawn. </summary>
    ''' <value> A <see cref="System.Boolean">Boolean</see> </value>
    Public Property Visible() As Boolean

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
    Private ReadOnly _Pane As Pane

    ''' <summary>
    ''' The format of the <see cref="Tick"/> labels. This field is only used if the
    ''' <see cref="Axis.CoordinateScale"/> is set to <see cref="CoordinateScaleType.Date"/>.
    ''' </summary>
    ''' <value>
    ''' A <see cref="System.String"/> as defined for the <see cref="JulianDate.ToString"/> function.
    ''' </value>
    Public Property ScaleFormat() As String

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> A System.String value. </value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

    ''' <summary> The text labels. </summary>
    Private _TextLabels As String()

    ''' <summary>
    ''' Gets or sets the <see cref="Axis"/> text labels for a
    ''' <see cref="CoordinateScaleType.Text"/> axis.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A String() </returns>
    Public Function TextLabels() As String()
        Return Me._TextLabels
    End Function

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="Labels"/> class that defines the default property values
''' for the <see cref="Labels"/> class.
''' </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public NotInheritable Class LabelsDefaults
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs this class. This constructor is private to ensure only a single instance of this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub New()
        MyBase.New()
        Me._Visible = True
        Me._DecimalPlaces = New AutoValue(0, True)
        Me._Font = New Font("Arial", 12, FontStyle.Regular)
        Me._FontColor = Color.Black
        Me._Filled = False
        Me._Framed = False
        Me._ScaleFormat = "&dd-&mmm-&yy &hh:&nn"
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _Instance As LabelsDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As LabelsDefaults
        If LabelsDefaults._Instance Is Nothing OrElse LabelsDefaults._Instance.IsDisposed Then
            SyncLock LabelsDefaults.SyncLocker
                LabelsDefaults._Instance = New LabelsDefaults()
            End SyncLock
        End If
        Return LabelsDefaults._Instance
    End Function

#Region "IDisposable Support"

    ''' <summary> Gets or sets a value indicating whether this instance is disposed. </summary>
    ''' <value> <c>True</c> if this instance is disposed; otherwise, <c>False</c>. </value>
    Private Property IsDisposed As Boolean ' To detect redundant calls

    ''' <summary> Releases unmanaged and - optionally - managed resources. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                          <c>False</c> to release only unmanaged resources. </param>
    Private Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    ' dispose managed state (managed objects).
                    If Me._Font IsNot Nothing Then
                        Me._Font.Dispose()
                        Me._Font = Nothing
                    End If
                End If
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

#End Region

    ''' <summary>
    ''' Gets or sets the default display mode for the axis value <see cref="Labels"/>
    ''' (<see cref="Labels.Visible"/> property). True to show the labels, False to hide them.
    ''' </summary>
    ''' <value> The visible. </value>
    Public Property Visible() As Boolean

    ''' <summary>
    ''' Gets or sets the default decimal places for the axis value <see cref="Labels"/>
    ''' (<see cref="Labels.DecimalPlaces"/> property).
    ''' </summary>
    ''' <value> The decimal places. </value>
    Public Property DecimalPlaces() As AutoValue

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Font">Font</see>
    ''' of the <see cref="Title"/> caption.
    ''' </summary>
    ''' <value> The font. </value>
    Public Property Font() As Font

    ''' <summary>
    ''' Gets or sets the default font color for the <see cref="Legend"/> entries
    ''' (<see cref="TextAppearance.FontColor"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property FontColor() As Color

    ''' <summary>
    ''' Gets or sets the default font filled mode for the <see cref="Axis"/> scale value appearance
    ''' <see cref="TextAppearance.Frame"/>. True for filled, False otherwise.
    ''' </summary>
    ''' <value> The filled. </value>
    Public Property Filled() As Boolean

    ''' <summary>
    ''' Gets or sets the default font framed mode for the <see cref="Axis"/> scale value appearance
    ''' <see cref="Labels.Appearance"/>
    ''' (<see cref="TextAppearance.Frame"/> property). True for framed, False otherwise.
    ''' </summary>
    ''' <value> The framed. </value>
    Public Property Framed() As Boolean

    ''' <summary>
    ''' Gets or sets the default setting for the <see cref="Axis"/> scale date format string
    ''' (<see cref="Tick.ScaleFormat"/> property).  This value is set as per the
    ''' <see cref="JulianDate.ToString"/> function.
    ''' </summary>
    ''' <value> The scale format. </value>
    Public Property ScaleFormat() As String

End Class

#End Region

