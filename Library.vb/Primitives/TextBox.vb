''' <summary>
''' A class that represents a text object on the graph.  A list of
''' <see cref="TextBox"/> objects is maintained by the <see cref="TextBoxCollection"/>
''' collection class.
''' </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class TextBox
    Implements ICloneable, IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initialize a <see cref="TextBox" /> with properties from the
    ''' <see cref="TextBoxDefaults" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
    Public Sub New(ByVal drawingPane As Pane)

        MyBase.New()
        If drawingPane Is Nothing Then
            Throw New ArgumentNullException(NameOf(drawingPane))
        End If

        Me._Text = "Text"
        Me._Location = New PointF(0, 0)
        Dim textBoxDefaults As TextBoxDefaults = TextBoxDefaults.Get()
        Me._Alignment = New Alignment(textBoxDefaults.AlignH, textBoxDefaults.AlignV)
        Me._CoordinateFrame = textBoxDefaults.CoordinateFrame
        Me._Appearance = New TextAppearance(textBoxDefaults.Font, textBoxDefaults.FontColor)
        Me._Pane = drawingPane

    End Sub

    ''' <summary>
    ''' Constructs a <see cref="TextBox"/> with specific values and default properties from the
    ''' <see cref="TextBoxDefaults"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="text">        The <see cref="TextBox"/> to be displayed.  This text can be
    '''                            multi-line by including new line ('\n') characters between the
    '''                            lines. </param>
    ''' <param name="x">           The x position of the text.  Units are based on the
    '''                            <see cref="CoordinateFrame"/> property.  The text will be aligned
    '''                            to this position based
    '''                            on the <see cref="Alignment.Horizontal"/> property. </param>
    ''' <param name="y">           The y position of the text.  Units are specified by the
    '''                            <see cref="CoordinateFrame"/> property.  The text will be aligned
    '''                            to this position based
    '''                            on the <see cref="Alignment.Vertical"/> property. </param>
    ''' <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
    Public Sub New(ByVal text As String, ByVal x As Single, ByVal y As Single, ByVal drawingPane As Pane)

        Me.New(drawingPane)
        If String.IsNullOrWhiteSpace(text) Then
            text = String.Empty
        End If
        Me._Text = [text]
        Me._Location = New PointF(x, y)

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The isr.Visuals.TextBox object from which to copy. </param>
    Public Sub New(ByVal model As isr.Visuals.TextBox)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Me._Text = model._Text
        Me._Alignment = model._Alignment
        Me._Location = model._Location
        Me._CoordinateFrame = model._CoordinateFrame
        Me._Appearance = model._Appearance.Copy()
        Me._Pane = model._Pane

    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> True if disposed. </summary>
    Private _Disposed As Boolean

    ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._Disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._Disposed = value
        End Set
    End Property

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._Alignment = Nothing
                    If Me._Appearance IsNot Nothing Then
                        Me._Appearance.Dispose()
                        Me._Appearance = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the isr.Visuals.TextBox. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the isr.Visuals.TextBox. </returns>
    Public Function Copy() As isr.Visuals.TextBox
        Return New isr.Visuals.TextBox(Me)
    End Function

    ''' <summary>
    ''' Renders this <see cref="TextBox"/> object to the specified
    ''' <see cref="graphics"/> device. This method is normally only called by
    ''' the Draw method of the parent <see cref="TextBoxCollection"/> collection object.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        Dim screenLocation As PointF = Me._Pane.GeneralTransform(Me._Location, Me._CoordinateFrame)

        If Me._Pane.DrawArea.Contains(screenLocation) Then

            ' Draw the text on the screen, including any frame and background fill elements
            Me._Appearance.Draw(graphicsDevice, Me._Text, screenLocation.X, screenLocation.Y,
                             Me._Alignment.Horizontal, Me._Alignment.Vertical, Me._Pane.ScaleFactor)

        Else

            Debug.Assert(Not Debugger.IsAttached, "text box location out of draw area")

        End If

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Alignment for this <see cref="TextBox"/> specified using the <see cref="Alignment"/> type.
    ''' </summary>
    ''' <value> The alignment. </value>
    Public Property Alignment() As isr.Visuals.Alignment

    ''' <summary> The appearance. </summary>
    Private _Appearance As TextAppearance

    ''' <summary>
    ''' gets a reference to the <see cref="isr.Visuals.TextAppearance">TextAppearance</see> class
    ''' used to render this <see cref="TextBox"/>
    ''' </summary>
    ''' <value> A <see cref="isr.Visuals.TextAppearance">TextAppearance</see> instance. </value>
    Public ReadOnly Property Appearance() As TextAppearance
        Get
            Return Me._Appearance
        End Get
    End Property

    ''' <summary>
    ''' The location of the <see cref="TextBox"/>.  Units are based on the
    ''' <see cref="CoordinateFrame"/> property.  Text will be aligned to this location based on the
    ''' <see cref="Alignment"/> property.
    ''' </summary>
    ''' <value> The location. </value>
    Public Property Location() As PointF

    ''' <summary>
    ''' The coordinate system to be used for defining the <see cref="TextBox"/> position.
    ''' </summary>
    ''' <value> A <see cref="CoordinateFrameType"/> </value>
    Public Property CoordinateFrame() As CoordinateFrameType

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
    Private ReadOnly _Pane As Pane

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> A System.String value. </value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

    ''' <summary>The <see cref="TextBox"/> to be displayed.  This text can be multi-line by
    ''' including new line ('\n') characters between the lines.</summary>
    Public Property [Text]() As String

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="TextBox"/> class that defines the default property values
''' for the <see cref="TextBox"/> class.
''' </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public NotInheritable Class TextBoxDefaults
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs this class. This constructor is private to ensure only a single instance of this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub New()
        MyBase.New()
        Me._AlignV = isr.Visuals.VerticalAlignment.Center
        Me._AlignH = isr.Visuals.HorizontalAlignment.Center
        Me._CoordinateFrame = CoordinateFrameType.AxisXYScale
        Me._Font = New Font("Arial", 14, FontStyle.Regular Or FontStyle.Bold)
        Me._FontColor = Color.Black
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _Instance As TextBoxDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As TextBoxDefaults
        If TextBoxDefaults._Instance Is Nothing OrElse TextBoxDefaults._Instance.IsDisposed Then
            SyncLock TextBoxDefaults.SyncLocker
                TextBoxDefaults._Instance = New TextBoxDefaults()
            End SyncLock
        End If
        Return TextBoxDefaults._Instance
    End Function

#Region "IDisposable Support"

    ''' <summary> Gets or sets a value indicating whether this instance is disposed. </summary>
    ''' <value> <c>True</c> if this instance is disposed; otherwise, <c>False</c>. </value>
    Private Property IsDisposed As Boolean ' To detect redundant calls

    ''' <summary> Releases unmanaged and - optionally - managed resources. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                          <c>False</c> to release only unmanaged resources. </param>
    Private Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    ' dispose managed state (managed objects).
                    If Me._Font IsNot Nothing Then
                        Me._Font.Dispose()
                        Me._Font = Nothing
                    End If
                End If
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

#End Region

    ''' <summary>
    ''' Default value for the vertical <see cref="TextBox"/>
    ''' text alignment (<see cref="Alignment.Vertical"/> property).
    ''' </summary>
    ''' <value> A  <see cref="VerticalAlignment"/> value. </value>
    Public Property AlignV() As VerticalAlignment

    ''' <summary>
    ''' Default value for the horizontal <see cref="TextBox"/>
    ''' text alignment (<see cref="Alignment.Horizontal"/> property).
    ''' </summary>
    ''' <value> A  <see cref="HorizontalAlignment"/> value. </value>
    Public Property AlignH() As HorizontalAlignment

    ''' <summary>
    ''' Gets or sets the default coordinate system to be used for defining the
    ''' <see cref="TextBox"/> location coordinates
    ''' (<see cref="TextBox.CoordinateFrame"/> property).
    ''' </summary>
    ''' <value> A <see cref="CoordinateFrameType"/> </value>
    Public Property CoordinateFrame() As CoordinateFrameType

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Font">Font</see>
    ''' of the <see cref="Title"/> caption.
    ''' </summary>
    ''' <value> The font. </value>
    Public Property Font() As Font

    ''' <summary>
    ''' Gets or sets the default font color for the <see cref="Legend"/> entries
    ''' (<see cref="TextAppearance.FontColor"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property FontColor() As Color

End Class

#End Region

