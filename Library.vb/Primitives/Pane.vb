''' <summary>
''' An abstract base class required to define a charts and graphs.  This class is inherited by
''' the <see cref="Pane"/> class to define specific characteristics for those charts or graph
''' types.
''' </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581.x. </para>
''' </remarks>
Public MustInherit Class Pane
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs a <see cref="Pane"/> with default area. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Protected Sub New()

        Me.New(New RectangleF(0, 0, 1, 1))

    End Sub

    ''' <summary>
    ''' Constructs a <see cref="Pane"/> with specified and default values as defined in the
    ''' <see cref="PaneDefaults"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="chartArea"> A rectangular screen area where the chart is to be displayed. This
    '''                          area can be any size, and can be resize at any time using the
    '''                          <see cref="PaneArea"/> property. </param>
    Protected Sub New(ByVal chartArea As RectangleF)

        MyBase.New()

        Me._PaneArea = chartArea
        Me._Axes = New AxisCollection(Me)
        Me._Legend = New Legend(Me)
        Me._Curves = New CurveCollection(Me)
        Me._TextBoxes = New TextBoxCollection(Me)
        Me._Arrows = New ArrowCollection

        Me._Title = New Title(String.Empty, Me)

        Me._IgnoreInitial = ScaleDefaults.[Get].IgnoreInitial
        Dim paneDeaults As PaneDefaults = PaneDefaults.[Get]
        Me._Visible = paneDeaults.Filled Or paneDeaults.Framed
        Me._PaneFrame = New Frame With {
            .IsOutline = paneDeaults.Framed,
            .Filled = paneDeaults.Filled,
            .Visible = paneDeaults.Filled Or paneDeaults.Framed,
            .LineColor = paneDeaults.FrameLineColor,
            .LineWidth = paneDeaults.FrameLineWidth,
            .FillColor = paneDeaults.FrameFillColor
        }

        Me._AxisFrame = New Frame With {
            .IsOutline = paneDeaults.Framed,
            .Filled = paneDeaults.Filled,
            .Visible = paneDeaults.Filled Or paneDeaults.Framed,
            .LineColor = paneDeaults.FrameLineColor,
            .LineWidth = paneDeaults.FrameLineWidth,
            .FillColor = paneDeaults.FrameFillColor
        }

        Me._BaseDimension = paneDeaults.BaseDimension
        Me._InnerGap = paneDeaults.InnerGap
        Me._OuterGap = paneDeaults.OuterGap
        Me._DrawAreaMargins = New MarginsF(Me._InnerGap, Me._InnerGap, Me._InnerGap, Me._InnerGap)

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The Pane object from which to copy. </param>
    Protected Sub New(ByVal model As Pane)

        Me.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If

        Me._Visible = model._Visible
        Me._PaneArea = model._PaneArea
        Me._Axes = model._Axes.Copy()
        Me._Legend = model._Legend.Copy()
        Me._Curves = model._Curves.Copy()
        Me._TextBoxes = model._TextBoxes.Copy()
        Me._Arrows = model._Arrows.Copy()

        Me._Title = model._Title.Copy()
        Me._PaneFrame = model._PaneFrame.Copy()
        Me._AxisFrame = model._AxisFrame.Copy()

        Me._IgnoreInitial = model._IgnoreInitial
        Me._BaseDimension = model._BaseDimension
        Me._InnerGap = model._InnerGap
        Me._OuterGap = model._OuterGap

    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> True if disposed. </summary>
    Private _Disposed As Boolean

    ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._Disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._Disposed = value
        End Set
    End Property

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._Arrows = Nothing
                    Me._Axes = Nothing
                    Me._Curves = Nothing
                    Me._TextBoxes = Nothing
                    If Me._AxisFrame IsNot Nothing Then
                        Me._AxisFrame.Dispose()
                        Me._AxisFrame = Nothing
                    End If
                    If Me._Legend IsNot Nothing Then
                        Me._Legend.Dispose()
                        Me._Legend = Nothing
                    End If
                    If Me._PaneFrame IsNot Nothing Then
                        Me._PaneFrame.Dispose()
                        Me._PaneFrame = Nothing
                    End If
                    If Me._Title IsNot Nothing Then
                        Me._Title.Dispose()
                        Me._Title = Nothing
                    End If
                    If Me._XAxis IsNot Nothing Then
                        Me._XAxis.Dispose()
                        Me._XAxis = Nothing
                    End If
                    If Me._YAxis IsNot Nothing Then
                        Me._YAxis.Dispose()
                        Me._YAxis = Nothing
                    End If
                    If Me._Y2Axis IsNot Nothing Then
                        Me._Y2Axis.Dispose()
                        Me._Y2Axis = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Add an <see cref="isr.Visuals.Arrow">Arrow</see> to the plot. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="lineColor">     The line <see cref="System.Drawing.Color">Color</see> for the
    '''                              arrow. </param>
    ''' <param name="arrowheadSize"> The size of the arrowhead, measured in points. </param>
    ''' <param name="x1">            The x position of the starting point that defines the arrow.  The
    '''                              units of this position are specified by the
    '''                              <see cref="CoordinateScale"/> property. </param>
    ''' <param name="y1">            The y position of the starting point that defines the arrow.  The
    '''                              units of this position are specified by the
    '''                              <see cref="CoordinateScale"/> property. </param>
    ''' <param name="x2">            The x position of the ending point that defines the arrow.  The
    '''                              units of this position are specified by the
    '''                              <see cref="CoordinateScale"/> property. </param>
    ''' <param name="y2">            The y position of the ending point that defines the arrow.  The
    '''                              units of this position are specified by the
    '''                              <see cref="CoordinateScale"/> property. </param>
    ''' <returns> A reference to the new <see cref="isr.Visuals.Arrow">Arrow</see>. </returns>
    Public Function AddArrow(ByVal lineColor As Color, ByVal arrowheadSize As Single,
                             ByVal x1 As Single, ByVal y1 As Single,
                             ByVal x2 As Single, ByVal y2 As Single) As Arrow

        Dim arrow As New Arrow(lineColor, arrowheadSize, x1, y1, x2, y2, Me)
        Me._Arrows.Add(arrow)

        Return arrow

    End Function

    ''' <summary> Add an (<see cref="Axis">Axis</see> object to the chart. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="caption">  The <see cref="Axis.Title"/>. </param>
    ''' <param name="axisType"> Specifies the <see cref="AxisType"/> </param>
    ''' <returns>
    ''' A reference to the newly created <see cref="Axis"/>. This can then be used to access all of
    ''' the axis properties that are not defined as arguments to the <see cref="AddAxis"/> method.
    ''' </returns>
    Public Function AddAxis(ByVal caption As String, ByVal axisType As AxisType) As Axis

        ' validate arguments.
        If String.IsNullOrWhiteSpace(caption) Then
            caption = String.Empty
        End If

        ' remember the reference to the axis for transformations
        Select Case axisType
            Case AxisType.X
                Me._XAxis = New Axis(caption, axisType, Me)
                Me._Axes.Add(Me._XAxis)
            Case AxisType.Y
                Me._YAxis = New Axis(caption, axisType, Me)
                Me._Axes.Add(Me._YAxis)
            Case AxisType.Y2
                Me._Y2Axis = New Axis(caption, axisType, Me)
                Me._Axes.Add(Me._Y2Axis)
            Case Else
                Debug.Assert(Not Debugger.IsAttached, "Unhandled axis type")
        End Select
        Return Me._Axes(Me._Axes.Count - 1)

    End Function

    ''' <summary>
    ''' Add a curve (<see cref="Curve"/> object) to the plot with the reference to the given axes.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="type">  A <see cref="CurveType">Curve Type</see> value. </param>
    ''' <param name="label"> A <see cref="System.String">String</see> label (legend entry) for this
    '''                      curve. </param>
    ''' <param name="xAxis"> Reference to the X <see cref="Axis"/> </param>
    ''' <param name="yAxis"> Reference to the Y <see cref="Axis"/> </param>
    ''' <returns>
    ''' A <see cref="Curve"/> class for the newly created curve. This can then be used to access all
    ''' of the curve properties that are not defined as arguments to the <see cref="AddCurve"/>
    ''' method.
    ''' </returns>
    Public Function AddCurve(ByVal type As CurveType, ByVal label As String, ByVal xAxis As Axis, ByVal yAxis As Axis) As Curve

        If String.IsNullOrWhiteSpace(label) Then label = String.Empty
        If xAxis Is Nothing Then Throw New ArgumentNullException(NameOf(xAxis))
        If yAxis Is Nothing Then Throw New ArgumentNullException(NameOf(yAxis))

        Dim curve As New Curve(type, label, xAxis, yAxis, Me)
        Me._Curves.Add(curve)

        Return curve

    End Function

    ''' <summary>
    ''' Add a curve (<see cref="Curve"/> object) to the plot with the given properties.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="type">        A <see cref="CurveType">Curve Type</see> value. </param>
    ''' <param name="label">       The text label (string) for the curve that will be used as a
    '''                            <see cref="Legend"/> entry. </param>
    ''' <param name="lineColor">   The color to used for the curve line, symbols, etc. </param>
    ''' <param name="symbolShape"> A symbol shape (<see cref="ShapeType"/>)
    '''                            that will be used for this curve. </param>
    ''' <param name="xAxis">       Reference to the X <see cref="Axis"/> </param>
    ''' <param name="yAxis">       Reference to the Y <see cref="Axis"/> </param>
    ''' <returns>
    ''' A <see cref="Curve"/> class for the newly created curve. This can then be used to access all
    ''' of the curve properties that are not defined as arguments to the <see cref="AddCurve"/>
    ''' method.
    ''' </returns>
    Public Function AddCurve(ByVal type As CurveType, ByVal label As String,
                             ByVal lineColor As Color, ByVal symbolShape As ShapeType,
                             ByVal xAxis As Axis, ByVal yAxis As Axis) As Curve

        If String.IsNullOrWhiteSpace(label) Then label = String.Empty
        If xAxis Is Nothing Then Throw New ArgumentNullException(NameOf(xAxis))
        If yAxis Is Nothing Then Throw New ArgumentNullException(NameOf(yAxis))

        Dim curve As New Curve(type, label, xAxis, yAxis, Me)
        curve.Cord.LineColor = lineColor
        curve.Symbol.LineColor = lineColor
        curve.Symbol.Shape = symbolShape
        Me._Curves.Add(curve)

        Return curve

    End Function

    ''' <summary>
    ''' Add a curve (<see cref="Curve"/> object) to the plot with the given data points and
    ''' properties.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="label">       The text label (string) for the curve that will be used as a
    '''                            <see cref="Legend"/> entry. </param>
    ''' <param name="x">           An array of <see cref="System.Double">Double Precision</see>
    '''                            X values (the independent values) that define the curve. </param>
    ''' <param name="y">           An array of <see cref="System.Double">Double Precision</see>
    '''                            Y values (the dependent values) that define the curve. </param>
    ''' <param name="lineColor">   The color to used for the curve line, symbols, etc. </param>
    ''' <param name="symbolShape"> A symbol shape (<see cref="ShapeType"/>)
    '''                            that will be used for this curve. </param>
    ''' <param name="xAxis">       Reference to the X <see cref="Axis"/> </param>
    ''' <param name="yAxis">       Reference to the Y <see cref="Axis"/> </param>
    ''' <returns>
    ''' A <see cref="Curve"/> class for the newly created curve. This can then be used to access all
    ''' of the curve properties that are not defined as arguments to the <see cref="AddCurve"/>
    ''' method.
    ''' </returns>
    Public Function AddCurve(ByVal label As String, ByVal x() As Double, ByVal y() As Double,
                             ByVal lineColor As Color, ByVal symbolShape As ShapeType,
                             ByVal xAxis As Axis, ByVal yAxis As Axis) As Curve

        If x Is Nothing Then Throw New ArgumentNullException(NameOf(x))
        If y Is Nothing Then Throw New ArgumentNullException(NameOf(y))
        If String.IsNullOrWhiteSpace(label) Then label = String.Empty
        If xAxis Is Nothing Then Throw New ArgumentNullException(NameOf(xAxis))
        If yAxis Is Nothing Then Throw New ArgumentNullException(NameOf(yAxis))

        Dim curve As New Curve(label, x, y, xAxis, yAxis, Me)
        curve.Cord.LineColor = lineColor
        curve.Symbol.LineColor = lineColor
        curve.Symbol.Shape = symbolShape
        Me._Curves.Add(curve)

        Return curve

    End Function

    ''' <summary> Add a <see cref="isr.Visuals.TextBox">Text Box</see> to the plot. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="[text]"> Test string. </param>
    ''' <param name="x">      The x position of the text.  Units are based on the
    '''                       <see cref="CoordinateScale"/> property.  The text will be aligned to
    '''                       this position based
    '''                       on the <see cref="Alignment.Horizontal"/> property. </param>
    ''' <param name="y">      The y position of the text.  Units are specified by the
    '''                       <see cref="CoordinateScale"/> property.  The text will be aligned to
    '''                       this position based
    '''                       on the <see cref="Alignment.Vertical"/> property. </param>
    ''' <returns> A TextBox. </returns>
    Public Function AddTextBox(ByVal [text] As String, ByVal x As Single, ByVal y As Single) As TextBox

        If String.IsNullOrWhiteSpace(text) Then text = String.Empty

        Dim textBox As New isr.Visuals.TextBox([text], x, y, Me)
        Me._TextBoxes.Add(textBox)
        Return textBox

    End Function

    ''' <summary>
    ''' Draws all elements in the <see cref="Pane"/> to the specified graphics device.  This routine
    ''' should be overridden by the inheriting classes.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to draw into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    Public MustOverride Sub Draw(ByVal graphicsDevice As Graphics)

    ''' <summary>
    ''' Draw the frame border around the <see cref="PaneArea"/> and
    ''' <see cref="Pane.AxisArea"/>.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    Protected Sub DrawFrames(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        ' Clip everything to the PaneArea
        graphicsDevice.SetClip(Me._PaneArea)

        Me._PaneFrame.Draw(graphicsDevice, Me._PaneArea)

        Me._AxisFrame.Draw(graphicsDevice, Me._AxisArea)

    End Sub

    ''' <summary>
    ''' Transform a data point from the specified coordinate type (<see cref="CoordinateFrameType"/>)
    ''' to screen coordinates (pixels).
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="userPoint">       The X,Y pair that defines the point in user coordinates. </param>
    ''' <param name="coordinateFrame"> A <see cref="CoordinateFrameType"/> type that defines the
    '''                                coordinate system in which the X,Y pair is defined. </param>
    ''' <returns>
    ''' A point in screen coordinates that corresponds to the specified <see paramref="userPoint"/>.
    ''' </returns>
    Friend Function GeneralTransform(ByVal userPoint As PointF, ByVal coordinateFrame As CoordinateFrameType) As PointF

        Dim screenPoint As New PointF

        If coordinateFrame = CoordinateFrameType.AxisFraction Then
            screenPoint.X = Me._AxisArea.Left + userPoint.X * Me._AxisArea.Width
            screenPoint.Y = Me._AxisArea.Top + userPoint.Y * Me._AxisArea.Height
        ElseIf coordinateFrame = CoordinateFrameType.AxisXYScale Then
            screenPoint.X = Me._XAxis.Transform(userPoint.X)
            screenPoint.Y = Me._YAxis.Transform(userPoint.Y)
        ElseIf coordinateFrame = CoordinateFrameType.AxisXY2Scale Then
            screenPoint.X = Me._XAxis.Transform(userPoint.X)
            screenPoint.Y = Me._Y2Axis.Transform(userPoint.Y)
        ElseIf coordinateFrame = CoordinateFrameType.PaneFraction Then
            screenPoint.X = Me._PaneArea.Left + userPoint.X * Me._PaneArea.Width
            screenPoint.Y = Me._PaneArea.Top + userPoint.Y * Me._PaneArea.Height
        Else
            Debug.Assert(Not Debugger.IsAttached, "Unhandled coordinate frame reference")
            screenPoint.X = userPoint.X
            screenPoint.Y = userPoint.Y
        End If

        Return screenPoint

    End Function

    ''' <summary>
    ''' Calculate the scaling factor based on the ratio of the current
    ''' <see cref="PaneArea"/> dimensions and the <see cref="BaseDimension"/>.  
    ''' This scaling factor is used to proportionally scale the features of the
    ''' <see cref="Pane"/> so that small graphs don't have huge fonts, and vice versa.
    ''' The scale factor applies to font sizes, symbol sizes, etc.
    ''' </summary>
    ''' <remarks>
    ''' Assume the standard width (BaseDimension) is 8.0 inches.  Therefore, if the PaneArea is 8.0
    ''' inches wide, then the fonts will be scaled at 1.0 if the PaneArea is 4.0 inches wide, the
    ''' fonts will be half-sized. If the PaneArea is
    ''' 16.0 inches wide, the fonts will be double-sized.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <returns>
    ''' A <see cref="System.Double">Double</see> value representing the scaling factor to use for the
    ''' rendering calculations.
    ''' </returns>
    Protected Function GetScaleFactor(ByVal graphicsDevice As Graphics) As Double

        Dim xInch As Double
        Dim yInch As Double

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        ' Determine the size of the PaneArea in inches

        ' check if we have printer or screen resolution
        If graphicsDevice.DpiX > 150 Then
            ' assuming we are drawing to the printer, use document coordinates.
            xInch = 0.01 * Me._PaneArea.Width
            yInch = 0.1 * Me._PaneArea.Height
        Else
            ' assuming that we print to screen, set the range based on the 
            ' pixels
            xInch = Me._PaneArea.Width / graphicsDevice.DpiX
            yInch = Me._PaneArea.Height / graphicsDevice.DpiY
        End If

        ' Limit the aspect ratio so Int64 plots don't have outrageous font sizes
        xInch = Math.Min(xInch, yInch * PaneDefaults.[Get].MaxAspectRatio)

        ' Scale the size depending on the client area width in linear fashion, limited
        ' to a minimum
        Return Math.Max(xInch / Me._BaseDimension, PaneDefaults.[Get].MinScaleFactor)

    End Function

    ''' <summary>
    ''' Prints all elements in the <see cref="Pane"/> to the specified graphics device.  This routine
    ''' should be overridden by the inheriting classes.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="graphicsDevice"> Reference to a graphic device to print into.  This is normally
    '''                               <see cref="System.Drawing.Printing.PrintPageEventArgs.graphics">graphics</see>
    '''                               of the <see cref="M:PrintPage"/> delegate. </param>
    ''' <param name="printArea">      The <see cref="System.Drawing.RectangleF">area</see> on the print
    '''                               document allotted for the chart. </param>
    Public MustOverride Sub Print(ByVal graphicsDevice As Graphics, ByVal printArea As RectangleF)

    ''' <summary>
    ''' Recalculate the axes scale ranges based on the current data range. Call this function anytime
    ''' you change, add, or remove curve data.  This method calculates a scale minimum, maximum, and
    ''' spacing for each axis based on the current curve data. Only the axis attributes (min, max,
    ''' spacing) that are set to auto-range (<see cref="Axis.Min"/>, <see cref="Axis.Max"/>,
    ''' <see cref="Tick.Spacing"/>) will be modified.  You must call
    ''' Invalidate() after calling AxisChange to make sure the display gets updated.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub Rescale()

        ' rescale all the axes
        Me._Axes.Rescale(Me._Curves, Me._IgnoreInitial)

    End Sub

    ''' <summary>
    ''' Restore the scale ranging to automatic mode, and recalculate the
    ''' <see cref="Axis"/> scale ranges.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Protected Sub ResetAutoScale()
        For Each axis As Axis In Me._Axes
            axis.AutoScale = True
        Next
        Me.Rescale()
    End Sub

    ''' <summary>
    ''' Calculate and set the <see cref="Pane.AxisArea"/> based on the
    ''' <see cref="DrawArea"/>.  The <see cref="Pane.AxisArea"/> is the plot area bounded by
    ''' the axes.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    Protected Sub SetAxisArea(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then Throw New ArgumentNullException(NameOf(graphicsDevice))

        ' Axis area starts out at the drawing area.  It gets reduced to make room for the 
        ' legend, scales, titles, etc.
        Me._AxisArea = Me._DrawArea

        ' Calculate the areas required for the X, Y, and Y2 axes, and reduce the 
        ' AxisArea by these amounts.
        Me._AxisArea = Me._Axes.GetAxisArea(graphicsDevice, Me._ScaleFactor, Me._AxisArea)

        ' adjust axis area for title space.
        Me._AxisArea = Me._Title.GetAxisArea(graphicsDevice, Me._ScaleFactor, Me._AxisArea)

        ' Calculate the stylus area, and back it out of the current axisArea
        Me._AxisArea = Me._Curves.GetAxisArea(Me._ScaleFactor, Me._AxisArea)

        ' Calculate the legend area, and back it out of the current axisArea
        Me._AxisArea = Me.Legend.GetAxisArea(graphicsDevice, Me._Curves, Me._ScaleFactor, Me._DrawArea, Me._AxisArea)

        ' set screen range for all axis.
        Me._Axes.SetScreenRange(Me._AxisArea)

    End Sub

    ''' <summary>
    ''' Calculates and sets the <see cref="DrawArea"/> based on the
    ''' <see cref="DrawAreaMargins"/>.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Protected Sub SetDrawArea()

        Me._DrawArea = Me._DrawAreaMargins.GetAdjustedArea(Me._PaneArea, Me._ScaleFactor)

    End Sub

    ''' <summary> Sets the pane size based on the client rectangle. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="clientRectangle"> The <see cref="Control.ClientRectangle"/> </param>
    Public Sub SetSize(ByVal clientRectangle As Rectangle)
        Dim area As RectangleF = RectangleF.op_Implicit(clientRectangle)
        area.Inflate(-Me._OuterGap, -Me._OuterGap)
        Me.PaneArea = area
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>Gets or sets lines/arrows on the graph</summary>
    Private _Arrows As ArrowCollection

    ''' <summary> Gets the list of <see cref="Arrow"/> items for this <see cref="Pane"/> </summary>
    ''' <value> A reference to an <see cref="Arrows"/> collection object. </value>
    Protected ReadOnly Property Arrows() As ArrowCollection
        Get
            Return Me._Arrows
        End Get
    End Property

    ''' <summary> The axes. </summary>
    Private _Axes As AxisCollection

    ''' <summary> Gets the <see cref="AxisCollection"/> </summary>
    ''' <value> The axes. </value>
    Protected ReadOnly Property Axes() As AxisCollection
        Get
            Return Me._Axes
        End Get
    End Property

    ''' <summary> The axis frame. </summary>
    Private _AxisFrame As Frame

    ''' <summary> Gets the <see cref="Axis"/> <see cref="Frame"/>. </summary>
    ''' <value> A <see cref="Frame"/> value. </value>
    Public ReadOnly Property AxisFrame() As Frame
        Get
            Return Me._AxisFrame
        End Get
    End Property

    ''' <summary>The rectangle that contains the area bounded by the axes, in
    ''' pixels.</summary>
    Private _AxisArea As RectangleF

    ''' <summary>
    ''' gets the rectangle that contains the area bounded by the axes (<see cref="AxisType.X"/>,
    ''' <see cref="AxisType.Y"/>, and <see cref="AxisType.Y2"/>)
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.RectangleF">Rectangle</see> in screen pixels. </value>
    Friend ReadOnly Property AxisArea() As RectangleF
        Get
            Return Me._AxisArea
        End Get
    End Property

    ''' <summary>
    ''' BaseDimension is A <see cref="System.Double">Double</see> value that sets "normal" plot size
    ''' on which all the settings are based.  The BaseDimension is in inches.  For example, if the
    ''' BaseDimension is 8.0 inches and the <see cref="Pane"/>
    ''' <see cref="Title"/> size is 14 points.  Then the pane title font
    ''' will be 14 points high when the <see cref="PaneArea"/> is approximately 8.0 inches wide.  If
    ''' the PaneArea is 4.0 inches wide, the pane title font will be
    ''' 7 points high.  Most features of the graph are scaled in this manner.
    ''' </summary>
    ''' <value> The base dimension reference for the <see cref="Pane"/> in inches. </value>
    Protected Property BaseDimension() As Double

    ''' <summary>A collection class for the curves on the graph</summary>
    Private _Curves As CurveCollection

    ''' <summary> Gets the list of <see cref="Curve"/> items for this <see cref="Pane"/> </summary>
    ''' <value> A reference to a <see cref="CurveCollection"/> collection object. </value>
    Friend ReadOnly Property Curves() As CurveCollection
        Get
            Return Me._Curves
        End Get
    End Property

    ''' <summary>The rectangle that defines the drawing area for the chart.  
    '''   Units are pixels.</summary>
    Private _DrawArea As RectangleF

    ''' <summary>
    ''' Gets the rectangle that defines the drawing area within the
    ''' <see cref="PaneArea"/>.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.RectangleF">Rectangle</see> in screen pixels. </value>
    Friend ReadOnly Property DrawArea() As RectangleF
        Get
            Return Me._DrawArea
        End Get
    End Property

    ''' <summary> Gets or sets the draw area margins. </summary>
    ''' <value> A <see cref="isr.Visuals.Margins"/> property. </value>
    Protected Property DrawAreaMargins() As MarginsF

    ''' <summary> The inner gap. </summary>
    Private _InnerGap As Single

    ''' <summary>
    ''' Gets or sets the gap between the <see cref="PaneArea"/> and
    ''' <see cref="DrawArea"/> borders. Scales linearly with the chart size.
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Protected Property InnerGap() As Single
        Get
            Return Me._InnerGap
        End Get
        Set(ByVal value As Single)
            Me._InnerGap = value
            Me._DrawAreaMargins = New MarginsF(value, value, value, value)
        End Set
    End Property

    ''' <summary>
    ''' A <see cref="System.Boolean">Boolean</see> value that affects the data range that is
    ''' considered for the automatic scale ranging.  If true, then initial data points where the Y
    ''' value is zero are not included when automatically determining the scale
    ''' <see cref="Axis.Min"/>, <see cref="Axis.Max"/>, and <see cref="Tick.Spacing"/> size. All data
    ''' after the first non-zero Y value are included.
    ''' </summary>
    ''' <value> The ignore initial. </value>
    Protected Property IgnoreInitial() As Boolean

    ''' <summary> Determines if the <see cref="Pane"/> will be shown. </summary>
    ''' <value> True to draw the chart. </value>
    Public Property Visible() As Boolean

    ''' <summary>A class for the graph legend</summary>
    Private _Legend As Legend

    ''' <summary> Accesses the <see cref="Legend"/> for this <see cref="Pane"/> </summary>
    ''' <value> A reference to a <see cref="Legend"/> object. </value>
    Public ReadOnly Property Legend() As Legend
        Get
            Return Me._Legend
        End Get
    End Property

    ''' <summary>
    ''' Gets the gap between the client area and the
    ''' <see cref="PaneArea"/> borders.
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Protected Property OuterGap() As Single

    ''' <summary> Gets the <see cref="Pane"/> <see cref="Frame"/>. </summary>
    ''' <value> A <see cref="Frame"/> value. </value>
    Public Property PaneFrame() As Frame

    ''' <summary>
    ''' Gets the rectangle that defines the full area into which the
    ''' <see cref="Pane"/> can be rendered.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.RectangleF">Rectangle</see> in screen pixels. </value>
    Public Property PaneArea() As RectangleF

    ''' <summary>
    ''' Gets the scale factor for the current size of the chart relative to the original size.
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Friend Property ScaleFactor() As Double

    ''' <summary> Gets the status message. </summary>
    ''' <value> A System.String value. </value>
    Protected Property StatusMessage() As String

    ''' <summary> The text boxes. </summary>
    Private _TextBoxes As New TextBoxCollection(Me)

    ''' <summary> Gets the list of <see cref="TextBox"/> items for this <see cref="Pane"/> </summary>
    ''' <value> A reference to a <see cref="TextBoxCollection"/> collection object. </value>
    Protected ReadOnly Property TextBoxes() As TextBoxCollection
        Get
            Return Me._TextBoxes
        End Get
    End Property

    ''' <summary> Gets or sets the pane <see cref="Title"/>. </summary>
    ''' <value> The title. </value>
    Public Property Title() As Title

    ''' <summary> The axis. </summary>
    Private _XAxis As Axis

    ''' <summary> The axis. </summary>
    Private _YAxis As Axis

    ''' <summary> The 2 axis. </summary>
    Private _Y2Axis As Axis

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary>
    ''' gets a new <see cref="DrawArea"/> relative to the <see cref="PaneArea"/>
    ''' based on the <see cref="innerGap"/> scaled to the "scaleFactor" fraction.  
    ''' That is, ScaledGap = innerGap * scaleFactor.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="scaleFactor"> The scaling factor for the features of the graph based on the
    '''                            <see cref="BaseDimension"/>.  This scaling factor is calculated
    '''                            by the <see cref="getScaleFactor"/> method.  The scale factor
    '''                            represents a linear multiple to be applied to font sizes, symbol
    '''                            sizes, etc. </param>
    ''' <returns> The drawing area <see cref="System.Drawing.RectangleF"/> </returns>
    Protected Function GetDrawingArea(ByVal scaleFactor As Double) As RectangleF

        ' Set the rectangle to the entire pan area
        Dim drawingArea As RectangleF = Me._PaneArea

        ' get the scaled margins
        Dim drawMargins As MarginsF = Me._DrawAreaMargins.GetScaledMargins(scaleFactor)

        ' reduce the size of the drawing area by the total horizontal and vertical
        ' spans
        drawingArea.Width -= drawMargins.Horizontal
        drawingArea.Height -= drawMargins.Vertical

        ' shift the draw rectangle by the top left margins
        drawingArea.Offset(drawMargins.TopLeft)

        Return drawingArea

    End Function

    ''' <summary>
    ''' Calculates the <see cref="innerGap"/> scaled to the "scaleFactor" fraction.  
    ''' That is, ScaledGap = innerGap * scaleFactor.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="scaleFactor"> The scaling factor for the features of the graph based on the
    '''                            <see cref="BaseDimension"/>.  This scaling factor is calculated
    '''                            by the <see cref="getScaleFactor"/> method.  The scale factor
    '''                            represents a linear multiple to be applied to font sizes, symbol
    '''                            sizes, etc. </param>
    ''' <returns>
    ''' A <see cref="System.Single">Single</see> in pixels, after scaling according to
    ''' <paramref name="scaleFactor"/>
    ''' </returns>
    Protected Function GetScaledGap(ByVal scaleFactor As Double) As Single
        Return Convert.ToSingle(Me._InnerGap * scaleFactor)
    End Function

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="Pane"/> class that defines the default property values
''' for the <see cref="Pane"/> class.
''' </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public NotInheritable Class PaneDefaults

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs this class. This constructor is private to ensure only a single instance of this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub New()
        MyBase.New()
        Me._BaseDimension = 8.0
        Me._FrameFillColor = Color.White
        Me._FrameLineColor = Color.Black
        Me._FrameLineWidth = 1
        Me._InnerGap = 4
        Me._Filled = True
        Me._Framed = True
        Me._OuterGap = 4
        Me._MaxAspectRatio = 2.0R
        Me._MinScaleFactor = 0.1R
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _Instance As PaneDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As PaneDefaults
        If PaneDefaults._Instance Is Nothing Then
            SyncLock PaneDefaults.SyncLocker
                PaneDefaults._Instance = New PaneDefaults()
            End SyncLock
        End If
        Return PaneDefaults._Instance
    End Function

#End Region

    ''' <summary>
    ''' Gets or sets the default dimension of the <see cref="Pane.PaneArea"/>, which defines a normal
    ''' sized plot.  This dimension is used to scale the fonts, symbols, etc. according to the actual
    ''' size of the
    ''' <see cref="Pane.PaneArea"/>.
    ''' </summary>
    ''' <value> The base dimension. </value>
    ''' <seealso cref="Pane.getScaleFactor"/>
    Public Property BaseDimension() As Double

    ''' <summary>
    ''' Gets or sets the default color for the <see cref="Pane.PaneArea"/> background.
    ''' (<see cref="Frame.FillColor"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property FrameFillColor() As Color

    ''' <summary>
    ''' Gets or sets the default color for the <see cref="Pane"/> frame border.
    ''' (<see cref="Frame.LineColor"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property FrameLineColor() As Color

    ''' <summary>
    ''' Gets or sets the default pen width for the <see cref="Pane"/> frame border.
    ''' (<see cref="Frame.LineWidth"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property FrameLineWidth() As Single

    ''' <summary>
    ''' Gets or sets the default value for the <see cref="Pane.innerGap"/> property. This is the size
    ''' of the margin around the edge of the <see cref="Pane.PaneArea"/>.
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property InnerGap() As Single

    ''' <summary>
    ''' Gets or sets the default frame fill mode for the <see cref="Pane"/>.
    ''' (<see cref="Frame.Filled"/> property). True to fill the <see cref="Pane.PaneArea"/> area or
    ''' False to keep it transparent.
    ''' </summary>
    ''' <value> The filled. </value>
    Public Property Filled() As Boolean

    ''' <summary>
    ''' Gets or sets the default frame outline mode for the <see cref="Pane"/>.
    ''' (<see cref="Frame.IsOutline"/> property). True to draw a frame around the
    ''' <see cref="Pane.PaneArea"/>, False otherwise.
    ''' </summary>
    ''' <value> The framed. </value>
    Public Property Framed() As Boolean

    ''' <summary>
    ''' Gets or sets the default value for the <see cref="Pane.Outergap"/> property. This is the size
    ''' of the margin around the outer edge of the <see cref="Pane.PaneArea"/>.
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property OuterGap() As Single

    ''' <summary>
    ''' Gets or sets the maximum aspect ratio (x/y) for scaling charts so that Int64 charts won't
    ''' have outrageous font sizes.
    ''' </summary>
    ''' <value> A <see cref="System.Double">Double</see> value. </value>
    Public Property MaxAspectRatio() As Double

    ''' <summary>
    ''' Gets or sets the minimum scale factor for scaling charts relative to the baseDimension.
    ''' </summary>
    ''' <value> A <see cref="System.Double">Double</see> value. </value>
    Public Property MinScaleFactor() As Double

End Class

#End Region

