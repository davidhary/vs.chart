''' <summary>
''' Encapsulates the chart <see cref="Legend"/> that is displayed in the <see cref="Pane"/>
''' </summary>
''' <remarks>
''' Use this class to ....(c) 2004 Integrated Scientific Resources, Inc. All rights reserved.
''' <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public Class Legend

    Implements ICloneable, IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a <see cref="Legend"/> with default values as defined in the
    ''' <see cref="LegendDefaults"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
    Public Sub New(ByVal drawingPane As Pane)

        MyBase.New()
        If drawingPane Is Nothing Then
            Throw New ArgumentNullException(NameOf(drawingPane))
        End If
        Dim legendDefaults As LegendDefaults = LegendDefaults.Get()
        Me._Placement = legendDefaults.Placement
        Me._Frame = New Frame With {
            .IsOutline = legendDefaults.Framed,
            .Filled = legendDefaults.Filled,
            .Visible = legendDefaults.Filled Or legendDefaults.Framed,
            .LineColor = legendDefaults.FrameColor,
            .LineWidth = legendDefaults.FrameWidth,
            .FillColor = legendDefaults.FillColor
        }

        Me._HorizontalStack = legendDefaults.HorizontalStack
        Me._Visible = legendDefaults.Visible
        Me._Appearance = New TextAppearance(legendDefaults.Font, legendDefaults.FontColor)
        Me._Appearance.Frame.Visible = False
        Me._Pane = drawingPane

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The XAxis object from which to copy. </param>
    Public Sub New(ByVal model As Legend)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Me._Area = model._Area
        Me._Placement = model._Placement
        Me._Frame = model._Frame.Copy()
        Me._HorizontalStack = model._HorizontalStack
        Me._Visible = model._Visible
        Me._Appearance = model._Appearance.Copy()
        Me._Pane = model._Pane
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> Gets (private) the dispose status sentinel. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Me._Appearance IsNot Nothing Then
                        Me._Appearance.Dispose()
                        Me._Appearance = Nothing
                    End If
                    If Me._Frame IsNot Nothing Then
                        Me._Frame.Dispose()
                        Me._Frame = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Private values the determine how legend space is allocated.  
    '''   <code>
    '''     item:     space  Cord  space      text      space<p>
    '''     width:    0.5w    2w   0.5w  Maximum Width  0.5w</p> 
    '''   </code></summary>
    Private ReadOnly _LineLeftMarginCharacterWidth As Single = 0.5

    ''' <summary> Width of the line right margin character. </summary>
    Private ReadOnly _LineRightMarginCharacterWidth As Single = 0.5

    ''' <summary> Width of the line length character. </summary>
    Private ReadOnly _LineLengthCharacterWidth As Single = 2

    ''' <summary> Width of the right margin character. </summary>
    Private ReadOnly _RightMarginCharacterWidth As Single = 0.5

    ''' <summary> Width of the legend horizontalgap character. </summary>
    Private ReadOnly _LegendHorizontalgapCharacterWidth As Single = 0.5

    ''' <summary> Height of the legend verticalgap character. </summary>
    Private ReadOnly _LegendVerticalgapCharacterHeight As Single = 0.5

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the Legend. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the Legend. </returns>
    Public Function Copy() As Legend
        Return New Legend(Me)
    End Function

    ''' <summary>
    ''' Renders the <see cref="Legend"/> to the specified <see cref="graphics"/> device This method
    ''' is normally only called by the Draw method of the parent <see cref="Pane"/> object.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics)

        ' if the legend is not visible, do nothing
        If Not Me._Visible Then
            Return
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        ' Clip everything to the draw area
        graphicsDevice.SetClip(Me._Pane.DrawArea)

        ' draw the frame before drawing the legend so that it appears in the background
        If Me._Pane.Curves.Count > 0 Then
            Me._Frame.Draw(graphicsDevice, Me._Area)
        End If

        ' Set up some scaled dimensions for calculating sizes and locations
        Dim charWidth As Single = Me._Appearance.MeasureString(graphicsDevice, "x", Me._Pane.ScaleFactor).Width
        Dim charHeight As Single = Me._Appearance.ScaledFont.Height
        Dim halfCharHeight As Single = charHeight / 2.0F

        Dim x, y As Single

        ' Loop for each curve in Curve Collection
        For Each curve As Curve In Me._Pane.Curves

            ' Calculate the x,y (TopLeft) location of the current curve legend label
            x = Me._Area.Left + charWidth * Me._LineLeftMarginCharacterWidth +
                                            ((curve.SerialNumber - 1) Mod Me._Columns) * Me._ColumnWidth
            y = Me._Area.Top + Convert.ToSingle(Math.Floor((curve.SerialNumber - 1) / Me._Columns)) * charHeight

            ' Draw the legend label for the current curve
            Me._Appearance.Draw(graphicsDevice, curve.Label,
                             x + (Me._LineRightMarginCharacterWidth + Me._LineLengthCharacterWidth) * charWidth,
                             y, HorizontalAlignment.Left, VerticalAlignment.Top, Me._Pane.ScaleFactor)

            ' Draw a sample curve to the left of the label text
            curve.Cord.Draw(graphicsDevice, x, y + halfCharHeight,
                            x + Me._LineLengthCharacterWidth * charWidth, y + halfCharHeight)

            ' Draw a sample symbol to the left of the label text				
            curve.Symbol.Draw(graphicsDevice, x + charWidth * Me._LineLengthCharacterWidth / 2,
                              y + halfCharHeight, Me._Pane.ScaleFactor)

        Next curve

    End Sub

    ''' <summary>
    ''' Calculates the <see cref="Legend"/> rectangle (<see cref="Area"/>), taking into account the
    ''' number of required legend entries, and the legend drawing preferences.  Adjusts the size of
    ''' the <see cref="Pane.AxisArea"/>
    ''' for the parent <see cref="Pane"/> to accommodate the space required by the legend.  The
    ''' legends are drawn into equally sized columns that are stacked per the
    ''' <see cref="HorizontalStack"/> property.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="curves">         A reference to the <see cref="CurveCollection"/> collection of
    '''                               curves for which a legend is required. </param>
    ''' <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
    '''                               <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                               calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                               The scale factor is applied to fonts, symbols, etc. </param>
    ''' <param name="drawArea">       The rectangle that contains the drawing area in pixels. </param>
    ''' <param name="axisArea">       The rectangle that contains the area bounded by the axes, in
    '''                               pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso> </param>
    ''' <returns> Adjusted <see cref="Pane.AxisArea"/> </returns>
    ''' <seealso cref="Pane.DrawArea"> DrawArea</seealso>
    ''' <seealso cref="Columns"/>
    Friend Function GetAxisArea(ByVal graphicsDevice As Graphics, ByVal curves As CurveCollection,
                                ByVal scaleFactor As Double, ByVal drawArea As RectangleF,
                                ByVal axisArea As RectangleF) As RectangleF

        ' If the legend is invisible, don't do anything
        If Not Me._Visible Then
            Return axisArea
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        ' Start with an empty rectangle
        Me._Area = System.Drawing.RectangleF.Empty
        Me._Columns = 1
        Me._ColumnWidth = 1

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        Dim charWidth As Single = Me._Appearance.MeasureString(graphicsDevice, "x", scaleFactor).Width
        Dim charHeight As Single = Me._Appearance.ScaledFont.Height
        Dim verticalgap As Single = charHeight * Me._LegendVerticalgapCharacterHeight
        Dim horizontalgap As Single = charWidth * Me._LegendHorizontalgapCharacterWidth
        Dim maxLabelWidth As Single = 0
        Dim widthAvailable As Single

        ' Loop through each curve in the curve list
        ' Find the maximum width of the legend labels
        For Each curve As Curve In curves

            ' Calculate the width of the label save the max width
            maxLabelWidth = Math.Max(maxLabelWidth,
                                     Me._Appearance.MeasureString(graphicsDevice, curve.Label, scaleFactor).Width)

        Next curve

        ' Is this legend horizontally stacked?
        If Me._HorizontalStack Then

            ' Determine the available space for horizontal stacking
            Select Case Me._Placement

                Case PlacementType.Right, PlacementType.Left

                    ' Never stack if the legend is to the right or left
                    widthAvailable = 0

                Case PlacementType.TopLeft, PlacementType.TopRight, PlacementType.BottomLeft, PlacementType.BottomRight

                    ' for the top & bottom, the axis frame width is available
                    widthAvailable = axisArea.Width

                Case PlacementType.InsideTopRight, PlacementType.InsideTopLeft, PlacementType.InsideBottomRight, PlacementType.InsideBottomLeft

                    ' for inside the axis area, use 1/2 of the axis frame width
                    widthAvailable = axisArea.Width / 2

                Case Else

                    ' shouldn't ever happen
                    Debug.Assert(Not Debugger.IsAttached, "Invalid legend location option")
                    widthAvailable = 0

            End Select

            ' width of one legend entry
            Me._ColumnWidth = charWidth * (Me._LineLeftMarginCharacterWidth +
              Me._LineRightMarginCharacterWidth + Me._LineLengthCharacterWidth +
              Me._RightMarginCharacterWidth) + maxLabelWidth

            ' Calculate the number of columns to accommodate all the legends in 
            ' one or more
            If maxLabelWidth > 0 Then
                Me._Columns = Convert.ToInt32(Math.Floor(widthAvailable / Me._ColumnWidth))
            End If
            ' You can never have more columns than legend entries
            If Me._Columns > curves.Count Then
                Me._Columns = curves.Count
            End If
            ' a safety check
            If Me._Columns = 0 Then
                Me._Columns = 1
            End If
        Else
            Me._ColumnWidth = charWidth * (Me._LineLeftMarginCharacterWidth +
              Me._LineRightMarginCharacterWidth + Me._LineLengthCharacterWidth +
              Me._RightMarginCharacterWidth) + maxLabelWidth
        End If

        ' total legend width
        Dim legendWidth As Single = Me._Columns * Me._ColumnWidth

        ' The height of the legend is the actual height of the lines of text
        '   (curves.Count/columns * height) 
        Dim legendHeight As Single = Convert.ToSingle(Math.Ceiling(curves.Count / Me._Columns)) * charHeight

        ' calculate the legend area based on the above determined parameters
        ' Also, adjust the plotArea and axisArea to reflect the space for the legend
        If curves.Count > 0 Then

            ' The switch statement assigns the left and top edges, and adjusts the axisArea
            ' as required.  The right and bottom edges are calculated at the bottom of the switch.
            Select Case Me._Placement

                Case PlacementType.Right

                    Me._Area.X = drawArea.Right - legendWidth
                    Me._Area.Y = axisArea.Top

                    axisArea.Width -= legendWidth + horizontalgap

                Case PlacementType.TopLeft

                    Me._Area.X = axisArea.Left
                    Me._Area.Y = axisArea.Top

                    axisArea.Y += legendHeight + verticalgap
                    axisArea.Height -= legendHeight + verticalgap

                Case PlacementType.TopRight

                    Me._Area.X = axisArea.Right - legendWidth
                    Me._Area.Y = axisArea.Top

                    axisArea.Y += legendHeight + verticalgap
                    axisArea.Height -= legendHeight + verticalgap

                Case PlacementType.BottomLeft

                    Me._Area.X = axisArea.Left
                    Me._Area.Y = drawArea.Bottom - legendHeight

                    axisArea.Height -= legendHeight + verticalgap

                Case PlacementType.BottomRight

                    Me._Area.X = axisArea.Right - legendWidth
                    Me._Area.Y = drawArea.Bottom - legendHeight

                    axisArea.Height -= legendHeight + verticalgap

                Case PlacementType.Left

                    Me._Area.X = drawArea.Left
                    Me._Area.Y = axisArea.Top

                    axisArea.X += legendWidth + horizontalgap
                    axisArea.Width -= legendWidth + horizontalgap

                Case PlacementType.InsideTopRight

                    Me._Area.X = axisArea.Right - legendWidth
                    Me._Area.Y = axisArea.Top

                Case PlacementType.InsideTopLeft

                    Me._Area.X = axisArea.Left
                    Me._Area.Y = axisArea.Top

                Case PlacementType.InsideBottomRight

                    Me._Area.X = axisArea.Right - legendWidth
                    Me._Area.Y = axisArea.Bottom - legendHeight

                Case PlacementType.InsideBottomLeft

                    Me._Area.X = axisArea.Left
                    Me._Area.Y = axisArea.Bottom - legendHeight

            End Select

            ' Calculate the Right and Bottom edges of the area
            Me._Area.Width = legendWidth
            Me._Area.Height = legendHeight

        End If

        ' return adjusted axis rectangle
        Return axisArea

    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> The appearance. </summary>
    Private _Appearance As TextAppearance

    ''' <summary>
    ''' Gets the <see cref="isr.Visuals.TextAppearance">TextAppearance</see> class used to render the
    ''' <see cref="Legend"/> entries.
    ''' </summary>
    ''' <value> A <see cref="isr.Visuals.TextAppearance">TextAppearance</see> instance. </value>
    Public ReadOnly Property Appearance() As TextAppearance
        Get
            Return Me._Appearance
        End Get
    End Property

    ''' <summary>
    ''' Gets the number of columns (horizontal stacking) to be used for drawing the legend.
    ''' </summary>
    ''' <value> A <see cref="T:System.Integer">integer</see> value. </value>
    Public Property Columns() As Integer

    ''' <summary> Gets the width of each column in the legend (pixels) </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property ColumnWidth() As Single

    ''' <summary> Gets the <see cref="Legend"/> <see cref="Frame"/>. </summary>
    ''' <value> A <see cref="Frame"/> value. </value>
    Public Property Frame() As Frame

    ''' <summary> Gets a property that shows or hides the <see cref="Legend"/> entirely. </summary>
    ''' <value>
    ''' A <see cref="System.Boolean">Boolean</see> True to show the
    ''' <see cref="Legend"/> or False to hide it.
    ''' </value>
    Public Property Visible() As Boolean

    ''' <summary>
    ''' Gets a property that allows the <see cref="Legend"/> items to stack horizontally in addition
    ''' to the vertical stacking.
    ''' </summary>
    ''' <value>
    ''' A <see cref="System.Boolean">Boolean</see> True to allow horizontal stacking or False
    ''' otherwise.
    ''' </value>
    Public Property HorizontalStack() As Boolean

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
    Private ReadOnly _Pane As Pane

    ''' <summary>
    ''' Gets the location of the <see cref="Legend"/> on the
    ''' <see cref="Pane"/> using the <see cref="Placement"/> type.
    ''' </summary>
    ''' <value> A <see cref="PlacementType"/> </value>
    Public Property Placement() As PlacementType

    ''' <summary> The area. </summary>
    Private _Area As RectangleF

    ''' <summary>
    ''' get the bounding rectangle for the <see cref="Legend"/> in screen coordinates.
    ''' </summary>
    ''' <value> A screen rectangle in pixels. </value>
    Public ReadOnly Property Area() As RectangleF
        Get
            Return Me._Area
        End Get
    End Property

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> A System.String value. </value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="legend"/> class that defines the default property values
''' for the <see cref="isr.Visuals.Legend"/> class.
''' </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public NotInheritable Class LegendDefaults
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs this class. This constructor is private to ensure only a single instance of this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub New()
        MyBase.New()
        Me._FrameWidth = 1
        Me._FrameColor = Color.Black
        Me._FillColor = Color.White
        Me._Placement = PlacementType.TopLeft
        Me._Framed = True
        Me._Visible = True
        Me._Filled = True
        Me._HorizontalStack = True
        Me._Font = New Font("Arial", 12, FontStyle.Regular)
        Me._FontColor = Color.Black
        Me._FontBold = False
        Me._FontItalic = False
        Me._FontUnderline = False
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _Instance As LegendDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As LegendDefaults
        If LegendDefaults._Instance Is Nothing OrElse LegendDefaults._Instance.IsDisposed Then
            SyncLock LegendDefaults.SyncLocker
                LegendDefaults._Instance = New LegendDefaults()
            End SyncLock
        End If
        Return LegendDefaults._Instance
    End Function

#Region "IDisposable Support"

    ''' <summary> Gets or sets a value indicating whether this instance is disposed. </summary>
    ''' <value> <c>True</c> if this instance is disposed; otherwise, <c>False</c>. </value>
    Private Property IsDisposed As Boolean ' To detect redundant calls

    ''' <summary> Releases unmanaged and - optionally - managed resources. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                          <c>False</c> to release only unmanaged resources. </param>
    Private Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    ' dispose managed state (managed objects).
                    If Me._Font IsNot Nothing Then
                        Me._Font.Dispose()
                        Me._Font = Nothing
                    End If
                End If
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

#End Region

    ''' <summary>
    ''' Gets or sets the default pen width for the <see cref="Legend"/> frame border.
    ''' (<see cref="Legend.Frame"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property FrameWidth() As Single

    ''' <summary>
    ''' Gets or sets the default color for the <see cref="Legend"/> frame border.
    ''' (<see cref="Legend.Frame"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property FrameColor() As Color

    ''' <summary>
    ''' Gets or sets the default color for the <see cref="Legend"/> background.
    ''' (<see cref="Legend.Frame"/> property).  Use of this color depends on the status of the
    ''' <see cref="Legend.Frame"/>property.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property FillColor() As Color

    ''' <summary>
    ''' Gets or sets the default placement for the <see cref="Legend"/> on the graph
    ''' (<see cref="Legend.Placement"/> property).  This property is defined as a
    ''' <see cref="isr.Visuals.PlacementType"/> enumeration.
    ''' </summary>
    ''' <value> The placement. </value>
    Public Property Placement() As PlacementType

    ''' <summary>
    ''' Gets or sets the default frame mode for the <see cref="Legend"/>. (<see cref="Legend.Frame"/>
    ''' property). true to draw a frame around the <see cref="Legend.Area"/>, false otherwise.
    ''' </summary>
    ''' <value> The framed. </value>
    Public Property Framed() As Boolean

    ''' <summary>
    ''' Gets or sets the default display mode for the <see cref="Legend"/>.
    ''' (<see cref="Legend.Visible"/> property). true to show the legend, false to hide it.
    ''' </summary>
    ''' <value> The visible. </value>
    Public Property Visible() As Boolean

    ''' <summary>
    ''' Gets or sets the default fill mode for the <see cref="Legend"/> background
    ''' (<see cref="Legend.Frame"/> property). true to fill-in the background with color, false to
    ''' leave the background transparent.
    ''' </summary>
    ''' <value> The filled. </value>
    Public Property Filled() As Boolean

    ''' <summary>
    ''' Gets or sets the default horizontal stacking mode for the <see cref="Legend"/>
    ''' (<see cref="Legend.HorizontalStack"/> property). True to allow horizontal legend item
    ''' stacking, false to allow only vertical legend orientation.
    ''' </summary>
    ''' <value> A stack of horizontals. </value>
    Public Property HorizontalStack() As Boolean

    ''' <summary>
    ''' Gets or sets the default <see cref="System.Drawing.Font">Font</see>
    ''' of the <see cref="Title"/> caption.
    ''' </summary>
    ''' <value> The font. </value>
    Public Property Font() As Font

    ''' <summary>
    ''' Gets or sets the default font color for the <see cref="Legend"/> entries
    ''' (<see cref="TextAppearance.FontColor"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property FontColor() As Color

    ''' <summary>
    ''' Gets or sets the default font bold mode for the <see cref="Legend"/> entries
    '''   (<see cref="TextAppearance.Bold"/> property). true
    ''' for a bold typeface, false otherwise.
    ''' </summary>
    ''' <value> The font bold. </value>
    Public Property FontBold() As Boolean

    ''' <summary>
    ''' Gets or sets the default font italic mode for the <see cref="Legend"/> entries
    ''' (<see cref="TextAppearance.Italic"/> property). true for an italic typeface, false otherwise.
    ''' </summary>
    ''' <value> The font italic. </value>
    Public Property FontItalic() As Boolean

    ''' <summary>
    ''' Gets or sets the default font underline mode for the <see cref="Legend"/> entries
    ''' (<see cref="TextAppearance.IsUnderline"/> property). true for an underlined typeface, false
    ''' otherwise.
    ''' </summary>
    ''' <value> The font underline. </value>
    Public Property FontUnderline() As Boolean

End Class
#End Region

