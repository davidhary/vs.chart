''' <summary>
''' Handles the drawing of the curve <see cref="Symbol"/> objects. The symbols are the small
''' shapes that appear over each defined point along the curve.
''' </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public Class Symbol

    Implements ICloneable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a <see cref="Symbol"/> with default values as defined in the
    ''' <see cref="SymbolDefaults"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
    Public Sub New(ByVal drawingPane As Pane)

        MyBase.New()

        Dim symbolDefaults As SymbolDefaults = SymbolDefaults.Get()
        Me._Size = symbolDefaults.Size
        Me._Shape = symbolDefaults.Shape
        Me._LineWidth = symbolDefaults.LineWidth
        Me._LineColor = symbolDefaults.LineColor
        Me._Visible = symbolDefaults.Visible
        Me._Filled = symbolDefaults.Filled

        Me._Pane = drawingPane

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The Symbol object from which to copy. </param>
    Public Sub New(ByVal model As Symbol)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Me._Size = model._Size
        Me._Shape = model._Shape
        Me._LineWidth = model._LineWidth
        Me._LineColor = model._LineColor
        Me._Visible = model._Visible
        Me._Filled = model._Filled
        Me._Pane = model._Pane

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the Symbol. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the Symbol. </returns>
    Public Function Copy() As Symbol
        Return New Symbol(Me)
    End Function

    ''' <summary>
    ''' Draw the <see cref="Symbol"/> to the specified <see cref="graphics"/> device at the specified
    ''' location.  This method draws a single symbol.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="x">              The x position of the center of the symbol in screen pixels. </param>
    ''' <param name="y">              The y position of the center of the symbol in screen pixels. </param>
    ''' <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
    '''                               <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                               calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                               The scale factor is applied to fonts, symbols, etc. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal x As Single, ByVal y As Single,
                    ByVal scaleFactor As Double)

        ' Only draw if the symbol is visible
        If Not Me._Visible Then Return

        If graphicsDevice Is Nothing Then Throw New ArgumentNullException(NameOf(graphicsDevice))

        Using brush As New SolidBrush(Me._LineColor)
            Using pen As New Pen(Me._LineColor, Me._LineWidth)
                ' Fill or draw the symbol as required
                If Me._Filled Then
                    Me.Fill(graphicsDevice, x, y, scaleFactor, pen, brush)
                Else
                    Me.Draw(graphicsDevice, x, y, scaleFactor, pen)
                End If
            End Using
        End Using

    End Sub

    ''' <summary>
    ''' Draw the <see cref="Symbol"/> to the specified <see cref="graphics"/>
    ''' device at the specified list of locations.  This method draws a series of symbols, and is
    ''' intended to provide a speed improvement over the single Draw() method.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="x">              The x position of the center of the symbol in screen pixels. </param>
    ''' <param name="y">              The y position of the center of the symbol in screen pixels. </param>
    ''' <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
    '''                               <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                               calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                               The scale factor is applied to fonts, symbols, etc. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal x() As Single, ByVal y() As Single, ByVal scaleFactor As Double)

        ' Only draw if the symbol is visible
        If Not Me._Visible Then
            Return
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If
        If x Is Nothing Then
            Throw New ArgumentNullException(NameOf(x))
        End If
        If y Is Nothing Then
            Throw New ArgumentNullException(NameOf(y))
        End If

        ' Only draw if the symbol is visible
        Using brush As New SolidBrush(Me._LineColor)
            Using pen As New Pen(Me._LineColor, Me._LineWidth)
                Dim numPoints As Integer = x.Length
                Dim i As Integer
                For i = 0 To numPoints - 1
                    If x(i) <> System.Single.MaxValue AndAlso y(i) <> System.Single.MaxValue Then
                        ' Fill or draw the symbol as required
                        If Me._Filled Then
                            Me.Fill(graphicsDevice, x(i), y(i), scaleFactor, pen, brush)
                        Else
                            Me.Draw(graphicsDevice, x(i), y(i), scaleFactor, pen)
                        End If
                    End If
                Next i
            End Using
        End Using

    End Sub

    ''' <summary>
    ''' Draw the <see cref="Symbol"/> to the specified <see cref="graphics"/>
    ''' device at the specified list of locations.  This method draws a series of symbols, and is
    ''' intended to provide a speed improvement over the single Draw() method.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="x">              The x position of the center of the symbol in screen pixels. </param>
    ''' <param name="y">              The y position of the center of the symbol in screen pixels. </param>
    ''' <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
    '''                               <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                               calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                               The scale factor is applied to fonts, symbols, etc. </param>
    ''' <param name="ignoreMissing">  True to ignore missing values. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal x() As Single, ByVal y() As Single,
                    ByVal scaleFactor As Double, ByVal ignoreMissing As Boolean)

        ' Only draw if the symbol is visible
        If Not Me._Visible Then
            Return
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If
        If x Is Nothing Then
            Throw New ArgumentNullException(NameOf(x))
        End If
        If y Is Nothing Then
            Throw New ArgumentNullException(NameOf(y))
        End If

        Using brush As New SolidBrush(Me._LineColor)
            Using pen As New Pen(Me._LineColor, Me._LineWidth)
                Dim numPoints As Integer = x.Length
                Dim i As Integer
                For i = 0 To numPoints - 1
                    If ignoreMissing OrElse (x(i) <> System.Single.MaxValue AndAlso
                                             y(i) <> System.Single.MaxValue) Then
                        ' Fill or draw the symbol as required
                        If Me._Filled Then
                            Me.Fill(graphicsDevice, x(i), y(i), scaleFactor, pen, brush)
                        Else
                            Me.Draw(graphicsDevice, x(i), y(i), scaleFactor, pen)
                        End If
                    End If
                Next i
            End Using
        End Using
    End Sub

    ''' <summary>
    ''' Draw the <see cref="Symbol"/> (outline only) to the specified
    ''' <see cref="graphics"/> device at the specified location.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="x">              The x position of the center of the symbol in screen pixels. </param>
    ''' <param name="y">              The y position of the center of the symbol in screen pixels. </param>
    ''' <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
    '''                               <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                               calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                               The scale factor is applied to fonts, symbols, etc. </param>
    ''' <param name="pen">            A pen with attributes of <see cref="Color"/> and
    '''                               <see cref="LineWidth"/> for this symbol. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal x As Single, ByVal y As Single,
                    ByVal scaleFactor As Double, ByVal pen As Pen)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        Dim scaledSize As Single = Convert.ToSingle(Me._Size * scaleFactor)
        Dim hsize As Single = scaledSize / 2
        Dim hsize1 As Single = hsize + 1

        Select Case Me._Shape
            Case ShapeType.Square
                graphicsDevice.DrawLine(pen, x - hsize, y - hsize, x + hsize, y - hsize)
                graphicsDevice.DrawLine(pen, x + hsize, y - hsize, x + hsize, y + hsize)
                graphicsDevice.DrawLine(pen, x + hsize, y + hsize, x - hsize, y + hsize)
                graphicsDevice.DrawLine(pen, x - hsize, y + hsize, x - hsize, y - hsize)
            Case ShapeType.Diamond
                graphicsDevice.DrawLine(pen, x, y - hsize, x + hsize, y)
                graphicsDevice.DrawLine(pen, x + hsize, y, x, y + hsize)
                graphicsDevice.DrawLine(pen, x, y + hsize, x - hsize, y)
                graphicsDevice.DrawLine(pen, x - hsize, y, x, y - hsize)
            Case ShapeType.Triangle
                graphicsDevice.DrawLine(pen, x, y - hsize, x + hsize, y + hsize)
                graphicsDevice.DrawLine(pen, x + hsize, y + hsize, x - hsize, y + hsize)
                graphicsDevice.DrawLine(pen, x - hsize, y + hsize, x, y - hsize)
            Case ShapeType.Circle
                graphicsDevice.DrawEllipse(pen, x - hsize, y - hsize, scaledSize, scaledSize)
            Case ShapeType.XCross
                graphicsDevice.DrawLine(pen, x - hsize, y - hsize, x + hsize1, y + hsize1)
                graphicsDevice.DrawLine(pen, x + hsize, y - hsize, x - hsize1, y + hsize1)
            Case ShapeType.Plus
                graphicsDevice.DrawLine(pen, x, y - hsize, x, y + hsize1)
                graphicsDevice.DrawLine(pen, x - hsize, y, x + hsize1, y)
            Case ShapeType.Star
                graphicsDevice.DrawLine(pen, x, y - hsize, x, y + hsize1)
                graphicsDevice.DrawLine(pen, x - hsize, y, x + hsize1, y)
                graphicsDevice.DrawLine(pen, x - hsize, y - hsize, x + hsize1, y + hsize1)
                graphicsDevice.DrawLine(pen, x + hsize, y - hsize, x - hsize1, y + hsize1)
            Case ShapeType.TriangleDown
                graphicsDevice.DrawLine(pen, x, y + hsize, x + hsize, y - hsize)
                graphicsDevice.DrawLine(pen, x + hsize, y - hsize, x - hsize, y - hsize)
                graphicsDevice.DrawLine(pen, x - hsize, y - hsize, x, y + hsize)
        End Select

    End Sub

    ''' <summary>
    ''' Renders the filled <see cref="Symbol"/> to the specified <see cref="graphics"/>
    ''' device at the specified location.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="x">              The x position of the center of the symbol in screen pixels. </param>
    ''' <param name="y">              The y position of the center of the symbol in screen pixels. </param>
    ''' <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
    '''                               <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                               calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                               The scale factor is applied to fonts, symbols, etc. </param>
    ''' <param name="pen">            A pen with attributes of <see cref="Color"/> and
    '''                               <see cref="LineWidth"/> for this symbol. </param>
    ''' <param name="brush">          A brush with the <see cref="Color"/> attribute for this symbol. </param>
    Public Sub Fill(ByVal graphicsDevice As Graphics, ByVal x As Single, ByVal y As Single,
                    ByVal scaleFactor As Double, ByVal pen As Pen, ByVal brush As Brush)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If
        If brush Is Nothing Then
            Throw New ArgumentNullException(NameOf(brush))
        End If

        Dim scaledSize As Single = Convert.ToSingle(Me._Size * scaleFactor)
        Dim hsize As Single = scaledSize / 2
        Dim hsize4 As Single = scaledSize / 3
        Dim hsize41 As Single = hsize4 + 1
        Dim hsize1 As Single = hsize + 1

        Dim polyPt(4) As PointF

        Select Case Me._Shape
            Case ShapeType.Square
                graphicsDevice.FillRectangle(brush, x - hsize, y - hsize, scaledSize, scaledSize)
            Case ShapeType.Diamond
                polyPt(0).X = x
                polyPt(0).Y = y - hsize
                polyPt(1).X = x + hsize
                polyPt(1).Y = y
                polyPt(2).X = x
                polyPt(2).Y = y + hsize
                polyPt(3).X = x - hsize
                polyPt(3).Y = y
                polyPt(4) = polyPt(0)
                graphicsDevice.FillPolygon(brush, polyPt)
            Case ShapeType.Triangle
                polyPt(0).X = x
                polyPt(0).Y = y - hsize
                polyPt(1).X = x + hsize
                polyPt(1).Y = y + hsize
                polyPt(2).X = x - hsize
                polyPt(2).Y = y + hsize
                polyPt(3) = polyPt(0)
                graphicsDevice.FillPolygon(brush, polyPt)
            Case ShapeType.Circle
                graphicsDevice.FillEllipse(brush, x - hsize, y - hsize, scaledSize, scaledSize)
            Case ShapeType.XCross
                graphicsDevice.FillRectangle(brush, x - hsize4, y - hsize4, hsize4 + hsize41, hsize4 + hsize41)
                graphicsDevice.DrawLine(pen, x - hsize, y - hsize, x + hsize1, y + hsize1)
                graphicsDevice.DrawLine(pen, x + hsize, y - hsize, x - hsize1, y + hsize1)
            Case ShapeType.Plus
                graphicsDevice.FillRectangle(brush, x - hsize4, y - hsize4, hsize4 + hsize41, hsize4 + hsize41)
                graphicsDevice.DrawLine(pen, x, y - hsize, x, y + hsize1)
                graphicsDevice.DrawLine(pen, x - hsize, y, x + hsize1, y)
            Case ShapeType.Star
                graphicsDevice.FillRectangle(brush, x - hsize4, y - hsize4, hsize4 + hsize41, hsize4 + hsize41)
                graphicsDevice.DrawLine(pen, x, y - hsize, x, y + hsize1)
                graphicsDevice.DrawLine(pen, x - hsize, y, x + hsize1, y)
                graphicsDevice.DrawLine(pen, x - hsize, y - hsize, x + hsize1, y + hsize1)
                graphicsDevice.DrawLine(pen, x + hsize, y - hsize, x - hsize1, y + hsize1)
            Case ShapeType.TriangleDown
                polyPt(0).X = x
                polyPt(0).Y = y + hsize
                polyPt(1).X = x + hsize
                polyPt(1).Y = y - hsize
                polyPt(2).X = x - hsize
                polyPt(2).Y = y - hsize
                polyPt(3) = polyPt(0)
                graphicsDevice.FillPolygon(brush, polyPt)
        End Select

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Gets or sets the filled mode of the <see cref="Symbol"/> shape.  Set to True to fill the
    ''' shape with color, or False for an outline symbol.  Note that symbols that are not closed,
    ''' such as <see cref="ShapeType.Plus"/>
    ''' cannot be filled.
    ''' </summary>
    ''' <value> The filled. </value>
    Public Property Filled() As Boolean

    ''' <summary> Gets or sets a property that shows or hides the <see cref="Symbol"/>. </summary>
    ''' <value> True to show the symbol, False to hide it. </value>
    Public Property Visible() As Boolean

    ''' <summary> Gets or sets the pen width for drawing the <see cref="Symbol"/> outline. </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property LineWidth() As Single

    ''' <summary> Gets or sets the line color of the <see cref="Symbol"/> </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property LineColor() As Color

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
    Private ReadOnly _Pane As Pane

    ''' <summary> Gets or sets the size of the <see cref="Symbol"/> </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property Size() As Single

    ''' <summary> Gets or sets the type (shape) of the <see cref="Symbol"/> </summary>
    ''' <value> A <see cref="ShapeType"/> value. </value>
    Public Property Shape() As isr.Visuals.ShapeType

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> A System.String value. </value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="Symbol"/> class that defines the default property values
''' for the <see cref="isr.Visuals.Symbol"/> class.
''' </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public NotInheritable Class SymbolDefaults

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs this class. This constructor is private to ensure only a single instance of this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub New()
        MyBase.New()
        Me._Filled = False
        Me._Visible = True
        Me._LineColor = Color.Red
        Me._LineWidth = 1.0F
        Me._Shape = isr.Visuals.ShapeType.Square
        Me._Size = 7
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _Instance As SymbolDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As SymbolDefaults
        If SymbolDefaults._Instance Is Nothing Then
            SyncLock SymbolDefaults.SyncLocker
                SymbolDefaults._Instance = New SymbolDefaults()
            End SyncLock
        End If
        Return SymbolDefaults._Instance
    End Function

#End Region

    ''' <summary>
    ''' Gets or sets the default fill mode for symbols (<see cref="Symbol.Filled"/> property). true
    ''' to have symbols filled in with color, false to leave them as outlines.
    ''' </summary>
    ''' <value> The filled. </value>
    Public Property Filled() As Boolean

    ''' <summary>
    ''' Gets or sets the default display mode for symbols (<see cref="Symbol.Visible"/> property).
    ''' true to display symbols, false to hide them.
    ''' </summary>
    ''' <value> The visible. </value>
    Public Property Visible() As Boolean

    ''' <summary>
    ''' Gets or sets the default color for drawing symbols (<see cref="Symbol.LineColor"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property LineColor() As Color

    ''' <summary>
    ''' Gets or sets the default pen width to be used for drawing curve symbols
    ''' (<see cref="Symbol.LineWidth"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property LineWidth() As Single

    ''' <summary>
    ''' Gets or sets the default symbol shape type for curves (<see cref="isr.Visuals.ShapeType"/>
    '''   property). Defined as a <see cref="isr.Visuals.ShapeType"/> enumeration.
    ''' </summary>
    ''' <value> The shape. </value>
    Public Property Shape() As isr.Visuals.ShapeType

    ''' <summary>
    ''' Gets or sets the default size for curve symbols (<see cref="Symbol.Size"/>.
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property Size() As Single

End Class
#End Region

