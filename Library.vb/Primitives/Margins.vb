''' <summary> Provides <see cref="T:System.Integer">integer</see> margins properties. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public Structure Margins

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs an instance of this class with specific margins. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">   Left margin. </param>
    ''' <param name="right">  Right margin. </param>
    ''' <param name="top">    Top margin. </param>
    ''' <param name="bottom"> Bottom margin. </param>
    Public Sub New(ByVal left As Integer, ByVal right As Integer, ByVal top As Integer, ByVal bottom As Integer)

        Me._Left = left
        Me._Right = right
        Me._Top = top
        Me._Bottom = bottom

    End Sub

    ''' <summary>
    ''' Constructs an instance of this class with specific margins to convert from
    ''' <see cref="System.Double">Double</see>
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">   Left margin. </param>
    ''' <param name="right">  Right margin. </param>
    ''' <param name="top">    Top margin. </param>
    ''' <param name="bottom"> Bottom margin. </param>
    Public Sub New(ByVal left As Double, ByVal right As Double, ByVal top As Double, ByVal bottom As Double)

        Me.New(Convert.ToInt32(left), Convert.ToInt32(right), Convert.ToInt32(top), Convert.ToInt32(bottom))

    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Left margin. </param>
    ''' <param name="right"> Right margin. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As Margins, ByVal right As Margins) As Boolean
        Return Margins.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Left margin. </param>
    ''' <param name="right"> Right margin. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As Margins, ByVal right As Margins) As Boolean
        Return Not Margins.Equals(left, right)
    End Operator

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Left margin. </param>
    ''' <param name="right"> Right margin. </param>
    ''' <returns>
    ''' <c>True</c> if arguments are the same type and represent the same value; otherwise,
    ''' <c>False</c>.
    ''' </returns>
    Public Overloads Shared Function Equals(ByVal left As Margins, ByVal right As Margins) As Boolean
        Return left.Equals(right)
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="obj"> Another object to compare to. </param>
    ''' <returns>
    ''' <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso Me.GetType() Is obj.GetType() AndAlso Me.Equals(CType(obj, Margins))
    End Function

    ''' <summary>
    ''' Returns True if the value of the <paramref name="other"/> equals to the instance value.
    ''' </summary>
    ''' <remarks>
    ''' Margins are the same if the have the same
    ''' <see cref="Left"/>, <see cref="Right"/>, <see cref="Top"/> and <see cref="Bottom"/>  value.
    ''' </remarks>
    ''' <param name="other"> The <see cref="Margins">Margins</see> to compare for equality with this
    '''                      instance. </param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    Public Overloads Function Equals(ByVal other As Margins) As Boolean
        Return Me._Left.Equals(other._Left) AndAlso Me._Right.Equals(other._Right) AndAlso
               Me._Top.Equals(other._Top) AndAlso Me._Bottom.Equals(other._Bottom)
    End Function

#End Region

#Region " METHODS "

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me._Left.GetHashCode Xor Me._Top.GetHashCode Xor Me._Right.GetHashCode Xor Me._Bottom.GetHashCode
    End Function

    ''' <summary> Calculates the margins scaled to the ScaleFactor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="scaleFactor"> The scaling factor. </param>
    ''' <returns> The margins after scaling by <paramref name="scaleFactor"/> </returns>
    Public Function GetScaledMargins(ByVal scaleFactor As Double) As Margins
        Return New Margins(Me._Left * scaleFactor, Me._Right * scaleFactor, Me._Top * scaleFactor, Me._Bottom * scaleFactor)
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets the bottom margin. </summary>
    ''' <value> A <see cref="T:System.Integer">integer</see> </value>
    Public Property Bottom() As Integer

    ''' <summary> Returns the sum of the left and right margins. </summary>
    ''' <value> A <see cref="T:System.Integer">integer</see> </value>
    Public ReadOnly Property Horizontal() As Integer
        Get
            Return Me._Left + Me._Right
        End Get
    End Property

    ''' <summary> Gets the left margin. </summary>
    ''' <value> A <see cref="T:System.Integer">integer</see> </value>
    Public Property Left() As Integer

    ''' <summary> Gets the right margin. </summary>
    ''' <value> A <see cref="T:System.Integer">integer</see> </value>
    Public Property Right() As Integer

    ''' <summary> Gets the top margin. </summary>
    ''' <value> A <see cref="T:System.Integer">integer</see> </value>
    Public Property Top() As Integer

    ''' <summary> Returns the top left point of the margins. </summary>
    ''' <value> A <see cref="System.Drawing.Point"/> </value>
    Public ReadOnly Property TopLeft() As Point
        Get
            Return New Point(Me._Left, Me._Top)
        End Get
    End Property

    ''' <summary> Returns the sum of the top and bottom margins. </summary>
    ''' <value> A <see cref="T:System.Integer">integer</see> </value>
    Public ReadOnly Property Vertical() As Integer
        Get
            Return Me._Top + Me._Bottom
        End Get
    End Property

#End Region

End Structure

''' <summary> Provides <see cref="System.Single">Single</see> margins properties. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public Structure MarginsF

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs an instance of this class with specific margins. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">   Left margin. </param>
    ''' <param name="right">  Right margin. </param>
    ''' <param name="top">    Top margin. </param>
    ''' <param name="bottom"> Bottom margin. </param>
    Public Sub New(ByVal left As Single, ByVal right As Single, ByVal top As Single, ByVal bottom As Single)

        Me._Left = left
        Me._Right = right
        Me._Top = top
        Me._Bottom = bottom

    End Sub

    ''' <summary>
    ''' Constructs an instance of this class with specific margins to convert from
    ''' <see cref="System.Double">Double</see>
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">   Left margin. </param>
    ''' <param name="right">  Right margin. </param>
    ''' <param name="top">    Top margin. </param>
    ''' <param name="bottom"> Bottom margin. </param>
    Public Sub New(ByVal left As Double, ByVal right As Double, ByVal top As Double, ByVal bottom As Double)

        Me.New(Convert.ToSingle(left), Convert.ToSingle(right), Convert.ToSingle(top), Convert.ToSingle(bottom))

    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Left margin. </param>
    ''' <param name="right"> Right margin. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As MarginsF, ByVal right As MarginsF) As Boolean
        Return Margins.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Left margin. </param>
    ''' <param name="right"> Right margin. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As MarginsF, ByVal right As MarginsF) As Boolean
        Return Not Margins.Equals(left, right)
    End Operator

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Left margin. </param>
    ''' <param name="right"> Right margin. </param>
    ''' <returns>
    ''' <c>True</c> if values are the same type and represent the same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Shared Function Equals(ByVal left As MarginsF, ByVal right As MarginsF) As Boolean
        Return left._Left.Equals(right._Left) AndAlso left._Right.Equals(right._Right) AndAlso
               left._Top.Equals(right._Top) AndAlso left._Bottom.Equals(right._Bottom)
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="obj"> Another object to compare to. </param>
    ''' <returns>
    ''' <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean

        Return If(obj IsNot Nothing AndAlso TypeOf obj Is Margins, Me.Equals(CType(obj, Margins)), False)

    End Function

    ''' <summary>
    ''' Returns True if the value of the <paramref name="compared"/> equals to the instance value.
    ''' </summary>
    ''' <remarks>
    ''' Margins are the same if the have the same
    ''' <see cref="Left"/>, <see cref="Right"/>, <see cref="Top"/> and <see cref="Bottom"/>  value.
    ''' </remarks>
    ''' <param name="compared"> The <see cref="Margins">Margins</see> to compare for equality with
    '''                         this instance. </param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    Public Overloads Function Equals(ByVal compared As MarginsF) As Boolean
        Return Margins.Equals(Me, compared)
    End Function

#End Region

#Region " METHODS "

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me._Left.GetHashCode Xor Me._Top.GetHashCode Xor Me._Right.GetHashCode Xor Me._Bottom.GetHashCode
    End Function

    ''' <summary> Calculates the margins scaled to the ScaleFactor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="scaleFactor"> The scaling factor. </param>
    ''' <returns> The margins after scaling by <paramref name="scaleFactor"/> </returns>
    Public Function GetScaledMargins(ByVal scaleFactor As Double) As MarginsF
        Return New MarginsF(Me._Left * scaleFactor, Me._Right * scaleFactor, Me._Top * scaleFactor, Me._Bottom * scaleFactor)
    End Function

    ''' <summary> Returns an area adjusted by the scaled margins. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="area"> The initial <see cref="System.Drawing.RectangleF"/> area. </param>
    ''' <returns>
    ''' A <see cref="System.Drawing.RectangleF"/> reduced by the horizontal and vertical spans and
    ''' shifted by the top-left margins.
    ''' </returns>
    Public Function GetAdjustedArea(ByVal area As RectangleF) As RectangleF

        ' reduce the size of the area by the total horizontal and vertical spans
        area.Width -= Me.Horizontal
        area.Height -= Me.Vertical

        ' shift the draw rectangle by the top left margins
        area.Offset(Me.TopLeft)

        Return area

    End Function

    ''' <summary> Returns an area adjusted by the scaled margins. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="area">        The initial <see cref="System.Drawing.RectangleF"/> area. </param>
    ''' <param name="scaleFactor"> The scaling factor. </param>
    ''' <returns>
    ''' A <see cref="System.Drawing.RectangleF"/> reduced by the horizontal and vertical spans and
    ''' shifted by the scaled (<paramref name="scaleFactor"/>) top-left margins.
    ''' </returns>
    Public Function GetAdjustedArea(ByVal area As RectangleF, ByVal scaleFactor As Double) As RectangleF

        Return Me.GetScaledMargins(scaleFactor).GetAdjustedArea(area)

    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets the bottom margin. </summary>
    ''' <value> A <see cref="System.Single">Single</see> </value>
    Public Property Bottom() As Single

    ''' <summary> Returns the sum of the left and right margins. </summary>
    ''' <value> A <see cref="System.Single">Single</see> </value>
    Public ReadOnly Property Horizontal() As Single
        Get
            Return Me._Left + Me._Right
        End Get
    End Property

    ''' <summary> Gets the left margin. </summary>
    ''' <value> A <see cref="System.Single">Single</see> </value>
    Public Property Left() As Single

    ''' <summary> Gets the right margin. </summary>
    ''' <value> A <see cref="System.Single">Single</see> </value>
    Public Property Right() As Single

    ''' <summary> Gets the top margin. </summary>
    ''' <value> A <see cref="System.Single">Single</see> </value>
    Public Property Top() As Single

    ''' <summary> Returns the top left point of the margins. </summary>
    ''' <value> A <see cref="System.Drawing.Point"/> </value>
    Public ReadOnly Property TopLeft() As PointF
        Get
            Return New PointF(Me._Left, Me._Top)
        End Get
    End Property

    ''' <summary> Returns the sum of the top and bottom margins. </summary>
    ''' <value> A <see cref="System.Single">Single</see> </value>
    Public ReadOnly Property Vertical() As Single
        Get
            Return Me._Top + Me._Bottom
        End Get
    End Property

#End Region

End Structure

