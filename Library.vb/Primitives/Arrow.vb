''' <summary>
''' Represents a graphic arrow or Cord object on the graph.  A list of Arrow objects is
''' maintained by the <see cref="ArrowCollection"/> collection class.
''' </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public Class Arrow
    Implements ICloneable, IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a instance defining the position of the arrow to be pre-specified.  All other
    ''' properties are set to default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="origin">      The starting point of the <see cref="Arrow"/>. </param>
    ''' <param name="destination"> The ending point of the <see cref="Arrow"/>. </param>
    ''' <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
    Public Sub New(ByVal origin As PointF, ByVal destination As PointF, ByVal drawingPane As Pane)

        MyBase.New()
        If drawingPane Is Nothing Then
            Throw New ArgumentNullException(NameOf(drawingPane))
        End If

        Dim arrowDefaults As ArrowDefaults = ArrowDefaults.[Get]
        Me._LineColor = arrowDefaults.LineColor
        Me._ArrowheadSize = arrowDefaults.ArrowheadSize
        Me._LineWidth = arrowDefaults.LineWidth
        Me._Origin = New PointF(0, 0)
        Me._Destination = New PointF(0.2, 0.2)
        Me._Arrowhead = arrowDefaults.Arrowhead
        Me._CoordinateFrame = arrowDefaults.CoordinateFrame

        Me._Origin = origin
        Me._Destination = destination
        Me._Pane = drawingPane

    End Sub

    ''' <summary>
    ''' Constructs a instance defining the position of the arrow to be pre-specified.  All other
    ''' properties are set to default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x1">          The x position of the starting point that defines the
    '''                            <see cref="Arrow"/>.  The units of this position are specified by
    '''                            the
    '''                            <see cref="CoordinateFrame"/> property. </param>
    ''' <param name="y1">          The y position of the starting point that defines the
    '''                            <see cref="Arrow"/>.  The units of this position are specified by
    '''                            the
    '''                            <see cref="CoordinateFrame"/> property. </param>
    ''' <param name="x2">          The x position of the ending point that defines the
    '''                            <see cref="Arrow"/>.  The units of this position are specified by
    '''                            the
    '''                            <see cref="CoordinateFrame"/> property. </param>
    ''' <param name="y2">          The y position of the ending point that defines the
    '''                            <see cref="Arrow"/>.  The units of this position are specified by
    '''                            the
    '''                            <see cref="CoordinateFrame"/> property. </param>
    ''' <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
    Public Sub New(ByVal x1 As Single, ByVal y1 As Single,
                   ByVal x2 As Single, ByVal y2 As Single, ByVal drawingPane As Pane)

        Me.New(New PointF(x1, y1), New PointF(x2, y2), drawingPane)

    End Sub

    ''' <summary>
    ''' Constructs a instance defining the position, color, and size of the
    ''' <see cref="Arrow"/> to be pre-specified.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="lineColor">     The line <see cref="System.Drawing.Color">Color</see> for the
    '''                              arrow. </param>
    ''' <param name="arrowheadSize"> The size of the arrowhead, measured in points. </param>
    ''' <param name="x1">            The x position of the starting point that defines the arrow.  The
    '''                              units of this position are specified by the
    '''                              <see cref="CoordinateFrame"/> property. </param>
    ''' <param name="y1">            The y position of the starting point that defines the arrow.  The
    '''                              units of this position are specified by the
    '''                              <see cref="CoordinateFrame"/> property. </param>
    ''' <param name="x2">            The x position of the ending point that defines the arrow.  The
    '''                              units of this position are specified by the
    '''                              <see cref="CoordinateFrame"/> property. </param>
    ''' <param name="y2">            The y position of the ending point that defines the arrow.  The
    '''                              units of this position are specified by the
    '''                              <see cref="CoordinateFrame"/> property. </param>
    ''' <param name="drawingPane">   Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
    Public Sub New(ByVal lineColor As Color, ByVal arrowheadSize As Single,
                   ByVal x1 As Single, ByVal y1 As Single,
                   ByVal x2 As Single, ByVal y2 As Single, ByVal drawingPane As Pane)

        Me.New(x1, y1, x2, y2, drawingPane)

        Me._LineColor = lineColor
        Me._ArrowheadSize = arrowheadSize

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The Arrow object from which to copy. </param>
    Public Sub New(ByVal model As Arrow)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Me._Origin = model._Origin
        Me._Destination = model._Destination
        Me._ArrowheadSize = model._ArrowheadSize
        Me._LineColor = model._LineColor
        Me._LineWidth = model._LineWidth
        Me._Arrowhead = model._Arrowhead
        Me._CoordinateFrame = model._CoordinateFrame
        Me._Pane = model._Pane

    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> True if disposed. </summary>
    Private _Disposed As Boolean

    ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._Disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._Disposed = value
        End Set
    End Property

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._LineColor = Nothing

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the Arrow. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the Arrow. </returns>
    Public Function Copy() As Arrow
        Return New Arrow(Me)
    End Function

    ''' <summary> Renders this object to the specified <see cref="Graphics"/> device. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.Graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then Throw New ArgumentNullException(NameOf(graphicsDevice))

        ' get the he origin of the arrow in screen coordinates
        Dim fromPix As PointF = Me._Pane.GeneralTransform(Me._Origin, Me._CoordinateFrame)

        Dim toPix As PointF = Me._Pane.GeneralTransform(Me._Destination, Me._CoordinateFrame)

        If Me._Pane.DrawArea.Contains(fromPix) And Me._Pane.DrawArea.Contains(toPix) Then

            ' get a scaled size for the arrowhead
            Dim scaledSize As Single = Convert.ToSingle(Me._ArrowheadSize * Me._Pane.ScaleFactor)

            ' calculate the length and the angle of the arrow "vector"
            Dim dy As Double = toPix.Y - fromPix.Y
            Dim dx As Double = toPix.X - fromPix.X
            Dim angle As Single = Convert.ToSingle(Math.Atan2(dy, dx)) * 180.0F / Convert.ToSingle(Math.PI)
            Dim length As Single = Convert.ToSingle(Math.Sqrt((dx * dx + dy * dy)))

            ' Save the old transform matrix
            Dim transformMatrix As System.Drawing.Drawing2D.Matrix = graphicsDevice.Transform

            ' Move the coordinate system so it is located at the starting point
            ' of this arrow
            graphicsDevice.TranslateTransform(fromPix.X, fromPix.Y)

            ' Rotate the coordinate system according to the angle of this arrow
            ' about the starting point
            graphicsDevice.RotateTransform(angle)

            ' get a pen according to this arrow properties
            Using pen As New Pen(Me._LineColor, Me._LineWidth)
                ' Draw the line segment for this arrow
                graphicsDevice.DrawLine(pen, 0, 0, length, 0)
            End Using

            ' Only show the arrowhead if required
            If Me._Arrowhead Then

                Using brush As New SolidBrush(Me._LineColor)
                    ' Create a polygon representing the arrowhead based on the scaled size
                    Dim polyPt(3) As PointF
                    Dim xSize As Single = scaledSize
                    Dim ySize As Single = scaledSize / 3.0F
                    polyPt(0).X = length
                    polyPt(0).Y = 0
                    polyPt(1).X = length - xSize
                    polyPt(1).Y = ySize
                    polyPt(2).X = length - xSize
                    polyPt(2).Y = -ySize
                    polyPt(3) = polyPt(0)

                    ' render the arrowhead
                    graphicsDevice.FillPolygon(brush, polyPt)
                End Using


            End If

            ' Restore the transform matrix back to its original state
            graphicsDevice.Transform = transformMatrix

        Else

            Debug.Assert(Not Debugger.IsAttached, "Arrow out of range")

        End If

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Gets or sets the size of the arrowhead. The display of the arrowhead can be enabled or
    ''' disabled with the <see cref="ArrowHead"/> property.
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property ArrowheadSize() As Single

    ''' <summary>
    ''' Gets or sets the coordinate system to be used for defining the <see cref="Arrow"/> position.
    ''' </summary>
    ''' <value> A <see cref="CoordinateFrameType"/> </value>
    Public Property CoordinateFrame() As CoordinateFrameType

    ''' <summary>
    ''' Gets or sets the destination or ending point of <see cref="Arrow"/> segment.
    ''' </summary>
    ''' <value>
    ''' A <see cref="System.Drawing.PointF">Point</see> with units according to the
    ''' <see cref="coordinateFrame"/>
    ''' </value>
    Public Property Destination() As PointF

    ''' <summary> Determines whether or not to draw an arrowhead. </summary>
    ''' <value>
    ''' A <see cref="System.Boolean">Boolean</see> <c>True</c> if an arrowhead is to be drawn, False
    ''' otherwise.
    ''' </value>
    Public Property Arrowhead() As Boolean

    ''' <summary>
    ''' The <see cref="System.Drawing.Color">Color</see> of the arrowhead and line segment.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property LineColor() As Color

    ''' <summary> The width of the line segment for the <see cref="Arrow"/> </summary>
    ''' <value> A <see cref="System.Single">Single</see> width in pixels. </value>
    Public Property LineWidth() As Single

    ''' <summary> Gets or sets the origin or starting point of <see cref="Arrow"/> segment. </summary>
    ''' <value>
    ''' A <see cref="System.Drawing.PointF">Point</see> with units according to the
    ''' <see cref="coordinateFrame"/>
    ''' </value>
    Public Property Origin() As PointF

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
    Private ReadOnly _Pane As Pane

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> A System.String value. </value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="Arrow" /> class that defines the default property values
''' for the <see cref="Arrow" /> class.
''' </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public NotInheritable Class ArrowDefaults

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs this class. This constructor is private to ensure only a single instance of this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub New()
        MyBase.New()
        Me._ArrowheadSize = 12.0F
        Me._CoordinateFrame = CoordinateFrameType.AxisXYScale
        Me._Arrowhead = True
        Me._LineWidth = 1.0F
        Me._LineColor = Color.Red
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _Instance As ArrowDefaults

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As ArrowDefaults
        If ArrowDefaults._Instance Is Nothing Then
            SyncLock ArrowDefaults.SyncLocker
                ArrowDefaults._Instance = New ArrowDefaults()
            End SyncLock
        End If
        Return ArrowDefaults._Instance
    End Function

#End Region

    ''' <summary> Gets or sets the default size for the <see cref="Arrow"/>. </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property ArrowheadSize() As Single

    ''' <summary>
    ''' Gets or sets the default coordinate system to be used for defining the
    ''' <see cref="Arrow"/> location coordinates
    ''' (<see cref="Arrow.CoordinateFrame"/> property).
    ''' </summary>
    ''' <value> A <see cref="CoordinateFrameType"/> </value>
    Public Property CoordinateFrame() As CoordinateFrameType

    ''' <summary>
    ''' Gets or sets the default display mode for the <see cref="Arrow"/> item Arrowhead
    ''' (<see cref="Arrow.ArrowHead"/> property).  true to show the arrowhead, false to hide it.
    ''' </summary>
    ''' <value> The arrowhead. </value>
    Public Property Arrowhead() As Boolean

    ''' <summary>
    ''' Gets or sets the default line width used for the <see cref="Arrow"/> line segment
    ''' (<see cref="Arrow.LineWidth"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property LineWidth() As Single

    ''' <summary>
    ''' Gets or sets the default color used for the <see cref="Arrow"/> line segment and arrowhead
    ''' (<see cref="Arrow.LineColor"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property LineColor() As Color

End Class

#End Region

