Imports isr.Core.Constructs

''' <summary> Defines a graph Axis. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public Class Axis

    Implements IDisposable

    ' update: Axis
    '   add AxisPoint to specify the point on the perpendicular AxisArea location of the axis
    '   allow to add multiple X axis like multiple Y axis
    '     each curve is associated with a pair of X,Y axis
    '     eventually we should be able to draw axis for each curve if not
    '     the same as other axes.
    '   Allow to draw axis tick marks at AxisPoint (default inside, outside)
    ' update: check why Y axis is off one pixel

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs an <see cref="Axis"/> with specific title and type and default properties from the
    ''' <see cref="AxisDefaults"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="title">       The <see cref="Title"/> caption for this axis. </param>
    ''' <param name="type">        The <see cref="AxisType"/> for this axis. </param>
    ''' <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
    Public Sub New(ByVal title As String, ByVal type As AxisType, ByVal drawingPane As Pane)

        MyBase.New()
        Me._StatusMessage = String.Empty
        If String.IsNullOrWhiteSpace(title) Then
            title = String.Empty
        End If
        If drawingPane Is Nothing Then
            Throw New ArgumentNullException(NameOf(drawingPane))
        End If

        Me._Pane = drawingPane
        Me._MajorTick = New Tick(True)
        Me._MinorTick = New Tick(False)
        Me._Grid = New Grid
        Me._TickLabels = New Labels(Me._Pane)
        Me._Title = New AxisTitle(title, Me._Pane)

        Dim scaleDefaults As ScaleDefaults = ScaleDefaults.[Get]
        Me._Visible = scaleDefaults.Visible
        Me._Max = scaleDefaults.Max.Copy()
        Me._Min = scaleDefaults.Min.Copy()
        Me._ScaleExponent = scaleDefaults.ScaleExponent.Copy()
        Me._CoordinateScale = New CoordinateScale With {
            .CoordinateScaleType = scaleDefaults.CoordinateScale
        }
        Me._Reversed = scaleDefaults.Reversed

        Me._Visible = True

        Me._LineColor = AxisDefaults.[Get].LineColor

        Me._AxisType = type
        Select Case Me._AxisType
            Case AxisType.X
                Me._Title.Appearance.Angle = 0.0F
                Me._TickLabels.Appearance.Angle = 0.0F
            Case AxisType.Y
                Me._Title.Appearance.Angle = -180.0F
                Me._TickLabels.Appearance.Angle = 90.0F
            Case AxisType.Y2
                Me._Title.Appearance.Angle = 0.0F
                Me._TickLabels.Appearance.Angle = -90.0F
            Case Else
                Debug.Assert(Not Debugger.IsAttached, "Unhandled axis type")
        End Select

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The Axis object from which to copy. </param>
    Protected Sub New(ByVal model As Axis)

        MyBase.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Me._AxisType = model._AxisType
        Me._Visible = model._Visible
        Me._Reversed = model._Reversed
        Me._Min = model._Min
        Me._Max = model._Max
        Me._CoordinateScale = model._CoordinateScale
        Me._ScaleExponent = model._ScaleExponent

        Me._Grid = model._Grid.Copy()
        Me._MajorTick = model._MajorTick.Copy()
        Me._MinorTick = model._MinorTick.Copy()
        Me._TickLabels = model._TickLabels.Copy()
        Me._Title = model._Title.Copy()

        Me._Visible = model._Visible
        Me._LineColor = model._LineColor

        Me._Pane = model._Pane

    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> True if disposed. </summary>
    Private _Disposed As Boolean

    ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._Disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._Disposed = value
        End Set
    End Property

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._LineColor = Nothing
                    If Me._Grid IsNot Nothing Then
                        Me._Grid.Dispose()
                        Me._Grid = Nothing
                    End If
                    If Me._MajorTick IsNot Nothing Then
                        Me._MajorTick.Dispose()
                        Me._MajorTick = Nothing
                    End If
                    If Me._MinorTick IsNot Nothing Then
                        Me._MinorTick.Dispose()
                        Me._MinorTick = Nothing
                    End If
                    If Me._TickLabels IsNot Nothing Then
                        Me._TickLabels.Dispose()
                        Me._TickLabels = Nothing
                    End If
                    If Me._Title IsNot Nothing Then
                        Me._Title.Dispose()
                        Me._Title = Nothing
                    End If
                    Me._Max = Nothing
                    Me._Min = Nothing
                    Me._ScaleExponent = Nothing

                End If

                ' Free shared unmanaged resources
            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Renders the <see cref="Axis"/> to the specified <see cref="Graphics"/>
    ''' device.  This method is normally only called by the Draw method of the parent
    ''' <see cref="Pane"/> object.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.Graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        Dim axisArea As RectangleF = Me._Pane.AxisArea
        Dim scaleFactor As Double = Me._Pane.ScaleFactor

        If Me._Min.Value < Me._Max.Value Then

            ' save the transform matrix
            Dim saveMatrix As System.Drawing.Drawing2D.Matrix = graphicsDevice.Transform

            Me.SetTransformMatrix(graphicsDevice, axisArea)

            If Me.Valid Then

                If Me._Visible Then

                    ' draw the axis at the bottom
                    ' upgrade: draw to defined axis point.
                    Using p As New Pen(Me._MajorTick.LineColor, Me._MajorTick.LineWidth)
                        graphicsDevice.DrawLine(p,
                                                Me._TransformedArea.Left, Me._TransformedArea.Bottom,
                                                Me._TransformedArea.Right, Me._TransformedArea.Bottom)
                    End Using

                End If

                If Me._MajorTick.Visible OrElse Me._MinorTick.Visible OrElse Me._TickLabels.Visible OrElse Me._Grid.Visible Then

                    If Me._CoordinateScale.CoordinateScaleType <> CoordinateScaleType.StripChart Then
                        ' set the major tick locations
                        Me._MajorTick.SetLocations(Me)

                        ' set the minor tick locations
                        Me._MinorTick.SetLocations(Me)
                    End If

                    ' draw the major ticks
                    Me._MajorTick.Draw(graphicsDevice, Me, scaleFactor)

                    ' draw minor ticks
                    Me._MinorTick.Draw(graphicsDevice, Me, scaleFactor)

                    ' draw the grid
                    Me._Grid.Draw(graphicsDevice, Me._MajorTick, Me._TransformedArea.Top)

                    ' draw ticks labels 
                    Me._TickLabels.Draw(graphicsDevice, Me, Me._MajorTick, scaleFactor)

                End If

            End If

            Me.DrawTitle(graphicsDevice, scaleFactor)

            ' restore the transform matrix
            graphicsDevice.Transform = saveMatrix

        End If

    End Sub

    ''' <summary> Get the range of the <see cref="Axis"/> for date scale. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The calculated date range. </returns>
    Public Function GetDateRange() As RangeDateTime
        Return New RangeDateTime(JulianDate.ToDateTime(Me._Min.Value), JulianDate.ToDateTime(Me._Max.Value))
    End Function

    ''' <summary>
    ''' Calculate the space required for this <see cref="Axis"/>
    ''' object.  This is the space between the drawArea and the <see cref="Pane.AxisArea"/> for this
    ''' particular axis.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.Graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
    '''                               <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                               calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                               The scale factor is applied to fonts, symbols, etc. </param>
    ''' <returns> A <see cref="System.Drawing.SizeF">SizeF</see> in pixels. </returns>
    Friend Function GetSpace(ByVal graphicsDevice As Graphics, ByVal scaleFactor As Double) As SizeF

        ' axisArea is the actual area of the plot as bounded by the axes
        Dim width As Single = 0
        Dim height As Single = 0
        Dim space As Single
        Dim spaceSize As SizeF

        If Not Me._Visible Then
            Return New SizeF(width, height)
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        ' Account for the Axis
        If Me._MajorTick.Visible Then
            ' get space required for ticks
            space = Me._MajorTick.GetScaledLength(scaleFactor)
        ElseIf Me._MinorTick.Visible Then
            ' get space required for ticks
            space = Me._MinorTick.GetScaledLength(scaleFactor)
        End If
        If Me.Horizontal Then
            height += space
        Else
            width += space
        End If

        If Me.TickLabels.Visible AndAlso (Not Me._CoordinateScale.IsStripChart) Then

            ' set bounding box for text labels
            Me._TickLabels.SetBoundingBox(graphicsDevice, Me, scaleFactor)

            ' get space required for tick labels
            spaceSize = Me._TickLabels.BoundingBox
            If Me.Horizontal Then
                height += spaceSize.Height
                width += spaceSize.Width / 2
            Else
                ' the Y axis size returns rotated size.
                height += spaceSize.Width / 2
                width += spaceSize.Height
            End If
        End If

        If Me.Title.Visible Then
            ' Add space for the axis title
            spaceSize = Me.Title.MeasureString(graphicsDevice, scaleFactor)
            If Me.Horizontal Then
                height += spaceSize.Height
            Else
                width += spaceSize.Height
            End If
        End If

        Return New SizeF(width, height)

    End Function

    ''' <summary>
    ''' Increments the screen positions for drawing time series major and minor tick marks and grid
    ''' lines.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Friend Sub IncrementTickOrigin()
        Me._MinorTick.IncrementOrigin()
        Me._MajorTick.IncrementOrigin()
    End Sub

    ''' <summary>
    ''' Reverse transform the user coordinates (scale value)
    ''' given a graphics device coordinate (pixels).  This method takes into account the scale range
    ''' (<see cref="Min"/> and <see cref="Max"/>), logarithmic state
    ''' (<see cref="CoordinateScale.IsLog"/>), scale reverse state (<see cref="Reversed"/>) and axis
    ''' type (<see cref="AxisType.X"/>,
    ''' <see cref="AxisType.Y"/>, or <see cref="AxisType.Y2"/>).  Note that
    ''' screen range must be set before calling this method.  This is best done every time the axis
    ''' is drawn (<see cref="Axis.Draw"/>.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="screenValue"> The screen pixel value, in graphics device coordinates to
    '''                            transform. </param>
    ''' <returns>
    ''' A <see cref="System.Single">Single</see> user scale value that corresponds to a screen pixel
    ''' location.
    ''' </returns>
    Public Function InverseTransform(ByVal screenValue As Single) As Double

        Dim userValue As Double = Me._UserScaleRange.Span / Me._ScreenScaleRange.Span

        ' see if the sign of the equation needs to be reversed
        If Me._Reversed = Me.Horizontal Then
            userValue *= Me._ScreenScaleRange.Max - screenValue
        Else
            userValue *= screenValue - Me._ScreenScaleRange.Min
        End If
        userValue += Me._UserScaleRange.Min

        If Me._CoordinateScale.IsLog Then
            userValue = Math.Pow(10.0, userValue)
        End If

        Return userValue

    End Function

    ''' <summary>
    ''' Transform the coordinate value from user coordinates (scale value)
    ''' to graphics device coordinates (pixels), assuming that the origin has been set to the "left"
    ''' of this axis, facing from the label side. Note that the left side corresponds to the scale
    ''' minimum for the X and Y2 axes, but it is the scale maximum for the Y axis. This method takes
    ''' into account the scale range (<see cref="Min"/> and
    ''' <see cref="Max"/>), scale type (<see cref="coordinatescale.IsLog"/>), scale reverse state
    ''' (<see cref="Reversed"/>) and axis type (<see cref="AxisType.X"/>,
    ''' <see cref="AxisType.Y"/>, or <see cref="AxisType.Y2"/>).  Note that
    ''' the axis screen range must set before calling this method.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x"> The coordinate value, in user scale units, to be transformed. </param>
    ''' <returns>
    ''' A <see cref="System.Single">Single</see> value transformed to screen space for use in calling
    ''' the <see cref="Draw"/> sub-methods.
    ''' </returns>
    Friend Function LocalTransform(ByVal x As Double) As Single

        ' Must take into account Log, and Reverse Axes
        Dim ratio As Double
        Dim rv As Single

        ratio = If(Me._CoordinateScale.IsLog,
            (x.Log10 - Me._UserScaleRange.Min) / Me._UserScaleRange.Span,
            (x - Me._UserScaleRange.Min) / Me._UserScaleRange.Span)
        rv = If(Me._Reversed AndAlso (Me._AxisType <> AxisType.Y) OrElse (Not Me._Reversed AndAlso (Me._AxisType = AxisType.Y)),
            Convert.ToSingle(Me._ScreenScaleRange.Span * (1.0F - ratio)),
            Convert.ToSingle(Me._ScreenScaleRange.Span * ratio))
        Return rv

    End Function

    ''' <summary>
    ''' Create an array of default values for a curve associated with this axis.  
    ''' The default values are simple ordinals based on the number of axis labels.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> An ordinal <see cref="System.Double">Double</see> array. </returns>
    Public Function MakeDefaultArray() As Double()

        Dim length As Integer = 10
        If Me._CoordinateScale.IsText AndAlso Me._TickLabels.TextLabels IsNot Nothing Then
            length = Me._TickLabels.TextLabels.Length
        End If
        Dim defaultArray(length) As Double

        Dim i As Integer
        For i = 0 To length - 1
            defaultArray(i) = i
        Next i
        Return defaultArray

    End Function

    ''' <summary>
    ''' Use existing range to set tick spacing, decimal spaces and scale magnitude multiplier.  
    ''' </summary>
    ''' <remarks> Use this method to rescale after setting the range manually. </remarks>
    Public Sub Rescale()

        ' call rescale with existing values
        Me.Rescale(Me._Min.Value, Me._Max.Value)

    End Sub

    ''' <summary>
    ''' Select a scale range for the data.  The scale auto ranges and sets the tick spacing, decimal
    ''' spaces and scale magnitude multiplier as necessary.  
    ''' The scaling also adjusts the minimum and maximum based on zero level.
    ''' <para>On Exit:</para>
    ''' <para><see cref="Min"/> is set to scale minimum (if
    ''' <see cref="isr.Visuals.AutoValueR.AutoScale"/> = True)</para>
    ''' <para><see cref="Max"/> is set to scale maximum (if
    ''' <see cref="isr.Visuals.AutoValueR.AutoScale"/> = True)</para>
    ''' <para><see cref="MajorTick"/> is set (if <see cref="isr.Visuals.AutoValueR.AutoScale"/> =
    ''' True)</para>
    ''' <para><see cref="MinorTick"/> is set (if <see cref="isr.Visuals.AutoValueR.AutoScale"/> =
    ''' True)</para>
    ''' <para><see cref="ScaleExponent"/> is set</para>
    ''' <para><see cref="Labels.DecimalPlaces"/> is set if
    ''' <see cref="isr.Visuals.AutoValueR.AutoScale"/> = True)</para>
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="minValue"> The minimum value of the data range for setting this
    '''                         <see cref="Axis"/> scale range. </param>
    ''' <param name="maxValue"> The maximum value of the data range for setting this
    '''                         <see cref="Axis"/> scale range. </param>
    Public Sub Rescale(ByVal minValue As Double, ByVal maxValue As Double)

        ' set initial range
        Me.SetInitialUserScaleRange(minValue, maxValue)

        ' adjust the scale range for the major spacing
        Me.SetFinalUserScaleRange()

        If Me._TickLabels.Visible Then

            ' Set the labels scale format (valid for date type only)
            Me._TickLabels.ScaleFormat = Me._MajorTick.ScaleFormat

            ' set the label decimal places
            Me._TickLabels.SetDecimalPlaces(Me, Me._MajorTick.Spacing, Me._ScaleExponent.Value, Me._CoordinateScale)

        End If

    End Sub

    ''' <summary> Set the range of the <see cref="Axis"/> for date scale. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="fromDate"> Starting <see cref="System.DateTime">DateTime</see> </param>
    ''' <param name="toDate">   Ending <see cref="System.DateTime">DateTime</see> </param>
    Public Sub SetDateRange(ByVal fromDate As DateTime, ByVal toDate As DateTime)
        Me._Min.Value = JulianDate.FromDateTime(fromDate)
        Me._Max.Value = JulianDate.FromDateTime(toDate)
    End Sub

    ''' <summary> Set the range of the <see cref="Axis"/> for date scale. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="fromDate"> Starting <see cref="System.DateTimeOffset">DateTime</see> </param>
    ''' <param name="toDate">   Ending <see cref="System.DateTimeOffset">DateTime</see> </param>
    Public Sub SetDateRange(ByVal fromDate As DateTimeOffset, ByVal toDate As DateTimeOffset)
        Me.SetDateRange(fromDate.DateTime, toDate.DateTime)
    End Sub

    ''' <summary>
    ''' Sets the axis scale to specified range.  Rescale can be called later to adjust the tick
    ''' spacing and tick locations.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="minValue"> The minimum value of the data range for setting this
    '''                         <see cref="Axis"/> scale range. </param>
    ''' <param name="maxValue"> The maximum value of the data range for setting this. </param>
    Public Sub SetRange(ByVal minValue As Double, ByVal maxValue As Double)

        ' set the range
        Me._Min.Value = minValue
        Me._Max.Value = maxValue

    End Sub

    ''' <summary> Sets the screen range for rendering the <see cref="Axis"/>. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="axisArea"> The <see cref="System.Drawing.RectangleF"/> that that contains the
    '''                         area bounded by the axes. </param>
    Friend Sub SetScreenRange(ByVal axisArea As RectangleF)

        ' save the <see cref="Pane.AxisArea"/> data for transforming scale values to pixels
        Me._ScreenScaleRange = If(Me.Horizontal, New RangeF(axisArea.Left, axisArea.Right), New RangeF(axisArea.Top, axisArea.Bottom))

    End Sub

    ''' <summary>
    ''' Transform the coordinate value from user coordinates (scale value)
    ''' to graphics device coordinates (pixels).  This method takes into account the scale range
    ''' (<see cref="Min"/> and <see cref="Max"/>), scale type (<see cref="Coordinatescale.IsLog"/>),
    ''' scale reverse state (<see cref="Reversed"/>)
    ''' and axis type (<see cref="AxisType.X"/>, <see cref="AxisType.Y"/>, or
    ''' <see cref="AxisType.Y2"/>).  
    ''' Note that the screen ranges must be set before using this method.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x"> The coordinate value, in user scale units, to be transformed. </param>
    ''' <returns>
    ''' A <see cref="System.Single">Single</see> coordinate value transformed to screen space for use
    ''' in the <see cref="Graphics"/> draw routines.
    ''' </returns>
    Public Function Transform(ByVal x As Double) As Single

        ' Must take into account Log, and Reverse Axes
        Dim ratio As Double = If(Me._CoordinateScale.IsLog,
            (x.Log10 - Me._UserScaleRange.Min) / Me._UserScaleRange.Span,
            (x - Me._UserScaleRange.Min) / Me._UserScaleRange.Span)
        Dim offset As Double = Me._ScreenScaleRange.Span * ratio
        If Me._Reversed AndAlso Me.Horizontal Then
            Return Convert.ToSingle(Me._ScreenScaleRange.Max - offset)
        ElseIf Me.Horizontal Then
            Return Convert.ToSingle(Me._ScreenScaleRange.Min + offset)
        ElseIf Me._Reversed Then
            Return Convert.ToSingle(Me._ScreenScaleRange.Min + offset)
        Else
            Return Convert.ToSingle(Me._ScreenScaleRange.Max - offset)
        End If

    End Function

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary>
    ''' Draw the title for this <see cref="Axis"/>.  On entry, it is assumed that the graphics
    ''' transform has been configured so that the origin is at the left side of this axis, and the
    ''' axis is aligned along the X coordinate direction.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.Graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="scaleFactor">    The scaling factor for the chart with reference to the chart
    '''                               <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                               calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                               The scale factor is applied to fonts, symbols, etc. </param>
    Private Sub DrawTitle(ByVal graphicsDevice As Graphics, ByVal scaleFactor As Double)

        If Not Me._Visible Then
            Return
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        Dim str As String = Me._Title.ToString(Me._ScaleExponent.Value)

        ' If the Axis is visible, draw the title
        If str.Length > 0 Then

            ' Calculate the title position in screen coordinates
            Dim x As Single = Me._ScreenScaleRange.Span / 2
            Dim y As Single = Me._MajorTick.GetScaledLength(scaleFactor) + Me._TickLabels.BoundingBox.Height
            Me._Title.Draw(graphicsDevice, Me, str, x, y, scaleFactor)

        End If

    End Sub

    ''' <summary>
    ''' Adjust the scale range to match the tick spacing and set the user scale range and exponent.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub SetFinalUserScaleRange()

        ' reset the spacing for getting the correct tick count.
        Me._MajorTick.SetSpacing(Me)
        Me._MinorTick.SetSpacing(Me)

        Select Case Me._CoordinateScale.CoordinateScaleType

            Case CoordinateScaleType.Date

                ' set the user range
                Me._UserScaleRange = New RangeR(Me._Min.Value, Me._Max.Value)

                ' Never use a magnitude shift for date scales
                Me._ScaleExponent.Value = 0

            Case CoordinateScaleType.Linear, CoordinateScaleType.StripChart

                ' Calculate the scale minimum
                If Me._Min.AutoScale AndAlso (Me._Min.Value <> 0) Then
                    Me._Min.Value -= Me._Min.Value.Mod(Me._MajorTick.Spacing)
                    If Me._Min.Value <> 0 Then
                        '_min.Value = majorTickSpacing * Math.Round(Me._min.Value / majorTickSpacing)
                        Dim roundingDigits As Integer = Me._MajorTick.DecimalPlaces(Me) + 2
                        roundingDigits = CInt(Math.Max(roundingDigits, 0))
                        roundingDigits = CInt(Math.Min(roundingDigits, 15))
                        Me._Min.Value = Math.Round(Me._Min.Value, roundingDigits)
                    End If
                End If
                ' Calculate the scale maximum
                If Me._Max.AutoScale Then
                    Dim range As Double = Me._Max.Value - Me._Min.Value
                    Dim rangeRemainder As Double = range.Mod(Me._MajorTick.Spacing)
                    If rangeRemainder <> 0 Then
                        range += Me._MajorTick.Spacing - range.Mod(Me._MajorTick.Spacing)
                    End If
                    'range = majorTickSpacing * Math.Round(range / majorTickSpacing)
                    Dim roundingDigits As Integer = Me._MajorTick.DecimalPlaces(Me) + 2
                    roundingDigits = CInt(Math.Max(roundingDigits, 0))
                    roundingDigits = CInt(Math.Min(roundingDigits, 15))
                    range = Math.Round(range, roundingDigits)
                    Me._Max.Value = Me._Min.Value + range
                End If

                ' set the scale range
                Me._UserScaleRange = New RangeR(Me._Min.Value, Me._Max.Value)

                ' set the scale magnitude if required
                If Me._ScaleExponent.AutoScale Then

                    ' get the scale exponent based on the range limits
                    Me._ScaleExponent.Value = Me._UserScaleRange.GetExponent(Me._IsEngineeringScale)

                End If

            Case CoordinateScaleType.Log

                ' set the user range
                Me._UserScaleRange = New RangeR(Me._Min.Value.Log10, Me._Max.Value.Log10)

                If Me._ScaleExponent.AutoScale Then
                    ' Never use a magnitude shift for log scales
                    Me._ScaleExponent.Value = 0
                End If

            Case CoordinateScaleType.Text

                ' set the user range
                Me._UserScaleRange = New RangeR(Me._Min.Value, Me._Max.Value)

                ' Never use a magnitude shift for text scales
                Me._ScaleExponent.Value = 0

            Case Else

                Debug.Assert(Not Debugger.IsAttached, $"Unhandled coordinate scale type {Me._CoordinateScale.CoordinateScaleType}")

        End Select

        ' set the tick values
        Me._MajorTick.SetValues(Me)

    End Sub

    ''' <summary>
    ''' Sets a reasonable scale given a range of data values.  The scale range is chosen based on
    ''' increments of 1, 2, or 5 (because they are even divisors of
    ''' 10).  This method auto scales based on the <see cref="AutoValueR.AutoScale"/> and
    ''' <see cref="AutoValueR.AutoScale"/> settings.  <para>On Exit:</para>
    ''' <para><see cref="Min"/> is set to scale minimum (if <see cref="AutoValueR.AutoScale"/> =
    ''' True)</para>
    ''' <para><see cref="Max"/> is set to scale maximum (if <see cref="AutoValueR.AutoScale"/> =
    ''' True)</para>
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="minValue"> The minimum value of the data range for setting this
    '''                         <see cref="Axis"/> scale range. </param>
    ''' <param name="maxValue"> The maximum value of the data range for setting this
    '''                         <see cref="Axis"/> scale range. </param>
    Private Sub SetInitialUserScaleRange(ByVal minValue As Double, ByVal maxValue As Double)

        ' if the scales are auto ranged, use the actual data values for the range
        If Me._Min.AutoScale Then
            Me._Min.Value = minValue
        End If
        If Me._Max.AutoScale Then
            Me._Max.Value = maxValue
        End If

        Select Case Me._CoordinateScale.CoordinateScaleType

            Case CoordinateScaleType.Date

                ' Date Scale

                ' Test for trivial condition of range = 0 and pick a suitable default
                If Me.Range < ScaleDefaults.[Get].MinRange Then

                    If Me._Max.AutoScale Then
                        Me._Max.Value = 1.0 + Me._Min.Value
                    Else
                        Me._Min.Value = Me._Max.Value - 1.0
                    End If

                End If

                ' Calculate the scale minimum
                If Me._Min.AutoScale Then
                    Me._Min.Value = Me._MajorTick.GetEvenDateSpacing(Me._Min.Value, -1)
                End If

                ' Calculate the scale maximum
                If Me._Max.AutoScale Then
                    Me._Max.Value = Me._MajorTick.GetEvenDateSpacing(Me._Max.Value, 1)
                End If

            Case CoordinateScaleType.Linear, CoordinateScaleType.StripChart

                ' linear scale
                ' Test for trivial condition of range = 0 and pick a suitable default
                If Me.Range < ScaleDefaults.[Get].MinRange Then
                    If Me._Max.AutoScale Then
                        Me._Max.Value = 1.0 + Me._Min.Value
                    Else
                        Me._Min.Value = Me._Max.Value - 1.0
                    End If
                End If

                ' This is the zero-lever test.  If minValue is within the zero lever fraction
                ' of the data range, then use zero.
                If Me._Min.AutoScale AndAlso (Me._Min.Value > 0) AndAlso ((Me._Min.Value / Me.Range) < ScaleDefaults.[Get].ZeroLever) Then
                    Me._Min.Value = 0
                End If
                ' Repeat the zero-lever test for cases where the maxValue is less than zero
                If Me._Max.AutoScale AndAlso (Me._Max.Value < 0) AndAlso (Math.Abs((Me._Max.Value / Me.Range)) < ScaleDefaults.[Get].ZeroLever) Then
                    Me._Max.Value = 0
                End If

            Case CoordinateScaleType.Log

                ' Log Scale: Check for bad data range

                If Me._Min.Value <= 0.0 AndAlso Me._Max.Value <= 0.0 Then
                    Me._Min.Value = 1.0
                    Me._Max.Value = 10.0
                ElseIf Me._Min.Value <= 0.0 Then
                    Me._Min.Value = Me._Max.Value / 10.0
                ElseIf Me._Max.Value <= 0.0 Then
                    Me._Max.Value = Me._Min.Value * 10.0
                End If

                ' Test for trivial condition of range = 0 and pick a suitable default
                If Me.Range < ScaleDefaults.[Get].MinRange Then
                    If Me._Max.AutoScale Then
                        Me._Max.Value = Me._Min.Value * 10.0
                    Else
                        Me._Min.Value = Me._Max.Value / 10.0
                    End If
                End If

                ' Get the nearest power of 10 (no partial log cycles allowed)
                If Me._Min.AutoScale Then
                    Me._Min.Value = Math.Pow(10.0R, Math.Floor(Math.Log10(Me._Min.Value)))
                End If
                If Me._Max.AutoScale Then
                    Me._Max.Value = Math.Pow(10.0R, Math.Ceiling(Math.Log10(Me._Max.Value)))
                End If

            Case CoordinateScaleType.Text

                ' if this is a text-based axis, then ignore all settings and make it simply ordinal

                ' if text labels are provided, then auto range to the number of labels
                If Me._TickLabels.TextLabels IsNot Nothing Then
                    If Me._Min.AutoScale Then
                        Me._Min.Value = 0
                    End If
                    If Me._Max.AutoScale Then
                        Me._Max.Value = Me._TickLabels.TextLabels.Length + 1
                    End If
                End If

                ' Test for trivial condition of range = 0 and pick a suitable default
                If Me.Range < 0.1 Then
                    If Me._Max.AutoScale Then
                        Me._Max.Value = Me._Min.Value + 10.0
                    Else
                        Me._Min.Value = Me._Max.Value - 10.0
                    End If
                End If

            Case Else

                Debug.Assert(Not Debugger.IsAttached, $"Unhandled coordinate scale type {Me._CoordinateScale.CoordinateScaleType}")

        End Select

    End Sub

    ''' <summary> Setup the Transform Matrix to handle drawing of this <see cref="Axis"/> </summary>
    ''' <remarks>
    ''' The flexibility offered by the transform matrix of the GDI+ drawing library allows the same
    ''' code to be employed for drawing all three axes. This methods translates and rotates the axis
    ''' so that it is oriented along the X direction with the origin at the left edge of the axis
    ''' when looking from the label size (i.e., from the left of the left Y axis or the right of the
    ''' right Y axis).
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.Graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="axisArea">       The axis area. </param>
    Private Sub SetTransformMatrix(ByVal graphicsDevice As Graphics, ByVal axisArea As RectangleF)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        Select Case Me._AxisType

            Case AxisType.X

                ' Move the origin to the BottomLeft of the axisArea, which is the left
                ' side of the X axis (facing from the label side)
                graphicsDevice.TranslateTransform(axisArea.Left, axisArea.Bottom)
                Me._TransformedArea = New RectangleF(0, -axisArea.Height, axisArea.Width, axisArea.Height)

            Case AxisType.Y

                ' Move the origin to the TopLeft of the axisArea, which is the left
                ' side of the axis (facing from the label side)
                graphicsDevice.TranslateTransform(axisArea.Left, axisArea.Top)

                ' rotate so this axis is in the left-right direction
                graphicsDevice.RotateTransform(90)

                ' define the transformed area
                Me._TransformedArea = New RectangleF(0, -axisArea.Width, axisArea.Height, axisArea.Width)

            Case AxisType.Y2

                ' Move the origin to the BottomRight of the axisArea, which is the left
                ' side of the Y2 axis (facing from the label side)
                graphicsDevice.TranslateTransform(axisArea.Right, axisArea.Bottom)

                ' rotate so this axis is in the left-right direction
                graphicsDevice.RotateTransform(-90)

                ' define the transformed area
                Me._TransformedArea = New RectangleF(0, axisArea.Width, axisArea.Height, -axisArea.Width)

            Case Else

                Debug.Assert(Not Debugger.IsAttached, "Unhandled axis type")

        End Select

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Gets the decimal places represented by the minimum absolute value of the range, min, or
    ''' maximum values.
    ''' </summary>
    ''' <value> The decimal places. </value>
    Public ReadOnly Property DecimalPlaces() As Integer
        Get
            Dim baseValue As Double = Math.Abs(Me.Range)
            Dim baseCandidate As Double = Math.Abs(Me._Max.Value)
            If (baseCandidate > ScaleDefaults.[Get].MinRange) AndAlso (baseCandidate < baseValue) Then
                baseValue = baseCandidate
            End If
            baseCandidate = Math.Abs(Me._Min.Value)
            If (baseCandidate > ScaleDefaults.[Get].MinRange) AndAlso (baseCandidate < baseValue) Then
                baseValue = baseCandidate
            End If
            Return baseValue.DecimalPlaces
        End Get
    End Property

    ''' <summary> Gets or sets the <see cref="Grid"/> for drawing major grid lines. </summary>
    ''' <value> A <see cref="Grid"/> value. </value>
    Public Property Grid() As isr.Visuals.Grid

    ''' <summary>
    ''' Gets or sets the overall auto scale property of the axis.  When set, this property sets the
    ''' axis range and spacing to auto scale.
    ''' </summary>
    ''' <value> The automatic scale. </value>
    Public Property AutoScale() As Boolean
        Get
            Return Me._Min.AutoScale
        End Get
        Set(ByVal value As Boolean)
            Me._Min.AutoScale = value
            Me._Max.AutoScale = value
            Me._MajorTick.AutoSpacing = value
            Me._MinorTick.AutoSpacing = value
        End Set
    End Property

    ''' <summary> Gets the horizontal type of the Axis. </summary>
    ''' <value> The horizontal. </value>
    Public ReadOnly Property Horizontal() As Boolean
        Get
            Return (Me._AxisType = AxisType.X)
        End Get
    End Property

    ''' <summary> Gets the validity status of the axis. </summary>
    ''' <remarks>
    ''' The axis is valid if is has a none empty range, non-negative spacing, and a manageable
    ''' spacing count.
    ''' </remarks>
    ''' <value> A <see cref="System.Boolean">Boolean</see> </value>
    Public ReadOnly Property Valid() As Boolean
        Get
            ' check for validity of range and spacing
            Return Not ((Me.Range <= 0) OrElse
                (Me._MajorTick.Visible AndAlso (Me._MajorTick.Spacing <= 0)) OrElse
                (Me._MinorTick.Visible AndAlso (Me._MinorTick.Spacing <= 0)) OrElse
                (Me._MajorTick.TickCount > 1000) OrElse (Me._MinorTick.TickCount > 5000))
        End Get

    End Property

    ''' <summary> Returns the Vertical property of the axis. </summary>
    ''' <value>
    ''' <see cref="System.Boolean">True</see> if vertical or
    '''   <see cref="System.Boolean">True</see> otherwise.
    ''' </value>
    Public ReadOnly Property IsVertical() As Boolean
        Get
            Return (Me._AxisType = AxisType.Y OrElse Me._AxisType = AxisType.Y2)
        End Get
    End Property

    ''' <summary>
    ''' Determines whether or not the <see cref="Axis"/> is shown. Note that even if the axis is not
    ''' visible, it can still be actively used to draw curves on a graph, it will just be invisible
    ''' to the user.
    ''' </summary>
    ''' <value> True to show the axis, false to disable all drawing of this axis. </value>
    Public Property Visible() As Boolean

    ''' <summary> The color to use for drawing the <see cref="Axis"/>. </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> value. </value>
    Public Property LineColor() As Color

    ''' <summary> The major tick. </summary>
    Private _MajorTick As Tick

    ''' <summary>
    ''' Gets reference to the major <see cref="Tick"/> object determining how major tick marks are
    ''' drawn.
    ''' </summary>
    ''' <value> A <see cref="Tick"/> reference. </value>
    Public ReadOnly Property MajorTick() As Tick
        Get
            Return Me._MajorTick
        End Get
    End Property

    ''' <summary> The minor tick. </summary>
    Private _MinorTick As Tick

    ''' <summary>
    ''' Gets references to the minor <see cref="Tick"/> object determining how major tick marks are
    ''' drawn.
    ''' </summary>
    ''' <value> A <see cref="Tick"/> reference. </value>
    Public ReadOnly Property MinorTick() As Tick
        Get
            Return Me._MinorTick
        End Get
    End Property

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
    Private ReadOnly _Pane As Pane

    ''' <summary>Screen scale range for transforming user to screen coordinates.  
    '''   Values are in pixels.</summary>
    Private _ScreenScaleRange As RangeF

    ''' <summary> Gets the axis range in screen coordinates. </summary>
    ''' <value> A <see cref="RangeF">Range</see> value. </value>
    Public ReadOnly Property ScreenScaleRange() As RangeF
        Get
            Return Me._ScreenScaleRange
        End Get
    End Property

    ''' <summary> Gets the status message. </summary>
    ''' <value> A System.String value. </value>
    Protected Property StatusMessage() As String

    ''' <summary> Gets the tick <see cref="Labels"/> </summary>
    ''' <value> The tick labels. </value>
    Public Property TickLabels() As Labels

    ''' <summary> The title. </summary>
    Private _Title As AxisTitle

    ''' <summary>
    ''' Gets the <see cref="Axis"/> <see cref="isr.Visuals.AxisTitle"/>. This normally shows the
    ''' basis and dimensions of the scale range, such as "Time (Years)".
    ''' </summary>
    ''' <value> A <see cref="isr.Visuals.AxisTitle"/> property. </value>
    Public ReadOnly Property Title() As AxisTitle
        Get
            Return Me._Title
        End Get
    End Property

    ''' <summary> The transformed area. </summary>
    Private _TransformedArea As RectangleF

    ''' <summary>
    ''' Gets the transformed area of the axis that it is oriented along the X direction with the
    ''' origin at the left edge of the axis when looking from the label size (i.e., from the left of
    ''' the left Y axis or the right of the right Y axis).
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.RectangleF">rectangle</see> </value>
    Friend ReadOnly Property TransformedArea() As RectangleF
        Get
            Return Me._TransformedArea
        End Get
    End Property

    ''' <summary> Gets the <see cref="AxisType"/> </summary>
    ''' <remarks> David, 10/15/07, 1.0.2844. Rename to AxisType. </remarks>
    ''' <value> The type of the axis. </value>
    Public Property AxisType() As AxisType

#End Region

#Region " SCALE "

    ''' <summary>
    ''' Gets the <see cref="isr.Visuals.CoordinateScale"/> for this <see cref="Scale"/>.
    ''' </summary>
    ''' <value> The coordinate scale. </value>
    Public Property CoordinateScale() As isr.Visuals.CoordinateScale

    ''' <summary> Determines if the scale values are reversed for this <see cref="Scale"/> </summary>
    ''' <value>
    ''' True for the X values to decrease to the right or the Y values to decrease upwards, false
    ''' otherwise.
    ''' </value>
    Protected Property Reversed() As Boolean

    ''' <summary>
    ''' Gets maximum auto value for this axis.  This value can be set automatically based on the
    ''' state of <see cref="AutoValueR.AutoScale"/>.  If this value is set manually, then
    ''' <see cref="AutoValueR.AutoScale"/> is set to False.
    ''' </summary>
    ''' <value>
    ''' A <see cref="AutoValueR"/> value in user scale units for
    ''' <see cref="CoordinateScaleType.Log"/> and <see cref="CoordinateScaleType.Linear"/> axes. For
    ''' <see cref="CoordinateScaleType.Text"/> axes, this value is an ordinal starting with 1.0.
    ''' For <see cref="CoordinateScaleType.Date"/> axes, this value is in an internal
    ''' (<see cref="System.Double"/> date format based on the <see cref="JulianDate"/>.
    ''' </value>
    Public Property Max() As AutoValueR

    ''' <summary>
    ''' Gets minimum auto value for this axis.  The value of this auto value automatically based on
    ''' the state of <see cref="AutoValueR.AutoScale"/>.  If this value is set manually, then
    ''' <see cref="AutoValueR.AutoScale"/> is set to false.
    ''' </summary>
    ''' <value>
    ''' A <see cref="AutoValueR"/> value in user scale units for
    ''' <see cref="CoordinateScaleType.Log"/> and <see cref="CoordinateScaleType.Linear"/> axes. For
    ''' <see cref="CoordinateScaleType.Text"/> axes, this value is an ordinal starting with 1.0.
    ''' For <see cref="CoordinateScaleType.Date"/> axes, this value is in an internal
    ''' (<see cref="System.Double"/> date format based on the <see cref="JulianDate"/>.
    ''' </value>
    Public Property Min() As AutoValueR

    ''' <summary> Gets the axis range. </summary>
    ''' <value> Max - Min values. </value>
    Public ReadOnly Property Range() As Double
        Get
            Return Me._Max.Value - Me._Min.Value
        End Get
    End Property

    ''' <summary>
    ''' Gets the engineering scale mode for setting the scale exponent.  If True, the exponent is in
    ''' multiples of 3.
    ''' </summary>
    ''' <value> The is engineering scale. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private ReadOnly Property IsEngineeringScale As Boolean = True

    ''' <summary>
    ''' Gets the exponent for scale values. This is used to limit the size of the displayed value
    ''' labels.  For example, if the value is really 2000000, then the graph could instead display
    ''' 2000 with a magnitude multiplier decade of 3 (10^3).  The Value can be determined
    ''' automatically depending on the state of <see cref="AutoValue.AutoScale"/>. If the Value is
    ''' set manually by the user, then <see cref="AutoValue.AutoScale"/>
    ''' is set to False. The magnitude multipliers.
    ''' </summary>
    ''' <value> The magnitude multiplier (power of 10) for the scale value labels. </value>
    Public Property ScaleExponent() As AutoValue

    ''' <summary> The user scale range. </summary>
    Private _UserScaleRange As RangeR

    ''' <summary>
    ''' Gets the user scale range for transforming user to screen coordinates. Values are in user
    ''' coordinates.
    ''' </summary>
    ''' <value> The user scale range. </value>
    Public ReadOnly Property UserScaleRange() As RangeR
        Get
            Return Me._UserScaleRange
        End Get
    End Property

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="Axis"/> class that defines the default property values
''' for the <see cref="Axis"/> class.
''' </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public NotInheritable Class AxisDefaults

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs this class. This constructor is private to ensure only a single instance of this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub New()
        MyBase.New()
        Me._LineColor = Color.Black
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _Instance As AxisDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As AxisDefaults
        If AxisDefaults._Instance Is Nothing Then
            SyncLock AxisDefaults.SyncLocker
                AxisDefaults._Instance = New AxisDefaults()
            End SyncLock
        End If
        Return AxisDefaults._Instance
    End Function

#End Region

    ''' <summary>
    ''' Gets or sets the default color for the <see cref="Axis"/> itself
    ''' (<see cref="Axis.LineColor"/> property).  This color only affects the tick marks and the axis
    ''' border.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property LineColor() As Color

End Class

#End Region

