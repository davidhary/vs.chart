Imports isr.Core.Constructs

''' <summary>
''' Contains the data and methods for an individual curves within a graph pane.  It carries the
''' settings for the curve including colors, symbols and sizes, line types, etc.
''' </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581.  </para><para>
''' David, 05/28/04, 1.0.1609. Add time series point tag  </para><para>
''' David, 06/02/04, 1.0.1614. Add time series book marks </para>
''' </remarks>
Public Class Curve

    Implements ICloneable, IDisposable

#Region " SHARED "

    ''' <summary> Draw a series of points with symbols. </summary>
    ''' <remarks> The default symbol is a rectangle. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> The graphics context. </param>
    ''' <param name="graphBrush">     graph brush. </param>
    ''' <param name="graphPen">       graph pen. </param>
    ''' <param name="dataSpace">      The data space rectangle. </param>
    ''' <param name="screenSpace">    The screen space rectangle. </param>
    ''' <param name="graphScale">     The scale conversion 'point', specifying the horizontal (X) and
    '''                               vertical (Y) conversion scales from the data to the screen
    '''                               space. </param>
    ''' <param name="dataPoints">     An array of data points which to draw. </param>
    ''' <param name="pointSize">      Defines the size of the symbol. </param>
    Public Shared Sub DrawPoints(ByVal graphicsDevice As Graphics, ByVal graphBrush As Brush, ByVal graphPen As Pen,
                                 ByVal dataSpace As RectangleF, ByVal screenSpace As Rectangle,
                                 ByVal graphScale As PointF,
                                 ByVal dataPoints() As PointF, ByVal pointSize As Size)
        If graphicsDevice Is Nothing Then Throw New ArgumentNullException(NameOf(graphicsDevice))
        If graphBrush Is Nothing Then Throw New ArgumentNullException(NameOf(graphBrush))
        If graphPen Is Nothing Then Throw New ArgumentNullException(NameOf(graphPen))
        If graphScale Is Nothing Then Throw New ArgumentNullException(NameOf(graphScale))
        If dataPoints Is Nothing Then Throw New ArgumentNullException(NameOf(dataPoints))

        Dim screenSymbol As Rectangle = New Rectangle(0, 0, 0, 0) With {.Size = pointSize}

        ' Draw the points.
        Dim pt As Integer
        Dim xOffset As Single = screenSpace.X - Convert.ToSingle(pointSize.Width) / 2
        Dim yOffset As Single = screenSpace.Y - Convert.ToSingle(pointSize.Height) / 2
        For pt = 0 To dataPoints.GetUpperBound(0)
            screenSymbol.X = Convert.ToInt32((dataPoints(pt).X - dataSpace.X) * graphScale.X + xOffset)
            screenSymbol.Y = Convert.ToInt32((dataPoints(pt).Y - dataSpace.Y) * graphScale.Y + yOffset)
            graphicsDevice.FillRectangle(graphBrush, screenSymbol)
            graphicsDevice.DrawRectangle(graphPen, screenSymbol)
        Next pt

    End Sub

    ''' <summary> Draws a series of zero-based rectangles. </summary>
    ''' <remarks> Use this method to draw a series of rectangles. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> The graphics context. </param>
    ''' <param name="graphBrush">     graph brush. </param>
    ''' <param name="graphPen">       graph pen. </param>
    ''' <param name="dataSpace">      The data space rectangle. </param>
    ''' <param name="screenSpace">    The screen space rectangle. </param>
    ''' <param name="graphScale">     The scale conversion 'point', specifying the horizontal (X) and
    '''                               vertical (Y) conversion scales from the data to the screen
    '''                               space. </param>
    ''' <param name="dataPoints">     An array of data points which to draw. </param>
    Public Shared Sub DrawRectangles(ByVal graphicsDevice As Graphics, ByVal graphBrush As Brush, ByVal graphPen As Pen,
                                     ByVal dataSpace As RectangleF, ByVal screenSpace As Rectangle,
                                     ByVal graphScale As PointF, ByVal dataPoints() As PointF)
        If graphicsDevice Is Nothing Then Throw New ArgumentNullException(NameOf(graphicsDevice))
        If graphBrush Is Nothing Then Throw New ArgumentNullException(NameOf(graphBrush))
        If graphPen Is Nothing Then Throw New ArgumentNullException(NameOf(graphPen))
        If graphScale Is Nothing Then Throw New ArgumentNullException(NameOf(graphScale))
        If dataPoints Is Nothing Then Throw New ArgumentNullException(NameOf(dataPoints))

        ' Make the transformed rectangles.
        Dim numRectangles As Integer
        numRectangles = dataPoints.GetUpperBound(0) - 1
        Dim rectangles(numRectangles) As RectangleF

        Dim pt As Integer
        For pt = 0 To numRectangles
            Dim rect As RectangleF = rectangles(pt)
            rect.X = (dataPoints(pt).X - dataSpace.X) * graphScale.X + screenSpace.X
            rect.Y = screenSpace.Y - dataSpace.Y * graphScale.Y
            rect.Width = (dataPoints(pt + 1).X - dataPoints(pt).X) * graphScale.X
            rect.Height = dataPoints(pt).Y * graphScale.Y
            If rect.Width < 0 Then
                rect.X += rect.Width
                rect.Width = -rect.Width
            End If
            If rect.Height < 0 Then
                rect.Y += rect.Height
                rect.Height = -rect.Height
            End If
        Next pt

        ' Draw the rectangles.
        graphicsDevice.FillRectangles(graphBrush, rectangles)
        graphicsDevice.DrawRectangles(graphPen, rectangles)

    End Sub

    ''' <summary> Draws a line segment between two points in the data coordinate space. </summary>
    ''' <remarks> Draw a segment in the data coordinate space. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice">     The graphics context. </param>
    ''' <param name="graphPen">           graph pen. </param>
    ''' <param name="dataSpace">          The data space rectangle. </param>
    ''' <param name="screenSpace">        The screen space rectangle. </param>
    ''' <param name="graphScale">         The scale conversion 'point', specifying the horizontal (X)
    '''                                   and vertical (Y) conversion scales from the data to the
    '''                                   screen space. </param>
    ''' <param name="segmentOrigin">      The origin point of the segment. </param>
    ''' <param name="segmentTermination"> The termination point of the segment. </param>
    Public Shared Sub DrawSegment(ByVal graphicsDevice As Graphics, ByVal graphPen As Pen,
                                  ByVal dataSpace As RectangleF, ByVal screenSpace As Rectangle,
                                  ByVal graphScale As PointF, ByVal segmentOrigin As PointF, ByVal segmentTermination As PointF)
        If graphicsDevice Is Nothing Then Throw New ArgumentNullException(NameOf(graphicsDevice))
        If graphPen Is Nothing Then Throw New ArgumentNullException(NameOf(graphPen))
        If graphScale Is Nothing Then Throw New ArgumentNullException(NameOf(graphScale))
        If segmentOrigin Is Nothing Then Throw New ArgumentNullException(NameOf(segmentOrigin))
        If segmentTermination Is Nothing Then Throw New ArgumentNullException(NameOf(segmentTermination))

        Dim sgmtOrigin As System.Drawing.PointF = New System.Drawing.PointF(0, 0)
        Dim sgmtTermination As System.Drawing.PointF = New System.Drawing.PointF(0, 0)
        sgmtOrigin.X = (segmentOrigin.X - dataSpace.X) * graphScale.X + screenSpace.X
        sgmtOrigin.Y = (segmentOrigin.Y - dataSpace.Y) * graphScale.Y + screenSpace.Y
        sgmtTermination.X = (segmentTermination.X - dataSpace.X) * graphScale.X + screenSpace.X
        sgmtTermination.Y = (segmentTermination.Y - dataSpace.Y) * graphScale.Y + screenSpace.Y
        graphicsDevice.DrawLine(graphPen, sgmtOrigin, sgmtTermination)
    End Sub

    ''' <summary> Draws a series of connected segments. </summary>
    ''' <remarks> Draw a series of connected points. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> The graphics context. </param>
    ''' <param name="graphPen">       graph pen. </param>
    ''' <param name="dataSpace">      The data space rectangle. </param>
    ''' <param name="screenSpace">    The screen space rectangle. </param>
    ''' <param name="graphScale">     The scale conversion 'point', specifying the horizontal (X) and
    '''                               vertical (Y) conversion scales from the data to the screen
    '''                               space. </param>
    ''' <param name="dataPoints">     An array of data points which to draw. </param>
    Public Shared Sub DrawSegments(ByVal graphicsDevice As Graphics, ByVal graphPen As Pen,
                                   ByVal dataSpace As RectangleF, ByVal screenSpace As Rectangle,
                                   ByVal graphScale As PointF, ByVal dataPoints() As System.Drawing.PointF)
        If graphicsDevice Is Nothing Then Throw New ArgumentNullException(NameOf(graphicsDevice))
        If graphPen Is Nothing Then Throw New ArgumentNullException(NameOf(graphPen))
        If graphScale Is Nothing Then Throw New ArgumentNullException(NameOf(graphScale))
        If dataPoints Is Nothing Then Throw New ArgumentNullException(NameOf(dataPoints))

        ' Transform the points.
        Dim numPoints As Integer
        numPoints = dataPoints.GetUpperBound(0)

        Dim screenPoints() As System.Drawing.PointF
        ReDim screenPoints(numPoints)
        Dim pt As Integer

        For pt = 0 To numPoints
            screenPoints(pt).X =
                (dataPoints(pt).X - dataSpace.X) * graphScale.X + screenSpace.X
            screenPoints(pt).Y =
                (dataPoints(pt).Y - dataSpace.Y) * graphScale.Y + screenSpace.Y
        Next pt

        ' Draw the transformed points.
        graphicsDevice.DrawLines(graphPen, screenPoints)

    End Sub

    ''' <summary> Returns the data space rectangle. </summary>
    ''' <remarks>
    ''' get the standard data space for drawing.  Unlike the original data space, the standard data
    ''' space for graphics is referenced at the top left (xMin,yMax) corresponding to the screen
    ''' space origin.  Consequently, a typical data space will have a negative height.
    ''' </remarks>
    ''' <param name="xMin"> The minimum horizontal data space. </param>
    ''' <param name="xMax"> The maximum horizontal data space. </param>
    ''' <param name="yMin"> The minimum vertical coordinate space. </param>
    ''' <param name="yMax"> The maximum vertical coordinate space. </param>
    ''' <returns> The data space. </returns>
    Public Shared Function GetDataSpace(ByVal xMin As Single, ByVal xMax As Single,
                                        ByVal yMin As Single, ByVal yMax As Single) As RectangleF
        Return New RectangleF(xMin, yMax, xMax - xMin, yMin - yMax)
    End Function

    ''' <summary> Compute the graph scale coefficients. </summary>
    ''' <remarks>
    ''' The graph scale coefficients convert from data to screen space coordinates.  The data space
    ''' coordinates are set for the standard graphing with a top left corner at (xMin,yMax).
    ''' </remarks>
    ''' <param name="dataSpace">   The data space rectangle. </param>
    ''' <param name="screenSpace"> The screen space rectangle. </param>
    ''' <returns> The graph scale. </returns>
    Public Shared Function GetGraphScale(ByVal dataSpace As RectangleF, ByVal screenSpace As Rectangle) As PointF

        Dim graphScale As PointF = New PointF(0, 0)
        If dataSpace.Width <> 0 Then
            graphScale.X = screenSpace.Width / dataSpace.Width
        End If
        If dataSpace.Height <> 0 Then
            ' note that we have inverted the data space by placing Y max at the 
            ' top left corner.  Consequently, the data space height needs to be treated 
            ' as negative
            graphScale.Y = screenSpace.Height / dataSpace.Height
        End If
        Return graphScale

    End Function

    ''' <summary>
    ''' Returns the rectangle of screen coordinates (pixels) allotted for the graph.
    ''' </summary>
    ''' <remarks>
    ''' The margins x, y elements specify the top left offsets of the graph space from the chart
    ''' control border.  The width and height accommodates the combined margins (left+right) and
    ''' (top+bottom).  The graph margin provides an effective way of setting the graph space
    ''' independent of the control size.
    ''' </remarks>
    ''' <param name="screenSpace">  The screen space rectangle. </param>
    ''' <param name="graphMargins"> The screen space rectangle. </param>
    ''' <returns> The graph space. </returns>
    Public Shared Function GetGraphSpace(ByVal screenSpace As Rectangle, ByVal graphMargins As Rectangle) As Rectangle
        Dim graphSpace As Rectangle = screenSpace
        graphSpace.Inflate(New Size(-graphMargins.Width, -graphMargins.Height))
        graphSpace.Location = graphMargins.Location
        Return graphSpace
    End Function

#End Region

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a <see cref="Curve"/> without specifying the X and Y data sets.  This permits
    ''' setting a curve for time series update-able data.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="type">        A <see cref="CurveType">Curve Type</see> value. </param>
    ''' <param name="label">       A <see cref="System.String">String</see> label (legend entry) for
    '''                            this curve. </param>
    ''' <param name="xAxis">       Reference to the X <see cref="Axis"/> </param>
    ''' <param name="yAxis">       Reference to the Y <see cref="Axis"/> </param>
    ''' <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
    Public Sub New(ByVal type As CurveType, ByVal label As String, ByVal xAxis As Axis, ByVal yAxis As Axis, ByVal drawingPane As Pane)
        MyBase.New()
        If drawingPane Is Nothing Then Throw New ArgumentNullException(NameOf(drawingPane))
        If xAxis Is Nothing Then Throw New ArgumentNullException(NameOf(xAxis))
        If yAxis Is Nothing Then Throw New ArgumentNullException(NameOf(yAxis))

        If String.IsNullOrWhiteSpace(label) Then
            label = String.Empty
        End If
        Me._TimeSeriesLength = 1200
        Me._Pane = drawingPane
        Me.Cord = New Cord
        Me._IgnoreMissing = CurveDefaults.[Get].IgnoreMissing
        Me._Symbol = New Symbol(Me._Pane)
        Me._Label = label
        Me._XAxis = xAxis
        Me._YAxis = yAxis
        ' superfluous per Code Analysis: Me._x = Nothing
        ' superfluous per Code Analysis: Me._y = Nothing
        Me.CurveType = type
        If Me.CurveType = CurveType.StripChart Then
            ' instantiate a stylus
            Me._Stylus = New Stylus(Me._Pane) With {.LineColor = Me.Cord.LineColor}
            Me._TimeSeriesBookmarks = New TimeSeriesBookmarkCollection(Me)
            Me.ClearTimeSeries()
        End If

    End Sub

    ''' <summary>
    ''' Constructs a <see cref="Curve"/> with given and default values as defined in the
    ''' <see cref="CurveDefaults"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="label">       A <see cref="System.String">String</see> label (legend entry) for
    '''                            this curve. </param>
    ''' <param name="x">           A array of <see cref="System.Double">Double Precision</see>
    '''                            values that define the independent (X axis) values for this
    '''                            curve. </param>
    ''' <param name="y">           A array of <see cref="System.Double">Double Precision</see>
    '''                            values that define the dependent (Y axis) values for this curve. 
    ''' </param>
    ''' <param name="xAxis">       Reference to the X <see cref="Axis"/> </param>
    ''' <param name="yAxis">       Reference to the Y <see cref="Axis"/> </param>
    ''' <param name="drawingPane"> Reference to the drawing <see cref="isr.Visuals.Pane">Pane</see> </param>
    Public Sub New(ByVal label As String, ByVal x() As Double, ByVal y() As Double,
                   ByVal xAxis As Axis, ByVal yAxis As Axis, ByVal drawingPane As Pane)
        Me.New(CurveType.XY, label, xAxis, yAxis, drawingPane)
        Me._X = x
        Me._Y = y

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The Curve object from which to copy. </param>
    Public Sub New(ByVal model As Curve)
        MyBase.New()
        If model Is Nothing Then Throw New ArgumentNullException(NameOf(model))
        Me._Symbol = model._Symbol.Copy()
        Me.Cord = model.Cord.Copy()
        Me._IgnoreMissing = model._IgnoreMissing
        Me._Label = model._Label
        Me._XAxis = model._XAxis
        Me._YAxis = model._YAxis
        Me._CurveType = model._CurveType
        Me._UseTimeSeriesTag = model._UseTimeSeriesTag
        Me._UseBookmark = model._UseBookmark
        Me._TimeSeriesBookmark = model._TimeSeriesBookmark
        If model._Stylus IsNot Nothing Then
            Me._Stylus = model._Stylus.Copy
        End If
        If model._X IsNot Nothing Then
            Me._X = CType(model._X.Clone(), Double())
        End If
        If model._Y IsNot Nothing Then
            Me._Y = CType(model._Y.Clone(), Double())
        End If
        Me._Pane = model._Pane
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> True if disposed. </summary>
    Private _Disposed As Boolean

    ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._Disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._Disposed = value
        End Set
    End Property

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._X = Nothing
                    Me._Y = Nothing
                    Me._XAxis = Nothing
                    Me._YAxis = Nothing
                    Me._TimeSeriesBookmarks = Nothing
                    Me._TimeSeriesBookmark = Nothing

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Adds a new time series book mark. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="bookmark"> The new <see cref="isr.Visuals.TimeSeriesBookMark">time series book
    '''                         mark</see> </param>
    ''' <returns>
    ''' The added <see cref="isr.Visuals.TimeSeriesBookMark">time series book mark</see> with its
    ''' serial number set to the index location in the collection.
    ''' </returns>
    Public Function AddBookmark(ByVal bookmark As isr.Visuals.TimeSeriesBookmark) As TimeSeriesBookmark
        bookmark.SerialNumber = Me._TimeSeriesBookmarks.Count
        Me._TimeSeriesBookmarks.Add(bookmark)
        Return bookmark
    End Function

    ''' <summary>
    ''' Adds a new time series points and shifts the time series axis and origin of time series tick
    ''' marks.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="timeSeriesPoint"> The new <see cref="isr.Visuals.TimeSeriesPointR">time series
    '''                                data point</see> </param>
    ''' <returns>
    ''' The new <see cref="isr.Visuals.TimeSeriesPointR">time series data point</see>
    ''' </returns>
    Private Function AddDataPoint(ByVal timeSeriesPoint As isr.Visuals.TimeSeriesPointR) As isr.Visuals.TimeSeriesPointR

        ' update the time series saved point
        Me.UpdateDataPoint(timeSeriesPoint)

        ' update time series.
        Me._TimeSeries(Me._TimeSeriesPointer) = Me._TimeSeriesPoint

        ' increment and roll over the pointer
        Me._TimeSeriesPointer = (Me._TimeSeriesPointer + 1) Mod Me._TimeSeriesLength
        Me._TimeSeriesPointerKeeper = Me._TimeSeriesPointer
        Me._TimeSeriesCount = Math.Min(Me._TimeSeriesCount + 1, Me._TimeSeriesLength)

        ' increment the axis origins
        Me._XAxis.IncrementTickOrigin()

        ' return the time series point
        Return Me._TimeSeriesPoint

    End Function

    ''' <summary>
    ''' Updates the time series curve data and adjusts the amplitude and time ranges.
    ''' </summary>
    ''' <remarks>
    ''' Use this method to add a new value to the strip chart.  Shifts the buffer one notch every
    ''' time a new data points comes in thus 'scrolling' the data along.
    ''' </remarks>
    ''' <param name="[time]">    The time (X or horizontal axis) value. </param>
    ''' <param name="amplitude"> The amplitude (Y, or vertical axis) value. </param>
    ''' <param name="tag">       Specifies the <see cref="TimeSeriesPointTags">time series tag</see> </param>
    ''' <returns>
    ''' The new <see cref="isr.Visuals.TimeSeriesPointR">time series data point</see>
    ''' </returns>
    Public Function AddDataPoint(ByVal [time] As DateTime, ByVal amplitude As Double, ByVal tag As TimeSeriesPointTags) As isr.Visuals.TimeSeriesPointR

        ' add the time series point and 'shift' time series axis and tick
        ' positions
        Me.AddDataPoint(New TimeSeriesPointR([time], amplitude, tag))

        If Me._YAxis.Max.AutoScale OrElse Me._YAxis.Min.AutoScale Then

            ' rescale the y axis.  We assume at this point that we have only a single
            ' strip chart.  Later on, we need to call the Curves.Rescale to rescale
            ' all their axes.
            'Dim range As RangeR = Me.getTimeSeriesAmplitudeRange()
            '_dataRange = PlanarRangeR.Empty
            Me._DataRange = New PlanarRangeR(RangeR.Empty, Me.GetTimeSeriesAmplitudeRange())

            Me._YAxis.Rescale(Me._DataRange.Y.Min, Me._DataRange.Y.Max)

        End If

        ' set the time axis major and minor tick positions
        ' this can only be done when drawing!
        ' Me._xAxis.SetTimeSeriesTicks(Me._timeSeriesPointer, Me._timeSeries)

        ' return the time series point
        Return Me._TimeSeriesPoint

    End Function

    ''' <summary>
    ''' Updates the time series curve data and adjusts the amplitude and time ranges.
    ''' </summary>
    ''' <remarks>
    ''' Use this method to add a new value to the strip chart.  Shifts the buffer one notch every
    ''' time a new data points comes in thus 'scrolling' the data along.
    ''' </remarks>
    ''' <param name="[time]">    The time (X or horizontal axis) value. </param>
    ''' <param name="amplitude"> The amplitude (Y, or vertical axis) value. </param>
    ''' <returns>
    ''' The new <see cref="isr.Visuals.TimeSeriesPointR">time series data point</see>
    ''' </returns>
    Public Function AddDataPoint(ByVal [time] As DateTime, ByVal amplitude As Double) As isr.Visuals.TimeSeriesPointR

        ' add the time series point and 'shift' time series axis and tick
        ' positions
        Me.AddDataPoint(New TimeSeriesPointR([time], amplitude, TimeSeriesPointTags.None))

        If Me._YAxis.Max.AutoScale OrElse Me._YAxis.Min.AutoScale Then

            ' rescale the y axis.  We assume at this point that we have only a single
            ' strip chart.  Later on, we need to call the Curves.Rescale to rescale
            ' all their axes.
            Dim range As RangeR = Me.GetTimeSeriesAmplitudeRange()

            Me._YAxis.Rescale(range.Min, range.Max)

        End If

        ' set the time axis major and minor tick positions
        ' this can only be done when drawing!
        ' Me._xAxis.SetTimeSeriesTicks(Me._timeSeriesPointer, Me._timeSeries)

        ' return the time series point
        Return Me._TimeSeriesPoint

    End Function

    ''' <summary>
    ''' Updates the time series curve data and adjusts the amplitude and time ranges.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="[time]">         The time (X or horizontal axis) value. </param>
    ''' <param name="amplitude">      The amplitude (Y, or vertical axis) value. </param>
    ''' <param name="amplitudeRange"> The amplitude <see cref="RangeR">range</see>. </param>
    ''' <returns>
    ''' The new <see cref="isr.Visuals.TimeSeriesPointR">time series data point</see>
    ''' </returns>
    Public Function AddDataPoint(ByVal [time] As DateTime, ByVal amplitude As Double, ByVal amplitudeRange As RangeR) As isr.Visuals.TimeSeriesPointR

        If amplitudeRange Is Nothing Then Throw New ArgumentNullException(NameOf(amplitudeRange))

        ' add the time series point and 'shift' time series axis and tick
        ' positions
        Me.AddDataPoint(New TimeSeriesPointR([time], amplitude))

        If Me._YAxis.Max.AutoScale OrElse Me._YAxis.Min.AutoScale Then

            ' rescale the y axis.  to the given range.
            Me._YAxis.Rescale(amplitudeRange.Min, amplitudeRange.Max)

        Else

            ' set the range to the given range.
            Me._YAxis.SetRange(amplitudeRange.Min, amplitudeRange.Max)

            ' set spacing -- this will not auto scale because auto scaling is off
            Me._YAxis.Rescale(amplitudeRange.Min, amplitudeRange.Max)

        End If

        ' return the time series point
        Return Me._TimeSeriesPoint

    End Function

    ''' <summary> Clears the time series. </summary>
    ''' <remarks>
    ''' Use this method to re-size and clear the buffer.  The Y value is set to zero and the X value
    ''' to the current time.
    ''' </remarks>
    Public Sub ClearTimeSeries()

        ' clear the time series book marks
        Me._TimeSeriesBookmarks.Clear()

        ' clear the count of data points
        Me._TimeSeriesCount = 0
        Me._TimeSeriesPointer = 0
        Me._TimeSeriesPointerKeeper = Me._TimeSeriesPointer

        ' set the buffer size to match the primary screen length
        Me._TimeSeriesLength = Math.Max(Screen.PrimaryScreen.WorkingArea.Width, Me._TimeSeriesLength)
        Dim zeroPoint As TimeSeriesPointR = New TimeSeriesPointR(DateTime.UtcNow, 0)
        Me._TimeSeries = New TimeSeriesPointR(Me._TimeSeriesLength - 1) {}
        For i As Integer = 0 To Me._TimeSeriesLength - 1
            Me._TimeSeries(i) = zeroPoint
        Next

    End Sub

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the Curve. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the Curve. </returns>
    Public Function Copy() As Curve
        Return New Curve(Me)
    End Function

    ''' <summary>
    ''' Renders this <see cref="Curve"/> to the specified
    ''' <see cref="graphics"/> device.  This method is normally only
    ''' called by the Draw method of the parent <see cref="isr.Visuals.CurveCollection"/>
    ''' collection object.  This version is optimized for speed and should be much faster than the
    ''' regular Draw() routine.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    Private Sub DrawLineChart(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        Dim numPoints As Integer = Me.PointCount

        If numPoints <= 0 Then
            Exit Sub
        End If

        Dim axisArea As RectangleF = Me._Pane.AxisArea
        ' Dim scaleFactor As Double = Me._pane.ScaleFactor

        ' set clip area
        graphicsDevice.SetClip(axisArea)

        Dim tmpX() As Single
        Dim tmpY() As Single
        ReDim tmpX(numPoints - 1)
        ReDim tmpY(numPoints - 1)

        ' Loop over each point in the curve
        For i As Integer = 0 To numPoints - 1
            If (Not Me._IgnoreMissing) AndAlso (Me._X(i) = System.Double.MaxValue OrElse
                                                Me._Y(i) = System.Double.MaxValue OrElse
                                                (Me._XAxis.CoordinateScale.IsLog AndAlso Me._X(i) <= 0.0) OrElse
                                                (Me._YAxis.CoordinateScale.IsLog AndAlso Me._Y(i) <= 0.0)) Then
                tmpX(i) = System.Single.MaxValue
                tmpY(i) = System.Single.MaxValue
            Else
                ' Transform the current point from user scale units to
                ' screen coordinates
                tmpX(i) = Me._XAxis.Transform(Me._X(i))
                tmpY(i) = Me._YAxis.Transform(Me._Y(i))
                If False Then
                    ' use the draw area to contain the range for preventing overflows.?
                    If Not Me._Pane.DrawArea.Contains(tmpX(i), tmpY(i)) Then
                        If i = 0 Then
                            tmpX(i) = axisArea.X
                            tmpY(i) = axisArea.Y
                        Else
                            tmpX(i) = tmpX(i - 1)
                            tmpY(i) = tmpY(i - 1)
                        End If
                    End If
                End If
            End If
        Next i

        Me.Cord.Draw(graphicsDevice, tmpX, tmpY, Me._IgnoreMissing)
        Me._Symbol.Draw(graphicsDevice, tmpX, tmpY, Me._Pane.ScaleFactor, Me._IgnoreMissing)

    End Sub

    ''' <summary>
    ''' Renders this <see cref="Curve"/> to the specified
    ''' <see cref="graphics"/> device.  This method is normally only
    ''' called by the Draw method of the parent <see cref="isr.Visuals.CurveCollection"/>
    ''' collection object.  This version is optimized for speed and should be much faster than the
    ''' regular Draw() routine.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    Private Sub DrawStripChart(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        Dim axisArea As RectangleF = Me._Pane.AxisArea
        Dim scaleFactor As Double = Me._Pane.ScaleFactor

        Dim numPoints As Integer = Me.PointCount
        If numPoints = 0 Then
            If Me._Stylus IsNot Nothing Then
                Dim pixY As Single = 0.5F * (axisArea.Top + axisArea.Bottom)
                Me._Stylus.Draw(graphicsDevice, pixY, axisArea, scaleFactor)
            End If
            Return
        End If

        ' draw the stylus
        If Me._Stylus IsNot Nothing Then
            Dim y As Double = Me.GetClippedTimeSeriesAmplitude()
            Dim pixY As Single = Me.YAxis.Transform(y)
            Me._Stylus.Draw(graphicsDevice, pixY, axisArea, scaleFactor)
        End If

        ' set clip area
        graphicsDevice.SetClip(axisArea)

        Dim tmpX() As Single
        Dim tmpY() As Single
        ReDim tmpX(numPoints - 1)
        ReDim tmpY(numPoints - 1)

        ' get the first data point to draw.
        Dim j As Integer
        If Me._UseBookmark Then
            j = Me._TimeSeriesBookmark.FromPoint.Index
        Else
            j = Me._TimeSeriesPointer - numPoints
            If j < 0 Then
                j += Me._TimeSeriesCount
            End If
        End If

        ' set initial x value
        Dim x As Single = Me._XAxis.ScreenScaleRange.Max - numPoints + 1

        ' set the values from the earliest to the latest value
        For i As Integer = 0 To numPoints - 1

            ' get the pointer to the next data point
            j = j Mod Me._TimeSeriesLength

            ' set the X coordinate to map onto the axis range
            tmpX(i) = x

            ' Transform the time series amplitude from user scale units to screen coordinates
            tmpY(i) = Me._YAxis.Transform(Me._TimeSeries(j).Y)

            ' get the pointer to the next data point
            j += 1

            ' increment x position
            x += 1

        Next i

        Me.Cord.Draw(graphicsDevice, tmpX, tmpY, Me._IgnoreMissing)
        Me._Symbol.Draw(graphicsDevice, tmpX, tmpY, Me._Pane.ScaleFactor, Me._IgnoreMissing)

    End Sub

    ''' <summary>
    ''' Renders this <see cref="Curve"/> to the specified
    ''' <see cref="graphics"/> device.  This method is normally only
    ''' called by the Draw method of the parent <see cref="isr.Visuals.CurveCollection"/>
    ''' collection object.  This version is optimized for speed and should be much faster than the
    ''' regular Draw() routine.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        If CurveType.StripChart = Me.CurveType Then

            Me.DrawStripChart(graphicsDevice)

        Else

            Me.DrawLineChart(graphicsDevice)

        End If

    End Sub

    ''' <summary>
    ''' Checks if the <see cref="X"/> or <see cref="Y"/> data arrays are missing for this
    ''' <see cref="Curve"/>.  If so, provide a suitable default array using ordinal values.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub CreateDefaultData()

        If CurveType.StripChart = Me.CurveType Then

        Else

            ' See if a default X array is required
            If Me._X Is Nothing Then
                ' if a Y array is available, just make the same number of elements
                Me._X = If(Me._Y Is Nothing, Me._XAxis.MakeDefaultArray(), Curve.MakeDefaultArray(Me._Y.Length))
            End If ' see if a default Y array is required
            If Me._Y Is Nothing Then
                ' if an X array is available, just make the same number of elements
                Me._Y = If(Me._X Is Nothing, Me._YAxis.MakeDefaultArray(), Curve.MakeDefaultArray(Me._X.Length))
            End If

        End If

    End Sub

    ''' <summary>
    ''' Adjusts the <see cref="Pane.AxisArea"/> for the
    ''' <see cref="isr.Visuals.Stylus">Stylus</see> size.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="scaleFactor"> The scaling factor for the chart with reference to the chart
    '''                            <see cref="Pane.BaseDimension"/>.  This scaling factor is
    '''                            calculated by the <see cref="Pane.getScaleFactor"/> method.  
    '''                            The scale factor is applied to fonts, symbols, etc. </param>
    ''' <param name="axisArea">    The rectangle that contains the area bounded by the axes, in
    '''                            pixels. <seealso cref="Pane.AxisArea">AxisArea</seealso> </param>
    ''' <returns> Adjusted <see cref="Pane.AxisArea"/> </returns>
    Friend Function GetAxisArea(ByVal scaleFactor As Double, ByVal axisArea As RectangleF) As RectangleF

        ' Leave room for the stylus
        If Me._Stylus IsNot Nothing Then

            ' adjust the stylus 
            Return Me._Stylus.GetAxisArea(scaleFactor, axisArea)

        End If

        ' return adjusted axis rectangle
        Return axisArea

    End Function

    ''' <summary> get the range of this curve. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="ignoreInitial"> Affects how initial zero y values are treated for setting the
    '''                              range of X values.  If True, then initial zero data points are
    '''                              ignored when determining the X range.  All data after the first
    '''                              non-zero Y value are included. </param>
    ''' <returns> The calculated range. </returns>
    Friend Function GetRange(ByVal ignoreInitial As Boolean) As PlanarRangeR

        ' empty the range
        Me._DataRange = PlanarRangeR.Empty

        If CurveType.StripChart = Me.CurveType Then

            Me._DataRange.ExtendRange(New PlanarRangeR(Me._DataRange.X, Me.GetTimeSeriesAmplitudeRange()))

        Else

            ' generate default arrays of ordinal values if any data arrays are missing
            ' 1.0.2118 no way.  This just causes problems.  Me.CheckData()

            ' Call the getRange() member function for the current
            ' curve to get the min and max values
            Me._DataRange.ExtendRange(PlanarRangeR.GetRange(Me._X, Me._Y, ignoreInitial))

        End If

        If Me._DataRange.Equals(PlanarRangeR.Empty) Then
            ' Use unit range if no data were available
            Return PlanarRangeR.Unity
        Else
            Return Me._DataRange
        End If

    End Function

    ''' <summary> Returns the time series amplitude clipped to the axis range. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The clipped time series amplitude. </returns>
    Private Function GetClippedTimeSeriesAmplitude() As Double

        Dim y As Double = Me._TimeSeriesPoint.Y
        y = Math.Max(y, Me._YAxis.Min.Value)
        y = Math.Min(y, Me._YAxis.Max.Value)
        Return y

    End Function

    ''' <summary>
    ''' gets the amplitude range for the <see cref="isr.Visuals.TimeSeriesPointR">Time Series</see>
    ''' data array.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The calculated time series amplitude range. </returns>
    Private Function GetTimeSeriesAmplitudeRange() As RangeR

        ' return the unit range if no data
        If Me._TimeSeries Is Nothing Then
            Return RangeR.Unity
        End If

        Dim numPoints As Integer = If(Me._XAxis.ScreenScaleRange Is Nothing,
            Me._TimeSeriesCount,
            Convert.ToInt32(Math.Min(Me._XAxis.ScreenScaleRange.Span, Me._TimeSeriesCount)))

        If numPoints = 0 Then
            Return RangeR.Unity
        End If

        ' get the first data point to draw.
        Dim j As Integer = Me._TimeSeriesPointer - numPoints
        If j < 0 Then
            j += Me._TimeSeriesCount
        End If

        Dim yTemp As Double
        Dim yMin As Double
        Dim yMax As Double

        If Me._UseTimeSeriesTag Then

            ' initialize arbitrary data just so that we have some range values
            Dim tempPoint As TimeSeriesPointR = Me._TimeSeries(j)
            Dim hasRangePoints As Boolean = False
            yTemp = tempPoint.Y
            yMin = yTemp
            yMax = yTemp

            ' set the values from the earliest to the latest value
            For i As Integer = 0 To numPoints - 1

                ' get the pointer to the next data point
                j = j Mod Me._TimeSeriesLength

                ' Transform the time series amplitude from user scale units to screen coordinates
                tempPoint = Me._TimeSeries(j)

                ' check if we already have range points
                If hasRangePoints Then
                    ' if so, check if this is a new range point
                    If tempPoint.IsRangePoint Then
                        ' if so, update the range
                        yTemp = tempPoint.Y
                        If yTemp < yMin Then
                            yMin = yTemp
                        ElseIf yTemp > yMax Then
                            yMax = yTemp
                        End If
                    End If
                Else
                    ' if no range point yet, check if this one is a range point
                    If tempPoint.IsRangePoint Then
                        ' if first range point than initialize values
                        hasRangePoints = True
                        yTemp = tempPoint.Y
                        yMin = yTemp
                        yMax = yTemp
                    End If
                End If

                ' get the pointer to the next data point
                j += 1

            Next i

        Else
            ' ignore range points. 
            ' initialize the values 
            yTemp = Me._TimeSeries(j).Y
            yMin = yTemp
            yMax = yTemp

            ' set the values from the earliest to the latest value
            For i As Integer = 0 To numPoints - 1

                ' get the pointer to the next data point
                j = j Mod Me._TimeSeriesLength

                ' Transform the time series amplitude from user scale units to screen coordinates
                yTemp = Me._TimeSeries(j).Y
                If yTemp < yMin Then
                    yMin = yTemp
                ElseIf yTemp > yMax Then
                    yMax = yTemp
                End If

                ' get the pointer to the next data point
                j += 1

            Next i

        End If

        Return New RangeR(yMin, yMax)

    End Function

    ''' <summary>
    ''' gets the time range for the <see cref="isr.Visuals.TimeSeriesPointR">Time Series</see>
    ''' data array.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The calculated time series time range. </returns>
    Public Function GetTimeSeriesTimeRange() As RangeR

        ' return the unit range if no data
        If Me._TimeSeries Is Nothing Then
            Return RangeR.Unity
        End If

        Dim numPoints As Integer = Convert.ToInt32(Math.Min(Me._XAxis.ScreenScaleRange.Span, Me._TimeSeriesCount))
        If numPoints < 2 Then
            Return RangeR.Unity
        End If

        ' get the last data point to draw.
        Dim j As Integer = Me._TimeSeriesPointer - 1
        If j < 0 Then
            j += Me._TimeSeriesCount
        End If

        ' get the first data point to draw.
        Dim i As Integer = Me._TimeSeriesPointer - numPoints
        If i < 0 Then
            i += Me._TimeSeriesCount
        End If

        ' get time scale in seconds since 1/1/0001
        Dim startTime As Double = Me._TimeSeries(i).Seconds
        Dim endTime As Double = Me._TimeSeries(j).Seconds
        Dim timeRange As Double = endTime - startTime

        If Me._XAxis.ScreenScaleRange.Span > Me._TimeSeriesCount Then

            ' if we have not collected a full range, extrapolate the range from the
            ' given data.
            timeRange *= Me._XAxis.ScreenScaleRange.Span / Me._TimeSeriesCount
            Return New RangeR(endTime - timeRange, endTime)

        Else

            ' if we have collected all data than the range is determined by
            ' the first and last data points
            Return New RangeR(startTime, endTime)

        End If

    End Function

    ''' <summary> generate a default array of ordinal values. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="length"> The number of values to generate. </param>
    ''' <returns> a floating point double type array of default ordinal values. </returns>
    Public Shared Function MakeDefaultArray(ByVal length As Integer) As Double()

        Dim defaultArray(length) As Double
        For i As Integer = 0 To length - 1
            defaultArray(i) = i + 1.0R
        Next i
        Return defaultArray

    End Function

    ''' <summary>
    ''' Pans the time series to display the onset of the book mark at the beginning of the time
    ''' series chart.  If the time series has fewer points than the screen range, the last points of
    ''' the time series will show time earlier than the first points reflecting the circular nature
    ''' of the time series.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="bookmark">       The new <see cref="isr.Visuals.TimeSeriesBookMark">time series
    '''                               book mark</see> </param>
    ''' <param name="indexLeftShift"> The left shift to add to the pan. </param>
    Public Sub PanTimeSeries(ByVal bookmark As isr.Visuals.TimeSeriesBookmark,
                             ByVal indexLeftShift As Integer)

        ' adjust the time series pointer to display the Bookmark at the beginning
        ' of the current chart span.
        Me._TimeSeriesPointer = bookmark.FromPoint.Index + Me.PointCount - 1 - indexLeftShift
        ' Me._timeSeriesPointer = Bookmark.FromPoint.Index + Convert.ToInt32(Me._xAxis.ScreenScaleRange.Range) - 1 - indexLeftShift
        '    If Bookmark.FromPoint.Index < Me._timeSeriesPointerKeeper Then
        '   Me._timeSeriesPointer = Math.Min(Me._timeSeriesPointer, Me._timeSeriesPointerKeeper)
        '  End If
        If Me._TimeSeriesPointer >= Me._TimeSeriesLength Then
            Me._TimeSeriesPointer -= Me._TimeSeriesLength
        End If

    End Sub

    ''' <summary>
    ''' Restores the time series time reference.  This should be done after pan and zoom operations.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub RestoreTimeSeries()
        Me._TimeSeriesPointer = Me._TimeSeriesPointerKeeper
    End Sub

    ''' <summary>
    ''' Updates the time series curve data and adjusts the amplitude and time ranges.
    ''' </summary>
    ''' <remarks>
    ''' Use this method to add a new value to the strip chart.  Shifts the buffer one notch every
    ''' time a new data points comes in thus 'scrolling' the data along.
    ''' </remarks>
    Public Sub ScaleTimeSeriesAxis()

        If Me._XAxis.CoordinateScale.CoordinateScaleType = CoordinateScaleType.StripChart Then
            ' this can only be done when drawing!
            ' set major tick locations and values
            Me._XAxis.MajorTick.SetLocations(Me._XAxis)
            Me._XAxis.MajorTick.SetValues(Me._XAxis, Me._TimeSeriesPointer, Me._TimeSeries)
        End If

    End Sub

    ''' <summary> Sets the Cartesian X, Y arrays for the given time series indexes. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="fromIndex"> The starting index. </param>
    ''' <param name="toIndex">   The ending index. </param>
    Public Sub SetCartesianTimeSeries(ByVal fromIndex As Integer, ByVal toIndex As Integer)

        If fromIndex < 0 Then
            fromIndex += Me._TimeSeriesLength
        End If
        If toIndex < 0 Then
            toIndex += Me._TimeSeriesLength
        End If
        Dim length As Integer = toIndex - fromIndex + 1
        If length <= 0 Then
            length += Me._TimeSeriesLength
        End If
        ReDim Me._X(length - 1)
        ReDim Me._Y(length - 1)
        For i As Integer = 0 To length - 1
            Me._X(i) = Me._TimeSeries(fromIndex).Seconds
            Me._Y(i) = Me._TimeSeries(fromIndex).Y
            fromIndex += 1
            If fromIndex >= Me._TimeSeriesLength Then
                fromIndex = 0
            End If
        Next i

    End Sub

    ''' <summary> Updates a new time series point without adding it to the time series. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="timeSeriesPoint"> The <see cref="isr.Visuals.TimeSeriesPointR">time series data
    '''                                point</see> </param>
    ''' <returns>
    ''' A <see cref="isr.Visuals.TimeSeriesPointR">time series data point</see>
    ''' updated with current time series information including the time series index.
    ''' </returns>
    Public Function UpdateDataPoint(ByVal timeSeriesPoint As isr.Visuals.TimeSeriesPointR) As isr.Visuals.TimeSeriesPointR

        ' update the time series saved point
        Me._TimeSeriesPoint = timeSeriesPoint

        ' record the time series index
        Me._TimeSeriesPoint.Index = Me._TimeSeriesPointer

        ' return the time series point
        Return Me._TimeSeriesPoint

    End Function

    ''' <summary> Updates the references to the X, Y data arrays. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x"> A array of <see cref="System.Double">Double Precision</see> values that
    '''                  define the independent (X axis) values for this curve. </param>
    ''' <param name="y"> A array of <see cref="System.Double">Double Precision</see> values that
    '''                  define the dependent (Y axis) values for this curve. </param>
    Public Sub UpdateData(ByVal x() As Double, ByVal y() As Double)

        Me._X = x
        Me._Y = y

        ' set the point count so that it would be ignored
        Me._PointCount = _IgnorePointCount

    End Sub

    ''' <summary>
    ''' Updates the references to the X, Y data arrays and sets the point count which to plot.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="x">          A array of <see cref="System.Double">Double Precision</see> values
    '''                           that define the independent (X axis) values for this curve. </param>
    ''' <param name="y">          A array of <see cref="System.Double">Double Precision</see> values
    '''                           that define the dependent (Y axis) values for this curve. </param>
    ''' <param name="pointCount"> The number of points to plot. </param>
    Public Sub UpdateData(ByVal x() As Double, ByVal y() As Double, ByVal pointCount As Integer)

        Me._X = x
        Me._Y = y
        Me._PointCount = pointCount

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' gets a reference to the <see cref="isr.Visuals.Cord"/> class defined for this
    ''' <see cref="Curve"/>.
    ''' </summary>
    ''' <value> The cord. </value>
    Public ReadOnly Property Cord() As Cord

    ''' <summary>
    ''' Gets the missing value mode for drawing graphs.  When true, graphs are drawing assuming all
    ''' values are valid.  This is important for quick graphics.
    ''' </summary>
    ''' <value> A <see cref="System.Boolean">True</see> </value>
    Public Property IgnoreMissing() As Boolean

    ''' <summary>
    ''' Gets the condition for the curve aught to use time series tag for such methods as fixing its
    ''' range.
    ''' </summary>
    ''' <value> The use time series tag. </value>
    Public Property UseTimeSeriesTag() As Boolean

    ''' <summary> Gets the book mark drawing option. </summary>
    ''' <value> The use bookmark. </value>
    Public Property UseBookmark() As Boolean

    ''' <summary>
    ''' Determines if this <see cref="Curve"/> is assigned to the
    ''' <see cref="AxisType.Y2"/>.
    ''' </summary>
    ''' <value>
    ''' True if the curve is assigned to the <see cref="isr.Visuals.AxisType.Y2"/>, False is the
    ''' curve is assigned to the <see cref="isr.Visuals.AxisType.Y"/>
    ''' </value>
    Public ReadOnly Property IsY2Axis() As Boolean
        Get
            Return (Me._YAxis.AxisType = AxisType.Y2)
        End Get
    End Property

    ''' <summary>
    ''' A <see cref="System.String">String</see> that represents the <see cref="isr.Visuals.Legend"/>
    ''' entry for the this
    ''' <see cref="Curve"/> object.
    ''' </summary>
    ''' <value> The label. </value>
    Public Property Label() As String

    ''' <summary>Gets or sets the value telling the curve use array size when 
    '''   plotting.</summary>
    Private Const _IgnorePointCount As Integer = -1

    ''' <summary> Number of points. </summary>
    Private _PointCount As Integer = _IgnorePointCount

    ''' <summary>
    ''' Gets the number of points that define this <see cref="Curve"/>. Returns the internal point
    ''' count cache if it is non-negative.  Otherwise the number of points in the <see cref="X"/> and
    ''' <see cref="Y"/> data arrays is returned.
    ''' </summary>
    ''' <value> The number of points. </value>
    Public ReadOnly Property PointCount() As Integer
        Get
            Return If(CurveType.StripChart = Me.CurveType,
                Convert.ToInt32(Math.Min(Me._XAxis.ScreenScaleRange.Span, Me._TimeSeriesCount)),
                If(Me._PointCount <> _IgnorePointCount,
                    Me._PointCount,
                    If(Me._X Is Nothing OrElse Me._Y Is Nothing, 0, Convert.ToInt32(Math.Min(Me._X.Length, Me._Y.Length)))))
        End Get
    End Property

    ''' <summary>Gets or sets reference to the drawing <see cref="isr.Visuals.Pane">Pane</see></summary>
    Private ReadOnly _Pane As Pane

    ''' <summary> The data range. </summary>
    Private _DataRange As PlanarRangeR = PlanarRangeR.Empty

    ''' <summary>
    ''' Returns the data range calculated when updating strip chart data or with
    ''' <see cref="M:getRange"/>.  With time series only the amplitude range is valid.
    ''' </summary>
    ''' <value> A <see cref="isr.Visuals.PlanarRangeR">range</see> value. </value>
    Public ReadOnly Property DataRange() As PlanarRangeR
        Get
            Return Me._DataRange
        End Get
    End Property

    ''' <summary> Gets the one-based serial order of the curve in the group. </summary>
    ''' <value> An <see cref="T:System.Integer">integer</see> value. </value>
    Public Property SerialNumber() As Integer

    ''' <summary> Gets the strip chart <see cref="isr.Visuals.Stylus">Stylus</see> </summary>
    ''' <value> A reference to the strip chart <see cref="isr.Visuals.Stylus">Stylus</see> </value>
    Public ReadOnly Property Stylus() As isr.Visuals.Stylus

    ''' <summary> Gets the status message. </summary>
    ''' <value> A System.String value. </value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

    ''' <summary>
    ''' gets a reference to the <see cref="isr.Visuals.Symbol"/> class defined for this
    ''' <see cref="Curve"/>.
    ''' </summary>
    ''' <value> The symbol. </value>
    Public ReadOnly Property Symbol() As Symbol

    ''' <summary> The time series. </summary>
    Private _TimeSeries() As TimeSeriesPointR

    ''' <summary>
    ''' Gets the current book mark to use for drawing if
    ''' <see cref="P:UseBookmark"/> is set.
    ''' </summary>
    ''' <value> A <see cref="TimeSeriesBookmark">time series book mark</see> value. </value>
    Public Property TimeSeriesBookmark() As TimeSeriesBookmark

    ''' <summary> The time series bookmarks. </summary>
    Private _TimeSeriesBookmarks As TimeSeriesBookmarkCollection

    ''' <summary> Gets the book marks collection for a time series curve. </summary>
    ''' <value> The time series bookmarks. </value>
    Public ReadOnly Property TimeSeriesBookmarks() As TimeSeriesBookmarkCollection
        Get
            Return Me._TimeSeriesBookmarks
        End Get
    End Property

    ''' <summary>Gets or sets the number of points collected since the last reset</summary>
    Private _TimeSeriesCount As Integer

    ''' <summary> Length of the time series. </summary>
    Private _TimeSeriesLength As Integer

    ''' <summary>
    ''' Gets or sets the length of the time series.  This value is set automatically to at least the
    ''' width of the active screen area.
    ''' </summary>
    ''' <value> The length of the time series. </value>
    Public Property TimeSeriesLength() As Integer
        Get
            Return Me._TimeSeriesLength
        End Get
        Set(ByVal value As Integer)
            Me._TimeSeriesLength = Math.Max(Screen.PrimaryScreen.WorkingArea.Width, value)
        End Set
    End Property

    ''' <summary>Gets or sets the current time timer series pointer during pan and
    '''   zoom operations.  This value is refreshed only when resetting or adding 
    '''   time series points thus keeping the time-series point
    '''   irrespective of changes to the operational time series points.</summary>
    Private _TimeSeriesPointerKeeper As Integer

    ''' <summary>Gets or sets the pointer to the first time series data point.</summary>
    Private _TimeSeriesPointer As Integer

    ''' <summary> The time series point. </summary>
    Private _TimeSeriesPoint As TimeSeriesPointR

    ''' <summary> Gets the last data point added to the time series. </summary>
    ''' <value> A <see cref="isr.Visuals.TimeSeriesPointR">TimeSeriesPointF</see> value. </value>
    Public ReadOnly Property TimeSeriesPoint() As TimeSeriesPointR
        Get
            Return Me._TimeSeriesPoint
        End Get
    End Property

    ''' <summary> Determines how this curve is drawn. </summary>
    ''' <remarks> David, 10/15/07, 1.0.2844. Rename to CurveType. </remarks>
    ''' <value> A <see cref="Visuals.CurveType"/> value. </value>
    Public Property CurveType() As Visuals.CurveType

    ''' <summary> The x coordinate. </summary>
    Private _X() As Double = Array.Empty(Of Double)()

    ''' <summary>
    ''' Returns the array of independent (X Axis) values that define this
    ''' <see cref="Curve"/>. The size of this array determines the number of points
    ''' that are plotted.  <see cref="System.Double.maxValue"/> values are considered "missing"
    ''' values, and are not plotted.  The curve will have a break at these points to indicate values
    ''' are missing.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A Double() </returns>
    Public Function X() As Double()
        Return Me._X
    End Function

    ''' <summary> The y coordinate. </summary>
    Private _Y() As Double = Array.Empty(Of Double)()

    ''' <summary>
    ''' Returns the array of dependent (Y Axis) values that define this
    ''' <see cref="Curve"/>. The size of this array determines the number of points
    ''' that are plotted.  Note that values defined as System.Double.maxValue are considered
    ''' "missing" values, and are not plotted.  The curve will have a break at these points to
    ''' indicate values are missing.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A Double() </returns>
    Public Function Y() As Double()
        Return Me._Y
    End Function

    ''' <summary>
    ''' Gets or sets a reference to the horizontal <see cref="Axis"/> associated with this curve.
    ''' </summary>
    ''' <value> An <see cref="Axis"/> reference. </value>
    Public Property XAxis() As Axis

    ''' <summary>
    ''' Gets or sets a reference to the vertical <see cref="Axis"/> associated with this curve.
    ''' </summary>
    ''' <value> An <see cref="Axis"/> reference. </value>
    Public Property YAxis() As Axis

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="Curve"/> class that defines the default property values
''' for the <see cref="Curve"/> class.
''' </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public NotInheritable Class CurveDefaults

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs this class. This constructor is private to ensure only a single instance of this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub New()
        MyBase.New()
        Me._CurveType = isr.Visuals.CurveType.XY
        Me._IgnoreMissing = True
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _Instance As CurveDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As CurveDefaults
        If CurveDefaults._Instance Is Nothing Then
            SyncLock CurveDefaults.SyncLocker
                CurveDefaults._Instance = New CurveDefaults()
            End SyncLock
        End If
        Return CurveDefaults._Instance
    End Function

#End Region

    ''' <summary> Default value for the curve type property (<see cref="CurveType"/>). </summary>
    ''' <value> A <see cref="CurveType"/> value. </value>
    Public Property CurveType() As CurveType

    ''' <summary> Default value for the way the curve handles missing values. </summary>
    ''' <value> A <see cref="Curve.IgnoreMissing"/> property. </value>
    Public Property IgnoreMissing() As Boolean

End Class

#End Region



