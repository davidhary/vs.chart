Imports isr.Core.Constructs

''' <summary> Defines a <see cref="System.Single">Single</see> time series point. </summary>
''' <remarks>
''' David, 05/12/04, 1.0.1593.x. Created <para>
''' David, 05/28/04, 1.0.1609.x. Add time series point tag </para><para>
''' David, 06/03/04, 1.0.1615.x. Add zero and partial copy </para><para>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Structure TimeSeriesPointF

#Region " TYPES "

    ''' <summary> Returns the time from seconds since 1-1-0001. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="seconds"> The time in seconds from 1-1-0001. </param>
    ''' <returns> A <see cref="System.DateTime">Time</see> value. </returns>
    Public Shared Function GetDateTime(ByVal seconds As Double) As DateTime
        Return New DateTime(Convert.ToInt64(seconds) * TimeSpan.TicksPerSecond)
    End Function

    ''' <summary> gets the rate of change between two time series points. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="first">  The first. </param>
    ''' <param name="second"> The second. </param>
    ''' <returns> The slope of the line between to two time series points. </returns>
    Public Shared Function GetRate(ByVal first As TimeSeriesPointF, ByVal second As TimeSeriesPointF) As Double

        Return If(first._T.Equals(second._T), 0.0F, (second._Y - first._Y) / (second.Seconds - first.Seconds))

    End Function

    ''' <summary> Gets the minimum time series point value. </summary>
    ''' <value> A <see cref="TimeSeriesPointF"/> value. </value>
    Public Shared ReadOnly Property MinValue() As TimeSeriesPointF
        Get
            Return New TimeSeriesPointF(DateTime.MinValue, Single.MinValue)
        End Get
    End Property

    ''' <summary> Gets the zero time series point value. </summary>
    ''' <value> A <see cref="TimeSeriesPointF"/> value. </value>
    Public Shared ReadOnly Property Zero() As TimeSeriesPointF
        Get
            Return New TimeSeriesPointF(DateTime.MinValue, 0)
        End Get
    End Property

#End Region

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a <see cref="TimeSeriesPointF">time series point</see> instance by its t, y, and
    ''' tag values.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="time">  A <see cref="System.DateTime">Time</see> value. </param>
    ''' <param name="value"> A <see cref="System.Single">Single</see> value. </param>
    ''' <param name="tag">   A <see cref="isr.Visuals.TimeSeriesPointTags">time series point tag</see>
    '''                      value. </param>
    Public Sub New(ByVal time As DateTime, ByVal value As Single, ByVal tag As TimeSeriesPointTags)
        Me._T = time
        Me._Y = value
        Me._Tag = tag
    End Sub

    ''' <summary>
    ''' Constructs a <see cref="TimeSeriesPointF">time series point</see> instance by its t and y
    ''' values.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="time">  A <see cref="System.DateTime">DateTime</see> value. </param>
    ''' <param name="value"> A <see cref="System.Single">Single</see> value. </param>
    Public Sub New(ByVal time As DateTime, ByVal value As Single)
        Me._T = time
        Me._Y = value
        Me._Tag = TimeSeriesPointTags.None
    End Sub

    ''' <summary>
    ''' Constructs a <see cref="TimeSeriesPointF">time series point</see> instance by its y value for
    ''' the current time.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="y"> A <see cref="System.Single">Single</see> value. </param>
    Public Sub New(ByVal y As Single)
        Me._T = DateTime.UtcNow
        Me._Y = y
        Me._Tag = TimeSeriesPointTags.None
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="model"> The <see cref="TimeSeriesPointF">time series point</see> from which to
    '''                      copy. </param>
    Public Sub New(ByVal model As TimeSeriesPointF)

        Me._T = model._T
        Me._Y = model._Y
        Me._Tag = model._Tag
        Me._Index = model._Index

    End Sub

    ''' <summary> The partial copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="model"> The <see cref="TimeSeriesPointF">time series point</see> from which to
    '''                      copy. </param>
    ''' <param name="y">     The new amplitude value. </param>
    Public Sub New(ByVal model As TimeSeriesPointF, ByVal y As Single)

        Me._T = model._T
        Me._Y = y
        Me._Tag = model._Tag
        Me._Index = model._Index

    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Time series point f to be compared. </param>
    ''' <param name="right"> Time series point f to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As TimeSeriesPointF, ByVal right As TimeSeriesPointF) As Boolean
        Return TimeSeriesPointF.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Time series point f to be compared. </param>
    ''' <param name="right"> Time series point f to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As TimeSeriesPointF, ByVal right As TimeSeriesPointF) As Boolean
        Return Not TimeSeriesPointF.Equals(left, right)
    End Operator

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Time series point f to be compared. </param>
    ''' <param name="right"> Time series point f to be compared. </param>
    ''' <returns>
    ''' <c>True</c> if arguments are the same type and represent the same value; otherwise,
    ''' <c>False</c>.
    ''' </returns>
    Public Overloads Shared Function Equals(ByVal left As TimeSeriesPointF, ByVal right As TimeSeriesPointF) As Boolean
        Return left.Equals(right)
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="obj"> Another object to compare to. </param>
    ''' <returns>
    ''' <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso Me.GetType() Is obj.GetType() AndAlso Me.Equals(CType(obj, TimeSeriesPointF))
    End Function

    ''' <summary>
    ''' Returns True if the value of the <paramref name="other"/> equals to the instance value.
    ''' </summary>
    ''' <remarks>
    ''' The two PlanarRanges are the same if the have the same
    ''' <see cref="T"/> and <see cref="Y"/> ranges.
    ''' </remarks>
    ''' <param name="other"> The <see cref="TimeSeriesPointF">TimeSeriesPointF</see> to compare for
    '''                      equality with this instance. </param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    Public Overloads Function Equals(ByVal other As TimeSeriesPointF) As Boolean
        Return Me._T.Equals(other._T) AndAlso Me._Y.Equals(other._Y)
    End Function

#End Region

#Region " METHODS "

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me._T.GetHashCode Xor Me._Y.GetHashCode
    End Function

    ''' <summary> gets the rate of change between this and another time series point. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="referenceTime"> The first time series point. </param>
    ''' <returns> The slope of the line between this and the given time series point. </returns>
    Public Function Rate(ByVal referenceTime As TimeSeriesPointF) As Double

        Return If(Me._T.Equals(referenceTime._T), 0.0R, (Me._Y - referenceTime._Y) / (Me.Seconds - referenceTime.Seconds))

    End Function

    ''' <summary> Returns the default string representation of the time series point. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The fully qualified type name. </returns>
    Public Overrides Function ToString() As String
        Return $"[{Me._T},{Me._Y}]"
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets the index of the time series point in a time series array. </summary>
    ''' <value> The index. </value>
    Public Property Index() As Integer

    ''' <summary> Returns true if the point is a Range point. </summary>
    ''' <value> The is range point. </value>
    Public ReadOnly Property IsRangePoint() As Boolean
        Get
            Return (Me._Tag And TimeSeriesPointTags.RangePoint) = TimeSeriesPointTags.RangePoint
        End Get
    End Property

    ''' <summary> gets the time value in seconds since 1/1/0001. </summary>
    ''' <value> Time value in <see cref="System.Double">double precision</see> seconds. </value>
    Public ReadOnly Property Seconds() As Double
        Get
            Return Me._T.Ticks * TimeSeriesPointR.SecondsPerTick
        End Get
    End Property

    ''' <summary> Gets the time series <see cref="TimeSeriesPointTags">tag</see> </summary>
    ''' <value> A <see cref="TimeSeriesPointTags">tag</see> </value>
    Public Property Tag() As TimeSeriesPointTags

    ''' <summary> Gets the <see cref="TimeSeriesPointF">time series point</see> time value. </summary>
    ''' <value> A <see cref="System.DateTime">Time</see> value. </value>
    Public Property T() As DateTime

    ''' <summary>
    ''' Gets the vertical <see cref="TimeSeriesPointF">time series point</see> value.
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> value. </value>
    Public Property Y() As Single

#End Region

End Structure

''' <summary> Defines a <see cref="System.Double">Double</see> time series point. </summary>
''' <remarks>
''' David, 06/03/04, 1.0.1615. Add zero and partial. <para>
''' David, 05/28/04, 1.0.1609. Add time series point tag. </para><para>
''' David, 05/12/04, 1.0.1593. Copy </para><para>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Structure TimeSeriesPointR

#Region " TYPES "

    ''' <summary> Returns the time from seconds since 1-1-0001. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="seconds"> The time in seconds from 1-1-0001. </param>
    ''' <returns> A <see cref="System.DateTime">Time</see> value. </returns>
    Public Shared Function GetDateTime(ByVal seconds As Double) As DateTime

        Return New DateTime(Convert.ToInt64(seconds) * TimeSpan.TicksPerSecond)

    End Function

    ''' <summary>
    ''' gets the amplitude range for the <see cref="isr.Visuals.TimeSeriesPointR">Time Series</see>
    ''' data array.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="timeSeries"> <see cref="isr.Visuals.TimeSeriesPointR">Time Series</see>
    '''                             data array. </param>
    ''' <returns> The calculated amplitude range. </returns>
    Public Shared Function GetAmplitudeRange(ByVal timeSeries() As TimeSeriesPointR) As RangeR

        ' return the unit range if no data
        If timeSeries Is Nothing Then
            Return RangeR.Unity
        End If

        Dim numPoints As Integer = timeSeries.Length

        ' initialize the values to the empty range
        Dim yTemp As Double
        yTemp = timeSeries(0).Y
        Dim yMin As Double = yTemp
        Dim yMax As Double = yTemp

        ' Loop over each point in the arrays
        For i As Integer = 0 To numPoints - 1

            yTemp = timeSeries(i).Y

            If yTemp < yMin Then
                yMin = yTemp
            ElseIf yTemp > yMax Then
                yMax = yTemp
            End If

        Next i

        Return New RangeR(yMin, yMax)

    End Function

    ''' <summary> gets the rate of change between two time series points. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="first">  The first time series point. </param>
    ''' <param name="second"> The second time series point. </param>
    ''' <returns> The slope of the line between to two time series points. </returns>
    Public Shared Function GetRate(ByVal first As TimeSeriesPointR, ByVal second As TimeSeriesPointR) As Double

        Return If(first._T.Equals(second._T), 0.0R, (second._Y - first._Y) / (second.Seconds - first.Seconds))

    End Function

    ''' <summary> Gets the minimum time series point value. </summary>
    ''' <value> A <see cref="TimeSeriesPointF"/> value. </value>
    Public Shared ReadOnly Property MinValue() As TimeSeriesPointR
        Get
            Return New TimeSeriesPointR(DateTime.MinValue, Double.MinValue)
        End Get
    End Property

    ''' <summary> The seconds per tick. </summary>
    Private Const _SecondsPerTick As Double = 1.0R / TimeSpan.TicksPerSecond

    ''' <summary> Gets the conversion factor from ticks to seconds. </summary>
    ''' <value> A <see cref="System.Double">double precision</see> value. </value>
    Public Shared ReadOnly Property SecondsPerTick() As Double
        Get
            Return TimeSeriesPointR._SecondsPerTick
        End Get
    End Property

    ''' <summary> Gets the zero time series point value. </summary>
    ''' <value> A <see cref="TimeSeriesPointR"/> value. </value>
    Public Shared ReadOnly Property Zero() As TimeSeriesPointR
        Get
            Return New TimeSeriesPointR(DateTime.MinValue, 0)
        End Get
    End Property

#End Region

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a <see cref="TimeSeriesPointF">time series point</see> instance by its t, y, and
    ''' tag values.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="time">  A <see cref="System.DateTime">DateTime</see> value. </param>
    ''' <param name="value"> A <see cref="System.Double">Double</see> value. </param>
    ''' <param name="tag">   A <see cref="isr.Visuals.TimeSeriesPointTags">time series point tag</see>
    '''                      value. </param>
    Public Sub New(ByVal time As DateTime, ByVal value As Double, ByVal tag As TimeSeriesPointTags)

        Me._T = time
        Me._Y = value
        Me._Tag = tag

    End Sub

    ''' <summary>
    ''' Constructs a <see cref="TimeSeriesPointR">time series point</see> instance by its t and y
    ''' values.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="time">  A <see cref="System.DateTime">DateTime</see> value. </param>
    ''' <param name="value"> A <see cref="System.Double">Double</see> value. </param>
    Public Sub New(ByVal time As DateTime, ByVal value As Double)

        Me._T = time
        Me._Y = value
        Me._Tag = TimeSeriesPointTags.None

    End Sub

    ''' <summary>
    ''' Constructs a <see cref="TimeSeriesPointR">time series point</see> instance by its y value for
    ''' the current time.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="y"> A <see cref="System.Double">Double</see> value. </param>
    Public Sub New(ByVal y As Double)

        Me._T = DateTime.UtcNow
        Me._Y = y
        Me._Tag = TimeSeriesPointTags.None

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="model"> The <see cref="TimeSeriesPointR">time series point</see> from which to
    '''                      copy. </param>
    Public Sub New(ByVal model As TimeSeriesPointR)

        Me._T = model._T
        Me._Y = model._Y
        Me._Tag = model._Tag
        Me._Index = model._Index

    End Sub

    ''' <summary> The partial copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="model"> The <see cref="TimeSeriesPointR">time series point</see> from which to
    '''                      copy. </param>
    ''' <param name="y">     The new amplitude value. </param>
    Public Sub New(ByVal model As TimeSeriesPointR, ByVal y As Double)

        Me._T = model._T
        Me._Y = y
        Me._Tag = model._Tag
        Me._Index = model._Index

    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Time series point r to be compared. </param>
    ''' <param name="right"> Time series point r to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As TimeSeriesPointR, ByVal right As TimeSeriesPointR) As Boolean
        Return TimeSeriesPointR.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Time series point r to be compared. </param>
    ''' <param name="right"> Time series point r to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As TimeSeriesPointR, ByVal right As TimeSeriesPointR) As Boolean
        Return Not TimeSeriesPointR.Equals(left, right)
    End Operator

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Time series point r to be compared. </param>
    ''' <param name="right"> Time series point r to be compared. </param>
    ''' <returns>
    ''' <c>True</c> if values are the same type and represent the same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Shared Function Equals(ByVal left As TimeSeriesPointR, ByVal right As TimeSeriesPointR) As Boolean
        Return left.Equals(right)
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="obj"> Another object to compare to. </param>
    ''' <returns>
    ''' <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso Me.GetType() Is obj.GetType() AndAlso Me.Equals(CType(obj, TimeSeriesPointR))
    End Function

    ''' <summary>
    ''' Returns True if the value of the <paramref name="other"/> equals to the instance value.
    ''' </summary>
    ''' <remarks>
    ''' The two PlanarRanges are the same if the have the same
    ''' <see cref="T"/> and <see cref="Y"/> values.
    ''' </remarks>
    ''' <param name="other"> The <see cref="TimeSeriesPointR">TimeSeriesPointR</see> to compare for
    '''                      equality with this instance. </param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    Public Overloads Function Equals(ByVal other As TimeSeriesPointR) As Boolean
        Return Me._T.Equals(other._T) AndAlso Me._Y.Equals(other._Y)
    End Function

#End Region

#Region " METHODS "

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me._T.GetHashCode Xor Me._Y.GetHashCode
    End Function

    ''' <summary> gets the rate of change between this and another time series point. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="referenceTime"> The first time series point. </param>
    ''' <returns> The slope of the line between this and the given time series point. </returns>
    Public Function Rate(ByVal referenceTime As TimeSeriesPointR) As Double

        Return If(Me._T.Equals(referenceTime._T), 0.0R, (Me._Y - referenceTime._Y) / (Me.Seconds - referenceTime.Seconds))

    End Function

    ''' <summary> Returns the default string representation of the time series point. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The fully qualified type name. </returns>
    Public Overrides Function ToString() As String
        Return $"[{Me._T},{Me._Y}]"
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets the index of the time series point in a time series array. </summary>
    ''' <value> The index. </value>
    Public Property Index() As Integer

    ''' <summary> Returns true if the point is a Range point. </summary>
    ''' <value> The is range point. </value>
    Public ReadOnly Property IsRangePoint() As Boolean
        Get
            Return (Me._Tag And TimeSeriesPointTags.RangePoint) = TimeSeriesPointTags.RangePoint
        End Get
    End Property

    ''' <summary> gets the time value in seconds since 1/1/0001. </summary>
    ''' <value> Time value in <see cref="System.Double">double precision</see> seconds. </value>
    Public ReadOnly Property Seconds() As Double
        Get
            Return Me._T.Ticks * TimeSeriesPointR.SecondsPerTick
        End Get
    End Property

    ''' <summary> Gets or sets the time series <see cref="TimeSeriesPointTags">tag</see> </summary>
    ''' <value> A <see cref="TimeSeriesPointTags">tag</see> </value>
    Public Property Tag() As TimeSeriesPointTags

    ''' <summary>
    ''' Gets or sets the <see cref="TimeSeriesPointF">time series point</see> time value.
    ''' </summary>
    ''' <value> A <see cref="System.DateTime">Time</see> value. </value>
    Public Property T() As DateTime

    ''' <summary>
    ''' Gets or sets the vertical <see cref="TimeSeriesPointR">time series point</see> value.
    ''' </summary>
    ''' <value> A <see cref="System.Double">Double</see> value. </value>
    Public Property Y() As Double

#End Region

End Structure

#Region " TYPES "

''' <summary> Enumerates the available time series tags. </summary>
''' <remarks> David, 2020-10-26. </remarks>
''' <seealso cref="isr.Visuals.Axis.AxisType"/>
<System.Flags()>
Public Enum TimeSeriesPointTags

    ''' <summary>No tag</summary>
    None = 0

    ''' <summary>Tags a time series point as a range point.  Only points tagged as
    '''   range points as selected when setting the range for auto scale.</summary>
    RangePoint = 1

End Enum

#End Region

''' <summary>
''' Defines a <see cref="System.Double">double precision</see> time series book mark.
''' </summary>
''' <remarks>
''' David, 05/12/04, 1.0.1593. Created <para>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Structure TimeSeriesBookmark

#Region " Constructors "

    ''' <summary>
    ''' Constructs a <see cref="TimeSeriesBookmark"/> instance by its
    ''' <see cref="TimeSeriesPointR">time series point</see>, index,
    ''' and caption.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="mainPoint"> The main book mark point <see cref="TimeSeriesPointR">time series
    '''                          point</see> </param>
    ''' <param name="fromPoint"> The starting <see cref="TimeSeriesPointR">time series point</see> </param>
    ''' <param name="toPoint">   The ending <see cref="TimeSeriesPointR">time series point</see> </param>
    ''' <param name="caption">   The book mark caption. </param>
    ''' <param name="tag">       The book mark tag. </param>
    Public Sub New(ByVal mainPoint As TimeSeriesPointR, ByVal fromPoint As TimeSeriesPointR, ByVal toPoint As TimeSeriesPointR,
                   ByVal caption As String, ByVal tag As String)

        If String.IsNullOrWhiteSpace(caption) Then
            Throw New ArgumentNullException(NameOf(caption))
        End If
        If String.IsNullOrWhiteSpace(tag) Then
            Throw New ArgumentNullException(NameOf(tag))
        End If

        Me._MainPoint = mainPoint
        Me._FromPoint = fromPoint
        Me._ToPoint = toPoint
        Me._Tag = tag
        Me._Caption = caption

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="model"> The <see cref="TimeSeriesBookmark">time series book mark</see> from which
    '''                      to copy. </param>
    Public Sub New(ByVal model As TimeSeriesBookmark)

        Me._MainPoint = model._MainPoint
        Me._FromPoint = model._FromPoint
        Me._ToPoint = model._ToPoint
        Me._Caption = model._Caption
        Me._Tag = model._Tag
        Me._SerialNumber = model._SerialNumber

    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Time series bookmark to be compared. </param>
    ''' <param name="right"> Time series bookmark to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As TimeSeriesBookmark, ByVal right As TimeSeriesBookmark) As Boolean
        Return TimeSeriesBookmark.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Time series bookmark to be compared. </param>
    ''' <param name="right"> Time series bookmark to be compared. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As TimeSeriesBookmark, ByVal right As TimeSeriesBookmark) As Boolean
        Return Not TimeSeriesBookmark.Equals(left, right)
    End Operator

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="left">  Time series bookmark to be compared. </param>
    ''' <param name="right"> Time series bookmark to be compared. </param>
    ''' <returns>
    ''' <c>True</c> if values are the same type and represent the same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Shared Function Equals(ByVal left As TimeSeriesBookmark, ByVal right As TimeSeriesBookmark) As Boolean
        Return left.Equals(right)
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="obj"> Another object to compare to. </param>
    ''' <returns>
    ''' <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso Me.GetType() Is obj.GetType() AndAlso Me.Equals(CType(obj, TimeSeriesBookmark))
    End Function

    ''' <summary>
    ''' Returns True if the value of the <paramref name="other"/> equals to the instance value.
    ''' </summary>
    ''' <remarks>
    ''' Time Series Marks are the same if the have the same
    ''' <see cref="FromPoint"/> and <see cref="MainPoint"/> and <see cref="ToPoint"/>
    ''' and <see cref="Caption"/> and <see cref="SerialNumber"/> and <see cref="Tag"/> values.
    ''' </remarks>
    ''' <param name="other"> The <see cref="TimeSeriesBookmark">TimeSeriesBookmark</see> to compare
    '''                      for equality with this instance. </param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    Public Overloads Function Equals(ByVal other As TimeSeriesBookmark) As Boolean
        Return Me._FromPoint.Equals(other._FromPoint) AndAlso
               Me._MainPoint.Equals(other._MainPoint) AndAlso
               Me._ToPoint.Equals(other._ToPoint) AndAlso
               Me._Caption.Equals(other._Caption) AndAlso
               Me._SerialNumber.Equals(other._SerialNumber) AndAlso
               Me._Tag.Equals(other._Tag)
    End Function

#End Region

#Region " METHODS "

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me._MainPoint.GetHashCode Xor Me._FromPoint.GetHashCode Xor Me._ToPoint.GetHashCode Xor
               Me._Caption.GetHashCode Xor Me._SerialNumber.GetHashCode Xor Me._Tag.GetHashCode
    End Function

    ''' <summary> Returns the time series index of the bookmark. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="timeSeries"> The <see cref="TimeSeriesBookmark">time series point</see> array. </param>
    ''' <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
    Public Function FindIndex(ByVal timeSeries() As TimeSeriesPointR) As Integer

        Return Array.IndexOf(timeSeries, Me._FromPoint)

    End Function

    ''' <summary> Returns the default string representation of the time series book mark. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> The fully qualified type name. </returns>
    Public Overloads Overrides Function ToString() As String
        Return If(Me._Caption.Length > 0, $"{Me._Caption}: {Me._FromPoint.T}", Me._FromPoint.T.ToString(Application.CurrentCulture))
    End Function

    ''' <summary> Returns the default string representation of the time series book mark. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="dateFormat"> The date format. </param>
    ''' <returns> A String that represents this object. </returns>
    Public Overloads Function ToString(ByVal dateFormat As String) As String
        If String.IsNullOrWhiteSpace(dateFormat) Then Throw New ArgumentNullException(NameOf(dateFormat))
        Return If(String.IsNullOrWhiteSpace(Me._Caption),
                    Me._FromPoint.T.ToString(Application.CurrentCulture),
                    $"{Me._Caption}: {Me._FromPoint.T.ToString(dateFormat, Application.CurrentCulture)}")
    End Function

#End Region

#Region " Properties"

    ''' <summary> Gets or sets the Bookmark caption. </summary>
    ''' <value> A <see cref="String"/> value. </value>
    Public Property Caption() As String

    ''' <summary>
    ''' Gets or sets the start <see cref="TimeSeriesPointR">time series point</see> of this Bookmark.
    ''' </summary>
    ''' <value> A <see cref="TimeSeriesPointR">time series point</see> value. </value>
    Public Property FromPoint() As TimeSeriesPointR

    ''' <summary>
    ''' Gets or sets the main <see cref="TimeSeriesPointR">time series point</see> of this Bookmark.
    ''' </summary>
    ''' <value> A <see cref="TimeSeriesPointR">time series point</see> value. </value>
    Public Property MainPoint() As TimeSeriesPointR

    ''' <summary> Gets or sets the book mark serial number in the book mark collection. </summary>
    ''' <value> The serial number. </value>
    Public Property SerialNumber() As Integer

    ''' <summary> Gets or sets the Bookmark tag. </summary>
    ''' <value> A <see cref="String"/> value. </value>
    Public Property Tag() As String

    ''' <summary>
    ''' Gets or sets the end <see cref="TimeSeriesPointR">time series point</see> of this Bookmark.
    ''' </summary>
    ''' <value> A <see cref="TimeSeriesPointR">time series point</see> value. </value>
    Public Property ToPoint() As TimeSeriesPointR

#End Region

End Structure
