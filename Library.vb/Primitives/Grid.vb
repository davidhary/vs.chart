''' <summary> Provides grid properties. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/04, 1.0.1581. Created </para>
''' </remarks>
Public Class Grid

    Implements ICloneable, IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs an instance of this class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()

        ' instantiate the base class
        MyBase.New()

        Dim gridDefaults As GridDefaults = GridDefaults.Get()
        Me.DashOff = gridDefaults.DashOff
        Me.DashOn = gridDefaults.DashOn
        Me.Visible = gridDefaults.Visible
        Me.LineColor = gridDefaults.LineColor
        Me.LineWidth = gridDefaults.LineWidth

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="model"> The grid object from which to copy. </param>
    Public Sub New(ByVal model As Grid)

        Me.New()
        If model Is Nothing Then
            Throw New ArgumentNullException(NameOf(model))
        End If
        Me.DashOff = model._DashOff
        Me.DashOn = model._DashOn
        Me.Visible = model._Visible
        Me.LineColor = model._LineColor
        Me.LineWidth = model._LineWidth

    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> True if disposed. </summary>
    Private _Disposed As Boolean

    ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._Disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._Disposed = value
        End Set
    End Property

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If


        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of grid. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the grid. </returns>
    Public Function Copy() As Grid
        Return New Grid(Me)
    End Function

    ''' <summary> Draw the grid. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    ''' <param name="tick">           Reference to the axis <see cref="isr.Visuals.Tick">tick
    '''                               specification</see>. </param>
    ''' <param name="topPix">         The pixel location of the far side of the Axis Area from this
    '''                               axis. This value is the axisArea.Height for the XAxis, or the
    '''                               axisArea.Width for the YAxis and Y2Axis. </param>
    Public Sub Draw(ByVal graphicsDevice As Graphics, ByVal tick As Tick, ByVal topPix As Single)

        If graphicsDevice Is Nothing Then Throw New ArgumentNullException(NameOf(graphicsDevice))
        If graphicsDevice Is Nothing Then Throw New ArgumentNullException(NameOf(graphicsDevice))
        If tick Is Nothing Then Throw New ArgumentNullException(NameOf(tick))

        Using gridPen As New Pen(Me._LineColor, Me._LineWidth)
            gridPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Custom
            Dim pattern(1) As Single
            pattern(0) = Me._DashOn
            pattern(1) = Me._DashOff
            gridPen.DashPattern = pattern

            ' loop for each major tick
            Dim x As Single
            For i As Integer = 1 To tick.ValidTickCount - 1
                ' draw the grid
                If Me._Visible Then
                    x = tick.GetLocation(i)
                    graphicsDevice.DrawLine(gridPen, x, 0.0F, x, topPix)
                End If
            Next
        End Using

    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' The "Dash Off" mode for drawing the grid.  This is the distance, in pixels, of the spaces
    ''' between the dash segments that make up the dashed grid lines.
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    ''' <seealso cref="DashOn"/>
    ''' <seealso cref="Visible"/>
    Public Property DashOff() As Single

    ''' <summary>
    ''' The "Dash On" mode for drawing the grid.  This is the distance, in pixels, of the dash
    ''' segments that make up the dashed grid lines.
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    ''' <seealso cref="DashOff"/>
    ''' <seealso cref="Visible"/>
    Public Property DashOn() As Single

    ''' <summary>
    ''' Determines if the major <see cref="Axis"/> grid lines (at each labeled value) will be shown.
    ''' </summary>
    ''' <value> True to show the grid lines, false otherwise. </value>
    Public Property Visible() As Boolean

    ''' <summary>
    ''' The color to use for drawing this <see cref="Axis"/> grid.  This affects only the grid lines,
    ''' since the <see cref="Title.Appearance"/> and
    ''' <see cref="Appearance"/> both have their own color specification.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property LineColor() As Color

    ''' <summary> Gets or sets the default pen width used for drawing the grid lines. </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    ''' <seealso cref="Visible"/>
    Public Property LineWidth() As Single

    ''' <summary> Gets or sets the status message. </summary>
    ''' <value> A System.String value. </value>
    <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StatusMessage() As String

#End Region

End Class

#Region " DEFAULTS "

''' <summary>
''' A simple subclass of the <see cref="grid"/> class that defines the default property values
''' for the <see cref="grid"/> class.
''' </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public NotInheritable Class GridDefaults

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs this class. This constructor is private to ensure only a single instance of this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub New()
        MyBase.New()
        Me._DashOn = 1.0F
        Me._DashOff = 5.0F
        Me._LineWidth = 1.0F
        Me._LineColor = Color.Black
        Me._Visible = True
    End Sub

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _Instance As GridDefaults

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As GridDefaults
        If GridDefaults._Instance Is Nothing Then
            SyncLock GridDefaults.SyncLocker
                GridDefaults._Instance = New GridDefaults()
            End SyncLock
        End If
        Return GridDefaults._Instance
    End Function

#End Region

    ''' <summary>
    ''' Gets or sets the default "dash on" size for drawing the <see cref="Axis"/> grid
    ''' (<see cref="Grid.DashOn"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property DashOn() As Single

    ''' <summary>
    ''' Gets or sets the default "dash off" size for drawing the <see cref="Axis"/> grid
    ''' (<see cref="Grid.DashOff"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property DashOff() As Single

    ''' <summary>
    ''' Gets or sets the default pen width for drawing the <see cref="Axis"/> grid
    ''' (<see cref="Grid.LineWidth"/> property).
    ''' </summary>
    ''' <value> A <see cref="System.Single">Single</see> in pixels. </value>
    Public Property LineWidth() As Single

    ''' <summary>
    ''' Gets or sets the default color for the <see cref="Axis"/> grid lines
    ''' (<see cref="Axis.LineColor"/> property).  This color only affects the grid lines.
    ''' </summary>
    ''' <value> A <see cref="System.Drawing.Color">Color</see> </value>
    Public Property LineColor() As Color

    ''' <summary>
    ''' Gets or sets the default display mode for the <see cref="Axis"/> grid lines
    ''' (<see cref="grid.Visible"/> property). True to show the grid lines, false to hide them.
    ''' </summary>
    ''' <value> The visible. </value>
    Public Property Visible() As Boolean

End Class

#End Region

