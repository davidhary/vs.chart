''' <summary> Removes flickering from painting controls with gdi+. </summary>
''' <remarks>
''' The class removes flickering by double buffering the drawing. It draws
'''   on a separate bitmap canvas file. Once all drawing is complete, you can then push the
'''   drawing to the main drawing canvas all at once. This will kill most if not all flickering
'''   the drawing produces. The double buffer slows down the line recorder significantly without
'''   removing much of the flickering. <para>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para><para>  
''' David, 07/27/05, 1.0.2034. Created </para>
''' </remarks>
Public Class GraphicsDoubleBuffer

    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="canvasWidth">  The canvas width. </param>
    ''' <param name="canvasHeight"> The canvas height. </param>
    Public Sub New(ByVal canvasWidth As Integer, ByVal canvasHeight As Integer)
        Me._Canvas = New Bitmap(canvasWidth, canvasHeight)
        Me._Graphics = Graphics.FromImage(Me._Canvas)
        Me._Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> True if disposed. </summary>
    Private _Disposed As Boolean

    ''' <summary> Gets or sets (private) the dispose status sentinel. </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._Disposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._Disposed = value
        End Set
    End Property

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Me._Canvas IsNot Nothing Then
                        Me._Canvas.Dispose()
                        Me._Canvas = Nothing
                    End If

                    If Me._Graphics IsNot Nothing Then
                        Me._Graphics.Dispose()
                        Me._Graphics = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#Region " METHODS  and  PROPERTIES "

    ''' <summary>The bitmap where drawing is down before it is pushed to the
    '''   object.</summary>
    Public Property [Canvas]() As Bitmap

    ''' <summary> The graphics. </summary>
    Private _Graphics As Graphics

    ''' <summary> Reference to graphics context for the buffered canvas. </summary>
    ''' <value> The graphics device. </value>
    Public ReadOnly Property GraphicsDevice() As Graphics
        Get
            Return Me._Graphics
        End Get
    End Property

    ''' <summary> Renders the double buffer to the screen. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="graphicsDevice"> Window forms graphics Object. </param>
    Public Sub Render(ByVal graphicsDevice As Graphics)
        If graphicsDevice IsNot Nothing AndAlso Me._Canvas IsNot Nothing Then
            graphicsDevice.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None
            graphicsDevice.DrawImage(Me._Canvas, 0, 0)
        End If

    End Sub

    ''' <summary> Returns true if double buffering can be achieved. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> True if we can double buffer, false if not. </returns>
    Public Function CanDoubleBuffer() As Boolean
        Return Graphics.FromImage(Me._Canvas) IsNot Nothing
    End Function

#End Region

End Class

