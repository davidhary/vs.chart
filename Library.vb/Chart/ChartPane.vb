''' <summary>
''' Draws a chart.  It is preferable to use the chart control because it has embedded double
''' buffering whereas with this double buffering must be set.
''' </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources<para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class ChartPane
    Inherits isr.Visuals.Pane
    Implements ICloneable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a <see cref="ChartPane"/> with default values, unit drawing area, and empty labels.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()

        MyBase.New()

    End Sub

    ''' <summary>
    ''' Constructs a <see cref="ChartPane"/> object with the specified drawing area and axis and
    ''' chart labels.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="chartArea"> A rectangular screen area where the chart is to be displayed. This
    '''                          area can be any size, and can be resize at any time using the
    '''                          <see cref="PaneArea"/> property. </param>
    Public Sub New(ByVal chartArea As RectangleF)

        MyBase.New(chartArea)

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="model"> The ChartPane object from which to copy. </param>
    Public Sub New(ByVal model As ChartPane)

        MyBase.New(model)

    End Sub

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' Invoke the base class dispose method

            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the ChartPane. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the ChartPane. </returns>
    Public Function Copy() As ChartPane
        Return New ChartPane(Me)
    End Function

    ''' <summary>
    ''' Creates a sample demo chart with three curves, titles, text boxes, arrows and legend.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="clientRectangle"> The client area to use for displaying the chart. </param>
    Public Sub CreateSampleOne(ByVal clientRectangle As Rectangle)

        ' set chart area and titles.
        MyBase.PaneArea = New RectangleF(10, 10, 10, 10)

        MyBase.Title.Caption = $"Wacky Widget Company {Environment.NewLine} Production Report"

        Dim xAxis As Axis = MyBase.AddAxis($"Time, Days {Environment.NewLine} (Since Plant Construction Startup)", AxisType.X)
        xAxis.Grid.Visible = True
        xAxis.TickLabels.Appearance.Angle = 60.0F

        Dim yAxis As Axis = MyBase.AddAxis($"Widget Production {Environment.NewLine} (units/hour)", AxisType.Y)
        yAxis.Grid.Visible = True

        MyBase.AxisFrame.FillColor = Color.LightGoldenrodYellow

        MyBase.SetSize(clientRectangle)

        Dim curve As Curve

        Dim x As Double() = {72, 200, 300, 400, 500, 600, 700, 800, 900, 1000}
        Dim y As Double() = {20, 10, 50, 40, 35, 60, 90, 25, 48, 75}
        curve = MyBase.AddCurve("Larry", x, y, Color.Red, ShapeType.Circle, xAxis, yAxis)
        curve.Symbol.Size = 14
        curve.Cord.LineWidth = 2.0F

        Dim x2 As Double() = {300, 400, 500, 600, 700, 800, 900}
        Dim y2 As Double() = {75, 43, 27, 62, 89, 73, 12}
        curve = MyBase.AddCurve("Moe", x2, y2, Color.Blue, ShapeType.Diamond, xAxis, yAxis)
        curve.Cord.Visible = False
        curve.Symbol.Filled = True
        curve.Symbol.Size = 14

        Dim x3 As Double() = {150, 250, 400, 520, 780, 940}
        Dim y3 As Double() = {5.2, 49.0, 33.8, 88.57, 99.9, 36.8}
        curve = MyBase.AddCurve("Curly", x3, y3, Color.Green, ShapeType.Triangle, xAxis, yAxis)
        curve.Symbol.Size = 14
        curve.Cord.LineWidth = 2.0F
        curve.Symbol.Filled = True

        Dim textBox As isr.Visuals.TextBox = MyBase.AddTextBox($"First Prod {Environment.NewLine} 21-Oct-99", 100.0F, 50.0F)
        textBox.Appearance.Italic = True
        textBox.Alignment = New Alignment(HorizontalAlignment.Center, VerticalAlignment.Bottom)
        textBox.Appearance.Frame.FillColor = Color.LightBlue

        Dim arrow As Arrow = MyBase.AddArrow(Color.Black, 12.0F, 100.0F, 47.0F, 72.0F, 25.0F)
        arrow.CoordinateFrame = CoordinateFrameType.AxisXYScale

        textBox = MyBase.AddTextBox("Upgrade", 700.0F, 50.0F)
        textBox.CoordinateFrame = CoordinateFrameType.AxisXYScale
        textBox.Appearance.Angle = 90
        textBox.Appearance.FontColor = Color.Black
        textBox.Appearance.Frame.Filled = True
        textBox.Appearance.Frame.FillColor = Color.LightGoldenrodYellow
        textBox.Appearance.Frame.Visible = False
        textBox.Alignment = New Alignment(HorizontalAlignment.Right, VerticalAlignment.Center)

        arrow = MyBase.AddArrow(Color.Black, 15, 700, 53, 700, 80)
        arrow.CoordinateFrame = CoordinateFrameType.AxisXYScale
        arrow.LineWidth = 2.0F

        textBox = MyBase.AddTextBox("Confidential", 0.8F, -0.03F)
        textBox.CoordinateFrame = CoordinateFrameType.AxisFraction
        textBox.Appearance.Angle = 15.0F
        textBox.Appearance.FontColor = Color.Red
        textBox.Appearance.Bold = True
        textBox.Appearance.FontSize = 16
        textBox.Appearance.Frame.Visible = True
        textBox.Appearance.Frame.LineColor = Color.Red
        textBox.Alignment = New Alignment(HorizontalAlignment.Left, VerticalAlignment.Bottom)

        MyBase.Rescale()

    End Sub

    ''' <summary>
    ''' Draw all elements in the <see cref="ChartPane"/> to the specified graphics device.  This
    ''' routine should be part of the Paint() update process.  Calling This method will redraw all
    ''' features of the graph.  No preparation is required other than an instantiated
    ''' <see cref="ChartPane"/> object.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    Public Overrides Sub Draw(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then Throw New ArgumentNullException(NameOf(graphicsDevice))

        ' clear and exit if not visible.
        If Not Me.Visible Then
            graphicsDevice.Clear(Me.PaneFrame.FillColor)
            Return
        End If

        ' set the scale factor relative to the base dimension)
        MyBase.ScaleFactor = MyBase.GetScaleFactor(graphicsDevice)

        ' allocate the drawing area
        MyBase.SetDrawArea()

        MyBase.SetAxisArea(graphicsDevice)

        ' Frame the pane and axis
        MyBase.DrawFrames(graphicsDevice)

        ' Draw the Pane Title
        MyBase.Title.Draw(graphicsDevice)

        ' Draw the Axes
        MyBase.Axes.Draw(graphicsDevice)

        ' Draw the curves
        MyBase.Curves.Draw(graphicsDevice)

        ' Draw the Legend
        MyBase.Legend.Draw(graphicsDevice)

        ' draw each text box at the screen pixel location
        MyBase.TextBoxes.Draw(graphicsDevice)

        ' draw each arrow at its screen coordinates
        MyBase.Arrows.Draw(graphicsDevice)

        ' Reset the clipping
        graphicsDevice.ResetClip()

    End Sub

    ''' <summary>
    ''' Prints all elements in the <see cref="Pane"/> to the specified graphics device.  This routine
    ''' should be overridden by the inheriting classes.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to print into.  This is normally
    '''                               <see cref="System.Drawing.Printing.PrintPageEventArgs.graphics">graphics</see>
    '''                               of the <see cref="M:PrintPage"/> delegate. </param>
    ''' <param name="printArea">      The <see cref="System.Drawing.RectangleF">area</see> on the print
    '''                               document allotted for the chart. </param>
    Public Overrides Sub Print(ByVal graphicsDevice As Graphics, ByVal printArea As RectangleF)

        ' exit if not visible.
        If Not Me.Visible Then
            Return
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then Throw New ArgumentNullException(NameOf(graphicsDevice))

        ' save the pane area
        Dim tempPaneArea As RectangleF = MyBase.PaneArea

        ' set the pane area to the print window
        MyBase.PaneArea = New RectangleF(printArea.Location, printArea.Size)
        Me.Draw(graphicsDevice)

        ' restore the pane area
        MyBase.PaneArea = tempPaneArea

    End Sub

#End Region

End Class

