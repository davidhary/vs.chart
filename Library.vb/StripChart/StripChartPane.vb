''' <summary>
''' Draws a strip chart.  It is preferable to use the strip chart control because it has embedded
''' double buffering whereas with this double buffering must be set.
''' </summary>
''' <remarks>
''' For optimal graphics, leverage the power of the .Net Framework. Some
'''   controls, such as the PictureBox are double buffered automatically. Also, pay attention to
'''   how the ResizeRedraw, Opaque, DoubleBuffer, and other control styles can benefit you.
'''   Consolidate painting code. The basic logic used to render your Windows Forms application
'''   should exist in the OnPaint and OnPaintBackground methods. Avoid creating drawing code in
'''   methods responding to Resize, Load, or Show events. Think before you paint. Obviously, draw
'''   as little as possible for the best performance. Make use of Regions and clipping
'''   rectangles.  Employ a double buffering technique if feasible. Know your trade offs. For
'''   example: keeping global Brush objects may benefit the speed of your application, however
'''   the memory footprint will be larger. Also, even though the floating point data type is
'''   larger than an integer, the use of floats will provide more accurate scaling. <para>
''' David, 05/12/04, 1.0.1593.  Created </para><para>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class StripChartPane

    Inherits isr.Visuals.Pane
    Implements ICloneable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a <see cref="StripChartPane"/> with default values, unit drawing area, and empty
    ''' labels.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()

        MyBase.New()

    End Sub

    ''' <summary>
    ''' Constructs a <see cref="StripChartPane"/> object with the specified drawing area and axis and
    ''' chart labels.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="chartArea"> A rectangular screen area where the chart is to be displayed. This
    '''                          area can be any size, and can be resize at any time using the
    '''                          <see cref="PaneArea"/> property. </param>
    Public Sub New(ByVal chartArea As RectangleF)

        MyBase.New(chartArea)

    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="model"> The StripChartPane object from which to copy. </param>
    Public Sub New(ByVal model As StripChartPane)

        MyBase.New(model)

    End Sub

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          <c>False</c> if this method releases only unmanaged resources. 
    ''' </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources
            End If

        Finally

            ' Invoke the base class dispose method

            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Updates the time series curve data. </summary>
    ''' <remarks>
    ''' Use this method to add a new value to the strip chart.  Shifts the buffer one notch every
    ''' time a new data points comes in thus 'scrolling' the data along.
    ''' </remarks>
    ''' <param name="time">      The time (X or horizontal axis) value. </param>
    ''' <param name="amplitude"> The amplitude (Y, or vertical axis) value. </param>
    Public Sub AddDataPoint(ByVal time As DateTime, ByVal amplitude As Double)
        MyBase.Curves(0).AddDataPoint(time, amplitude)
    End Sub

    ''' <summary> Updates the time series curve data. </summary>
    ''' <remarks>
    ''' Use this method to add a new value to the strip chart.  Shifts the buffer one notch every
    ''' time a new data points comes in thus 'scrolling' the data along.
    ''' </remarks>
    ''' <param name="time">      The time (X or horizontal axis) value. </param>
    ''' <param name="amplitude"> The amplitude (Y, or vertical axis) value. </param>
    Public Sub AddDataPoint(ByVal time As DateTimeOffset, ByVal amplitude As Double)
        MyBase.Curves(0).AddDataPoint(time.DateTime, amplitude)
    End Sub

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the StripChartPane. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <returns> A new, independent copy of the StripChartPane. </returns>
    Public Function Copy() As StripChartPane
        Return New StripChartPane(Me)
    End Function

    ''' <summary>
    ''' Creates a sample demo chart with three curves, titles, text boxes, arrows and legend.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="clientRectangle"> The client area to use for displaying the chart. </param>
    Public Sub CreateSampleOne(ByVal clientRectangle As Rectangle)

        ' set chart area and titles.
        MyBase.PaneArea = New RectangleF(10, 10, 10, 10)
        MyBase.Title.Visible = False
        MyBase.Legend.Visible = False
        MyBase.AxisFrame.FillColor = Color.WhiteSmoke

        ' amplitude Y-axis is first so that the vertical grid labels (x grid) are
        ' drawn over the Y axis labels
        Dim yAxis As Axis = MyBase.AddAxis("Amplitude", AxisType.Y)
        yAxis.Grid.Visible = True
        yAxis.Grid.LineColor = System.Drawing.Color.DarkGray
        yAxis.TickLabels.Visible = True
        yAxis.TickLabels.DecimalPlaces = New AutoValue(0, True)
        yAxis.Title.Visible = False
        yAxis.Max = New AutoValueR(1, False)
        yAxis.Min = New AutoValueR(-1, False)

        ' time series X axis
        Dim xAxis As Axis = MyBase.AddAxis("Time", AxisType.X)
        xAxis.Visible = False
        xAxis.CoordinateScale = New CoordinateScale(CoordinateScaleType.StripChart)
        xAxis.Max = New AutoValueR(1, False)
        xAxis.Min = New AutoValueR(0, False)

        xAxis.Grid.Visible = True
        xAxis.Grid.LineColor = System.Drawing.Color.DarkGray

        xAxis.MajorTick.Visible = True
        xAxis.MajorTick.ScaleFormat = "T"

        xAxis.MinorTick.Visible = False

        xAxis.TickLabels.Visible = True
        xAxis.TickLabels.Appearance.Angle = -90.0F
        xAxis.TickLabels.Appearance.FontColor = System.Drawing.Color.DarkGray

        xAxis.TickLabels.Appearance.Frame.Visible = True
        xAxis.TickLabels.Appearance.Frame.IsOutline = False
        xAxis.TickLabels.Appearance.Frame.Filled = True
        xAxis.TickLabels.Appearance.Frame.FillColor = MyBase.AxisFrame.FillColor
        xAxis.Title.Visible = False

        MyBase.SetSize(clientRectangle)

        Dim curve As Curve
        curve = MyBase.AddCurve(CurveType.StripChart, "time series", xAxis, yAxis)
        curve.Cord.LineColor = Color.Red
        curve.Symbol.Shape = ShapeType.Star
        curve.Symbol.Size = 1
        curve.Symbol.Visible = False

        MyBase.Rescale()

    End Sub

    ''' <summary>
    ''' Draw all elements in the <see cref="StripChartPane"/> to the specified graphics device.  This
    ''' routine should be part of the Paint() update process.  Calling This method will redraw all
    ''' features of the graph.  No preparation is required other than an instantiated
    ''' <see cref="StripChartPane"/> object.
    ''' </summary>
    ''' <remarks>
    ''' In a strip chart it is presumed that, at each chart speed, pixel spacing correspond to a fix
    ''' time scale, e.graphicsDevice., 1 per 100 ms or 1 per 20 ms. Therefore, each new data point
    ''' corresponds to a screen point.  What is different as the speed changes are the time marks on
    ''' the chart.  Thus, to draw the curve time wise requires to translate the curve index back from
    ''' the current point to the left side of the chart.  To draw the grid requires to locate the
    ''' index which most closely corresponds to the current chart spacing. For drawing the grid, we
    ''' assume a fixed grid count of 10 major divisions per window and 5 minor divisions per major
    ''' division.  This corresponds to the standard charting when changing speed does not affect the
    ''' divisions of the paper but rather how the paper is labeled.  At higher speed, each division
    ''' corresponds to a Int16er time span.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to be drawn into.  This is normally
    '''                               <see cref="PaintEventArgs.graphics"/> of the
    '''                               <see cref="M:Paint"/> method. </param>
    Public Overrides Sub Draw(ByVal graphicsDevice As Graphics)

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        ' clear and exit if not visible.
        If Not Me.Visible Then
            graphicsDevice.Clear(Me.PaneFrame.FillColor)
            Return
        End If

        ' set the scale factor relative to the base dimension)
        MyBase.ScaleFactor = MyBase.GetScaleFactor(graphicsDevice)

        ' allocate the drawing area
        MyBase.SetDrawArea()

        ' Calculate the axis area, deducting the area for the scales, titles, legend, etc.
        MyBase.SetAxisArea(graphicsDevice)

        ' Frame the pane and axis
        MyBase.DrawFrames(graphicsDevice)

        ' Draw the Pane Title
        MyBase.Title.Draw(graphicsDevice)

        ' set the time series scale
        MyBase.Curves.ScaleTimeSeriesAxis()

        ' Draw the Axes
        MyBase.Axes.Draw(graphicsDevice)

        ' draw the curve on top of the label and grids.
        MyBase.Curves.Draw(graphicsDevice)

        ' Reset the clipping
        graphicsDevice.ResetClip()

    End Sub

    ''' <summary>
    ''' Prints all elements in the <see cref="Pane"/> to the specified graphics device.  This routine
    ''' should be overridden by the inheriting classes.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="graphicsDevice"> Reference to a graphic device to print into.  This is normally
    '''                               <see cref="System.Drawing.Printing.PrintPageEventArgs.graphics">graphics</see>
    '''                               of the <see cref="M:PrintPage"/> delegate. </param>
    ''' <param name="printArea">      The <see cref="System.Drawing.RectangleF">area</see> on the print
    '''                               document allotted for the chart. </param>
    Public Overrides Sub Print(ByVal graphicsDevice As Graphics, ByVal printArea As RectangleF)

        ' exit if not visible.
        If Not Me.Visible Then
            Return
        End If

        ' validate argument.
        If graphicsDevice Is Nothing Then
            Throw New ArgumentNullException(NameOf(graphicsDevice))
        End If

        ' save the pane area
        Dim tempPaneArea As RectangleF = MyBase.PaneArea

        ' set the pane area to the print window
        MyBase.PaneArea = New RectangleF(printArea.Location, printArea.Size)
        Me.Draw(graphicsDevice)

        ' restore the pane area
        MyBase.PaneArea = tempPaneArea

    End Sub

#End Region

End Class

