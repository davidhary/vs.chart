<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class StripChartControl
    Inherits System.Windows.Forms.UserControl

        <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If disposing Then

                ' Free managed resources when explicitly called
                If Me._stripChartPane IsNot Nothing Then
                    Me._stripChartPane.Dispose()
                    Me._stripChartPane = Nothing
                End If


                If components IsNot Nothing Then
                    components.Dispose()
                End If

            End If
            ' Free shared unmanaged resources

        Finally

            ' Invoke the base class dispose method  
            MyBase.Dispose(disposing)

        End Try

    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    End Sub

End Class
