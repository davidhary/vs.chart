''' <summary>Draws a simple line strip chart.</summary>
''' <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 07/27/05, 1.0.2034.x">
''' Created
''' </para></remarks>
<Description("Line Recorder - Windows Forms Custom Control"),
 System.Drawing.ToolboxBitmap(GetType(isr.Visuals.LineRecorder))>
Public Class LineRecorder
    Inherits System.Windows.Forms.UserControl

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs the class initializing components. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()

        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        ' onInitialize()

        ' This method is required by the Windows Form Designer.
        Me.InitializeComponent()

        Me._GridSize = 12
        Me._Amplitude = 50
        Me._LowerLimit = 25
        Me._Maximum = 100
        Me._RefreshInterval = 1000
        Me._UpperLimit = 75

        ' Add any initialization after the InitializeComponent() call
        Me.SetStyle(System.Windows.Forms.ControlStyles.UserPaint Or
                 System.Windows.Forms.ControlStyles.AllPaintingInWmPaint Or
                 System.Windows.Forms.ControlStyles.DoubleBuffer Or
                 System.Windows.Forms.ControlStyles.CacheText Or
                 System.Windows.Forms.ControlStyles.Opaque Or
                 System.Windows.Forms.ControlStyles.ResizeRedraw, True)
        Me.UpdateStyles()

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                                                   resources; <see langword="false" /> to
    '''                                                   release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If Me._Thread IsNot Nothing Then Me._Thread.Abort()
                If Me.components IsNot Nothing Then Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


#End Region

#Region " METHODS  and  PROPERTIES "

    ''' <summary> The amplitude. </summary>
    Private _Amplitude As Integer

    ''' <summary> The current amplitude to plot. </summary>
    ''' <value> The amplitude. </value>
    <Category("Behavior"), DefaultValue(50), Description("The current amplitude to plot.")>
    Public Property Amplitude() As Integer
        Get
            Return Me._Amplitude
        End Get
        Set(ByVal value As Integer)
            If value > Me.Maximum Then
                Me._Amplitude = Me._Maximum
            ElseIf value < Me._Minimum Then
                Me._Amplitude = Me._Minimum
            Else
                Me._Amplitude = value
            End If
        End Set
    End Property

    ''' <summary>Gets or sets the amplitudes stored.</summary>
    Private ReadOnly _Amplitudes As New ArrayList

    ''' <summary> Returns the default size for this control. </summary>
    ''' <value> The default <see cref="T:System.Drawing.Size" /> of the control. </value>
    Protected Overrides ReadOnly Property DefaultSize() As System.Drawing.Size
        Get
            Return New System.Drawing.Size(200, 100)
        End Get
    End Property

    ''' <summary>Gets or sets the vertical and horizontal step size in pixels.</summary>
    Private Const _DivisionSize As Integer = 2

    ''' <summary> Size of the grid. </summary>
    Private _GridSize As Integer

    ''' <summary> The grid size in pixels. </summary>
    ''' <value> The size of the grid. </value>
    <Category("Behavior"), DefaultValue(12), Description("The grid size in pixels.")>
    Public Property GridSize() As Integer
        Get
            Return Me._GridSize
        End Get
        Set(ByVal value As Integer)
            If value < 2 Then
                value = LineRecorder._DivisionSize
            End If
            Me._GridSize = value
        End Set
    End Property

    ''' <summary> Name of the instance. </summary>
    Private ReadOnly _InstanceName As String = "LineRecorder"

    ''' <summary> Gets the instance name of the instance of this class. </summary>
    ''' <remarks>
    ''' Use this property to identify the instance of the class.  If instance name is not set,
    ''' returns .ToString.
    ''' </remarks>
    ''' <value> <c>InstanceName</c> is a String property that can be read from (read only). </value>
    Public ReadOnly Property InstanceName() As String
        Get
            Return If(String.IsNullOrWhiteSpace(Me._InstanceName), Me._InstanceName, MyBase.ToString)
        End Get
    End Property

    ''' <summary> The lower limit. </summary>
    Private _LowerLimit As Integer

    ''' <summary> The lower value of the normal range. </summary>
    ''' <value> The lower limit. </value>
    <Category("Behavior"), DefaultValue(25), Description("The lower value of the normal range.")>
    Public Property LowerLimit() As Integer
        Get
            Return Me._LowerLimit
        End Get
        Set(ByVal value As Integer)
            If value > Me.UpperLimit Then
                Me._LowerLimit = Me._UpperLimit
            ElseIf value < Me._Minimum Then
                Me._LowerLimit = Me._Minimum
            Else
                Me._LowerLimit = value
            End If
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The maximum. </summary>
    Private _Maximum As Integer

    ''' <summary> The upper bound of the amplitude range. </summary>
    ''' <value> The maximum value. </value>
    <Category("Behavior"), DefaultValue(100), Description("The upper bound of the amplitude range.")>
    Public Property Maximum() As Integer
        Get
            Return Me._Maximum
        End Get
        Set(ByVal value As Integer)
            Me._Maximum = If(value < Me._UpperLimit, Me._UpperLimit, value)
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> The minimum. </summary>
    Private _Minimum As Integer

    ''' <summary> The lower bound of the amplitude range. </summary>
    ''' <value> The minimum value. </value>
    <Category("Behavior"), DefaultValue(0), Description("The lower bound of the amplitude range.")>
    Public Property Minimum() As Integer
        Get
            Return Me._Minimum
        End Get
        Set(ByVal value As Integer)
            Me._Minimum = If(value > Me.LowerLimit, Me._LowerLimit, value)
            Me.Invalidate()
        End Set
    End Property

    ''' <summary>Gets or sets the strip chart position within the grid box.</summary>
    Private _Mover As Integer

    ''' <summary> Starts or resumes recording. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub Play()

        If Me._Thread Is Nothing OrElse (Me._Thread.ThreadState = Threading.ThreadState.Aborted) Then
            Me._Thread = New Thread(New ThreadStart(AddressOf Me.ProcessThread))
            Me._Thread.Start()
        ElseIf (Me._Thread.ThreadState = Threading.ThreadState.Suspended) Then
            Me._Thread.Start()
        ElseIf (Me._Thread.ThreadState = Threading.ThreadState.Stopped) Then
            Me._Thread.Start()
        End If

    End Sub

    ''' <summary> Pauses the recorder. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub Pause()

        If Me._Thread IsNot Nothing AndAlso (Me._Thread.ThreadState <> Threading.ThreadState.Suspended) Then
            Me._Thread.Start()
        End If

    End Sub

    ''' <summary>Stops the recorder.</summary>
    Public Sub [Stop]()

        If Me._Thread IsNot Nothing Then
            Me._Thread.Abort()
        End If

    End Sub

    ''' <summary> Processes the data. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Private Sub ProcessThread()

        Do While (Me._Thread.ThreadState = Threading.ThreadState.Running)

            If Me._Mover >= Me._GridSize - LineRecorder._DivisionSize Then
                Me._Mover = 0
            Else
                Me._Mover += LineRecorder._DivisionSize
            End If

            If Me._Amplitudes.Count < (Me.Width / LineRecorder._DivisionSize) Then

                ' add a new amplitude
                Me._Amplitudes.Add(Me._Amplitude)

            Else

                ' shift the amplitudes
                For i As Integer = 0 To Me._Amplitudes.Count - 2
                    Me._Amplitudes(i) = Me._Amplitudes(i + 1)
                Next
                Me._Amplitudes(Me._Amplitudes.Count - 1) = Me._Amplitude

            End If

            ' redraw
            Me.Invalidate()

            ' wait before the next redraw.
            Thread.Sleep(Me._RefreshInterval)

        Loop

    End Sub

    ''' <summary> The refresh interval. </summary>
    Private _RefreshInterval As Integer

    ''' <summary> The refreshing interval of the recorder between 1 and 1000 milliseconds. </summary>
    ''' <value> The refresh interval. </value>
    <Category("Behavior"), DefaultValue(1000),
    Description("The refreshing interval of the recorder between 1 and 1000 milliseconds.")>
    Public Property RefreshInterval() As Integer
        Get
            Return Me._RefreshInterval
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then
                value = 1
            ElseIf value > 1000 Then
                value = 1000
            End If
            Me._RefreshInterval = value
        End Set
    End Property

    ''' <summary>Reference to the thread the runs the strip chart.</summary>
    Private _Thread As Thread

    ''' <summary> The upper limit. </summary>
    Private _UpperLimit As Integer

    ''' <summary> The upper value of the normal range. </summary>
    ''' <value> The upper limit. </value>
    <Category("Behavior"), DefaultValue(75), Description("The upper value of the normal range.")>
    Public Property UpperLimit() As Integer
        Get
            Return Me._UpperLimit
        End Get
        Set(ByVal value As Integer)
            If value > Me.Maximum Then
                Me._UpperLimit = Me._Maximum
            ElseIf value < Me._LowerLimit Then
                Me._UpperLimit = Me._LowerLimit
            Else
                Me._UpperLimit = value
            End If
            Me.Invalidate()
        End Set
    End Property

    ''' <summary> Hold True to highlight out of bounds values. </summary>
    ''' <value> The use limits. </value>
    <Category("Behavior"), DefaultValue(False),
      Description("Hold True to highlight out of bounds values.")>
    Public Property UseLimits() As Boolean

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Draw the chart and the grid. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)

        If e Is Nothing Then Return

        MyBase.OnPaint(e)

        Dim graphicsDevice As Graphics = e.Graphics

        ' draw the horizontal grid.
        Dim position As Integer = 0
        For i As Integer = 1 To Convert.ToInt32(Me.Height / Me._GridSize)
            position += Me._GridSize
            graphicsDevice.DrawLine(Pens.DarkGreen, 0, position, Me.Width, position)
        Next i

        ' draw the vertical grid.
        position = -Me._Mover
        For i As Integer = 1 To Convert.ToInt32(Me.Width / Me._GridSize)
            position += Me._GridSize
            graphicsDevice.DrawLine(Pens.DarkGreen, position, 0, position, Me.Height)
        Next i

        Dim scale As Double = Me.Height / (Me._Maximum - Me._Minimum)
        Dim drawingPen As System.Drawing.Pen = Pens.Yellow
        Dim currentX As Integer = Me.Width - LineRecorder._DivisionSize * Me._Amplitudes.Count
        Dim previousX As Integer
        Dim currentY As Integer
        Dim previousY As Integer
        Dim currentAmplitude As Integer
        Dim previousAmplitude As Integer
        If Me._UseLimits AndAlso (Me._Maximum > Me.UpperLimit) AndAlso (Me._Minimum < Me._LowerLimit) Then
            For i As Integer = 1 To Me._Amplitudes.Count - 1
                previousX = currentX
                currentX += LineRecorder._DivisionSize
                currentAmplitude = Convert.ToInt32(Me._Amplitudes(i), Globalization.CultureInfo.CurrentCulture)
                previousAmplitude = Convert.ToInt32(Me._Amplitudes(i - 1), Globalization.CultureInfo.CurrentCulture)
                currentY = Convert.ToInt32(scale * (Me._Maximum - Convert.ToInt32(Me._Amplitudes(i), Globalization.CultureInfo.CurrentCulture)))
                previousY = Convert.ToInt32(scale * (Me._Maximum - previousAmplitude), Globalization.CultureInfo.CurrentCulture)
                drawingPen = If(currentAmplitude > Me.UpperLimit Or currentAmplitude < Me._LowerLimit, Pens.Red, Pens.Yellow)
                graphicsDevice.DrawLine(drawingPen, currentX, currentY, previousX, previousY)
            Next i
        Else
            For i As Integer = 1 To Me._Amplitudes.Count - 1
                currentAmplitude = Convert.ToInt32(Me._Amplitudes(i), Globalization.CultureInfo.CurrentCulture)
                previousAmplitude = Convert.ToInt32(Me._Amplitudes(i - 1), Globalization.CultureInfo.CurrentCulture)
                previousX = currentX
                currentX += LineRecorder._DivisionSize
                currentY = Convert.ToInt32(scale * (Me._Maximum - currentAmplitude), Globalization.CultureInfo.CurrentCulture)
                previousY = Convert.ToInt32(scale * (Me._Maximum - previousAmplitude), Globalization.CultureInfo.CurrentCulture)
                graphicsDevice.DrawLine(drawingPen, currentX, currentY, previousX, previousY)
            Next i
        End If

        ControlPaint.DrawBorder3D(graphicsDevice, 0, 0, Me.Width, Me.Height, Border3DStyle.Sunken)

    End Sub

    ''' <summary> Paints the background of the control. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.PaintEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnPaintBackground(ByVal e As System.Windows.Forms.PaintEventArgs)
        If e IsNot Nothing Then
            MyBase.OnPaintBackground(e)

            If Me._UseLimits AndAlso (Me._Maximum > Me.UpperLimit) AndAlso (Me._Minimum < Me._LowerLimit) Then
                Dim myColor As Color = Color.FromArgb(64, Color.White)
                Using myBrush As New SolidBrush(myColor)
                    Dim scale As Double = Me.Height / (Me.Maximum - Me.Minimum)
                    Dim range As New Rectangle(0, Convert.ToInt32(scale * (Me.Maximum - Me.UpperLimit)), Me.Width, Convert.ToInt32(scale * (Me.UpperLimit - Me.LowerLimit)))
                    Dim graphicsDevice As Graphics = e.Graphics
                    graphicsDevice.FillRectangle(myBrush, range)
                End Using
            End If
        End If

    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.Resize" /> event. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnResize(ByVal e As System.EventArgs)
        If e IsNot Nothing Then
            MyBase.OnResize(e)
            If Me._Amplitudes.Count > (Me.Width / LineRecorder._DivisionSize) Then
                Me._Amplitudes.RemoveRange(0, Me._Amplitudes.Count - Convert.ToInt32(Me.Width / LineRecorder._DivisionSize))
            End If
        End If
    End Sub

#End Region

End Class
