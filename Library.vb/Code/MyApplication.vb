Namespace My

    ''' <summary> Handles application level functionality. </summary>
    ''' <remarks>
    ''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para>
    ''' </remarks>
    Partial Friend Class MyApplication

#Region " APPLICATION LOG "

        ''' <summary> Adds a log message and severity to the log. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="severity"> Specifies the message severity. </param>
        ''' <param name="details">  Specifies the message details. </param>
        ''' <returns> Message or empty string. </returns>
        Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal details As String) As String
            If details IsNot Nothing Then
                My.Application.Log.WriteEntry(details, severity)
                Return details
            End If
            Return String.Empty
        End Function

        ''' <summary> Adds a log message and severity to the log. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="severity"> Specifies the message severity. </param>
        ''' <param name="format">   Specifies the message format. </param>
        ''' <param name="args">     Specified the message arguments. </param>
        ''' <returns> Message or empty string. </returns>
        Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String
            If format IsNot Nothing Then
                Return WriteLogEntry(severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            End If
            Return String.Empty
        End Function

        ''' <summary> Adds a log message and severity to the log. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="severity"> Specifies the message severity. </param>
        ''' <param name="messages"> Message information to log. </param>
        ''' <returns> Message or empty string. </returns>
        Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal messages As String()) As String
            If messages IsNot Nothing Then
                Return WriteLogEntry(severity, String.Join(",", messages))
            End If
            Return String.Empty
        End Function

        ''' <summary> Adds exception details to the error log.  Includes stack and data. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="ex">             Specifies the exception. </param>
        ''' <param name="severity">       Specifies the exception severity. </param>
        ''' <param name="additionalInfo"> Specifies additional information. </param>
        Private Shared Sub WriteExceptionDetailsThis(ByVal ex As Exception, ByVal severity As TraceEventType,
                                              ByVal additionalInfo As String)

            My.Application.Log.WriteException(ex, severity, additionalInfo)
            If ex IsNot Nothing AndAlso ex.StackTrace IsNot Nothing Then
                Dim stackTrace As String() = ex.StackTrace.Split(CChar(Environment.NewLine))
                WriteLogEntry(severity, stackTrace)
            End If
            If ex.Data IsNot Nothing AndAlso ex.Data.Count > 0 Then
                For Each keyValuePair As System.Collections.DictionaryEntry In ex.Data
                    My.Application.Log.WriteEntry(keyValuePair.Key.ToString & "=" & keyValuePair.Value.ToString, severity)
                Next
            End If
            If ex.InnerException IsNot Nothing Then
                MyApplication.WriteExceptionDetailsThis(ex.InnerException, severity, "(Inner Exception)")
            End If

        End Sub

        ''' <summary> Adds exception details to the error log. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="ex">             Specifies the exception. </param>
        ''' <param name="severity">       Specifies the exception severity. </param>
        ''' <param name="additionalInfo"> Specifies additional information. </param>
        Public Shared Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType,
                                            ByVal additionalInfo As String)
            ' write exception details.
            MyApplication.WriteExceptionDetailsThis(ex, severity, additionalInfo)

        End Sub

        ''' <summary> Adds exception details to the error log. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="ex"> Specifies the exception. </param>
        Public Shared Sub WriteExceptionDetails(ByVal ex As Exception)
            MyApplication.WriteExceptionDetails(ex, TraceEventType.Error, String.Empty)
        End Sub

#End Region

#Region " APPLICATION METHODS "

        ''' <summary> Returns a string selected value. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="condition"> The condition for selecting the true or false parts. </param>
        ''' <param name="truePart">  The part selected if the condition is True. </param>
        ''' <param name="falsePart"> The part selected if the condition is false. </param>
        ''' <returns>
        ''' The <paramref name="truePart"></paramref> if <paramref name="condition"></paramref>
        ''' is <c>True</c>; otherwise, the <paramref name="falsePart"></paramref>
        ''' </returns>
        Public Shared Function ImmediateIf(ByVal condition As Boolean, ByVal truePart As String, ByVal falsePart As String) As String
            If condition Then
                Return truePart
            Else
                Return falsePart
            End If
        End Function

        ''' <summary> Returns an Integer selected value. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="condition"> The condition for selecting the true or false parts. </param>
        ''' <param name="truePart">  The part selected if the condition is True. </param>
        ''' <param name="falsePart"> The part selected if the condition is false. </param>
        ''' <returns>
        ''' The <paramref name="truePart"></paramref> if <paramref name="condition"></paramref>
        ''' is <c>True</c>; otherwise, the <paramref name="falsePart"></paramref>
        ''' </returns>
        Public Shared Function ImmediateIf(ByVal condition As Boolean, ByVal truePart As Integer, ByVal falsePart As Integer) As Integer
            If condition Then
                Return truePart
            Else
                Return falsePart
            End If
        End Function

        ''' <summary> Gets the na. </summary>
        ''' <value> The na. </value>
        Public Shared ReadOnly Property NA() As String
            Get
                Return "NA"
            End Get
        End Property

        ''' <summary> Returns an array of values from 0 to elementCount - 1. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="elementCount"> Number of ramp elements. </param>
        ''' <returns> an array of values from 0 to elementCount - 1. </returns>
        Public Function Ramp(ByVal elementCount As Integer) As Integer()
            Dim values(elementCount - 1) As Integer
            For i As Integer = 0 To elementCount - 1
                values(i) = i
            Next i
            Return values
        End Function

        ''' <summary> Converts the Double array to a string array. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="values"> The values. </param>
        ''' <returns> the Double array to a string array. </returns>
        Private Shared Function ToStringArray(ByVal values() As Double) As String()
            If values Is Nothing OrElse values.Length = 0 Then
                Dim ranges() As String = {}
                Return ranges
            Else
                Dim ranges(values.Length - 1) As String
                For i As Integer = 0 To values.Length - 1
                    ranges(i) = values(i).ToString(Globalization.CultureInfo.CurrentCulture)
                Next i
                Return ranges
            End If
        End Function

        ''' <summary> Converts the Double array to a string array. </summary>
        ''' <remarks> David, 2020-10-26. </remarks>
        ''' <param name="values"> The values. </param>
        ''' <returns> The converted string array. </returns>
        Private Shared Function ToStringArray(ByVal values() As Integer) As String()
            If values Is Nothing OrElse values.Length = 0 Then
                Dim ranges() As String = {}
                Return ranges
            Else
                Dim ranges(values.Length - 1) As String
                For i As Integer = 0 To values.Length - 1
                    ranges(i) = values(i).ToString(Globalization.CultureInfo.CurrentCulture)
                Next i
                Return ranges
            End If
        End Function

#End Region

    End Class

End Namespace

