Imports System.Runtime.CompilerServices

Friend Module Extensions

    ''' <summary> Returns the number of decimal places for displaying the auto value. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> The number of decimal places for displaying the auto value. </returns>
    <Extension()>
    Public Function DecimalPlaces(ByVal value As Double) As Integer
        Dim candidate As Integer = 0
        value = Math.IEEERemainder(Math.Abs(value), 1)
        Dim minimum As Double = Math.Max(value / 1000.0, 0.0000001)
        Do While value > minimum
            candidate += 1
            value = Math.IEEERemainder(10 * value, 1)
        Loop
        Return candidate
    End Function

    ''' <summary> Returns the number of decimal places for displaying the auto value. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> the number of decimal places for displaying the auto value. </returns>
    <Extension()>
    Public Function DecimalPlaces(ByVal value As Single) As Integer
        Return CType(value, Double).DecimalPlaces
    End Function

    ''' <summary> Calculate a base 10 logarithm in a safe manner to avoid math exceptions. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="value"> The value for which the logarithm is to be calculated. </param>
    ''' <returns>
    ''' The value of the logarithm, or 0 if the <paramref name="value"/>
    ''' argument was negative or zero.
    ''' </returns>
    <Extension()>
    Public Function Log10(ByVal value As Double) As Double
        Return Math.Log10(Math.Max(value, Double.Epsilon))
    End Function

    ''' <summary> Calculate the modulus (remainder) in a safe manner so that divide by zero errors are
    ''' avoided. </summary>
    ''' <param name="value">    The divisor. </param>
    ''' <param name="dividend"> The dividend. </param>
    ''' <returns> the value of the modulus, or zero for the divide-by-zero case. </returns>
    <Extension()>
    Public Function [Mod](ByVal value As Double, ByVal dividend As Double) As Double

        If dividend = 0 Then
            Return 0
        End If
        Dim temp As Double = value / dividend
        Return dividend * (temp - Math.Floor(temp))
    End Function

End Module
